<?php
return [
    'url' => 'https://fcm.googleapis.com/fcm/send',
    'server_key' => env('FCM_SERVER_KEY', null),
];
