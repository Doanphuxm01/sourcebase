<?php
/**
 * User: NghiaTB
 * Date: 7/1/2020
 * Time: 11:23 AM
 */

return [
    'SOCKET_URL' => env('SOCKET_URL', 'https://socket.dones.ai/api/socket'),
    'URL_ASSET' => 'https://api.dones.ai/',
    'URL_IMAGE_LIFE' => env('URL_IMAGE', 'https://api.dones.ai/'),
];
