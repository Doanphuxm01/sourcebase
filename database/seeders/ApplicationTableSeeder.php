<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Elibs\eFunction;
use Illuminate\Support\Facades\DB;
use App\Models\Postgres\Application;
use App\Models\Postgres\Image;
use App\Models\Postgres\ApplicationStore;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApplicationStore::where('is_system', 1)->delete();

        $appDefault = [
            [
                'name' => 'Google Mail',
                'link_url' => 'https://www.google.com/intl/vi/gmail/about/',
            ],
            [
                'name' => 'Skype',
                'link_url' => 'https://www.skype.com/en/features/skype-web/',
            ],
            [
                'name' => 'Telegram',
                'link_url' => 'https://web.telegram.org/z/',
            ],
            [
                'name' => 'Base',
                'link_url' => 'https://account.base.vn/a/login?app=&return=',
            ],
            [
                'name' => 'Zoom',
                'link_url' => 'https://zoom.us/join',
            ],
            [
                'name' => 'Microsoft Teams',
                'link_url' => 'https://www.microsoft.com/vi-vn/microsoft-teams/log-in',
            ],
            [
                'name' => 'Google Calendar',
                'link_url' => 'https://calendar.google.com/calendar/u/0/r',
            ],
            [
                'name' => 'Google Drive',
                'link_url' => 'https://www.google.com.vn/drive/about.html',
            ],
            [
                'name' => 'Whatsapp',
                'link_url' => 'https://web.whatsapp.com/',
            ],
            [
                'name' => 'Slack',
                'link_url' => 'https://slack.com/signin#/signin',
            ],
            [
                'name' => 'Facebook Messenger',
                'link_url' => 'https://www.messenger.com/',
            ],
            [
                'name' => 'Trello',
                'link_url' => 'https://trello.com/login',
            ],
            [
                'name' => 'LinkedIn',
                'link_url' => 'https://www.linkedin.com/feed/',
            ],
            [
                'name' => 'Twitter',
                'link_url' => 'https://twitter.com/?lang=vi',
            ],
            [
                'name' => 'Instagram',
                'link_url' => 'https://www.instagram.com/',
            ],
            [
                'name' => 'Outlook',
                'link_url' => 'https://office.live.com/start/Outlook.aspx?ui=vi-VN&rs=HR',
            ],
            [
                'name' => 'Facebook',
                'link_url' => 'https://www.facebook.com/',
            ],
            [
                'name' => 'Google Keep',
                'link_url' => 'https://keep.google.com/',
            ],
            [
                'name' => 'Discord',
                'link_url' => 'https://discord.com/login',
            ],
            [
                'name' => 'Youtube',
                'link_url' => 'https://www.youtube.com/?hl=vi',
            ],
            [
                'name' => 'Notion',
                'link_url' => 'https://www.notion.so/login',
            ],
            [
                'name' => 'Evernote',
                'link_url' => 'https://www.evernote.com/Login.action',
            ],
            [
                'name' => 'GitHub',
                'link_url' => 'https://github.com/login',
            ],
            [
                'name' => 'Dropbox',
                'link_url' => 'https://www.dropbox.com/login',
            ],
            [
                'name' => 'Asana',
                'link_url' => 'https://app.asana.com/',
            ],
            [
                'name' => 'Google Translate',
                'link_url' => 'https://translate.google.com/?hl=vi',
            ],
            [
                'name' => 'Hangouts',
                'link_url' => 'https://hangouts.google.com/',
            ],
            [
                'name' => 'Office 365',
                'link_url' => 'https://www.office.com/',
            ],
            [
                'name' => 'OneDrive',
                'link_url' => 'https://onedrive.live.com/about/vi-vn/signin',
            ],
            [
                'name' => 'Todoist',
                'link_url' => 'https://todoist.com/',
            ],
        ];

        $arrInsertAppDefault = [];

        foreach ($appDefault as $value) {
            $icon = Image::where('name', 'like', '%' . eFunction::generateSlug($value['name'], '-') . '%')->where('collection_type', 8)->whereNull('deleted_at')->first();
            $url = eFunction::getHost($value['link_url']);
            $arrInsertAppDefault[] = [
                'name' => $value['name'],
                'slug' => eFunction::generateSlug($value['name'], '-'),
                'link_url' => $value['link_url'],
                // 'applications_type_id' => $value['applications_type_id'],
                'type' => 0,
                'is_system' => 1,
                'is_accept' => 1,
                'x_frame' => eFunction::getXFrameOptions($url),
                'application_image_id' => !empty($icon) ? $icon->id : null,
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        ApplicationStore::insert($arrInsertAppDefault);
    }
}
