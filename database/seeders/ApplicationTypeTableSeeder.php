<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Elibs\eFunction;
use Illuminate\Support\Facades\DB;

class ApplicationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $applicationType = [
            'Quản trị',
            'Lưu trữ',
            'Văn phòng ảo',
            'To-do list',
            'Quản lí dự án',
            'Theo dõi mục tiêu',
            'Theo dõi tiến độ',
            'Bảng tính',
            'Email',
            'Chat',
            'Marketing',
            'Salekit',
            'Showroom ảo',
            'Chăm sóc khách hàng',
            'Docs & Wiki',
            'Đào tạo',
            'Cộng đồng',
            'Tin tức',
            'Sự kiện',
            'Họp trực tuyến',
        ];
        $arrInsertAppType = [];
        foreach ($applicationType as $type) {
            $arrInsertAppType[] = [
                'name' => $type,
                'slug' => eFunction::generateSlug($type, '-'),
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        DB::table('applications_type')->insert($arrInsertAppType);
    }
}
