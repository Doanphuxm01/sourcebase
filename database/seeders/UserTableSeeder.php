<?php

namespace Database\Seeders;

use App\Models\Postgres\Account;
use App\Models\Postgres\Organization;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::create([
            'name' => 'Mandu',
            'slug' => Organization::SLUG,
            'is_active' => 1,
            'major_id' => 24,
            'package_id' => 1,
            'scale' => 5,
            'exp_date' => '2099-12-30',
            'due_date' => '2021-12-30',
            'is_trial' => 0,
        ]);

        Account::create([
            'name' => 'Root Mandu',
            'email' => 'admin@gmail.com',
            'username' => 'root-mandu',
            'password' => \Hash::make('Ab@123456'),
            'is_root' => 1,
            'is_active' => 1,
            'organization_id' => 1,
            'is_check_active' => 1
        ]);
    }
}
