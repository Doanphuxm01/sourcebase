<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatedecentralizationPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decentralization_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->smallInteger('create')->default(0);
            $table->smallInteger('read')->default(0);
            $table->smallInteger('update')->default(0);
            $table->smallInteger('delete')->default(0);

            $table->unsignedBigInteger('permission_id')->nullable();
            $table->unsignedBigInteger('decentralization_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();

            $table->timestamps();

            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade');
            $table->foreign('decentralization_id')->references('id')->on('decentralizations')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });

        $this->updateTimestampDefaultValue('decentralization_permissions', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decentralization_permissions');
    }
}
