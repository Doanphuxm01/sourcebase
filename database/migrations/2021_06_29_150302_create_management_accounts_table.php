<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatemanagementAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('management_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('manager_id')->nullable();
            $table->unsignedBigInteger('account_id')->nullable();

            $table->foreign('manager_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('management_accounts', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_accounts');
    }
}
