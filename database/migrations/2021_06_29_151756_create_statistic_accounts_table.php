<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatestatisticAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('account_id')->nullable();
            $table->unsignedBigInteger('application_id')->nullable();
            $table->unsignedBigInteger('statistic_application_id')->nullable();
            $table->unsignedBigInteger('clicks')->default(0);
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
            $table->foreign('statistic_application_id')->references('id')->on('statistic_applications')->onDelete('cascade');
            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('statistic_accounts', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistic_accounts');
    }
}
