<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnApplicationTypeStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_stores', function (Blueprint $table) {
            $table->unsignedBigInteger('application_type_id')->nullable();
            $table->foreign('application_type_id')->references('id')->on('applications_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_stores', function (Blueprint $table) {
            $table->dropForeign(['application_type_id']);
            $table->dropColumn('application_type_id');
        });
    }
}
