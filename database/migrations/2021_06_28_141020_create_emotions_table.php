<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateemotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emotions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('source')->nullable();

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('emotions', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emotions');
    }
}
