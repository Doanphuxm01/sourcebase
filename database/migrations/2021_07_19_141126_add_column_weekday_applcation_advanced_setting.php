<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnWeekdayApplcationAdvancedSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_advanced_settings', function (Blueprint $table) {
            $table->unsignedBigInteger('monday')->default(0);
            $table->unsignedBigInteger('tuesday')->default(0);
            $table->unsignedBigInteger('wednesday')->default(0);
            $table->unsignedBigInteger('thursday')->default(0);
            $table->unsignedBigInteger('friday')->default(0);
            $table->unsignedBigInteger('saturday')->default(0);
            $table->unsignedBigInteger('sunday')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_advanced_settings', function (Blueprint $table) {
            $table->dropColumn('monday');
            $table->dropColumn('tuesday');
            $table->dropColumn('wednesday');
            $table->dropColumn('thursday');
            $table->dropColumn('friday');
            $table->dropColumn('saturday');
            $table->dropColumn('sunday');
        });

    }
}
