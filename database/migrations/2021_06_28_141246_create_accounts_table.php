<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('email');
            $table->string('username');
            $table->string('password', 60);
            $table->string('phone_number')->nullable();
            $table->string('position')->nullable();
            $table->string('language')->default('vi')->comment('vi: Vietnamese, en: English');
            $table->unsignedBigInteger('is_root')->default(0)->comment('0: not root, 1: root');
            $table->date('birthday')->nullable();
            $table->unsignedBigInteger('gender')->default(1)->comment('0: Female, 1: Male');

            $table->smallInteger('is_active')->default(1);

            $table->string('api_web_access_token')->nullable();
            $table->string('api_ios_access_token')->nullable();
            $table->string('api_android_access_token')->nullable();

            $table->unsignedBigInteger('profile_image_id')->nullable();
            $table->unsignedBigInteger('background_image_id')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();

            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('profile_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('background_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');

            $table->index(['id', 'deleted_at']);
        });

        $this->updateTimestampDefaultValue('accounts', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
