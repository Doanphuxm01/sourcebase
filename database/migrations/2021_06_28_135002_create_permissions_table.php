<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatepermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('key')->nullable();
            $table->smallInteger('is_active')->default(1)->comment('0: Disable, 1: Active');

            $table->softDeletes();
            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('permissions', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
