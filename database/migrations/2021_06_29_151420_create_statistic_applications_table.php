<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatestatisticApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistic_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('room_id')->nullable();
            $table->unsignedBigInteger('space_id')->nullable();
            $table->unsignedBigInteger('application_id')->nullable();
            $table->date('date_at')->nullable();
            $table->unsignedBigInteger('clicks')->default(0);

            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade');
            $table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');

            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('statistic_applications', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistic_applications');
    }
}
