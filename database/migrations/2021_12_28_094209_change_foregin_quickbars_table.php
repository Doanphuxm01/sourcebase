<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeForeginQuickbarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quick_bars', function (Blueprint $table) {
            $table->dropForeign('quick_bars_application_id_foreign');
            // $table->foreign('application_id')->references('id')->on('application_stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quick_bars', function (Blueprint $table) {

        });
    }
}
