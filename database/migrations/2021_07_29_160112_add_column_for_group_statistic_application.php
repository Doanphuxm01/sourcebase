<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnForGroupStatisticApplication extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('statistic_applications', function (Blueprint $table) {
            $table->unsignedBigInteger('organization_id')->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');

            $table->unsignedBigInteger('application_advanced_setting_id')->nullable();
            $table->foreign('application_advanced_setting_id')->references('id')->on('application_advanced_settings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('statistic_applications', function (Blueprint $table) {
            $table->dropForeign('statistic_applications_application_advanced_setting_id_foreign');
            $table->dropColumn('application_advanced_setting_id');

            $table->dropForeign('statistic_applications_organization_id_foreign');
            $table->dropColumn('organization_id');
        });

    }
}
