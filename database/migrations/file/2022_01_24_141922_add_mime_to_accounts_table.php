<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMimeToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_stores', function (Blueprint $table) {
            $table->tinyInteger('is_file')->default(0);
            $table->string('file_mime')->nullable();
            $table->unsignedBigInteger('file_size')->default(0);
            $table->string('file_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_stores', function (Blueprint $table) {
            $table->dropColumn('is_file');
            $table->dropColumn('file_mime');
            $table->dropColumn('file_size');
            $table->dropColumn('file_path');
        });
    }
}
