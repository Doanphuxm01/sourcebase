<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatespacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spaces', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('type')->default(1)->comment('0: Internal, 1: Customer');
            $table->unsignedBigInteger('major_id')->nullable();
            $table->unsignedBigInteger('space_image_id')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();

            $table->foreign('major_id')->references('id')->on('majors')->onDelete('cascade');
            $table->foreign('space_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('spaces', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spaces');
    }
}
