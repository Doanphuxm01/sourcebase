<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeKeyAccountApplicationGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_groups', function (Blueprint $table) {
            $table->dropForeign('application_groups_created_by_foreign');
            $table->dropColumn('created_by');

            $table->unsignedBigInteger('account_id')->nullable();
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_groups', function (Blueprint $table) {
            $table->dropForeign('application_groups_account_id_foreign');
            $table->dropColumn('account_id');

            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
        });

    }
}
