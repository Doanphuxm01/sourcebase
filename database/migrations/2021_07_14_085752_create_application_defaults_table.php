<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateapplicationDefaultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_defaults', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('status')->default(1);
            $table->string('link_android')->nullable();
            $table->string('link_ios')->nullable();
            $table->string('link_url')->nullable();
            $table->string('source')->nullable();

            $table->softDeletes();
            $table->timestamps();

        });

        $this->updateTimestampDefaultValue('application_defaults', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_defaults');
    }
}
