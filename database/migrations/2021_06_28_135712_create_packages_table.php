<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreatepackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('is_active')->default(1)->comment('0: Disable, 1: Active');
            $table->smallInteger('is_trial')->default(0)->comment('0: not trial, 1: trial');
            $table->unsignedFloat('point')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('packages', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
