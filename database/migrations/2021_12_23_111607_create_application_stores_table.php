<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_stores', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('is_default')->default(1)->comment('0: default app, 1: not default app');
            $table->smallInteger('type')->default(1);
            $table->string('link_android')->nullable();
            $table->string('link_ios')->nullable();
            $table->string('link_url')->nullable();
            $table->unsignedBigInteger('application_image_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->tinyInteger('x_frame')->default(0);
            $table->tinyInteger('is_accept')->default(1);
            $table->tinyInteger('is_system')->default(0);
            $table->string('domain')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('application_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_stores');
    }
}
