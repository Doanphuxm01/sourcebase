<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateorganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('is_active')->default(1)->comment('0: Disable, 1: Active');
            $table->unsignedBigInteger('major_id')->nullable();
            $table->unsignedBigInteger('package_id')->nullable();
            $table->unsignedBigInteger('scale')->default(1);
            $table->string('description')->nullable();

            $table->foreign('major_id')->references('id')->on('majors')->onDelete('cascade');
            $table->foreign('package_id')->references('id')->on('packages')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        $this->updateTimestampDefaultValue('organizations', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
