<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DeleteColumnForTableApplicationAdvencedSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_advanced_settings', function (Blueprint $table) {
            $table->dropColumn('weekday');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_advanced_settings', function (Blueprint $table) {
            $table->unsignedBigInteger('weekday')->nullable();

        });

    }
}
