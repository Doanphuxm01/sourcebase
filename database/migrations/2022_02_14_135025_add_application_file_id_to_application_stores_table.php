<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApplicationFileIdToApplicationStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_stores', function (Blueprint $table) {
            $table->tinyInteger('application_file_id')->nullable();
            $table->foreign('application_file_id')->references('id')->on('images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*xoa*/
        Schema::table('application_stores', function (Blueprint $table) {
            $table->dropForeign('application_stores_application_file_id_foreign');
            $table->dropForeign('application_file_id');
        });
    }
}
