<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAndDeletecolumnForTableApplicationGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('application_groups', function (Blueprint $table) {
            $table->dropColumn('is_accept');
            $table->dropColumn('is_leader');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('application_groups', function (Blueprint $table) {
            $table->dropForeign('application_groups_created_by_foreign');
            $table->dropColumn('created_by');

            $table->unsignedBigInteger('is_leader')->default(0);
            $table->unsignedBigInteger('is_accept')->default(1);
        });

    }
}
