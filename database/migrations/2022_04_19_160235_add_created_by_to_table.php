<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreatedByToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quick_bars', function (Blueprint $table) {
            $table->tinyInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quick_bars', function (Blueprint $table) {
            $table->dropForeign('quick_bars_created_by');
            $table->dropForeign('created_by');
        });
    }
}
