<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChangeCollumTypeAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('type_account');
//            $table->tinyInteger('type_account')->default(0)->comment('0: account nhân viên | 1: account khách hàng')->change();
//            $table->enum('type_account', array('0','1'))->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
//            $table->tinyInteger('type_account')->nullable()->comment('0: account nhân viên | 1: account khách hàng');
//            $table->enum('type_account', array('0','1'))->default('0')->change();
        });
    }
}
