<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('md5_file')->default('');
            $table->string('source_thumb')->nullable();
            $table->string('source')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();

            $table->unsignedBigInteger('file_size')->default(0);
            $table->unsignedInteger('width')->default(0);
            $table->tinyInteger('type')->default(1);
            $table->tinyInteger('collection_type')->default(0);
            $table->unsignedInteger('height')->default(0);

            $table->string('mimes')->nullable();
            $table->string('duration')->nullable();
            $table->string('dimension')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });

        $this->updateTimestampDefaultValue('images', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
