<?php

use Illuminate\Database\Schema\Blueprint;
use \App\Database\Migration;

class CreateapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->smallInteger('is_default')->default(1)->comment('0: default app, 1: not default app');
            $table->smallInteger('type')->default(1);
            $table->string('link_android')->nullable();
            $table->string('link_ios')->nullable();
            $table->string('link_url')->nullable();
            $table->date('date_start')->nullable();
            $table->date('date_end')->nullable();

            $table->unsignedBigInteger('application_image_id')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('account_id')->nullable()->comment('use for tag in application');
            $table->unsignedBigInteger('room_id')->nullable();
            $table->unsignedBigInteger('space_id')->nullable();
            $table->unsignedBigInteger('organization_id')->nullable();
            $table->unsignedBigInteger('application_group_id')->nullable();

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('application_image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('space_id')->references('id')->on('spaces')->onDelete('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
            $table->foreign('application_group_id')->references('id')->on('application_groups')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });

        $this->updateTimestampDefaultValue('applications', ['updated_at'], ['created_at']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
