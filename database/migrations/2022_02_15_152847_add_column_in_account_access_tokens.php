<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInAccountAccessTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_access_tokens', function (Blueprint $table) {
            $table->string('ip_used')->nullable();
            $table->string('location')->nullable();
            $table->string('device_name')->nullable();
            $table->string('user_agent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_access_tokens', function (Blueprint $table) {
            $table->dropColumn('ip_used');
            $table->dropColumn('location');
            $table->dropColumn('device_name');
            $table->dropColumn('user_agent');
        });
    }
}
