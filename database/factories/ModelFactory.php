<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\Account::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'remember_token' => '',
        'permission_group_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Image::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'md5_file' => '',
        'source_thumb' => '',
        'source' => '',
        'organization_id' => '',
        'file_size' => '',
        'width' => '',
        'type' => '',
        'height' => '',
        'mimes' => '',
        'duration' => '',
        'dimension' => '',
        'created_by' => '',
    ];
});


$factory->define(App\Models\Postgres\Organization::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
        'major_id' => '',
        'package_id' => '',
        'scale' => '',
        'description' => '',
    ];
});

$factory->define(App\Models\Postgres\Permission::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
    ];
});

$factory->define(App\Models\Postgres\Major::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
        'description' => '',
    ];
});

$factory->define(App\Models\Postgres\Package::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
        'point' => '',
    ];
});

$factory->define(App\Models\Postgres\Group::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
        'organization_id' => '',
        'created_by' => '',
    ];
});

$factory->define(App\Models\Postgres\Decentralization::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\DecentralizationPermission::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\Position::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\ManagementAccount::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'manager_id' => '',
        'account_id' => '',
    ];
});

$factory->define(App\Models\Postgres\ManagementAccount::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'manager_id' => '',
        'account_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Space::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_default' => '',
        'type' => '',
        'link_android' => '',
        'link_ios' => '',
        'link_url' => '',
        'date_start' => '',
        'date_end' => '',
        'application_image_id' => '',
        'created_by' => '',
        'account_id' => '',
        'room_id' => '',
        'space_id' => '',
        'organization_id' => '',
        'application_group_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Application::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_default' => '',
        'type' => '',
        'link_android' => '',
        'link_ios' => '',
        'link_url' => '',
        'date_start' => '',
        'date_end' => '',
        'application_image_id' => '',
        'created_by' => '',
        'account_id' => '',
        'room_id' => '',
        'space_id' => '',
        'organization_id' => '',
        'application_group_id' => '',
    ];
});

$factory->define(App\Models\Postgres\ApplicationGroup::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
        'room_id' => '',
        'space_id' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\ApplicationDefault::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\ApplicationAdvancedSetting::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'application_id' => '',
        'time_start' => '',
        'time_end' => '',
        'weekday' => '',
        'clicks' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Room::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'type' => '',
        'is_pin' => '',
        'position' => '',
        'space_id' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Weekday::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\QuickBar::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'application_id' => '',
        'position' => '',
        'space_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Post::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'content' => '',
        'views' => '',
        'space_id' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Comment::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'content' => '',
        'image_id' => '',
        'post_id' => '',
        'parent_id' => '',
        'space_id' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\Emotion::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'source' => '',
    ];
});

$factory->define(App\Models\Postgres\ApplicationLocation::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\StatictisSpace::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'organization_id' => '',
        'space_id' => '',
        'visited' => '',
    ];
});

$factory->define(App\Models\Postgres\RoomLocations::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'room_id' => '',
        'position' => '',
        'space_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\SpaceAccount::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'space_id' => '',
        'organization_id' => '',
        'is_blocked' => '',
    ];
});

$factory->define(App\Models\Postgres\RoomAccount::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'room_id' => '',
        'space_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\PostImage::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'post_id' => '',
        'image_id' => '',
        'space_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\PostEmotion::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'created_by' => '',
        'post_id' => '',
        'space_id' => '',
        'emotion_id' => '',
        'organization_id' => '',
    ];
});
$factory->define(App\Models\Postgres\AccountGroup::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'group_id' => '',
        'organization_id' => '',
    ];
});


$factory->define(App\Models\Postgres\StatisticApplication::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'room_id' => '',
        'space_id' => '',
        'application_id' => '',
        'date_at' => '',
        'clicks' => '',
    ];
});

$factory->define(App\Models\Postgres\StatisticAccount::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'application_id' => '',
        'statistic_application_id' => '',
        'clicks' => '',
    ];
});

$factory->define(App\Models\Postgres\Notification::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'message' => '',
        'is_read' => '',
        'account_id' => '',
        'created_by' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\SpaceGroup::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'space_id' => '',
        'group_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\ApplicationsType::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_active' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountWorkLife::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\AccountWorkLife::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\Postgres\AccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'background_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'account_life_id' => '',
        'account_work_id' => '',
        'remember_token' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'background_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'account_life_id' => '',
        'account_work_id' => '',
        'remember_token' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'background_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'account_life_id' => '',
        'account_work_id' => '',
        'remember_token' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'background_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'account_life_id' => '',
        'account_work_id' => '',
        'remember_token' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'email' => '',
        'username' => '',
        'password' => '',
        'phone_number' => '',
        'position' => '',
        'language' => '',
        'is_root' => '',
        'birthday' => '',
        'gender' => '',
        'is_active' => '',
        'api_web_access_token' => '',
        'api_ios_access_token' => '',
        'api_android_access_token' => '',
        'profile_image_id' => '',
        'background_image_id' => '',
        'organization_id' => '',
        'created_by' => '',
        'account_life_id' => '',
        'account_work_id' => '',
        'remember_token' => '',
    ];
});

$factory->define(App\Models\Postgres\SpaceAccountLifeWork::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'space_id' => '',
        'organization_id' => '',
        'is_admin' => '',
    ];
});
$factory->define(App\Models\Postgres\RoomAccountsWorkLife::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_life_id' => '',
        'account_id' => '',
        'room_id' => '',
        'space_id' => '',
        'organization_id' => '',
    ];
});

$factory->define(App\Models\Postgres\ApplicationStore::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'name' => '',
        'slug' => '',
        'is_default' => '',
        'type' => '',
        'link_android' => '',
        'link_ios' => '',
        'link_url' => '',
        'application_image_id' => '',
        'created_by' => '',
        'x_frame' => '',
        'is_accept' => '',
        'is_system' => '',
        'domain' => '',
    ];
});

$factory->define(App\Models\Postgres\AccountAccessTokens::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'account_id' => '',
        'token' => '',
        'last_used_at' => '',
    ];
});

$factory->define(App\Models\Postgres\StatisticAccountTimeUsedApp::class, function (Faker\Generator $faker) {
    return [
        'id' => '',
        'room_id' => '',
        'space_id' => '',
        'application_id' => '',
        'application_store_id' => '',
        'account_id' => '',
        'start_time' => '',
        'end_time' => '',
        'time_used' => '',
    ];
});

/* NEW MODEL FACTORY */
