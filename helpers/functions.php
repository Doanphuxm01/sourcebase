<?php 

if (!function_exists('res')){
    function res($data = [], $code = 0, $message = ''){
        $message = $message ? $message : __('notification.system.successful-data-retrieval');
        return \App\Elibs\eResponse::response($code,$message,$data);
    }
}