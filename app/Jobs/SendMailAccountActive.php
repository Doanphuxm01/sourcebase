<?php

namespace App\Jobs;

use App\Models\Postgres\Account;
use App\Repositories\Postgres\Eloquent\AccountRepository;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\Production\AccountService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;
use App\Models\Postgres\AccountLife;
use Illuminate\Support\Str;

class SendMailAccountActive implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $customerId;
    protected $event;
    public $accountService;
    public function __construct(
        $customerId,
        $event
    )
    {
        $this->customerId = $customerId;
        $this->event = $event;
    }

    /**
     * Execute the job.
     *
     * @param AccountServiceInterface $accountService
     * @return bool
     */
    public function handle(AccountServiceInterface $accountService)
    {
        $customerId = $this->customerId;
        $event = $this->event;
        $array['password'] = '';
        if(!empty($customerId['is_array'])) {
            foreach ($customerId as $item) {
                $accountService->sendRabbitMQForMail($item, $event, $array['password'], '', '');
            }
            return true;
        } else {
            $accountService->sendRabbitMQForMail($customerId, $event, $array['password'], '', '');
        }
        return false;
    }
}
