<?php

namespace App\Jobs;

use App\Elibs\eCrypt;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $account_ids= [];
    protected $payload= [];

    /**
     * Create a new job instance.
     * @param string|array $account_ids
     * @param string|array|integer $payload
     * @return void
     */
    public function __construct($account_ids , $payload)
    {
        //
        $this->account_ids = $account_ids;
        $this->payload = $payload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $accountRecive = $this->account_ids;
        $data = $this->payload;
        $event = $this->payload['event'];
        $notification = [
            "title" => env('APP_NAME'),
            "body" => __('notification.socket.'.$event, $data),
        ];
        $sendFcm = \UserNotificationHelper::sendWithAccountId($accountRecive,$data,$notification);
        $data = json_encode([
            'is_loop' => is_array($accountRecive) ? 1 : 0,
            'user_recive' => $accountRecive,
            'payload' => eCrypt::encryptAES(json_encode($data))
//            'payload' => $data
        ]);

        $payload = $data;

        $ch = curl_init(config('systems.SOCKET_URL') . '/send-notification');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload)
            )
        );

        // Submit the POST request
        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
    }
}
