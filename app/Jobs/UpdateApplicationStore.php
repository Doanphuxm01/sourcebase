<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationStore;
use Illuminate\Support\Facades\DB;

class UpdateApplicationStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applications = Application::whereNull('deleted_at')->get();
        $applicationsGetAll = $applications->toArray();
        foreach ($applicationsGetAll as $app) {
            $data = [
                'name' => !empty($app['name']) ? $app['name'] : null,
                'slug' => !empty($app['slug']) ? $app['slug'] : null,
                'type' => !empty($app['type']) ? $app['type'] : 1,
                'link_android' => !empty($app['link_android']) ? $app['link_android'] : null,
                'is_default' => 0,
                'link_ios' => !empty($app['link_ios']) ? $app['link_ios'] : null,
                'link_url' => !empty($app['link_url']) ? $app['link_url'] : null,
                'application_image_id' => !empty($app['application_image_id']) ? $app['application_image_id'] : null,
                'created_by' => !empty($app['created_by']) ? $app['created_by'] : null,
                // 'room_id' => !empty($app['room_id']) ? $app['room_id'] : null,
                // 'space_id' => !empty($app['space_id']) ? $app['space_id'] : null,
                'organization_id' => !empty($app['organization_id']) ? $app['organization_id'] : null,
                'is_accept' => !empty($app['is_accept']) ? $app['is_accept'] : 0,
                'x_frame' => !empty($app['x_frame']) ? $app['x_frame'] : 0,
                'is_system' => 0,
                'domain' => !empty($app['domain']) ? $app['domain'] : null,
                'created_at' => !empty($app['created_at']) ? $app['created_at'] : now(),
                'updated_at' => !empty($app['updated_at']) ? $app['updated_at'] : now(),
            ];
            ApplicationStore::create($data);
        }
        foreach ($applicationsGetAll as $app) {
            $applicationStore = ApplicationStore::where('slug', $app['slug'])->where('name', $app['name'])->where('organization_id', $app['organization_id'])->first();
            if (!empty($applicationStore)) {
                Application::where('id', $app['id'])->update(['application_store_id' => $applicationStore->id]);
            }
        }
        return true;
    }
}
