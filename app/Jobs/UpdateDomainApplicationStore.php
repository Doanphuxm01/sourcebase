<?php

namespace App\Jobs;

use App\Elibs\eFunction;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Postgres\ApplicationStore;

class UpdateDomainApplicationStore implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle()
    {
        $model = ApplicationStore::whereNotNull('link_url')->whereNull('domain')->get()->toArray();
        if (!empty($model)) {
            foreach ($model as $temp) {
                ApplicationStore::where('id', $temp['id'])->update(['domain' => eFunction::getDomain($temp['link_url'])]);
            }
        }
        return false;
    }
}
