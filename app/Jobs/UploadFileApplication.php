<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UploadFileApplication implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $file;
    protected $folder;
    protected $newPath;

    public function __construct($file, $folder, $newPath)
    {
        $this->file = $file;
        $this->folder = $folder;
        $this->newPath = $newPath;
    }

    /**
     * Execute the job.
     *
     * @param $file
     * @param $folder
     * @param $newPath
     * @return void
     */
    public function handle()
    {
        $qFolder = $this->folder;
        $qNameFile = $this->newPath;
        $url = Storage::disk('public');
        $url->put("public/file/' . $qFolder, $qNameFile", (string) $qNameFile, 'public');
        $this->file->storeAs('public/file/' . $this->folder, $this->newPath);
    }
}
