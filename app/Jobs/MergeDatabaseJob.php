<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Console\Command;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class MergeDatabaseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::table('life_accounts')->whereNull('deleted_at')->orderBy('life_accounts.id')->chunk(1, function ($accounts) {
                $accountDones = $accounts->toArray();
                // Nếu account dones tồn tại
                if (!empty($accountDones[0])) {
                    $accountDones = (array)$accountDones[0];
                    //Kiểm tra sự tồn tại của account trong dones pro
                    $accountPro = DB::table('accounts')->whereNull('deleted_at')->where('email', $accountDones['email'])->first();
                    // Lấy ảnh mà account tạo ra
                    $images = DB::table('life_images')->where('created_by', $accountDones['id'])->whereNull('deleted_at')->get();
                    // Lấy space mà account tạo ra
                    $spaces = DB::table('life_spaces')->where('created_by', $accountDones['id'])->whereNull('deleted_at')->get();
                    // Lấy space mà account tham gia vào
                    $spaceAccount = DB::table('life_space_accounts')->where('account_id', $accountDones['id'])->get();
                    // Lấy rooms mà account tạo ra
                    $rooms = DB::table('life_rooms')->where('created_by', $accountDones['id'])->whereNull('deleted_at')->get();
                    // Lấy rooms mà account tham gia vào
                    $roomAccount = DB::table('life_room_accounts')->where('account_id', $accountDones['id'])->get();
                    // Lấy spaces account work life mà account life tham gia vào
                    $spaceAccountWorkLife = DB::table('space_accounts_work_life')->where('account_life_id', $accountDones['id'])->get();
                    // Lấy room account work life mà account life tham gia vào
                    $roomAccountWorkLife = DB::table('room_accounts_work_life')->where('account_life_id', $accountDones['id'])->get();
                    // Lấy application store mà account life tạo ra vào
                    $applicationStore = DB::table('life_application_stores')->whereNull('deleted_at')->where('created_by', $accountDones['id'])->get();
                    // Lấy quickbar mà account life có
                    $quickbars = DB::table('life_quick_bars')->where('account_id', $accountDones['id'])->get();
                    // Lấy application group mà account life có
                    $applicationGroups = DB::table('life_application_groups')->where('created_by', $accountDones['id'])->get();
                    // Lấy application trong room mà account life có
                    $applications = DB::table('life_applications')->where('created_by', $accountDones['id'])->get();
                    // Lây application advanced settings mà account life tạo
                    $applicationAdvancedSettings = DB::table('life_application_advanced_settings')->where('created_by', $accountDones['id'])->get();

                    $checkImages = $images->toArray();
                    $checkSpace = $spaces->toArray();
                    $checkSpaceAccount = $spaceAccount->toArray();
                    $checkRooms = $rooms->toArray();
                    $checkRoomAccount = $roomAccount->toArray();
                    $checkSpaceAccountWorkLife = $spaceAccountWorkLife->toArray();
                    $checkRoomAccountWorkLife = $roomAccountWorkLife->toArray();
                    $checkApplicationStore = $applicationStore->toArray();
                    $checkQuickbars = $quickbars->toArray();
                    $checkApplicationGroups = $applicationGroups->toArray();
                    $checkApplications = $applications->toArray();
                    $checkApplicationAdvancedSettings = $applicationAdvancedSettings->toArray();

                    // Lấy danh sách space được tạo bởi account
                    if ($accountPro) {
                        // Nếu account đã tồn tại
                        // Lấy thông tin life images account tạo
                        if (!empty($checkImages)) {
                            $images->map(function ($el) use ($accountPro) {
                                $el->created_by = $accountPro->id;
                                // $el->is_dones = 1;
                            });
                            $images = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkImages);
                            DB::table('images')->insert($images);
                            // MergeImagesJob::dispatch($images, $accountPro->id);
                        }

                        // Insert Space
                        if (!empty($checkSpace)) {
                            $spaces->map(function ($el) use ($accountPro) {
                                if ($el->space_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->space_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->space_image_id = $imageNew->id;
                                        } else {
                                            $el->space_image_id = NULL;
                                        }
                                    } else {
                                        $el->space_image_id = NULL;
                                    }
                                } else {
                                    $el->space_image_id = NULL;
                                }
                                $el->created_by = $accountPro->id;
                                $el->type = 3;
                            });
                            $spaces = array_map(function ($el) {
                                unset($el->id);
                                unset($el->uuid);
                                return (array)$el;
                            }, $checkSpace);
                            DB::table('spaces')->insert($spaces);
                            // MergeSpaceJob::dispatch($spaces, $accountPro->id);
                            // $sizeJobSpace = \Queue::size('MergeSpaceJob');
                            // \Log::info($sizeJobSpace);
                        }

                        // Insert Space Account
                        if (!empty($checkSpaceAccount)) {
                            $spaceAccount->map(function ($el) use ($accountPro) {
                                $findLifeSpace = DB::table('life_spaces')->select('id', 'slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew) {
                                        $el->space_id = $idSpaceNew->id;
                                        $el->account_id = $accountPro->id;
                                    } else {
                                        $el->space_id = NULL;
                                        $el->account_id = NULL;
                                    }
                                } else {
                                    $el->space_id = NULL;
                                    $el->account_id = NULL;
                                }
                            });
                            $spaceAccount = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkSpaceAccount);
                            DB::table('space_accounts')->insert($spaceAccount);
                            DB::table('space_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        // Insert Room
                        if (!empty($checkRooms)) {
                            $rooms->map(function ($el) use ($accountPro) {
                                $findLifeSpace = DB::table('life_spaces')->select('id', 'slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew) {
                                        $el->space_id = $idSpaceNew->id;
                                        $el->created_by = $accountPro->id;
                                    } else {
                                        $el->space_id = NULL;
                                        $el->created_by = NULL;
                                    }
                                } else {
                                    $el->space_id = NULL;
                                    $el->created_by = NULL;
                                }
                            });
                            $rooms = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRooms);
                            DB::table('rooms')->insert($rooms);
                            DB::table('rooms')->whereNull('space_id')->delete();
                        }

                        // Insert Room Account
                        if (!empty($checkRoomAccount)) {
                            $roomAccount->map(function ($el) use ($accountPro) {
                                $findLifeSpace = DB::table('life_spaces')->select('id', 'slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                $findLifeRoom = DB::table('life_rooms')->select('id', 'slug')->where('id', $el->room_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace && $findLifeRoom) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    $idRoomNew = DB::table('rooms')->select('id')->where('slug', $findLifeRoom->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew && $idRoomNew) {
                                        $el->room_id = $idRoomNew->id;
                                        $el->space_id = $idSpaceNew->id;
                                        $el->account_id = $accountPro->id;
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                        $el->account_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                    $el->account_id = NULL;
                                }
                            });
                            $roomAccount = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRoomAccount);
                            DB::table('room_accounts')->insert($roomAccount);
                            DB::table('room_accounts')->whereNull('account_id')->where('room_id')->whereNull('space_id')->delete();
                        }

                        // Insert Space Account Work Life
                        if (!empty($checkSpaceAccountWorkLife)) {
                            $spaceAccountWorkLife->map(function ($el) use ($accountPro) {
                                $el->account_id = $accountPro->id;
                                unset($el->account_life_id);
                            });
                            $spaceAccountWorkLife = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkSpaceAccountWorkLife);
                            DB::table('space_accounts')->insert($spaceAccountWorkLife);
                            DB::table('space_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        // Insert Room Account Work Life
                        if (!empty($checkRoomAccountWorkLife)) {
                            $roomAccountWorkLife->map(function ($el) use ($accountPro) {
                                $el->account_id = $accountPro->id;
                                unset($el->account_life_id);
                            });
                            $roomAccountWorkLife = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRoomAccountWorkLife);
                            DB::table('room_accounts')->insert($roomAccountWorkLife);
                            DB::table('room_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        // Insert Application Store
                        if (!empty($checkApplicationStore)) {
                            $applicationStore->map(function ($el) use ($accountPro) {
                                if ($el->application_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->application_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->application_image_id = $imageNew->id;
                                        } else {
                                            $el->application_image_id = NULL;
                                        }
                                    } else {
                                        $el->application_image_id = NULL;
                                    }
                                } else {
                                    $el->application_image_id = NULL;
                                }
                                $el->created_by = $accountPro->id;
                            });

                            $applicationStore = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationStore);
                            DB::table('application_stores')->insert($applicationStore);
                        }

                        // Insert Quickbar
                        if (!empty($checkQuickbars)) {
                            $quickbars->map(function ($el) use ($accountPro) {
                                if ($el->application_id && $el->space_id) {
                                    $applicationStore = DB::table('life_application_stores')->where('id', $el->application_id)->first();
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    if ($applicationStore && $space) {
                                        $applicationStoreNew = DB::table('application_stores')->where('name', $applicationStore->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        if ($applicationStoreNew && $spaceNew) {
                                            $el->application_id = $applicationStoreNew->id;
                                            $el->space_id = $spaceNew->id;
                                        } else {
                                            $el->application_id = NULL;
                                            $el->space_id = NULL;
                                        }
                                    } else {
                                        $el->application_id = NULL;
                                        $el->space_id = NULL;
                                    }
                                } else {
                                    $el->application_id = NULL;
                                    $el->space_id = NULL;
                                }

                                if ($el->created_by) {
                                    $account = DB::table('life_accounts')->where('id', $el->created_by)->first();
                                    if ($account) {
                                        $accountNew = DB::table('accounts')->where('email', $account->email)->first();
                                        if ($accountNew) {
                                            $el->created_by = $accountNew->id;
                                        }
                                    }
                                }
                                $el->account_id = $accountPro->id;
                            });

                            $quickbars = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkQuickbars);
                            DB::table('quick_bars')->insert($quickbars);
                            // DB::table('quick_bars')->whereNull('space_id')->whereNull('application_id')->delete();
                        }

                        // Insert Application Group
                        if (!empty($checkApplicationGroups)) {
                            $applicationGroups->map(function ($el) use ($accountPro) {
                                if ($el->room_id && $el->space_id) {
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    $room = DB::table('life_rooms')->where('id', $el->room_id)->first();
                                    if ($room && $space) {
                                        $roomNew = DB::table('rooms')->where('name', $room->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        if ($roomNew && $spaceNew) {
                                            $el->room_id = $roomNew->id;
                                            $el->space_id = $spaceNew->id;
                                        } else {
                                            $el->room_id = NULL;
                                            $el->space_id = NULL;
                                        }
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                }

                                $el->created_by = $accountPro->id;
                            });

                            $applicationGroups = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationGroups);
                            DB::table('application_groups')->insert($applicationGroups);
                        }

                        // Insert Applications
                        if (!empty($checkApplications)) {
                            $applications->map(function ($el) use ($accountPro) {
                                if ($el->room_id && $el->space_id) {
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    $room = DB::table('life_rooms')->where('id', $el->room_id)->first();
                                    $applicationStore = DB::table('life_application_stores')->where('id', $el->application_store_id)->first();
                                    if ($room && $space && $applicationStore) {
                                        $roomNew = DB::table('rooms')->where('name', $room->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        $applicationStoreNew = DB::table('application_stores')->where('name', $applicationStore->name)
                                            ->where(function ($q) use ($accountPro) {
                                                $q->orWhere('created_by', null)->orWhere('created_by', $accountPro->id);
                                            })->first();
                                        if ($roomNew && $spaceNew && $applicationStoreNew) {
                                            $el->room_id = $roomNew->id;
                                            $el->space_id = $spaceNew->id;
                                            $el->application_store_id = $applicationStoreNew->id;
                                        } else {
                                            $el->room_id = NULL;
                                            $el->space_id = NULL;
                                            $el->application_store_id = NULL;
                                        }
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                        $el->application_store_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                    $el->application_store_id = NULL;
                                }

                                if ($el->application_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->application_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->application_image_id = $imageNew->id;
                                        } else {
                                            $el->application_image_id = NULL;
                                        }
                                    } else {
                                        $el->application_image_id = NULL;
                                    }
                                } else {
                                    $el->application_image_id = NULL;
                                }

                                if ($el->application_group_id) {
                                    $applicationGroup = DB::table('life_application_groups')->where('id', $el->application_group_id)->first();
                                    if ($applicationGroup) {
                                        $applicationGroupNew = DB::table('application_groups')->where('name', $applicationGroup->name)->first();
                                        if ($applicationGroupNew) {
                                            $el->application_group_id = $applicationGroupNew->id;
                                        } else {
                                            $el->application_group_id = NULL;
                                        }
                                    } else {
                                        $el->application_group_id = NULL;
                                    }
                                } else {
                                    $el->application_group_id = NULL;
                                }

                                $el->created_by = $accountPro->id;
                                $el->account_id = NULL;
                            });

                            $applications = array_map(function ($el) {
                                unset($el->id);
                                unset($el->is_default);
                                return (array)$el;
                            }, $checkApplications);
                            DB::table('applications')->insert($applications);
                            DB::table('applications')->whereNull('room_id')->whereNull('space_id')->delete();
                        }

                        // Insert ApplicationAdvancedSettings
                        if (!empty($checkApplicationAdvancedSettings)) {
                            $applicationAdvancedSettings->map(function ($el) use ($accountPro) {
                                if ($el->application_id) {
                                    $application = DB::table('life_applications')->where('id', $el->application_id)->where('created_by', $el->created_by)->first();
                                    if ($application) {
                                        $applicationNew = DB::table('applications')->where('name', $application->name)->where('created_by', $accountPro->id)->first();
                                        if ($applicationNew) {
                                            $el->application_id = $applicationNew->id;
                                        } else {
                                            $el->application_id = NULL;
                                        }
                                    } else {
                                        $el->application_id = NULL;
                                    }
                                } else {
                                    $el->application_id = NULL;
                                }

                                $el->created_by = $accountPro->id;
                            });

                            $applicationAdvancedSettings = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationAdvancedSettings);
                            DB::table('application_advanced_settings')->insert($applicationAdvancedSettings);
                        }
                    } else {
                        // Nếu account đó không tồn tại
                        // Thêm account vào bảng account dones pro và trả về account_id
                        // $accountDones['is_dones'] = 1;
                        $accountDones['background_image_id'] = NULL;
                        $accountDones['profile_image_id'] = NULL;
                        $accountDones['main_space'] = NULL;
                        unset($accountDones['id']);
                        unset($accountDones['is_dones_life']);

                        // Thêm accounts
                        $idAccountDones = DB::table('accounts')->insertGetId((array)$accountDones);

                        // Thêm images người dùng tạo
                        if (!empty($checkImages)) {
                            $images->map(function ($el) use ($idAccountDones) {
                                $el->created_by = $idAccountDones;
                                // $el->is_dones = 1;
                            });
                            $images = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkImages);
                            DB::table('images')->insert($images);
                            // MergeImagesJob::dispatch($images, $idAccountDones);
                        }

                        // Insert Space
                        if (!empty($checkSpace)) {
                            $spaces->map(function ($el) use ($idAccountDones) {
                                if ($el->space_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->space_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->space_image_id = $imageNew->id;
                                        } else {
                                            $el->space_image_id = NULL;
                                        }
                                    } else {
                                        $el->space_image_id = NULL;
                                    }
                                } else {
                                    $el->space_image_id = NULL;
                                }

                                $el->created_by = $idAccountDones;
                                $el->type = 3;
                            });
                            $spaces = array_map(function ($el) {
                                unset($el->id);
                                unset($el->uuid);
                                return (array)$el;
                            }, $checkSpace);
                            DB::table('spaces')->insert($spaces);
                            // MergeSpaceJob::dispatch($spaces, $idAccountDones);
                        }

                        // Insert Space Account
                        if (!empty($checkSpaceAccount)) {
                            $spaceAccount->map(function ($el) use ($idAccountDones) {
                                $findLifeSpace = DB::table('life_spaces')->select('slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew) {
                                        $el->account_id = $idAccountDones;
                                        $el->space_id = $idSpaceNew->id;
                                    } else {
                                        $el->space_id = NULL;
                                        $el->account_id = NULL;
                                    }
                                } else {
                                    $el->space_id = NULL;
                                    $el->account_id = NULL;
                                }
                            });
                            $spaceAccount = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkSpaceAccount);
                            DB::table('space_accounts')->insert($spaceAccount);
                            DB::table('space_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        // Insert Room
                        if (!empty($checkRooms)) {
                            $rooms->map(function ($el) use ($idAccountDones) {
                                $findLifeSpace = DB::table('life_spaces')->select('id', 'slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew) {
                                        $el->space_id = $idSpaceNew->id;
                                        $el->created_by = $idAccountDones;
                                    } else {
                                        $el->space_id = NULL;
                                        $el->created_by = NULL;
                                    }
                                } else {
                                    $el->space_id = NULL;
                                    $el->created_by = NULL;
                                }
                            });
                            $rooms = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRooms);
                            DB::table('rooms')->insert($rooms);
                            DB::table('rooms')->whereNull('space_id')->delete();
                        }

                        // Insert Room Account
                        if (!empty($checkRoomAccount)) {
                            $roomAccount->map(function ($el) use ($idAccountDones) {
                                $findLifeSpace = DB::table('life_spaces')->select('id', 'slug')->where('id', $el->space_id)->whereNull('deleted_at')->first();
                                $findLifeRoom = DB::table('life_rooms')->select('id', 'slug')->where('id', $el->room_id)->whereNull('deleted_at')->first();
                                if ($findLifeSpace && $findLifeRoom) {
                                    $idSpaceNew = DB::table('spaces')->select('id')->where('slug', $findLifeSpace->slug)->whereNull('deleted_at')->first();
                                    $idRoomNew = DB::table('rooms')->select('id')->where('slug', $findLifeRoom->slug)->whereNull('deleted_at')->first();
                                    if ($idSpaceNew && $idRoomNew) {
                                        $el->room_id = $idRoomNew->id;
                                        $el->space_id = $idSpaceNew->id;
                                        $el->account_id = $idAccountDones;
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                        $el->account_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                    $el->account_id = NULL;
                                }
                            });
                            $roomAccount = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRoomAccount);
                            DB::table('room_accounts')->insert($roomAccount);
                            DB::table('room_accounts')->whereNull('account_id')->where('room_id')->whereNull('space_id')->delete();
                        }

                        // Insert Space Account Work Life
                        if (!empty($checkSpaceAccountWorkLife)) {
                            $spaceAccountWorkLife->map(function ($el) use ($idAccountDones) {
                                $el->account_id = $idAccountDones;
                                unset($el->account_life_id);
                            });
                            $spaceAccountWorkLife = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkSpaceAccountWorkLife);
                            DB::table('space_accounts')->insert($spaceAccountWorkLife);
                            DB::table('space_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        // Insert Room Account Work Life
                        if (!empty($checkRoomAccountWorkLife)) {
                            $roomAccountWorkLife->map(function ($el) use ($idAccountDones) {
                                $el->account_id = $idAccountDones;
                                unset($el->account_life_id);
                            });
                            $roomAccountWorkLife = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkRoomAccountWorkLife);
                            DB::table('room_accounts')->insert($roomAccountWorkLife);
                            DB::table('room_accounts')->whereNull('account_id')->whereNull('space_id')->delete();
                        }

                        //Insert Application Store
                        if (!empty($checkApplicationStore)) {
                            $applicationStore->map(function ($el) use ($idAccountDones) {
                                if ($el->application_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->application_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->application_image_id = $imageNew->id;
                                        } else {
                                            $el->application_image_id = NULL;
                                        }
                                    } else {
                                        $el->application_image_id = NULL;
                                    }
                                } else {
                                    $el->application_image_id = NULL;
                                }

                                $el->created_by = $idAccountDones;
                            });

                            $applicationStore = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationStore);
                            DB::table('application_stores')->insert($applicationStore);
                        }

                        //Insert Quickbar
                        if (!empty($checkQuickbars)) {
                            $quickbars->map(function ($el) use ($idAccountDones) {
                                if ($el->application_id && $el->space_id) {
                                    $applicationStore = DB::table('life_application_stores')->where('id', $el->application_id)->first();
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    if ($applicationStore && $space) {
                                        $applicationStoreNew = DB::table('application_stores')->where('name', $applicationStore->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        if ($applicationStoreNew && $spaceNew) {
                                            $el->application_id = $applicationStoreNew->id;
                                            $el->space_id = $spaceNew->id;
                                        } else {
                                            $el->application_id = NULL;
                                            $el->space_id = NULL;
                                        }
                                    } else {
                                        $el->application_id = NULL;
                                        $el->space_id = NULL;
                                    }
                                } else {
                                    $el->application_id = NULL;
                                    $el->space_id = NULL;
                                }

                                if ($el->created_by) {
                                    $account = DB::table('life_accounts')->where('id', $el->created_by)->first();
                                    if ($account) {
                                        $accountNew = DB::table('accounts')->where('email', $account->email)->first();
                                        if ($accountNew) {
                                            $el->created_by = $accountNew->id;
                                        }
                                    }
                                }
                                $el->account_id = $idAccountDones;
                            });

                            $quickbars = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkQuickbars);
                            DB::table('quick_bars')->insert($quickbars);
                            // DB::table('quick_bars')->whereNull('space_id')->whereNull('application_id')->delete();
                        }

                        //Insert Application Group
                        if (!empty($checkApplicationGroups)) {
                            $applicationGroups->map(function ($el) use ($idAccountDones) {
                                if ($el->room_id && $el->space_id) {
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    $room = DB::table('life_rooms')->where('id', $el->room_id)->first();
                                    if ($room && $space) {
                                        $roomNew = DB::table('rooms')->where('name', $room->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        if ($roomNew && $spaceNew) {
                                            $el->room_id = $roomNew->id;
                                            $el->space_id = $spaceNew->id;
                                        } else {
                                            $el->room_id = NULL;
                                            $el->space_id = NULL;
                                        }
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                }

                                $el->created_by = $idAccountDones;
                            });

                            $applicationGroups = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationGroups);
                            DB::table('application_groups')->insert($applicationGroups);
                        }

                        // Insert Applications
                        if (!empty($checkApplications)) {
                            $applications->map(function ($el) use ($idAccountDones) {
                                if ($el->room_id && $el->space_id) {
                                    $space = DB::table('life_spaces')->where('id', $el->space_id)->first();
                                    $room = DB::table('life_rooms')->where('id', $el->room_id)->first();
                                    $applicationStore = DB::table('life_application_stores')->where('id', $el->application_store_id)->first();
                                    if ($room && $space && $applicationStore) {
                                        $roomNew = DB::table('rooms')->where('name', $room->name)->first();
                                        $spaceNew = DB::table('spaces')->where('name', $space->name)->first();
                                        $applicationStoreNew = DB::table('application_stores')->where('name', $applicationStore->name)
                                            ->where(function ($q) use ($idAccountDones) {
                                                $q->orWhere('created_by', null)->orWhere('created_by', $idAccountDones);
                                            })->first();
                                        if ($roomNew && $spaceNew && $applicationStoreNew) {
                                            $el->room_id = $roomNew->id;
                                            $el->space_id = $spaceNew->id;
                                            $el->application_store_id = $applicationStoreNew->id;
                                        } else {
                                            $el->room_id = NULL;
                                            $el->space_id = NULL;
                                            $el->application_store_id = NULL;
                                        }
                                    } else {
                                        $el->room_id = NULL;
                                        $el->space_id = NULL;
                                        $el->application_store_id = NULL;
                                    }
                                } else {
                                    $el->room_id = NULL;
                                    $el->space_id = NULL;
                                    $el->application_store_id = NULL;
                                }

                                if ($el->application_image_id) {
                                    $image = DB::table('life_images')->where('id', $el->application_image_id)->first();
                                    if ($image) {
                                        $imageNew = DB::table('images')->where('md5_file', $image->md5_file)->whereNull('deleted_at')->first();
                                        if ($imageNew) {
                                            $el->application_image_id = $imageNew->id;
                                        } else {
                                            $el->application_image_id = NULL;
                                        }
                                    } else {
                                        $el->application_image_id = NULL;
                                    }
                                } else {
                                    $el->application_image_id = NULL;
                                }

                                if ($el->application_group_id) {
                                    $applicationGroup = DB::table('life_application_groups')->where('id', $el->application_group_id)->first();
                                    if ($applicationGroup) {
                                        $applicationGroupNew = DB::table('application_groups')->where('name', $applicationGroup->name)->first();
                                        if ($applicationGroupNew) {
                                            $el->application_group_id = $applicationGroupNew->id;
                                        } else {
                                            $el->application_group_id = NULL;
                                        }
                                    } else {
                                        $el->application_group_id = NULL;
                                    }
                                } else {
                                    $el->application_group_id = NULL;
                                }

                                $el->created_by = $idAccountDones;
                                $el->account_id = NULL;
                            });

                            $applications = array_map(function ($el) {
                                unset($el->id);
                                unset($el->is_default);
                                return (array)$el;
                            }, $checkApplications);
                            DB::table('applications')->insert($applications);
                            DB::table('applications')->whereNull('room_id')->whereNull('space_id')->delete();
                        }

                        // Insert ApplicationAdvancedSettings
                        if (!empty($checkApplicationAdvancedSettings)) {
                            $applicationAdvancedSettings->map(function ($el) use ($idAccountDones) {
                                if ($el->application_id) {
                                    $application = DB::table('life_applications')->where('id', $el->application_id)->where('created_by', $el->created_by)->first();
                                    if ($application) {
                                        $applicationNew = DB::table('applications')->where('name', $application->name)->where('created_by', $idAccountDones)->first();
                                        if ($applicationNew) {
                                            $el->application_id = $applicationNew->id;
                                        } else {
                                            $el->application_id = NULL;
                                        }
                                    } else {
                                        $el->application_id = NULL;
                                    }
                                } else {
                                    $el->application_id = NULL;
                                }

                                $el->created_by = $idAccountDones;
                            });

                            $applicationAdvancedSettings = array_map(function ($el) {
                                unset($el->id);
                                return (array)$el;
                            }, $checkApplicationAdvancedSettings);
                            DB::table('application_advanced_settings')->insert($applicationAdvancedSettings);
                        }
                    }
                }
            });

            // Lấy spaces account work life mà account pro tham gia vào
            $spaceAccountWorkLifePro = DB::table('space_accounts_work_life')->whereNotNull('account_id')->get()->toArray();
            if (!empty($spaceAccountWorkLifePro)) {
                $spaceAccountWorkLifePro = array_map(function ($el) {
                    unset($el->id);
                    unset($el->account_life_id);
                    return (array)$el;
                }, $spaceAccountWorkLifePro);
                DB::table('space_accounts')->insert($spaceAccountWorkLifePro);
            }

            // Lấy rooms account work life mà account pro tham gia vào
            $roomAccountWorkLifePro = DB::table('room_accounts_work_life')->whereNotNull('account_id')->get()->toArray();
            if (!empty($roomAccountWorkLifePro)) {
                $roomAccountWorkLifePro = array_map(function ($el) {
                    unset($el->id);
                    unset($el->account_life_id);
                    return (array)$el;
                }, $roomAccountWorkLifePro);
                DB::table('room_accounts')->insert($roomAccountWorkLifePro);
            }

            return Command::SUCCESS;
        } catch (\Exception $ex) {

            \Log::error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
