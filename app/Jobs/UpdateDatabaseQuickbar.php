<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Postgres\Application;
use App\Models\Postgres\QuickBar;

class UpdateDatabaseQuickbar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $apps = Application::whereNull('deleted_at')->get()->toArray();
        foreach ($apps as $app) {
            QuickBar::where('application_id', $app['id'])->update(['application_id' => $app['application_store_id']]);
        }
        return true;
    }
}
