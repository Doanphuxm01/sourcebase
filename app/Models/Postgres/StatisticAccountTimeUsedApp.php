<?php namespace App\Models\Postgres;
use App\Models\Base;


class StatisticAccountTimeUsedApp extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statistic_account_time_used_app';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_id',
        'space_id',
        'application_id',
        'application_store_id',
        'account_id',
        'start_time',
        'end_time',
        'time_used',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\StatisticAccountTimeUsedAppPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\StatisticAccountTimeUsedAppObserver);
    }

    // Relations
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }

    public function application()
    {
        return $this->belongsTo(Application::class, 'application_id', 'id');
    }

    public function applicationStore()
    {
        return $this->belongsTo(ApplicationStore::class, 'application_store_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'room_id' => $this->room_id,
            'space_id' => $this->space_id,
            'application_id' => $this->application_id,
            'application_store_id' => $this->application_store_id,
            'account_id' => $this->account_id,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'time_used' => $this->time_used,
        ];
    }

}
