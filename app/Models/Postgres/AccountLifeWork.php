<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Elasticquent\ElasticquentTrait;

class AccountLifeWork extends Base
{
    use ElasticquentTrait;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account_life_work';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'phone_number',
        'position',
        'language',
        'is_root',
        'birthday',
        'gender',
        'is_active',
        'api_web_access_token',
        'api_ios_access_token',
        'api_android_access_token',
        'profile_image_id',
        'background_image_id',
        'organization_id',
        'created_by',
        'account_life_id',
        'account_work_id',
        'remember_token',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $indexSettings = [
        'analysis' => [
            'char_filter' => [
                'replace' => [
                    'type' => 'mapping',
                    'mappings' => [
                        '&=> and '
                    ],
                ],
            ],
            'filter' => [
                'word_delimiter' => [
                    'type' => 'word_delimiter',
                    'split_on_numerics' => false,
                    'split_on_case_change' => true,
                    'generate_word_parts' => true,
                    'generate_number_parts' => true,
                    'catenate_all' => true,
                    'preserve_original' => true,
                    'catenate_numbers' => true,
                ]
            ],
            'analyzer' => [
                'default' => [
                    'type' => 'custom',
                    'char_filter' => [
                        'html_strip',
                        'replace',
                    ],
                    'tokenizer' => 'whitespace',
                    'filter' => [
                        'lowercase',
                        'word_delimiter',
                    ],
                ],
            ],
        ],
    ];

    function getIndexName(): string
    {
        return 'account_life_work';
    }

    function getTypeName(): string
    {
        return 'account_life_work';
    }

    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\AccountLifeWorkPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\AccountLifeWorkObserver);
    }

    // Relations
    public function profileImage()
    {
        return $this->hasOne(Image::class, 'id', 'profile_image_id');
    }

    public function backgroundImage()
    {
        return $this->hasOne(Image::class, 'id', 'background_image_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

//    public function accountLife()
//    {
//        return $this->belongsTo(\App\Models\AccountLife::class, 'account_life_id', 'id');
//    }
//
//    public function accountWork()
//    {
//        return $this->belongsTo(\App\Models\AccountWork::class, 'account_work_id', 'id');
//    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'username' => $this->username,
            'password' => $this->password,
            'phone_number' => $this->phone_number,
            'position' => $this->position,
            'language' => $this->language,
            'is_root' => $this->is_root,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'is_active' => $this->is_active,
            'api_web_access_token' => $this->api_web_access_token,
            'api_ios_access_token' => $this->api_ios_access_token,
            'api_android_access_token' => $this->api_android_access_token,
            'profile_image_id' => $this->profile_image_id,
            'background_image_id' => $this->background_image_id,
            'organization_id' => $this->organization_id,
            'created_by' => $this->created_by,
            'account_life_id' => $this->account_life_id,
            'account_work_id' => $this->account_work_id,
            'remember_token' => $this->remember_token,
        ];
    }

}
