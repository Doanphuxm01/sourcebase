<?php namespace App\Models\Postgres;
use App\Models\Base;


class AccountAccessTokens extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account_access_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'token',
        'last_used_at',
        'ip_used',
        'location',
        'device_name',
        'user_agent',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\AccountAccessTokensPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\AccountAccessTokensObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'token' => $this->token,
            'last_used_at' => $this->last_used_at,
            'ip_used' => $this->ip_used,
            'location' => $this->location,
            'device_name' => $this->device_name,
            'user_agent' => $this->user_agent,
        ];
    }

}
