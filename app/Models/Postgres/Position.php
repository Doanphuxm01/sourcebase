<?php namespace App\Models\Postgres;
use App\Models\Base;


class Position extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'positions';

    const IS_ACTIVE = 1;

    const POSITION = [
        'Cấp cao',
        'Chủ tịch/Phó chủ tịch',
        'Giám đốc',
        'Quản lý',
        'Nhân viên'
    ];

    const LANGUAGE_POSITION = [
        'cap-cao' => [
            'en' => 'General Assembly'
        ],
        'chu-tichpho-chu-tich' => [
            'en' => 'President/Vice President'
        ],
        'giam-doc' => [
            'en' => 'Directorate'
        ],
        'quan-ly' => [
            'en' => 'Manager'
        ],
        'nhan-vien' => [
            'en' => 'Staff'
        ],

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_active',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\PositionPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\PositionObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
        ];
    }

}
