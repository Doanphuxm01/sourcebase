<?php namespace App\Models\Postgres;
use App\Models\Base;


class StatisticAccount extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statistic_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'application_id',
        'organization_id',
        'statistic_application_id',
        'clicks',
        'date_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\StatisticAccountPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\StatisticAccountObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'account_id', 'id');
    }

    public function application()
    {
        return $this->belongsTo(\App\Models\Postgres\Application::class, 'application_id', 'id')->with('applicationImage');
    }
    public function room()
    {
        return $this->belongsTo(\App\Models\Postgres\Room::class, 'room_id', 'id')->withTrashed();
    }
    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id')->withTrashed();
    }

    public function statisticApplication()
    {
        return $this->belongsTo(\App\Models\Postgres\StatisticApplication::class, 'statistic_application_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'organization_id' => $this->organization_id,
            'application_id' => $this->application_id,
            'statistic_application_id' => $this->statistic_application_id,
            'clicks' => $this->clicks,
        ];
    }

}
