<?php namespace App\Models\Postgres;

use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class LifeNotifications extends Base
{
    // event socket space
    const TYPE_SPACE = 'life_space';
    const INVITE_SPACE = 'invite_to_life_space';
    const DELETE_ACCOUNT_SPACE = 'delete_account_life_space';
    const UPDATE_SPACE = 'update_life_space';
    const DELETE_SPACE = 'delete_life_space';
    const ADMIN_SPACE = 'admin_life_space';
    const ACCEPT_JOIN_SPACE = 'accept_join_life_space';
    const REQUEST_JOIN_SPACE = 'request_join_space';
    const REQUEST_JOIN_SPACE_MERGE = 'request_join_space_merge';
    const REPLY_JOIN_SPACE = 'approved_join_space';


    // event socket room
    const TYPE_ROOM = 'life_room';
    const INVITE_ROOM = 'invite_to_life_room';
    const ADD_ACCOUNT_ROOM = 'add_account_life_room';
    const DELETE_ROOM = 'delete_life_room';
    const UPDATE_ROOM = 'update_life_room';
    const DELETE_ACCOUNT_ROOM = 'delete_account_life_room';
    const ADMIN_ROOM = 'admin_life_room';
    const ACCEPT_JOIN_ROOM = 'accept_join_life_room';

    // event socket application
    const TYPE_APP = 'life_applications';
    const ADD_LINK_APP = 'add_link_life_application';
    const CREATE_APP = 'create_life_application';
    const APP_WAIT_APPOVE = 'wait_approve_life_application';
    const APP_APPROVE = 'approve_life_application';
    const APP_REJECT = 'reject_life_application';
    const DELETE_APP = 'delete_life_application';
    const UPDATE_APP = 'update_life_application';

    // event socket quickbar
    const TYPE_QUICKBAR = 'life_quickbar';
    const QUICKBAR_ENABLE_ALL = 'enable_all_life_quickbar';
    const QUICKBAR_DELETE = 'delete_life_quickbar';
    const QUICKBAR_UPDATE = 'update_life_quickbar';

    // event socket group application
    const TYPE_GROUP_APP = 'life_application_group';
    const CREATE_GROUP_APP = 'create_life_application_group';
    const UPDATE_GROUP_APP = 'update_life_application_group';
    const DELETE_GROUP_APP = 'delete_life_application_group';

    // event socket system
    const TYPE_SYSTEM = 'life_system';
    const APP_INTERACT = 'life_application_interact';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'life_notifications';
    protected $casts = [
        'message' => 'array'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'is_read',
        'account_id',
        'created_by',
        'type',
        'event',
        'space_id',
        'room_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];


    // Relations
    public function account()
    {
        return $this->belongsTo(AccountLife::class, 'account_id', 'id');
    }

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'is_read' => $this->is_read,
            'account_id' => $this->account_id,
            'created_by' => $this->created_by,
            'type' => $this->type,
            'event' => $this->event,
            'space_id' => $this->space_id,
            'room_id' => $this->room_id,
        ];
    }
}
