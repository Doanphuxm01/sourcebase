<?php namespace App\Models\Postgres;

use App\Models\Base;


class SpaceAccountLifeWork extends Base
{

    const IS_ADMIN = 1;
    const IS_MEMBER = 0;
    
    const IS_ACCEPT = 1;
    const NOT_ACCEPT = 0;

    const ACCOUNT_LIFE = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */

    public $timestamps = FALSE;
    static $unguarded = TRUE;
    const table_name = 'space_accounts_work_life';
    protected $table = self::table_name;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'account_life_id',
        'space_id',
        'organization_id',
        'is_admin',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [];

    protected $presenter = \App\Presenters\Postgres\SpaceAccountLifeWorkPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\SpaceAccountLifeWorkObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function accountLife()
    {
        return $this->belongsTo(AccountLife::class, 'account_life_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }

    public function accounts()
    {
        return $this->hasOne(Account::class, 'id', 'account_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }




    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'account_life_id' => $this->account_life_id,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
            'is_admin' => $this->is_admin,
        ];
    }
}
