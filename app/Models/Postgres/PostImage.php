<?php namespace App\Models\Postgres;
use App\Models\Base;


class PostImage extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'image_id',
        'space_id',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\PostImagePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\PostImageObserver);
    }

    // Relations
    public function post()
    {
        return $this->belongsTo(\App\Models\Postgres\Post::class, 'post_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(\App\Models\Postgres\Image::class, 'image_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'post_id' => $this->post_id,
            'image_id' => $this->image_id,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
        ];
    }

}
