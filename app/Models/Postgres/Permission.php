<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    const HAS_PERMISSION = 1;

    const CONTROLLER_KEY = [
        'ApiGroupController' => 'group',
        'ApiManageAccountController' => 'customer',
        'ApiManageAccountController' => 'account',
        'ApiSpaceController' => 'space',
        'ApiStatisticController' => 'statistic'
    ];

    const LIST_CONTROLLER = [
        'ApiGroupController',
        'ApiManageAccountController',
        'ApiManageAccountController',
        'ApiSpaceController',
        'ApiStatisticController'
    ];

    const METHOD_POST = 'POST';
    const METHOD_UPDATE = 'UPDATE';
    const METHOD_GET = 'GET';
    const METHOD_DELETE = 'DELETE';

    const LIST_METHOD = [
      'POST' => 'create',
      'GET' => 'read',
      'UPDATE' => 'update',
      'DELETE' => 'delete'
    ];

    const LANGUAGE_PERMISSION = [
        'group' => [
            'en' => 'Manager Group'
        ],
        'manage-customer' => [
            'en' => 'Manager Customer'
        ],
        'manage-account' => [
            'en' => 'Manager Account'
        ],
        'space' => [
            'en' => 'Manager Space'
        ],
        'statistic' => [
            'en' => 'Manager Statistic'
        ],

    ];

    const IS_ACTIVE = 1;

    const IS_ENABLE = 1;
    const DISABLE = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'key',
        'slug',
        'is_active',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\PermissionPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\PermissionObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'key' => $this->key,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
        ];
    }

}
