<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationGroup extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'application_groups';

    const IS_ACTIVE = 1;

    const NON_GET_APPLICATION_GROUP = 0;
    const GET_APPLICATION_GROUP = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'room_id',
        'space_id',
        'created_by',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\ApplicationGroupPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\ApplicationGroupObserver);
    }

    // Relations
    public function space()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }

    public function applications()
    {
        return $this->hasMany(Application::class,'application_group_id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'room_id' => $this->room_id,
            'space_id' => $this->space_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
