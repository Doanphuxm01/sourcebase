<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Base
{

    use SoftDeletes;
    // event socket space
    const TYPE_SPACE = 'space';
    const ACCOUNT_SPACE = 'add_account_in_space';
    const DEL_ACCOUNT_SPACE = 'delete_account_in_space';
    const DELETE_SPACE = 'delete_space';
    const UPDATE_SPACE = 'update_space';
    const ADMIN_SPACE = 'admin_space';

    // event socket room
    const TYPE_ROOM = 'room';
    const ACCOUNT_ROOM = 'add_account_in_room';
    const DELETE_ROOM = 'delete_room';
    const UPDATE_ROOM = 'update_room';
    const DEL_ACCOUNT_ROOM = 'delete_account_id_room';
    const ADMIN_ROOM = 'admin_room';

    // event socket app
    const TYPE_APP = 'application';
    const APP_WAIT_APPOVE = 'app_wait_appove';
    const CREATE_APP = 'create_app';
    const ADD_LINK_APP = 'app_add_link';
    const APP_REJECT = 'app_reject';
    const APP_APPROVE = 'app_approve';
    const DELETE_APP = 'delete_app';
    const UPDATE_APP = 'update_app';
    const CLICK_APP = 'click_app';

    // event socket quickbar
    const TYPE_QUICKBAR = 'quickbar';
    const QUICKBAR_ENABLE_ALL = 'enable_quickbar';
    const QUICKBAR_DELETE = 'delete_quickbar';
    const QUICKBAR_UPDATE = 'update_quickbar';

    // event socket group app
    const TYPE_GROUP_APP = 'application_group';
    const CREATE_GROUP_APP = 'create_application_group';
    const UPDATE_GROUP_APP = 'update_application_group';
    const DELETE_GROUP_APP = 'delete_application_group';

    // event system
    const TYPE_SYSTEM = 'system';
    const APP_INTERACT = 'app_interact';

    //event Account
    const INVITE_TO_SYSTEM = 'invite_to_system';
    // define const
    const IS_READ = 1;

    const READ_ALL = 1;

    const NOT_READ = 0;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'is_read',
        'account_id',
        'created_by',
        'organization_id',
        'type',
    ];

    protected $casts = [
        'message' => 'array'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\NotificationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\NotificationObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'message' => $this->message,
            'is_read' => $this->is_read,
            'account_id' => $this->account_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
            'type' => $this->type,
        ];
    }

}
