<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'views',
        'space_id',
        'created_by',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\PostPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\PostObserver);
    }

    // Relations
    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }


    public function createdBy()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'created_by', 'id');
    }

    public function postImage()
    {
        return $this->hasManyThrough(Image::class, PostImage::class,'post_id','id','id','image_id');
    }

    public function postComment()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    public function postEmotion()
    {
        return $this->hasMany(PostEmotion::class, 'post_id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'views' => $this->views,
            'space_id' => $this->space_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
