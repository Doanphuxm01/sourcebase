<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Base
{

    use SoftDeletes;

    const TYPE_ROOM_PUBLIC = 1;
    const TYPE_ROOM_PRIVATE = 2;

    const IS_PIN = 1;
    const NOT_PIN = 0;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'is_pin',
        'position',
        'space_id',
        'created_by',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\RoomPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\RoomObserver);
    }

    // Relations
    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    public function account()
    {
        return $this->belongsToMany(Account::class, 'room_accounts', 'room_id', 'account_id')->withPivot('space_id', 'organization_id');
    }

    public function accountLife()
    {
        return $this->belongsToMany(AccountLife::class, 'room_accounts_work_life', 'room_id', 'account_life_id')->withPivot('space_id');
    }

    public function accountLifeInAccountId()
    {
        return $this->belongsToMany(Account::class, 'room_accounts_work_life', 'room_id', 'account_id')->withPivot('space_id', 'organization_id');
    }

    public function createdBy()
    {
        return $this->hasOne(Account::class, 'id', 'created_by');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'type' => $this->type,
            'is_pin' => $this->is_pin,
            'position' => $this->position,
            'space_id' => $this->space_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
