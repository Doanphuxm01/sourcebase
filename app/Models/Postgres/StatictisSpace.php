<?php namespace App\Models\Postgres;
use App\Models\Base;


class StatictisSpace extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statictis_spaces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'organization_id',
        'space_id',
        'visited',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\StatictisSpacePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\StatictisSpaceObserver);
    }

    // Relations
    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'organization_id' => $this->organization_id,
            'space_id' => $this->space_id,
            'visited' => $this->visited,
        ];
    }

}
