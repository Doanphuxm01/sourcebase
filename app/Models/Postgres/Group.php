<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Base
{

    use SoftDeletes;

    const IS_ACTIVE = 1;
    const TYPE_EMPLOYEE = 1;
    const TYPE_CUSTOMER = 2;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'organization_id',
        'created_by',
        'type',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\GroupPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\GroupObserver);
    }

    // Relations
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function account()
    {
        return $this->belongsToMany(Account::class, 'account_groups', 'group_id', 'account_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(Account::class, 'created_by', 'id');
    }

    public function profileImage()
    {
        return $this->hasOne(Image::class, 'id', 'profile_image_id');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'organization_id' => $this->organization_id,
            'created_by' => $this->created_by,
            'type' => $this->created_by,
        ];
    }

}
