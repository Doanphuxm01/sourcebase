<?php

namespace App\Models\Postgres;

use App\Models\Base;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Elasticquent\ElasticquentTrait;
class AccountLife extends Base
{
    use ElasticquentTrait;
    use HasFactory;

    public    $timestamps = FALSE;
    static    $unguarded          = TRUE;
    const     table_name        = 'life_accounts';
    protected $table              = self::table_name;
    static    $basicFiledsForList = [
        'name',
        'email',
        'username',
        'password',
        'phone_number',
        'position',
        'language',
        'is_root',
        'birthday',
        'gender',
        'is_active',
        'api_web_access_token',
        'api_ios_access_token',
        'api_android_access_token',
        'profile_image_id',
        'background_image_id',
        'organization_id',
        'created_by',
        'account_life_id',
        'account_work_id',
        'remember_token',
    ];

    public function profileImage()
    {
        return $this->hasOne(LifeImage::class, 'id', 'profile_image_id');
    }

    public function spaceAccountLifeWork()
    {
        return $this->hasMany(SpaceAccountLifeWork::class, 'account_life_id', 'id');
    }

    public function roomAccountWorkLife()
    {
        return $this->hasMany(RoomAccountsWorkLife::class, 'account_id', 'id');
    }
}
