<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;


class Decentralization extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'decentralizations';

    const IS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'organization_id',
        'created_by',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['laravel_through_key'];

    protected $dates  = [];

    protected $presenter = \App\Presenters\DecentralizationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\DecentralizationObserver);
    }

    // Relations
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function createdBy()
    {
        return $this->hasOne(Account::class, 'id', 'created_by');
    }

    public function decentralizationPermissions(){
        return $this->hasManyThrough(Permission::class, DecentralizationPermission::class,'decentralization_id','id','id','permission_id');
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'organization_id' => $this->organization_id,
            'created_by' => $this->created_by,
        ];
    }


}
