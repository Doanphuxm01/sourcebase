<?php namespace App\Models\Postgres;
use App\Models\Base;


class ApplicationDefault extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'application_defaults';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'status',
        'link_android',
        'link_ios',
        'link_url',
        'source',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\ApplicationDefaultPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\ApplicationDefaultObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'status' => $this->status,
            'link_android' => $this->link_android,
            'link_ios' => $this->link_ios,
            'link_url' => $this->link_url,
            'source' => $this->source,
        ];
    }

}
