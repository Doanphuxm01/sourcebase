<?php namespace App\Models\Postgres;
use App\Models\Base;


class StatisticApplication extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statistic_applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_id',
        'space_id',
        'application_advanced_setting_id',
        'organization_id',
        'application_id',
        'date_at',
        'clicks',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    const IS_WARNING = 0;
    protected $hidden = [];

    protected $dates = [];

    protected $presenter = \App\Presenters\Postgres\StatisticApplicationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\StatisticApplicationObserver);
    }

    // Relations
    public function room()
    {
        return $this->belongsTo(\App\Models\Postgres\Room::class, 'room_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function application()
    {
        return $this->belongsTo(\App\Models\Postgres\Application::class, 'application_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    public function applicationAdvancedSetting()
    {
        return $this->belongsTo(\App\Models\Postgres\ApplicationAdvancedSetting::class, 'application_advanced_setting_id', 'id');
    }

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'room_id' => $this->room_id,
            'space_id' => $this->space_id,
            'application_advanced_setting_id' => $this->application_advanced_setting_id,
            'organization_id' => $this->organization_id,
            'application_id' => $this->application_id,
            'date_at' => $this->date_at,
            'clicks' => $this->clicks,
        ];
    }

}
