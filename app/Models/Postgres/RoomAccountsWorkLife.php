<?php namespace App\Models\Postgres;
use App\Models\Base;


class RoomAccountsWorkLife extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'room_accounts_work_life';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_life_id',
        'account_id',
        'room_id',
        'space_id',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\RoomAccountsWorkLifePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\RoomAccountsWorkLifeObserver);
    }

    // Relations
    public function accountLife()
    {
        return $this->belongsTo(AccountLife::class, 'account_life_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_life_id' => $this->account_life_id,
            'account_id' => $this->account_id,
            'room_id' => $this->room_id,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
        ];
    }

}
