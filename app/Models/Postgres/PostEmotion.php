<?php namespace App\Models\Postgres;
use App\Models\Base;


class PostEmotion extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_emotions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'created_by',
        'post_id',
        'space_id',
        'emotion_id',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\PostEmotionPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\PostEmotionObserver);
    }

    // Relations
    public function post()
    {
        return $this->belongsTo(\App\Models\Postgres\Post::class, 'post_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function emotion()
    {
        return $this->belongsTo(\App\Models\Postgres\Emotion::class, 'emotion_id', 'id');
    }


    public function createdBy()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'created_by', 'id');
    }


    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'created_by' => $this->created_by,
            'post_id' => $this->post_id,
            'space_id' => $this->space_id,
            'emotion_id' => $this->emotion_id,
            'organization_id' => $this->organization_id,
        ];
    }

}
