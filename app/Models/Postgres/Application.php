<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const GROUP_TYPE_HAS_GROUP = 0;
    const GROUP_TYPE_NEW_GROUP = 1;

    const AVATAR_APPLICATION = 'application';

    const TYPE_APP_NORMAL = 0;
    const TYPE_APP_BAR = 1;

    const IS_SYSTEM = 1;

    const NOT_ACCEPT = 0;
    const ACCEPT = 1;
    const APPROVE = 1;

    const TYPE_VIEW_SINGLE = 'single';
    const TYPE_VIEW_GROUP = 'group';

    const CLICK_NOT_SETTING = 0;
    const CLICK_ENOUGH = 1;
    const CLICK_NOT_ENOUGH = 2;

    const TYPE_FILE = 0;
    const TYPE_LINK = 1;
    const FILE_MAX = 20480;

    const DB_APPLICATION = 0;
    const DB_APPLICATION_STORE = 1;

    protected $fillable = [
        'name',
        'slug',
        'type',
        'is_accept',
        'link_android',
        'link_ios',
        'link_url',
        'date_start',
        'date_end',
        'application_image_id',
        'created_by',
        'account_id',
        'room_id',
        'space_id',
        'organization_id',
        'application_group_id',
        'x_frame',
        'domain',
        'is_system',
        'applications_type_id',
        'application_store_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\ApplicationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\ApplicationObserver);
    }

    // Relations
    public function applicationImage()
    {
        return $this->hasOne(Image::class, 'id', 'application_image_id');
    }

    public function applicationType()
    {
        return $this->hasOne(ApplicationsType::class, 'id', 'applications_type_id');
    }

    public function account()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'account_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'created_by', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function room()
    {
        return $this->belongsTo(\App\Models\Postgres\Room::class, 'room_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    public function applicationGroup()
    {
        return $this->belongsTo(\App\Models\Postgres\ApplicationGroup::class, 'application_group_id', 'id');
    }

    public function advancedSetting()
    {
        return $this->hasMany(\App\Models\Postgres\ApplicationAdvancedSetting::class, 'application_id', 'id');
    }

    public function quickBar()
    {
        return $this->hasMany(QuickBar::class, 'application_id', 'id');
    }

    public function statisticClicks(){
        return $this->hasOne(StatisticApplication::class)->selectRaw('statistic_applications.application_id,SUM(statistic_applications.clicks) as total_clicks')->groupBy('statistic_applications.application_id');
    }

    public function statisticAccounts()
    {
        return $this->hasOne(StatisticAccount::class)->selectRaw('statistic_accounts.application_id,count(DISTINCT statistic_accounts.account_id) as total_accounts')->groupBy('statistic_accounts.application_id');
    }

    public function applicationStore()
    {
        return $this->hasOne(ApplicationStore::class, 'id', 'application_store_id')->with('applicationImage');
    }

    public function applicationStoreFile()
    {
        return $this->hasOne(ApplicationStore::class, 'id', 'application_store_id')->with('applicationFile');
    }

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'type' => $this->type,
            'link_android' => $this->link_android,
            'link_ios' => $this->link_ios,
            'link_url' => $this->link_url,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'application_image_id' => $this->application_image_id,
            'created_by' => $this->created_by,
            'account_id' => $this->account_id,
            'room_id' => $this->room_id,
            'is_accept' => $this->is_accept,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
            'application_group_id' => $this->application_group_id,
            'x_frame' => $this->x_frame,
            'is_system' => $this->is_system,
            'applications_type_id' => $this->applications_type_id,
            'application_store_id' => $this->application_store_id,
        ];
    }

}
