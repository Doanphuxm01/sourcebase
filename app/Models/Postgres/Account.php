<?php namespace App\Models\Postgres;

use App\Models\AuthenticationBase;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends AuthenticationBase
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    const HAS_PERMISSION = 1;

    const TYPE_SIGN_IN_WEB = 1;
    const TYPE_SIGN_IN_WEB_APP = 2;
    const TYPE_SIGN_IN_IOS = 3;
    const TYPE_SIGN_IN_ANDROID = 4;

    const FIRST_LOGIN = 1;

    const LOGIN_NORM = 0;

    const KEY_SIGN_IN = [
        1 => 'api_web_access_token',
        2 => 'api_web_app_access_token',
        3 => 'api_ios_access_token',
        4 => 'api_android_access_token',
    ];

    const STRING_REDIS = '_account_blocked';

    const QUEUE_NAME_SEND_EMAIL = 'mandu_send_email';

    const IS_ROOT = 1;

    const IS_NOT_ROOT = 0;

    const IS_ACTIVE = 1;
    const IS_DISABLE = 0;

    const IS_ADMIN = 1;
    const IS_NOT_ADMIN = 0;

    const IS_ADMIN_CUSTOMER = 1;
    const IS_NOT_ADMIN_CUSTOMER = 0;

    const TYPE_ACCOUNT_CLIENT = 1;
    const TYPE_ACCOUNT_STAFF = 0;

    const METHOD_GET = 'GET';
    const METHOD_DELETE = 'DELETE';

    const SIGN_OUT = 'signOut';

    const NEW_ACCOUNT = 1;
    const RESET_PASSWORD = 2;
    const NEW_ACCOUNT_GOOGLE = 3;
    const ACTIVE_ACCOUNT = 4;
    const CHANGE_ACCOUNT = 5;
    const APP_INTERACT = 6;
    const NEW_ORGANIZATION = 7;
    const VERIFICATION_ACCOUNT = 8;
    const ACCOUNT_APPROVAL = 9;
    const ACCOUNT_DENNY = 10;

    const IS_LEADER = 1;

    const ALL_ACCOUNT = -1;

    const AVATAR = 'avatar';
    const BACKGROUND = 'background';

    const VALUE_EXPIRED = 1;

    const GET_ACCOUNT_INFO = 1;
    const NON_GET_ACCOUNT_INFO = 0;

    const KEY_RESET_PASSWORD = '|';

    const RESET_YES_PASSWORD_KEY_API = 1;
    const RESET_NO_PASSWORD_KEY_API = 0;

    const IS_ACCOUNT_DONES_PRO = 'is_account_dones_pro';
    const PATTERN = [
        self::NEW_ACCOUNT => 'RegisterNormal',
        self::RESET_PASSWORD => 'ForgotPassword',
        self::NEW_ACCOUNT_GOOGLE => 'RegisterByGoogleMandu',
        self::ACTIVE_ACCOUNT => 'RegisterByRoot',
        self::CHANGE_ACCOUNT => 'ChangePasswordDonesPro',
        self::APP_INTERACT => 'AppInteract',
        self::NEW_ORGANIZATION => 'RootCreateOrganization',
        self::VERIFICATION_ACCOUNT => 'VerificationAccountMandu',
        self::ACCOUNT_APPROVAL => 'AccountApprovalMandu',
        self::ACCOUNT_DENNY => 'AccountDennyMandu',
    ];

    const LANGUAGE_VI = 'vi';
    const LANGUAGE = [
        'vi' => [
            'key' => 'vi',
            'value' => 'Việt Nam'
        ],
        'en' => [
            'key' => 'en',
            'value' => 'English'
        ]
    ];

    const WEEKDAYS = [
        'vi' => [
            1 => 'Thứ Hai',
            2 => 'Thứ Ba',
            3 => 'Thứ Tư',
            4 => 'Thứ Năm',
            5 => 'Thứ Sáu',
            6 => 'Thứ Bảy',
            7 => 'Chủ Nhật',
        ],
        'en' => [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ]
    ];

    const DONES_PRO = 1;
    const DONES = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'phone_number',
        'position_id',
        'language',
        'is_root',
        'birthday',
        'gender',
        'google_id',
        'remember_token',
        'is_active',
        'api_web_access_token',
        'api_web_app_access_token',
        'api_ios_access_token',
        'api_android_access_token',
        'profile_image_id',
        'background_image_id',
        'organization_id',
        'decentralization_id',
        'created_by',
        'is_leader',
        'space_id',
        'main_space',
        'is_first_login',
        'is_check_active',
        'link_address',
        'type_account',
        'is_admin'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'facebook_token'];

    protected $dates  = ['deleted_at'];

    protected $presenter = \App\Presenters\AccountPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\AccountObserver);
    }

    // Relations
    public function profileImage()
    {
        return $this->hasOne(Image::class, 'id', 'profile_image_id');
    }

    public function backgroundImage()
    {
        return $this->hasOne(Image::class, 'id', 'background_image_id');
    }

    public function position()
    {
        return $this->hasOne(Position::class, 'id', 'position_id');
    }

    public function createdBy()
    {
        return $this->hasOne(Account::class, 'id', 'created_by');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function decentralization()
    {
        return $this->hasOne(Decentralization::class, 'id', 'decentralization_id');
    }

    public function managers()
    {
        return $this->hasManyThrough(Account::class, ManagementAccount::class, 'account_id', 'id', 'id', 'manager_id');
    }

    public function spaceAccount()
    {
        return $this->hasMany(SpaceAccount::class, 'account_id', 'id');
    }

    public function mainSpace()
    {
        return $this->hasOne(Space::class, 'id', 'main_space');
    }

    public function roomAccount()
    {
        return $this->hasMany(RoomAccount::class, 'account_id', 'id');
    }

    public function rooms()
    {
        return $this->belongsToMany(Room::class, 'room_accounts', 'account_id', 'room_id');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            "is_active" => $this->is_active,
            "is_root" => $this->is_root,
            "username" => $this->username,
            "organization_id" => $this->organization_id,
            "phone_number" => $this->phone_number,
            'language' => $this->language,
            'token' => $this->api_web_access_token,
            'is_leader' => $this->is_leader,
            'space_id' => $this->space_id,
            'is_first_login' => $this->is_first_login,
            'avatar' => !empty($this->present()->profileImage()->source) ? asset($this->present()->profileImage()->source) : '',
            'cover' => !empty($this->present()->backgroundImage()->source) ? asset($this->present()->backgroundImage()->source) : '',
        ];
    }

    public static function keyRedisOtpForEmail($mail)
    {
        return 'otp_mandu_' . $mail;
    }

    public static function keyRedisOtpForEmailForResetPassWord($mail)
    {
        return 'otp_mandu_reset_password_' . $mail;
    }
}
