<?php namespace App\Models\Postgres;
use App\Models\Base;


class ManagementAccount extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'management_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'manager_id',
        'account_id',
        'created_by',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\ManagementAccountPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\ManagementAccountObserver);
    }

    // Relations
    public function manager()
    {
        return $this->belongsTo(Account::class, 'manager_id', 'id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(Account::class, 'created_by', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'manager_id' => $this->manager_id,
            'account_id' => $this->account_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
