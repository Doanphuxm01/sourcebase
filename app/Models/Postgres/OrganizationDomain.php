<?php

namespace App\Models\Postgres;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrganizationDomain extends Model
{
    use HasFactory;
    protected $table = 'organization_domains';

    protected $fillable= [
        'organization_id',
        'domain',
    ];
}
