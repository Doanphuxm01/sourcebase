<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class LifeImage extends Base
{

    use SoftDeletes;
    use Notifiable;

    const MAX_SIZE_IMAGE = 5000000;
    const MAX_SIZE_VIDEO = 50000000;

    const IMAGE = 1;
    const VIDEO = 2;

    const USING = 1;
    const NOT_USING = 0;

    const COLLECTION_TYPE_DEFAULT = 1;
    const COLLECTION_TYPE_SPACE = 2;
    const COLLECTION_TYPE_APP = 3;
    const COLLECTION_TYPE_TIMELINE = 4;
    const COLLECTION_TYPE_AVATAR = 5;
    const COLLECTION_TYPE_BACKGROUND = 6;
    const COLLECTION_TYPE_POST = 7;
    const COLLECTION_TYPE_LIBRARY = 8;

    const AVATAR = 0;
    const COLLECTION = 1;

    const IS_ACTIVE = 1;

    const ROLE = [
        1 => [
            'key' => 1,
            'value' => 'Admin'
        ]
    ];

    const ARR_IS_ACTIVE = [0, 1];

    const CREATE = 'create';
    const UPDATE = 'update';

    const JPG = 'image/jpg';
    const PNG = 'image/png';
    const JPEG = 'image/jpeg';
    const GIF = 'image/gif';
    const MP4 = 'video/mp4';

    const FILEPATH = 'files/organization/organization_';

    const SELF = 1;//Đây là cho các dữ liệu tự tạo
    const MIXED = 2;//Đây là dữ liệu do bên khác tạo và chạy

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'life_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'md5_file',
        'source_thumb',
        'source',
        'organization_id',
        'collection_type',
        'file_size',
        'width',
        'type',
        'height',
        'mimes',
        'duration',
        'dimension',
        'created_by',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

//    protected $presenter = \App\Presenters\ImagePresenter::class;
//
//    public static function boot()
//    {
//        parent::boot();
//        parent::observe(new \App\Observers\ImageObserver);
//    }

    // Relations
//    public function organization()
//    {
//        return $this->belongsTo(Organization::class, 'organization_id', 'id');
//    }
//
//    public function applications()
//    {
//        return $this->hasMany(Application::class, 'application_image_id', 'id');
//    }
//
//    public function spaces()
//    {
//        return $this->hasMany(Space::class, 'space_image_id', 'id');
//    }

    public function avatar()
    {
        return $this->hasMany(Account::class, 'profile_image_id', 'id');
    }

//    public function background()
//    {
//        return $this->hasMany(Account::class, 'background_image_id', 'id');
//    }

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'md5_file' => $this->md5_file,
            'source_thumb' => $this->source_thumb,
            'source' => $this->source,
            'organization_id' => $this->organization_id,
            'file_size' => $this->file_size,
            'width' => $this->width,
            'type' => $this->type,
            'height' => $this->height,
            'mimes' => $this->mimes,
            'duration' => $this->duration,
            'dimension' => $this->dimension,
            'created_by' => $this->created_by,
        ];
    }

}
