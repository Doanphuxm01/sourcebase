<?php namespace App\Models\Postgres;
use App\Models\Base;


class ApplicationAdvancedSetting extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'application_advanced_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id',
        'time_start',
        'time_end',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'clicks',
        'created_by',
        'organization_id',
    ];

    const ARRAY_WEEKDAYS = [
        0 => 'sunday',
        1 => 'monday',
        2 => 'tuesday',
        3 => 'wednesday',
        4 => 'thursday',
        5 => 'friday',
        6 => 'saturday',
    ];

    const WEEKDAYS = [1, 2, 3, 4, 5, 6, 0];
    const MONDAY = 1;
    const TUESDAY = 2;
    const WEDNESDAY = 3;
    const THURSDAY = 4;
    const FRIDAY = 5;
    const SATURDAY = 6;
    const SUNDAY = 0;

    const VALUE_WEEKDAYS = 1;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\ApplicationAdvancedSettingPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\ApplicationAdvancedSettingObserver);
    }

    // Relations
    public function application()
    {
        return $this->belongsTo(\App\Models\Postgres\Application::class, 'application_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'application_id' => $this->application_id,
            'time_start' => $this->time_start,
            'time_end' => $this->time_end,
            'monday' => $this->monday,
            'tuesday' => $this->tuesday,
            'wednesday' => $this->wednesday,
            'thursday' => $this->thursday,
            'friday' => $this->friday,
            'saturday' => $this->saturday,
            'sunday' => $this->sunday,
            'clicks' => $this->clicks,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
