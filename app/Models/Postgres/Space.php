<?php namespace App\Models\Postgres;

use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Space extends Base
{

    // use SoftDeletes;

    const IS_ACTIVE = 1;
    const IS_ADMIN = 1;
    const TYPE_SPACE_INTERNAL = 1;
    const TYPE_SPACE_CUSTOMER = 2;
    const PACKAGE_ID = 1;
    const UPLOAD_COLLECTION = 0;
    const SELECTED_COLLECTION = 1;
    const IS_BLOCKED = 1;
    const NOT_BLOCKED = 0;

    const DEFAULT_SPACE = 1;
    const NOT_DONES_LIFE = 0;

    const IS_ADMIN_TEXT = 'is_admin';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spaces';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'type',
        'is_default',
        'major_id',
        'space_image_id',
        'organization_id',
        'created_by',
        'is_show',
        'is_public',
        'uuid',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [];

    protected $presenter = \App\Presenters\SpacePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\SpaceObserver);
    }

    // Relations
    public function major()
    {
        return $this->belongsTo(\App\Models\Postgres\Major::class, 'major_id', 'id');
    }

    public function spaceImage()
    {
        return $this->hasOne(\App\Models\Postgres\Image::class, 'id', 'space_image_id');
    }

    public function createdBy()
    {
        return $this->hasOne(\App\Models\Postgres\Account::class, 'id', 'created_by');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    public function account()
    {
        return $this->belongsToMany(Account::class, 'space_accounts', 'space_id', 'account_id');
    }

    public function accountLife()
    {
        return $this->belongsToMany(AccountLife::class, SpaceAccountLifeWork::table_name, 'space_id', 'account_id');
    }

    public function accountLifeList()
    {
        return $this->belongsToMany(AccountLife::class, SpaceAccountLifeWork::table_name, 'space_id', 'account_life_id');
    }

    public function group()
    {
        return $this->belongsToMany(Group::class, 'space_groups', 'space_id', 'group_id');
    }


    public function room()
    {
        return $this->hasMany(Room::class, 'space_id', 'id');
    }

    public function spaceAccountWorklife()
    {
        return $this->hasMany(SpaceAccountLifeWork::class, 'space_id', 'id');
    }

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'type' => $this->type,
            'is_default' => $this->is_default,
            'major_id' => $this->major_id,
            'space_image_id' => $this->space_image_id,
            'organization_id' => $this->organization_id,
            'created_by' => $this->created_by,
            'is_show' => $this->is_show,
            'is_public' => $this->is_public,
            'uuid' => $this->uuid,
        ];
    }

}
