<?php namespace App\Models\Postgres;
use App\Models\Base;


class SpaceGroup extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'space_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'space_id',
        'group_id',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\SpaceGroupPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\SpaceGroupObserver);
    }

    // Relations
    public function space()
    {
        return $this->belongsTo(Space::class, 'space_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'space_id' => $this->space_id,
            'group_id' => $this->group_id,
            'organization_id' => $this->organization_id,
        ];
    }

}
