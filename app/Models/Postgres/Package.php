<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'packages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const IS_TRIAL = 1;
    const IS_ACTIVE = 1;

    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'is_trial',
        'point',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\PackagePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\PackageObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_trial' => $this->is_trial,
            'is_active' => $this->is_active,
            'point' => $this->point,
        ];
    }

}
