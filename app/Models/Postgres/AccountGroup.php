<?php namespace App\Models\Postgres;
use App\Models\Base;


class AccountGroup extends Base
{

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'group_id',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\AccountGroupPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\AccountGroupObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'account_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(\App\Models\Postgres\Group::class, 'group_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }

    

    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'group_id' => $this->group_id,
            'organization_id' => $this->organization_id,
        ];
    }

}
