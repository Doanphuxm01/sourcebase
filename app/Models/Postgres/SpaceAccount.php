<?php namespace App\Models\Postgres;

use App\Models\Base;


class SpaceAccount extends Base
{

    const IS_ADMIN = 1;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'space_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'space_id',
        'organization_id',
        'is_admin',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\SpaceAccountPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\SpaceAccountObserver);
    }

    // Relations
    public function account()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'account_id', 'id');
    }

    public function accounts()
    {
        return $this->hasOne(Account::class, 'id', 'account_id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
            'is_admin' => $this->is_admin,
        ];
    }
}
