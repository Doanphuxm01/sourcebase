<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Major extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'majors';

    const IS_ACTIVE = 1;

    const LANGUAGE_MAJOR = [
        'giao-duc' => [
            'en' => 'Education'
        ],
        'xe-co' => [
            'en' => 'Vehicle'
        ],
        'am-thuc' => [
            'en' => 'Food'
        ],
        'am-nhac' => [
            'en' => 'Music'
        ],
        'the-thao' => [
            'en' => 'Sport'
        ],
        'du-lich' => [
            'en' => 'Travel'
        ],
        'giai-tri' => [
            'en' => 'Entertainment'
        ],
        'nghe-thuat' => [
            'en' => 'Art'
        ],
        'tham-my' => [
            'en' => 'Beauty'
        ],
        'do-hoa' => [
            'en' => 'Graphics'
        ],
        'thoi-trang' => [
            'en' => 'Fashion'
        ],
        'bat-dong-san' => [
            'en' => 'Real estate'
        ],
        'kien-truc-xay-dung' => [
            'en' => 'Build'
        ],
        'cong-nghe' => [
            'en' => 'Technology'
        ],
        'luat' => [
            'en' => 'Law'
        ],
        'bao-chi' => [
            'en' => 'Newspapers'
        ],
        'khoa-hoc' => [
            'en' => 'Science'
        ],
        'xa-hoi' => [
            'en' => 'Society'
        ],
        'truyen-thong' => [
            'en' => 'Media'
        ],
        'suc-khoe' => [
            'en' => 'Health'
        ],
        'tai-chinh' => [
            'en' => 'Finance'
        ],
        'kinh-te' => [
            'en' => 'Economy'
        ],
        'y-te' => [
            'en' => 'Medical'
        ],
        'mua-sam' => [
            'en' => 'Shopping'
        ]
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'description',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\MajorPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\MajorObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'description' => $this->description,
        ];
    }

}
