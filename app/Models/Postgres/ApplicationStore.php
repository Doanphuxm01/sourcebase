<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationStore extends Base
{

    // use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'application_stores';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'is_default',
        'type',
        'link_android',
        'link_ios',
        'link_url',
        'application_image_id',
        'created_by',
        'x_frame',
        'is_accept',
        'is_system',
        'organization_id',
        'domain',
        'created_at',
        'updated_at',
        'file_mime',
        'file_size',
        'is_file',
        'file_path',
        'application_file_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\ApplicationStorePresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\ApplicationStoreObserver);
    }

    // Relations
    public function applicationImage()
    {
        return $this->hasOne(Image::class, 'id', 'application_image_id');
    }

    public function applicationFile()
    {
        return $this->hasOne(Image::class, 'id', 'application_file_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(Account::class, 'created_by', 'id');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_default' => $this->is_default,
            'type' => $this->type,
            'link_android' => $this->link_android,
            'link_ios' => $this->link_ios,
            'link_url' => $this->link_url,
            'application_image_id' => $this->application_image_id,
            'created_by' => $this->created_by,
            'x_frame' => $this->x_frame,
            'is_accept' => $this->is_accept,
            'is_system' => $this->is_system,
            'domain' => $this->domain,
        ];
    }

}
