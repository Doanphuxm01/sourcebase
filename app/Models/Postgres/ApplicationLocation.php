<?php namespace App\Models\Postgres;
use App\Models\Base;


class ApplicationLocation extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'application_locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const TYPE_APPLICATION = 1;
    const TYPE_APPLICATION_GROUP = 2;

    const TYPE_ADD_NEW = 1;
    const TYPE_UPDATE = 2;

    protected $fillable = [
        'organization_id',
        'account_id',
        'application_id',
        'application_group_id',
        'type',// 1 => Là ứng dụng, 2 là nhóm ứng dụng
        'space_id',
        'room_id',
        'position',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\Postgres\ApplicationLocationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\Postgres\ApplicationLocationObserver);
    }

    // Relations


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'organization_id' => $this->organization_id,
            'account_id' => $this->account_id,
            'application_id' => $this->application_id,
            'application_group_id' => $this->application_group_id,
            'type' => $this->type,
            'space_id' => $this->space_id,
            'room_id' => $this->room_id,
            'position' => $this->position,
        ];
    }

}
