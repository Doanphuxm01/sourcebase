<?php namespace App\Models\Postgres;
use App\Models\Base;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Base
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organizations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const IS_ACTIVE = 1;

    const IS_PRO = 0;

    const REDIS_EXP_NAME = 'organization_expiration_';

    const SCALE = [
        1 => '0 - 49',
        2 => '50 - 99',
        3 => '100 - 499',
        4 => '500 - 4999',
        5 => '5000+',
    ];

    protected $fillable = [
        'name',
        'slug',
        'is_active',
        'major_id',
        'package_id',
        'scale',
        'description',
        'exp_date',
        'due_date',
        'is_trial',
    ];

    const SLUG = 'mandu';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\OrganizationPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\OrganizationObserver);
    }

    // Relations
    public function major()
    {
        return $this->belongsTo(Major::class, 'major_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
            'major_id' => $this->major_id,
            'package_id' => $this->package_id,
            'scale' => $this->scale,
            'description' => $this->description,
            'exp_date' => $this->exp_date,
            'is_trial' => $this->is_trial,
            'due_date' => $this->due_date,
        ];
    }

}
