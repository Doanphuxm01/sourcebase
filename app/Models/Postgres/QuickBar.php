<?php namespace App\Models\Postgres;

use App\Models\Base;


class QuickBar extends Base
{

    const APP_NEW = 1;
    const CHOOSE_SPACE = 2;
    const CHOOSE_ROOM = 3;
    const ENABLE_ALL = 1;
    const ENABLE_SELECT = 2;
    const ENABLE_ONE = 0;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'quick_bars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'application_id',
        'position',
        'space_id',
        'organization_id',
        'created_by',
        'enable_all',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [];

    protected $presenter = \App\Presenters\Postgres\QuickBarPresenter::class;

//    public static function boot()
//    {
//        parent::boot();
//        parent::observe(new \App\Observers\Postgres\QuickBarObserver);
//    }

    // Relations
    public function account()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'account_id', 'id');
    }

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\Postgres\Account::class, 'created_by', 'id');
    }

    public function application()
    {
        return $this->belongsTo(ApplicationStore::class, 'application_id', 'id');
    }

    public function space()
    {
        return $this->belongsTo(\App\Models\Postgres\Space::class, 'space_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(\App\Models\Postgres\Organization::class, 'organization_id', 'id');
    }



    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'account_id' => $this->account_id,
            'application_id' => $this->application_id,
            'position' => $this->position,
            'space_id' => $this->space_id,
            'organization_id' => $this->organization_id,
            'enable_all' => $this->enable_all,
            'created_by' => $this->created_by,
        ];
    }
}
