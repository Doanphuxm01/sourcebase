<?php namespace App\Models\Postgres;
use App\Models\Base;


class DecentralizationPermission extends Base
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'decentralization_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'create',
        'read',
        'update',
        'delete',
        'permission_id',
        'decentralization_id',
        'created_by',
        'organization_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates  = [];

    protected $presenter = \App\Presenters\DecentralizationPermissionPresenter::class;

    public static function boot()
    {
        parent::boot();
        parent::observe(new \App\Observers\DecentralizationPermissionObserver);
    }

    // Relations
    public function permission()
    {
        return $this->belongsTo(Permission::class, 'permission_id', 'id');
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'organization_id', 'id');
    }

    public function createdBy()
    {
        return $this->hasOne(Account::class, 'id', 'created_by');
    }


    // Utility Functions

    /*
     * API Presentation
     */
    public function toAPIArray()
    {
        return [
            'id' => $this->id,
            'create' => $this->create,
            'read' => $this->read,
            'update' => $this->update,
            'delete' => $this->delete,
            'permission_id' => $this->permission_id,
            'decentralization_id' => $this->decentralization_id,
            'created_by' => $this->created_by,
            'organization_id' => $this->organization_id,
        ];
    }

}
