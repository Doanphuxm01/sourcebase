<?php

namespace App\Models;

use App\Models\Postgres\Image;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticationContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Request;

class AuthenticationBase extends LocaleStorableBase implements AuthenticationContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    public function setPasswordAttribute($password)
    {
        if (empty($password)) {
            $this->attributes['password'] = '';
        } else {
            $this->attributes['password'] = \Hash::make($password);
        }
    }

    public function setAPIAccessToken()
    {
        // $user = null;
        // do {
        //     $code = md5(\Hash::make($this->id.$this->email.$this->password.time().mt_rand()));
        //     $user = static::whereApiWebAccessToken($code)->first();
        // } while (isset($user));

        $code = md5(\Hash::make($this->id.$this->email.$this->password.time().mt_rand()));
            
        switch ($this->platform) {
            case 'api_web_access_token':
                $this->api_web_access_token = $code;
                break;
            
            case 'api_web_app_access_token':
                $this->api_web_app_access_token = $code;
                break;
            
            case 'api_ios_access_token':
                $this->api_ios_access_token = $code;
                break;
            
            case 'api_android_access_token':
                $this->api_android_access_token = $code;
                break;
            
            default:
                return false;
                break;
        }
        
        unset($this->platform);

        return $code;
    }

    // Relation

    public function profileImage()
    {
        return $this->belongsTo(Image::class, 'profile_image_id', 'id');
    }


    public function getProfileImageUrl($width = 0, $height = 0)
    {
        if ($this->profile_image_id == 0) {
            return \URLHelper::asset('img/user.png', 'common');
        }
        if ($width == 0 && $height == 0) {
            return $this->profileImage->url;
        } else {
            return $this->profileImage->url;
        }
    }
}
