<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryBindServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\BaseRepositoryInterface::class,
            \App\Repositories\Eloquent\BaseRepository::class
        );

        $this->app->singleton(
            \App\Repositories\LogRepositoryInterface::class,
            \App\Repositories\Eloquent\LogRepository::class
        );

        $this->app->singleton(
            \App\Repositories\AuthenticationRepositoryInterface::class,
            \App\Repositories\Eloquent\AuthenticationRepository::class
        );


        $this->app->singleton(
            \App\Repositories\SingleKeyModelRepositoryInterface::class,
            \App\Repositories\Eloquent\SingleKeyModelRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\AccountRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\AccountRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ImageRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ImageRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\OrganizationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\OrganizationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PermissionRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PermissionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\MajorRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\MajorRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PackageRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PackageRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ImageRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ImageRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\GroupRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\GroupRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\DecentralizationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\DecentralizationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\DecentralizationPermissionRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\DecentralizationPermissionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PositionRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PositionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ManagementAccountRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ManagementAccountRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\SpaceRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\SpaceRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationGroupRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationGroupRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationDefaultRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationDefaultRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationAdvancedSettingRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationAdvancedSettingRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\RoomRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\RoomRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\QuickBarRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\QuickBarRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PostRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PostRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\CommentRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\CommentRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\EmotionRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\EmotionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationLocationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationLocationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationGroupLocationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationGroupLocationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PostImageRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PostImageRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\PostEmotionRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\PostEmotionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\StatisticApplicationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\StatisticApplicationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\StatisticAccountRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\StatisticAccountRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\RoomAccountRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\RoomAccountRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\NotificationRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\NotificationRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\AccountLifeWorkRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\AccountLifeWorkRepositoryRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\ApplicationStoreRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\ApplicationStoreRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Postgres\AccountAccessTokensRepositoryInterface::class,
            \App\Repositories\Postgres\Eloquent\AccountAccessTokensRepository::class
        );

        /* NEW BINDING */
    }
}
