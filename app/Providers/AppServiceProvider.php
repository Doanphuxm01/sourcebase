<?php

namespace App\Providers;

use App\Models\Postgres\QuickBar;
use App\Observers\Postgres\QuickBarObserver;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Postgres\AccountAccessTokens;
use App\Observers\Postgres\AccountAccessTokensObserver;
use App\Models\Postgres\Image;
use App\Observers\ImageObserver;
use App\Models\Postgres\Space;
use App\Observers\SpaceObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Model Observer
        AccountAccessTokens::observe(AccountAccessTokensObserver::class);
        Space::observe(SpaceObserver::class);
        Image::observe(ImageObserver::class);
//        QuickBar::observe(QuickBarObserver::class);

        Validator::extend('valid_array', function($attribute, $value, $parameters){
            $value = array_filter($value,function ($value){
                if(is_numeric($value)){
                    return $value;
                }
            });
            if(is_array($value) && count($value) > 0){
                return true;
            }
            return false;
        });

        Collection::macro('paginate', function($perPage, $total = null, $page = null, $pageName = 'page') {
            $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
            return new LengthAwarePaginator(
                $this->forPage($page, $perPage),
                $total ?: $this->count(),
                $perPage,
                $page,
                [
                    'path' => LengthAwarePaginator::resolveCurrentPath(),
                    'pageName' => $pageName,
                ]
            );
        });
    }
}
