<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ServiceBindServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(
            \App\Services\AuthenticationServiceInterface::class,
            \App\Services\Production\AuthenticationService::class
        );

        $this->app->singleton(
            \App\Services\BaseServiceInterface::class,
            \App\Services\Production\BaseService::class
        );


        $this->app->singleton(
            \App\Services\Postgres\AccountServiceInterface::class,
            \App\Services\Postgres\Production\AccountService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\FileUploadServiceInterface::class,
            \App\Services\Postgres\Production\FileUploadService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PackageServiceInterface::class,
            \App\Services\Postgres\Production\PackageService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\OrganizationServiceInterface::class,
            \App\Services\Postgres\Production\OrganizationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ImageServiceInterface::class,
            \App\Services\Postgres\Production\ImageService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\GroupServiceInterface::class,
            \App\Services\Postgres\Production\GroupService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\DecentralizationServiceInterface::class,
            \App\Services\Postgres\Production\DecentralizationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\DecentralizationPermissionServiceInterface::class,
            \App\Services\Postgres\Production\DecentralizationPermissionService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PositionServiceInterface::class,
            \App\Services\Postgres\Production\PositionService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PermissionServiceInterface::class,
            \App\Services\Postgres\Production\PermissionService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ManagementAccountServiceInterface::class,
            \App\Services\Postgres\Production\ManagementAccountService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\SpaceServiceInterface::class,
            \App\Services\Postgres\Production\SpaceService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationGroupServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationGroupService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationDefaultServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationDefaultService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationAdvancedSettingServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationAdvancedSettingService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\RoomServiceInterface::class,
            \App\Services\Postgres\Production\RoomService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\QuickBarServiceInterface::class,
            \App\Services\Postgres\Production\QuickBarService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\EmotionServiceInterface::class,
            \App\Services\Postgres\Production\EmotionService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PostServiceInterface::class,
            \App\Services\Postgres\Production\PostService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\CommentServiceInterface::class,
            \App\Services\Postgres\Production\CommentService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationLocationServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationLocationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\ApplicationGroupLocationServiceInterface::class,
            \App\Services\Postgres\Production\ApplicationGroupLocationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PostEmotionServiceInterface::class,
            \App\Services\Postgres\Production\PostEmotionService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\PostImageServiceInterface::class,
            \App\Services\Postgres\Production\PostImageService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\StatisticApplicationServiceInterface::class,
            \App\Services\Postgres\Production\StatisticApplicationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\StatisticAccountServiceInterface::class,
            \App\Services\Postgres\Production\StatisticAccountService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\RoomAccountServiceInterface::class,
            \App\Services\Postgres\Production\RoomAccountService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\NotificationServiceInterface::class,
            \App\Services\Postgres\Production\NotificationService::class
        );

        $this->app->singleton(
            \App\Services\Postgres\AccountLifeWorkServiceInterface::class,
            \App\Services\Postgres\Production\AccountLifeWorkService::class
        );

        /* NEW BINDING */
    }
}
