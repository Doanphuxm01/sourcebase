<?php

namespace App\Presenters;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Postgres\Manager;
use App\Models\Postgres\Account;

class ManagementAccountPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Manager
    * */
    public function manager()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ManagerModel');
            $cached = Redis::hget($cacheKey, $this->entity->manager_id);

            if( $cached ) {
                $manager = new Manager(json_decode($cached, true));
                $manager['id'] = json_decode($cached, true)['id'];
                return $manager;
            } else {
                $manager = $this->entity->manager;
                Redis::hsetnx($cacheKey, $this->entity->manager_id, $manager);
                return $manager;
            }
        }

        $manager = $this->entity->manager;
        return $manager;
    }

    /**
    * @return \App\Models\Postgres\Account
    * */
    public function account()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_id);

            if( $cached ) {
                $account = new Account(json_decode($cached, true));
                $account['id'] = json_decode($cached, true)['id'];
                return $account;
            } else {
                $account = $this->entity->account;
                Redis::hsetnx($cacheKey, $this->entity->account_id, $account);
                return $account;
            }
        }

        $account = $this->entity->account;
        return $account;
    }


}
