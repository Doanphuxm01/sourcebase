<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Account;
use App\Models\Application;
use App\Models\StatisticApplication;

class StatisticAccountPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Account
    * */
    public function account()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_id);

            if( $cached ) {
                $account = new Account(json_decode($cached, true));
                $account['id'] = json_decode($cached, true)['id'];
                return $account;
            } else {
                $account = $this->entity->account;
                Redis::hsetnx($cacheKey, $this->entity->account_id, $account);
                return $account;
            }
        }

        $account = $this->entity->account;
        return $account;
    }

    /**
    * @return \App\Models\Application
    * */
    public function application()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ApplicationModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_id);

            if( $cached ) {
                $application = new Application(json_decode($cached, true));
                $application['id'] = json_decode($cached, true)['id'];
                return $application;
            } else {
                $application = $this->entity->application;
                Redis::hsetnx($cacheKey, $this->entity->application_id, $application);
                return $application;
            }
        }

        $application = $this->entity->application;
        return $application;
    }

    /**
    * @return \App\Models\StatisticApplication
    * */
    public function statisticApplication()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('StatisticApplicationModel');
            $cached = Redis::hget($cacheKey, $this->entity->statistic_application_id);

            if( $cached ) {
                $statisticApplication = new StatisticApplication(json_decode($cached, true));
                $statisticApplication['id'] = json_decode($cached, true)['id'];
                return $statisticApplication;
            } else {
                $statisticApplication = $this->entity->statisticApplication;
                Redis::hsetnx($cacheKey, $this->entity->statistic_application_id, $statisticApplication);
                return $statisticApplication;
            }
        }

        $statisticApplication = $this->entity->statisticApplication;
        return $statisticApplication;
    }

    
}
