<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Room;
use App\Models\Space;
use App\Models\Application;
use App\Models\ApplicationStore;
use App\Models\Account;

class StatisticAccountTimeUsedAppPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Room
    * */
    public function room()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('RoomModel');
            $cached = Redis::hget($cacheKey, $this->entity->room_id);

            if( $cached ) {
                $room = new Room(json_decode($cached, true));
                $room['id'] = json_decode($cached, true)['id'];
                return $room;
            } else {
                $room = $this->entity->room;
                Redis::hsetnx($cacheKey, $this->entity->room_id, $room);
                return $room;
            }
        }

        $room = $this->entity->room;
        return $room;
    }

    /**
    * @return \App\Models\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Application
    * */
    public function application()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ApplicationModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_id);

            if( $cached ) {
                $application = new Application(json_decode($cached, true));
                $application['id'] = json_decode($cached, true)['id'];
                return $application;
            } else {
                $application = $this->entity->application;
                Redis::hsetnx($cacheKey, $this->entity->application_id, $application);
                return $application;
            }
        }

        $application = $this->entity->application;
        return $application;
    }

    /**
    * @return \App\Models\ApplicationStore
    * */
    public function applicationStore()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ApplicationStoreModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_store_id);

            if( $cached ) {
                $applicationStore = new ApplicationStore(json_decode($cached, true));
                $applicationStore['id'] = json_decode($cached, true)['id'];
                return $applicationStore;
            } else {
                $applicationStore = $this->entity->applicationStore;
                Redis::hsetnx($cacheKey, $this->entity->application_store_id, $applicationStore);
                return $applicationStore;
            }
        }

        $applicationStore = $this->entity->applicationStore;
        return $applicationStore;
    }

    /**
    * @return \App\Models\Account
    * */
    public function account()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_id);

            if( $cached ) {
                $account = new Account(json_decode($cached, true));
                $account['id'] = json_decode($cached, true)['id'];
                return $account;
            } else {
                $account = $this->entity->account;
                Redis::hsetnx($cacheKey, $this->entity->account_id, $account);
                return $account;
            }
        }

        $account = $this->entity->account;
        return $account;
    }

    
}
