<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Room;
use App\Models\Space;
use App\Models\Application;

class StatisticApplicationPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Room
    * */
    public function room()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('RoomModel');
            $cached = Redis::hget($cacheKey, $this->entity->room_id);

            if( $cached ) {
                $room = new Room(json_decode($cached, true));
                $room['id'] = json_decode($cached, true)['id'];
                return $room;
            } else {
                $room = $this->entity->room;
                Redis::hsetnx($cacheKey, $this->entity->room_id, $room);
                return $room;
            }
        }

        $room = $this->entity->room;
        return $room;
    }

    /**
    * @return \App\Models\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Application
    * */
    public function application()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ApplicationModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_id);

            if( $cached ) {
                $application = new Application(json_decode($cached, true));
                $application['id'] = json_decode($cached, true)['id'];
                return $application;
            } else {
                $application = $this->entity->application;
                Redis::hsetnx($cacheKey, $this->entity->application_id, $application);
                return $application;
            }
        }

        $application = $this->entity->application;
        return $application;
    }

    
}
