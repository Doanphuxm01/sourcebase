<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Post;
use App\Models\Space;
use App\Models\Emotion;
use App\Models\Organization;

class PostEmotionPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Post
    * */
    public function post()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('PostModel');
            $cached = Redis::hget($cacheKey, $this->entity->post_id);

            if( $cached ) {
                $post = new Post(json_decode($cached, true));
                $post['id'] = json_decode($cached, true)['id'];
                return $post;
            } else {
                $post = $this->entity->post;
                Redis::hsetnx($cacheKey, $this->entity->post_id, $post);
                return $post;
            }
        }

        $post = $this->entity->post;
        return $post;
    }

    /**
    * @return \App\Models\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Emotion
    * */
    public function emotion()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('EmotionModel');
            $cached = Redis::hget($cacheKey, $this->entity->emotion_id);

            if( $cached ) {
                $emotion = new Emotion(json_decode($cached, true));
                $emotion['id'] = json_decode($cached, true)['id'];
                return $emotion;
            } else {
                $emotion = $this->entity->emotion;
                Redis::hsetnx($cacheKey, $this->entity->emotion_id, $emotion);
                return $emotion;
            }
        }

        $emotion = $this->entity->emotion;
        return $emotion;
    }

    /**
    * @return \App\Models\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    
}
