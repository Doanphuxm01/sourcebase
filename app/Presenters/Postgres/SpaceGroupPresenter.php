<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Space;
use App\Models\Group;
use App\Models\Organization;

class SpaceGroupPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Group
    * */
    public function group()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('GroupModel');
            $cached = Redis::hget($cacheKey, $this->entity->group_id);

            if( $cached ) {
                $group = new Group(json_decode($cached, true));
                $group['id'] = json_decode($cached, true)['id'];
                return $group;
            } else {
                $group = $this->entity->group;
                Redis::hsetnx($cacheKey, $this->entity->group_id, $group);
                return $group;
            }
        }

        $group = $this->entity->group;
        return $group;
    }

    /**
    * @return \App\Models\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    
}
