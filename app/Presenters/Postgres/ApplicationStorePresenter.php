<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Image;

class ApplicationStorePresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = ['application_image'];

    /**
    * @return \App\Models\Image
    * */
    public function applicationImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->applicationImage;
                Redis::hsetnx($cacheKey, $this->entity->application_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->applicationImage;
        return $image;
    }

    
}
