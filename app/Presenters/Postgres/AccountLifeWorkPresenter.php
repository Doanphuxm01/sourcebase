<?php

namespace App\Presenters\Postgres;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Postgres\Image;
use App\Models\Postgres\Organization;
//use App\Models\Postgres\AccountLife;
//use App\Models\Postgres\AccountWork;

class AccountLifeWorkPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = ['profile_image','background_image'];

    /**
    * @return \App\Models\Image
    * */
    public function profileImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->profile_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->profileImage;
                Redis::hsetnx($cacheKey, $this->entity->profile_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->profileImage;
        return $image;
    }

    /**
    * @return \App\Models\Image
    * */
    public function backgroundImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->background_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->backgroundImage;
                Redis::hsetnx($cacheKey, $this->entity->background_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->backgroundImage;
        return $image;
    }

    /**
    * @return \App\Models\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    /**
    * @return \App\Models\AccountLife
    * */
    public function accountLife()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountLifeModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_life_id);

            if( $cached ) {
                $accountLife = new AccountLife(json_decode($cached, true));
                $accountLife['id'] = json_decode($cached, true)['id'];
                return $accountLife;
            } else {
                $accountLife = $this->entity->accountLife;
                Redis::hsetnx($cacheKey, $this->entity->account_life_id, $accountLife);
                return $accountLife;
            }
        }

        $accountLife = $this->entity->accountLife;
        return $accountLife;
    }

    /**
    * @return \App\Models\AccountWork
    * */
    public function accountWork()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountWorkModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_work_id);

            if( $cached ) {
                $accountWork = new AccountWork(json_decode($cached, true));
                $accountWork['id'] = json_decode($cached, true)['id'];
                return $accountWork;
            } else {
                $accountWork = $this->entity->accountWork;
                Redis::hsetnx($cacheKey, $this->entity->account_work_id, $accountWork);
                return $accountWork;
            }
        }

        $accountWork = $this->entity->accountWork;
        return $accountWork;
    }

    
}
