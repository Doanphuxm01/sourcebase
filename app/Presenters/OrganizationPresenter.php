<?php

namespace App\Presenters;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Postgres\Major;
use App\Models\Postgres\Package;

class OrganizationPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Postgres\Major
    * */
    public function major()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('MajorModel');
            $cached = Redis::hget($cacheKey, $this->entity->major_id);

            if( $cached ) {
                $major = new Major(json_decode($cached, true));
                $major['id'] = json_decode($cached, true)['id'];
                return $major;
            } else {
                $major = $this->entity->major;
                Redis::hsetnx($cacheKey, $this->entity->major_id, $major);
                return $major;
            }
        }

        $major = $this->entity->major;
        return $major;
    }

    /**
    * @return \App\Models\Postgres\Package
    * */
    public function package()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('PackageModel');
            $cached = Redis::hget($cacheKey, $this->entity->package_id);

            if( $cached ) {
                $package = new Package(json_decode($cached, true));
                $package['id'] = json_decode($cached, true)['id'];
                return $package;
            } else {
                $package = $this->entity->package;
                Redis::hsetnx($cacheKey, $this->entity->package_id, $package);
                return $package;
            }
        }

        $package = $this->entity->package;
        return $package;
    }


}
