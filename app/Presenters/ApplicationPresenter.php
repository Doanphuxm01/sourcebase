<?php

namespace App\Presenters;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Postgres\Image;
use App\Models\Postgres\Account;
use App\Models\Postgres\Room;
use App\Models\Postgres\Space;
use App\Models\Postgres\Organization;
use App\Models\Postgres\ApplicationGroup;

class ApplicationPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = ['application_image'];

    /**
    * @return \App\Models\Postgres\Image
    * */
    public function applicationImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->applicationImage;
                Redis::hsetnx($cacheKey, $this->entity->application_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->applicationImage;
        return $image;
    }

    /**
    * @return \App\Models\Postgres\Account
    * */
    public function account()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_id);

            if( $cached ) {
                $account = new Account(json_decode($cached, true));
                $account['id'] = json_decode($cached, true)['id'];
                return $account;
            } else {
                $account = $this->entity->account;
                Redis::hsetnx($cacheKey, $this->entity->account_id, $account);
                return $account;
            }
        }

        $account = $this->entity->account;
        return $account;
    }

    /**
    * @return \App\Models\Postgres\Room
    * */
    public function room()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('RoomModel');
            $cached = Redis::hget($cacheKey, $this->entity->room_id);

            if( $cached ) {
                $room = new Room(json_decode($cached, true));
                $room['id'] = json_decode($cached, true)['id'];
                return $room;
            } else {
                $room = $this->entity->room;
                Redis::hsetnx($cacheKey, $this->entity->room_id, $room);
                return $room;
            }
        }

        $room = $this->entity->room;
        return $room;
    }

    /**
    * @return \App\Models\Postgres\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Postgres\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    /**
    * @return \App\Models\Postgres\ApplicationGroup
    * */
    public function applicationGroup()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ApplicationGroupModel');
            $cached = Redis::hget($cacheKey, $this->entity->application_group_id);

            if( $cached ) {
                $applicationGroup = new ApplicationGroup(json_decode($cached, true));
                $applicationGroup['id'] = json_decode($cached, true)['id'];
                return $applicationGroup;
            } else {
                $applicationGroup = $this->entity->applicationGroup;
                Redis::hsetnx($cacheKey, $this->entity->application_group_id, $applicationGroup);
                return $applicationGroup;
            }
        }

        $applicationGroup = $this->entity->applicationGroup;
        return $applicationGroup;
    }


}
