<?php

namespace App\Presenters;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Account;
use App\Models\Space;
use App\Models\Organization;

class SpaceAccountLifeWorkPresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = [];

    /**
    * @return \App\Models\Account
    * */
    public function account()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('AccountModel');
            $cached = Redis::hget($cacheKey, $this->entity->account_id);

            if( $cached ) {
                $account = new Account(json_decode($cached, true));
                $account['id'] = json_decode($cached, true)['id'];
                return $account;
            } else {
                $account = $this->entity->account;
                Redis::hsetnx($cacheKey, $this->entity->account_id, $account);
                return $account;
            }
        }

        $account = $this->entity->account;
        return $account;
    }

    /**
    * @return \App\Models\Space
    * */
    public function space()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('SpaceModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_id);

            if( $cached ) {
                $space = new Space(json_decode($cached, true));
                $space['id'] = json_decode($cached, true)['id'];
                return $space;
            } else {
                $space = $this->entity->space;
                Redis::hsetnx($cacheKey, $this->entity->space_id, $space);
                return $space;
            }
        }

        $space = $this->entity->space;
        return $space;
    }

    /**
    * @return \App\Models\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    
}
