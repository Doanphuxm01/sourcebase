<?php

namespace App\Presenters;
use App\Presenters\BasePresenter;

use Illuminate\Support\Facades\Redis;
use App\Models\Major;
use App\Models\Image;
use App\Models\Organization;

class SpacePresenter extends BasePresenter
{
    protected $multilingualFields = [];

    protected $imageFields = ['space_image'];

    /**
    * @return \App\Models\Major
    * */
    public function major()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('MajorModel');
            $cached = Redis::hget($cacheKey, $this->entity->major_id);

            if( $cached ) {
                $major = new Major(json_decode($cached, true));
                $major['id'] = json_decode($cached, true)['id'];
                return $major;
            } else {
                $major = $this->entity->major;
                Redis::hsetnx($cacheKey, $this->entity->major_id, $major);
                return $major;
            }
        }

        $major = $this->entity->major;
        return $major;
    }

    /**
    * @return \App\Models\Image
    * */
    public function spaceImage()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('ImageModel');
            $cached = Redis::hget($cacheKey, $this->entity->space_image_id);

            if( $cached ) {
                $image = new Image(json_decode($cached, true));
                $image['id'] = json_decode($cached, true)['id'];
                return $image;
            } else {
                $image = $this->entity->spaceImage;
                Redis::hsetnx($cacheKey, $this->entity->space_image_id, $image);
                return $image;
            }
        }

        $image = $this->entity->spaceImage;
        return $image;
    }

    /**
    * @return \App\Models\Organization
    * */
    public function organization()
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel('OrganizationModel');
            $cached = Redis::hget($cacheKey, $this->entity->organization_id);

            if( $cached ) {
                $organization = new Organization(json_decode($cached, true));
                $organization['id'] = json_decode($cached, true)['id'];
                return $organization;
            } else {
                $organization = $this->entity->organization;
                Redis::hsetnx($cacheKey, $this->entity->organization_id, $organization);
                return $organization;
            }
        }

        $organization = $this->entity->organization;
        return $organization;
    }

    
}
