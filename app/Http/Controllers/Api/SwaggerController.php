<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @OA\Swagger(
 *      schemes={"http", "https"},
 *      @OA\Info(
 *          version="1.0.0",
 *          title="Mandu",
 *          description="Dones development company",
 *          @OA\Contact(
 *              email="phunguyen@dones.ai"
 *          )
 *      ),
 *  )
 */

/**
 *
 * @OA\SecurityScheme(
 *     securityScheme="Bearer",
 *     type="apiKey",
 *     in="header",
 *     name="Authorization"
 * )
 * @OA\SecurityScheme(
 *     securityScheme="default",
 *     type="apiKey",
 *     in="header",
 *     name="swagger"
 * )
 * @OA\SecurityScheme(
 *     securityScheme="Platform",
 *     type="apiKey",
 *     in="header",
 *     name="Platform"
 * )
 */

// SETTING
/**
 * @OA\Get(
 *        path = "/api/v1/setting/module",
 *        tags = {"SETTING"},
 *        summary = "Trả về tất cả các quyền account đang có",
 *        description = "Trả về tất cả quyền account đang có",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */


//DATA DEFAULT
/**
 * @OA\Get(
 *        path = "/api/v1/get-info-new-version",
 *        tags = {"Data Default"},
 *        summary = "Lấy thông tin phiên bản mới",
 *        description = "Lấy thông tin phiên bản mới",
 *      security={
 *         {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-position",
 *        tags = {"Data Default"},
 *        summary = "Trả về tất cả các chức danh",
 *        description = "Trả về tất cả các chức danh",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-scale",
 *        tags = {"Data Default"},
 *        summary = "Trả về tất cả các quy mô",
 *        description = "Trả về tất cả các quy mô",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *     path="/api/v1/account/update-main-space",
 *     summary="Cập nhật thông tin main space",
 *     tags = {"Account"},
 *     security={{"Bearer": {}},{"default": {}}},
 *     @OA\Parameter(
 *          name="main_space",description="id của space",required=false,in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Response(response=200,description="Success"),
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/account/get-all",
 *        tags = {"Data Default"},
 *        summary = "Lấy tất cả tài khoản",
 *        description = "Lấy tất cả tài khoản",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      *      @OA\Parameter(
 *          name="space_id",
 *          required=false,
 *          in="query",
 *          description="id space",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      *      *      @OA\Parameter(
 *          name="space_type",
 *          required=false,
 *          in="query",
 *          description="type space",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      *      *      @OA\Parameter(
 *          name="search",
 *          required=false,
 *          in="query",
 *          description="key word search",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-emotion",
 *        tags = {"Data Default"},
 *        summary = "Trả về tất cả các biểu cảm",
 *        description = "Trả về tất cả các biểu cảm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-major",
 *        tags = {"Data Default"},
 *        summary = "Trả về tất cả các ngành",
 *        description = "Trả về tất cả các ngành",
 *      @OA\Parameter(
 *          name="key_word",
 *          description="Nhập từ khóa",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-language",
 *        tags = {"Data Default"},
 *        summary = "Trả về tất cả các ngôn ngữ",
 *        description = "Trả về tất cả các ngôn ngữ",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-all-permission",
 *        tags = {"Data Default"},
 *        summary = "Lấy tất cả các quyền",
 *        description = "Lấy tất cả các quyền",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END DATA DEFAULT

//ACCOUNT
/**
 * @OA\Post(
 *        path = "/api/v1/sign-up",
 *        tags = {"Sign up"},
 *        summary = "Đăng ký tài khoản",
 *        description = "Đăng ký tài khoản",
 *      @OA\Parameter(
 *          name="name",
 *          required=false,
 *          in="query",
 *          description="Tên người dùng",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="username",
 *          required=false,
 *          in="query",
 *          description="Tài khoản",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="email",
 *          required=false,
 *          in="query",
 *          description="Email",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="password",
 *          required=false,
 *          in="query",
 *          description="Mật khẩu",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="phone_number",
 *          required=false,
 *          in="query",
 *          description="Số điện thoại",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="device_name",description="Tên thiết bị",required=true,in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *    @OA\Parameter(
 *          name="lang",
 *          required=false,
 *          in="query",
 *          description="Language Vietnam|vi - English|en",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_address",
 *          required=false,
 *          in="query",
 *          description="Link Bán Hàng / Địa Chỉ Cửa Hàng (Nếu CÓ)",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/update",
 *        tags = {"Sign up"},
 *        summary = "Update tài khoản và tạo tổ chức",
 *        description = "Update tài khoản và tạo tổ chức",
 *      @OA\Parameter(
 *          name="id",
 *          required=false,
 *          in="query",
 *          description="Id của account vừa tạo",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="position_id",
 *          required=false,
 *          in="query",
 *          description="Chức danh",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="organization_name",
 *          required=false,
 *          in="query",
 *          description="Tên tổ chức",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="scale",
 *          required=false,
 *          in="query",
 *          description="Quy mô",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="major_id",
 *          required=false,
 *          in="query",
 *          description="Id của ngành",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/sign-in",
 *        tags = {"Sign up"},
 *        summary = "Đăng nhập tài khoản",
 *        description = "Nếu organization_id = null thì yêu cầu về trang cập nhật tổ chức",
 *      @OA\Parameter(
 *          name="username",
 *          required=false,
 *          in="query",
 *          description="Tài khoản",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="password",
 *          required=false,
 *          in="query",
 *          description="Mật khẩu",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="device_name",description="Tên thiết bị",required=true,in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="lang",
 *          required=false,
 *          in="query",
 *          description="Language Vietnam|vi - English|en",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/sign-out",
 *        summary="Đăng xuất tài khoản",
 *        tags={"Sign up"},
 *        description="Đăng xuất tài khoản khỏi ứng dụng",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/sign-in-with-google",
 *        summary="Đăng nhập với tài khoản google",
 *        tags={"Sign up"},
 *        description="Đăng nhập với tài khoản google",
 *      @OA\Parameter(
 *          name="google_id",
 *          required=false,
 *          in="query",
 *          description="Id của google",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="email",
 *          required=false,
 *          in="query",
 *          description="Email",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="name",
 *          required=false,
 *          in="query",
 *          description="Tên người dùng",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="device_name",description="Tên thiết bị",required=true,in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/send-email",
 *        summary="gửi email để lấy lại tài khoản",
 *        tags={"Sign up"},
 *      @OA\Parameter(
 *          name="email",
 *          required=false,
 *          in="query",
 *          description="Email",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="lang",
 *          required=false,
 *          in="query",
 *          description="Language Vietnam|vi - English|en",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *        description="gửi email để lấy lại tài khoản",
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/get-reset-password",
 *        summary="Truy cập vào template reset password tài khoản",
 *        tags={"Sign up"},
 *      @OA\Parameter(
 *          name="token",
 *          required=false,
 *          in="query",
 *          description="Token",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *        description="Đăng xuất tài khoản khỏi ứng dụng",
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/reset-password",
 *        summary="Reset password cho tài khoản",
 *        tags={"Sign up"},
 *      @OA\Parameter(
 *          name="id",
 *          required=false,
 *          in="query",
 *          description="id user",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="email",
 *          required=false,
 *          in="query",
 *          description="email user",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="password",
 *          required=false,
 *          in="query",
 *          description="Password",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *        description="Đăng xuất tài khoản khỏi ứng dụng",
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "api/v1/active-account",
 *        summary="Kích hoạt tài khoản",
 *        tags={"Sign up"},
 *      @OA\Parameter(
 *          name="email",
 *          required=true,
 *          in="query",
 *          description="email",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="code",
 *          required=true,
 *          in="query",
 *          description="code",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *       @OA\Parameter(
 *          name="is_reset_password",
 *          required=false,
 *          in="query",
 *          description="1 | Check otp reset password",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *        description="Đăng xuất tài khoản khỏi ứng dụng",
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "api/v1/send-code-active-account",
 *        summary="Gửi mã kích hoạt tài khoản qua Email",
 *        tags={"Sign up"},
 *      @OA\Parameter(
 *          name="email",
 *          required=true,
 *          in="query",
 *          description="email",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="name",
 *          required=false,
 *          in="query",
 *          description="name",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="is_reset_password",
 *          required=false,
 *          in="query",
 *          description="1 | Check otp reset password",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *        description="Đăng xuất tài khoản khỏi ứng dụng",
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END ACCOUNT

//COLLECTION
/**
 * @OA\Get(
 *        path = "/api/v1/collection",
 *        tags = {"Collection"},
 *        summary = "Danh sách bộ sưu tập",
 *        description = "Danh sách bộ sưu tập",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="Số bản ghi trên trang, mặc định: 20",
 *          required=false,
 *          example="20"
 *      ),
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="Nhảy trang",
 *          required=false,
 *          example="1"
 *      ),
 *      @OA\Parameter(
 *          name="collectionName",
 *          in="query",
 *          description="Tên media",
 *          required=false,
 *          example="Video 1"
 *      ),
 *      @OA\Parameter(
 *          name="type",
 *          in="query",
 *          description="Kiểu media: 1: Ảnh 2: Video",
 *          required=false,
 *          example="2"
 *      ),
 *      @OA\Parameter(
 *          name="collectionType",
 *          in="query",
 *          description="Chọn bộ sưu tập",
 *          required=false,
 *          example="1"
 *      ),
 *      @OA\Parameter(
 *          name="createdBy",
 *          in="query",
 *          description="Sở hữu của mình: true|false",
 *          required=false,
 *          example=true
 *      ),
 *      @OA\Parameter(
 *          name="startDate",
 *          in="query",
 *          description="Ngày bắt đầu",
 *          required=false,
 *          example="2021-07-01"
 *      ),
 *      @OA\Parameter(
 *          name="endDate",
 *          in="query",
 *          description="Ngày kết thúc",
 *          required=false,
 *          example="2021-07-01"
 *      ),
 *      @OA\Parameter(
 *          name="listIconFlag",
 *          in="query",
 *          description="danh sách icon mặc định",
 *          required=false,
 *          example="0"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/icon",
 *        tags = {"Collection"},
 *        summary = "Danh sách icon mặc định",
 *        description = "Danh sách icon mặc định",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="Số bản ghi trên trang, mặc định: 20",
 *          required=false,
 *          example="20"
 *      ),
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="Nhảy trang",
 *          required=false,
 *          example="1"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/collection-type",
 *        tags = {"Collection"},
 *        summary = "Chọn bộ sưu tập",
 *        description = "Chọn bộ sưu tập",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\POST(
 * path="/api/v1/collection",
 * summary="Thêm mới media vào bộ sưu tập",
 * description="Thêm mới media vào bộ sưu tập",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * tags={"Collection"},
 * @OA\RequestBody(
 *   request="Collection",
 *   required=false,
 *   description="Thêm media vào bộ sưu tập",
 *   @OA\MediaType(
 *     mediaType="multipart/form-data",
 *     @OA\Schema(
 *          @OA\Property(property="file", type="string", format="binary"),
 *      )
 *   )
 * ),
 * 	@OA\Response(response=200,description="Success"),
 * 	@OA\Response(response=400,description="Bad request"),
 * 	@OA\Response(response=404,description="Page Not Found"),
 * 	@OA\Response(response=500,description="System Error")
 * )
 *
 */
/**
 * @OA\Delete(
 * path="/api/v1/collection",
 * summary="Xóa media trong bộ sưu tập",
 * description="Xóa media trong bộ sưu tập",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * tags={"Collection"},
 * @OA\RequestBody(
 *    required=false,
 *    description="Xóa media trong bộ sưu tập",
 *    @OA\JsonContent(
 *       required={"colectionIds"},
 *       @OA\Property(property="colectionIds", type="object", example={1,2}),
 *    ),
 * ),
 * 	@OA\Response(response=200,description="Success"),
 * 	@OA\Response(response=400,description="Bad request"),
 * 	@OA\Response(response=404,description="Page Not Found"),
 * 	@OA\Response(response=500,description="System Error")
 * )
 *
 */
//END COLLECTION

// GROUP
/**
 * @OA\Get(
 *        path = "/api/v1/group",
 *        tags = {"Group"},
 *        summary = "Danh sách nhóm",
 *        description = "Danh sách nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="Số bản ghi trên trang, mặc định: 20",
 *          required=false,
 *          example="20"
 *      ),
 *      @OA\Parameter(
 *          name="page",
 *          in="query",
 *          description="Nhảy trang",
 *          required=false,
 *          example="1"
 *      ),
 *      @OA\Parameter(
 *          name="keyword",
 *          in="query",
 *          description="Tên nhóm",
 *          required=false,
 *          example="IT"
 *      ),
 *      @OA\Parameter(
 *          name="type",
 *          in="query",
 *          description="Loại nhóm",
 *          required=false,
 *          example="1: Nhân viên, 2: khách hàng"
 *      ),
 *      @OA\Parameter(
 *          name="startDate",
 *          in="query",
 *          description="Ngày bắt đầu",
 *          required=false,
 *          example="2021-07-01"
 *      ),
 *      @OA\Parameter(
 *          name="endDate",
 *          in="query",
 *          description="Ngày kết thúc",
 *          required=false,
 *          example="2021-07-01"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/group/list",
 *        tags = {"Group"},
 *        summary = "Danh sách tất cả nhóm",
 *        description = "Danh sách tất cả nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/group/{groupId}",
 *        tags = {"Group"},
 *        summary = "Hiển thị 1 nhóm",
 *        description = "Hiển thị 1 nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\Parameter(
 *   description="id của nhóm",
 *   in="path",
 *   name="groupId",
 *   required=true,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/group",
 *        tags = {"Group"},
 *        summary = "Thêm mới nhóm",
 *        description = "Thêm mới nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\RequestBody(
 *    required=false,
 *    description="Biểu mẫu thêm mới nhóm",
 *    @OA\JsonContent(
 *       required={"groupName"},
 *       @OA\Property(property="groupName", type="string", example="Phát triển phần mềm"),
 *       required={"type"},
 *       @OA\Property(property="type", type="number", example="1|2"),
 *       required={"accountIds"},
 *       @OA\Property(property="accountIds", type="object", example={1,2}),
 *    ),
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/group-delete",
 *        tags = {"Group"},
 *        summary = "Xóa nhiều nhóm",
 *        description = "Xóa nhiều nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\RequestBody(
 *    required=false,
 *    description="Biểu mẫu thêm mới nhóm",
 *    @OA\JsonContent(
 *       required={"groupIds"},
 *       @OA\Property(property="groupIds", type="object", example={1,2}),
 *    ),
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/group/{groupId}",
 *        tags = {"Group"},
 *        summary = "Sửa dữ liệu nhóm",
 *        description = "Sửa dữ liệu nhóm",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\Parameter(
 *   description="id của nhóm",
 *   in="path",
 *   name="groupId",
 *   required=false,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=false,
 *    description="Biểu mẫu thêm mới nhóm",
 *    @OA\JsonContent(
 *       required={"groupName"},
 *       @OA\Property(property="groupName", type="string", example="Phát triển phần mềm"),
 *       required={"accountIds"},
 *       @OA\Property(property="accountIds", type="object", example={1,2}),
 *    ),
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

// END GROUP

//DECENTRALIZATION ACCOUNT
/**
 * @OA\Get(
 *        path = "/api/v1/decentralization/list",
 *        tags = {"Decentralization"},
 *        summary = "Danh sách các nhóm quyền",
 *        description = "Danh sách các nhóm quyền",
 *     @OA\Parameter(
 *          name="page",
 *          description="Số của trang muốn lấy dữ liệu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi cần lấy",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="order",
 *          description="Cột sắp xếp: id, name..",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="direction",
 *          description="Kiểu sắp xếp: desc, asc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="key_word",
 *          description="Nhập từ khóa",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/decentralization/create",
 *        tags = {"Decentralization"},
 *        summary = "Tạo mới nhóm quyền",
 *        description = "Tạo mới nhóm quyền",
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của nhóm quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "permissions[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "permission_id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "read",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "create",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "update",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "delete",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách các id của permission và hoạt đông (Theo thứ tự là 1 (Xem), 2 (Thêm), 3 (Chỉnh sửa), 4 (Xóa))"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/decentralization/detail",
 *        tags = {"Decentralization"},
 *        summary = "Chi tiết nhóm quyền",
 *        description = "Chi tiết nhóm quyền",
 *      @OA\Parameter(
 *          name="id",
 *          description="Id của nhóm quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/decentralization/update",
 *        tags = {"Decentralization"},
 *        summary = "Cập nhật nhóm quyền",
 *        description = "Cập nhật nhóm quyền",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của nhóm quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của nhóm quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "permissions[]",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "decentralization_permission_id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "permission_id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "read",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "create",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "update",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "delete",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách các id của permission và hoạt đông (Theo thứ tự là 1 (Xem), 2 (Thêm), 3 (Chỉnh sửa), 4 (Xóa))"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/decentralization/delete",
 *        tags = {"Decentralization"},
 *        summary = "Xóa nhóm quyền",
 *        description = "Xóa nhóm quyền",
 *     @OA\Parameter(
 *          name = "ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các nhóm quyền"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/decentralization/get-all",
 *        tags = {"Decentralization"},
 *        summary = "Lấy tất cả nhóm quyền",
 *        description = "Lấy tất cả nhóm quyền",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END DECENTRALIZATION ACCOUNT

//INFO ACCOUNT
/**
 * @OA\Get(
 *        path = "/api/v1/info-account/detail",
 *        tags = {"Info Account"},
 *        summary = "Chi tiết tài khoản",
 *        description = "Chi tiết tài khoản",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/info-account/update",
 *        tags = {"Info Account"},
 *        summary = "Cập nhật tài khoản",
 *        description = "Cập nhật tài khoản",
 *      @OA\Parameter(
 *          name="name",
 *          required=false,
 *          in="query",
 *          description="Họ và tên",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="username",
 *          required=false,
 *          in="query",
 *          description="Tên tài khoản",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="birthday",
 *          required=false,
 *          in="query",
 *          description="Ngày sinh",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="phone_number",
 *          required=false,
 *          in="query",
 *          description="Số điện thoại",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="position_id",
 *          required=false,
 *          in="query",
 *          description="Chức danh",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="organization_name",
 *          required=false,
 *          in="query",
 *          description="Tên tổ chức",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_avatar",
 *          description="Ảnh đại diện",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_background",
 *          description="Ảnh bìa",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="profile_image_id",
 *          required=false,
 *          in="query",
 *          description="Id ảnh đại diện",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="background_image_id",
 *          required=false,
 *          in="query",
 *          description="Id ảnh cover",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/info-account/change-password",
 *        tags = {"Info Account"},
 *        summary = "Thay đổi mật khẩu",
 *        description = "Thay đổi mật khẩu",
 *     @OA\Parameter(
 *          name="password",
 *          description="Mật khẩu hiện tại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="new_password",
 *          description="Mật khẩu mới",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/info-account/change-language",
 *        tags = {"Info Account"},
 *        summary = "Thay đổi ngôn ngữ",
 *        description = "Thay đổi ngôn ngữ",
 *     @OA\Parameter(
 *          name="key",
 *          description="Key của ngôn ngữ",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */


/**
 * @OA\Post(
 *        path = "/api/v1/info-account/update-device-token",
 *        tags = {"Info Account"},
 *        summary = "Thay đổi Token thiết bị",
 *        description = "Thay đổi Token thiết bị",
 *     @OA\Parameter(
 *          name="device_type",
 *          description="Android : 4, Ios: 3",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="device_token",
 *          description="device token",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END INFO ACCOUNT

//ACCOUNT
/**
 * @OA\Get(
 *        path = "/api/v1/account/list",
 *        tags = {"Account"},
 *        summary = "Danh sách các tài khoản",
 *        description = "Danh sách các tài khoản",
 *     @OA\Parameter(
 *          name="page",
 *          description="Số của trang muốn lấy dữ liệu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi cần lấy",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="order",
 *          description="Cột sắp xếp: id, name..",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="direction",
 *          description="Kiểu sắp xếp: desc, asc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="key_word",
 *          description="Nhập từ khóa",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="decentralization_id",
 *          description="Id của quyền truy cập",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="is_active",
 *          description="Trạng thái hoạt đông ( 0 => Không hoạt động, 1 => hoạt động)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="type_account",
 *          description="Trạng thái hoạt đông ( 0 => Nhân viên, 1 => khách hàng)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/create",
 *        tags = {"Account"},
 *        summary = "Tạo mới tài khoản",
 *        description = "Tạo mới tài khoản",
 *     @OA\Parameter(
 *          name="username",
 *          description="Tên tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Họ và tên",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="phone_number",
 *          description="Số điện thoại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="email",
 *          description="Email",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="password",
 *          description="Mật khẩu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="decentralization_id",
 *          description="Id của quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_avatar",
 *          description="Ảnh đại diện",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_id",
 *          description="ID ảnh đại diện",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "manager_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id người quản lý"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/account/detail",
 *        tags = {"Account"},
 *        summary = "Chi tiết tài khoản",
 *        description = "Chi tiết tài khoản",
 *      @OA\Parameter(
 *          name="id",
 *          description="Id của tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/update",
 *        tags = {"Account"},
 *        summary = "Cập nhật tài khoản",
 *        description = "Cập nhật tài khoản",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="username",
 *          description="Tên tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Họ và tên",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="phone_number",
 *          description="Số điện thoại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="email",
 *          description="Email",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="password",
 *          description="Mật khẩu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="decentralization_id",
 *          description="Id của quyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_avatar",
 *          description="Ảnh đại diện",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="profile_image_id",
 *          description="ID ảnh đại diện",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "manager_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id người quản lý"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/account/delete",
 *        tags = {"Account"},
 *        summary = "Xóa tài khoản",
 *        description = "Xóa tài khoản",
 *     @OA\Parameter(
 *          name = "ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các tài khoản"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/change-status",
 *        tags = {"Account"},
 *        summary = "Cập nhật trạng thái tài khoản",
 *        description = "Cập nhật trạng thái tài khoản",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_active",
 *          description="Trạng thái của tài khoản (0 => Không hoạt đông, 1 => hoạt đông)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/account/list-customer",
 *        tags = {"Account"},
 *        summary = "Màn list khách hàng",
 *        description = "Chi tiết",
 *      @OA\Parameter(
 *          name="search",
 *          description="search",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/account/list-account-approve",
 *        tags = {"Account"},
 *        summary = "Màn list khách hàng đang đợi duyệt",
 *        description = "Chi tiết",
 *    @OA\Parameter(
 *          name="order_by",
 *          description="Sắp xếp theo thứ tự : || asc: cũ nhất || desc : mới nhất",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *    @OA\Parameter(
 *          name="key_word",
 *          description="Tìm kiếm",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *    @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/approve-all",
 *        tags = {"Account"},
 *        summary = "Duyệt thành viên Customer",
 *        description = "Duyệt thành viên Customer",
 *     @OA\Parameter(
 *          name="is_accept",
 *          description="is_accept: 0 ( không duyệt ) || is_accept: 1 ( có duyệt )",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="approve_all",
 *          description="approve_all = 0 duyệt đơn lẻ || approve_all = 1 duyệt tất cả",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *   @OA\Parameter(
 *          name="id",
 *          description="id account",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/account/detail-customer/{id}",
 *        tags = {"Account"},
 *        summary = "Chi tiết Customer",
 *        description = "Chi tiết",
 *    @OA\Parameter(
 *          name="id",
 *          description="id user",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/detail-customer/{id}/update",
 *        tags = {"Account"},
 *        summary = "Cập nhập Customer",
 *        description = "Cập nhập Customer",
 *      @OA\Parameter(
 *          name="id",
 *          description="id user",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="rank",
 *          description="rank",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/account/admin-customer",
 *        tags = {"Account"},
 *        summary = "Xét quyền Admin cho Account",
 *        description = "Xét quyền Admin cho Account",
 *      @OA\Parameter(
 *          name="id",
 *          description="id user",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_admin",
 *          description="is_admin ( 0: ko là admin || 1: là admin )",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END ACCOUNT

// MAJOR
/**
 * @OA\Get(
 *        path = "/api/v1/major/list",
 *        tags = {"Major"},
 *        summary = "Danh sách các ngành",
 *        description = "Danh sách các ngành",
 *     @OA\Parameter(
 *          name="key_word",
 *          description="Tên ngành",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="is_active",
 *          description="Trạng thái hoạt đông ( 0 => Không hoạt động, 1 => hoạt động)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
// END MOJOR

// SPACE

/**
 * @OA\Delete(
 *        path = "/api/v1/space/account/leave/{id}",
 *        tags = {"Space"},
 *        summary = "rời khỏi space",
 *        description = "rời khỏi space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *      },
 *     @OA\Parameter(
 *          name="id",
 *          description="id của space",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/space/approve-join",
 *        tags = {"Space"},
 *        summary = "Duyệt thành viên vào space",
 *        description = "Duyệt thành viên vào space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *      },
 *     @OA\Parameter(
 *          name="id",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="account_life_id",
 *          description="id của account",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_accept",
 *          description="trạng thái duyệt: 1|0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/space/approve-all",
 *        tags = {"Space"},
 *        summary = "Duyệt, hủy tất cả thành viên vào space",
 *        description = "Duyệt, hủy tất cả thành viên vào space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "ids[]",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của space"
 *      ),
 *     @OA\Parameter(
 *          name="is_accept",
 *          description="trạng thái duyệt: 1|0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/space/list/wait-approve",
 *        tags = {"Space"},
 *        summary = "Danh sách space có account yêu cầu vào",
 *        description = "Danh sách space có account yêu cầu vào",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/space/account/wait-approve",
 *        tags = {"Space"},
 *        summary = "Danh sách account yêu cầu vào space",
 *        description = "Danh sách account yêu cầu vào space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\Parameter(
 *   description="trang hiện tại",
 *   in="query",
 *   name="page",
 *   required=false,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 * @OA\Parameter(
 *   description="Số bản ghi trên 1 trang",
 *   in="query",
 *   name="limit",
 *   required=false,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 * @OA\Parameter(
 *   description="id của space",
 *   in="query",
 *   name="id",
 *   required=true,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 * @OA\Parameter(
 *   description="tên user",
 *   in="query",
 *   name="key_word",
 *   required=false,
 *   @OA\Schema(
 *      type="string",
 *    )
 * ),
 * @OA\Parameter(
 *   description="asc: tăng dần-cũ nhất| desc: giảm dần-mới nhất| default: desc",
 *   in="query",
 *   name="order_by",
 *   required=false,
 *   @OA\Schema(
 *      type="string",
 *    )
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/space/list",
 *        tags = {"Space"},
 *        summary = "Danh sách space",
 *        description = "danh sách space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="page",
 *          description="Trang hiện tại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi trên 1 trang: mặc định 10",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="10",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="spaceName",
 *          description="Tên của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="startDate",
 *          description="Ngày bắt đầu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="endDate",
 *          description="Ngày kết thúc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="majorId",
 *          description="Chọn ngành space(id của Ngành)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="type",
 *          description="Chọn loại space: 1: Nội bộ | 2: Khách hàng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *              enum={1,2},
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/space-no-per/list",
 *        tags = {"Space"},
 *        summary = "Danh sách space không dùng quyền",
 *        description = "danh sách space không dùng quyền",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="page",
 *          description="Trang hiện tại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi trên 1 trang: mặc định 10",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="10",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="spaceName",
 *          description="Tên của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="startDate",
 *          description="Ngày bắt đầu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="endDate",
 *          description="Ngày kết thúc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="majorId",
 *          description="Chọn ngành space(id của Ngành)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="type",
 *          description="Chọn loại space: 1: Nội bộ | 2: Khách hàng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *              enum={1,2},
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/space-no-per/list/{spaceId}/group",
 *        tags = {"Space"},
 *        summary = "Danh sách group trong space không phân quyền",
 *        description = "danh sách group trong space không phân quyền",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="spaceId",
 *          description="id của space",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="type",
 *          description="type của group",
 *          required=false,
 *          in="path",
 *          @OA\Schema(
 *              type="number",
 *              example="Null|1:Nhân viên|2:Khách hàng",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/space/search/room-application",
 *        tags = {"Space"},
 *        summary = "tìm kiếm room và application",
 *        description = "tìm kiếm room và application",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         }
 *      },
 *      @OA\Parameter(
 *          name="space_id",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="name",
 *          description="từ khóa tìm kiếm",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get (
 *        path = "/api/v1/space-no-per/list/{id}/account",
 *        tags = {"Space"},
 *        summary = "Danh sách tất cả account trong space không phân quyền",
 *        description = "danh sách tất cả account trong space không phân quyền",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="id",
 *          description="id của space",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="type_account",
 *          description="0: Nhân viên, 1: khách hàng, null: get all",
 *          required=false,
 *          in="path",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      *      @OA\Parameter(
 *          name="search",
 *          description="search username",
 *          in="path",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/space-no-per/detail/{spaceId}",
 *        tags = {"Space"},
 *        summary = "Chi tiết space trong màn space",
 *        description = "Chi tiết space trong màn space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\Parameter(
 *   description="id của space",
 *   in="path",
 *   name="spaceId",
 *   required=true,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 *      @OA\Parameter(
 *          name="page",
 *          description="Trang hiện tại của danh sách tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi trên 1 trang: mặc định 10 của account",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="10",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="keyWord",
 *          description="Từ khóa tìm kiếm tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/space/detail/{spaceId}",
 *        tags = {"Space"},
 *        summary = "Chi tiết space trong màn quản lý",
 *        description = "Chi tiết space trong màn quản lý",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\Parameter(
 *   description="id của space",
 *   in="path",
 *   name="spaceId",
 *   required=true,
 *   @OA\Schema(
 *      type="integer",
 *      format="int64"
 *    )
 * ),
 *      @OA\Parameter(
 *          name="page",
 *          description="Trang hiện tại của danh sách tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi trên 1 trang: mặc định 10 của account",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="10",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="keyWord",
 *          description="Từ khóa tìm kiếm tài khoản",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/space/create",
 *        tags = {"Space"},
 *        summary = "Tạo mới space",
 *        description = "Tạo mới space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="spaceName",
 *          description="Tên space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="Loại space: 1: Nội bộ | 2: Khách hàng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="majorId",
 *          description="Mã của ngành",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="collectionId",
 *          description="Lấy ảnh trong bộ sưu tập",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="0"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="main_space",
 *          description="Set làm space mặc định 1|0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="0"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_show",
 *          description="Cho space hiển thị với cộng đồng: 1|0",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1"
 *          )
 *      ),
 * @OA\RequestBody(
 *   request="Space",
 *   required=false,
 *   description="Upload ảnh mới cho space",
 *   @OA\MediaType(
 *     mediaType="multipart/form-data",
 *     @OA\Schema(
 *          @OA\Property(property="file", type="string", format="binary"),
 *      )
 *   )
 * ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space"
 *      ),
 *     @OA\Parameter(
 *          name = "groupIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của nhóm"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/space/account/add",
 *        tags = {"Space"},
 *        summary = "Thêm tài khoản vào space",
 *        description = "Thêm tài khoản vào space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="Id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space"
 *      ),
 *     @OA\Parameter(
 *          name = "groupIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của nhóm"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/space/update",
 *        tags = {"Space"},
 *        summary = "Sửa space",
 *        description = "Sửa space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="Id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="spaceName",
 *          description="Tên space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="Loại space: 1: Nội bộ | 2: Khách hàng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="majorId",
 *          description="Mã của ngành",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="collectionId",
 *          description="Lấy ảnh có sẵn trong bộ sưu tập",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_show",
 *          description="Cho space hiển thị với cộng đồng: 1|0",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *              example="1"
 *          )
 *      ),
 * @OA\RequestBody(
 *   request="Space",
 *   required=false,
 *   description="Upload ảnh mới cho space",
 *   @OA\MediaType(
 *     mediaType="multipart/form-data",
 *     @OA\Schema(
 *          @OA\Property(property="file", type="string", format="binary"),
 *      )
 *   )
 * ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space"
 *      ),
 *     @OA\Parameter(
 *          name = "groupIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của nhóm"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Delete(
 *        path = "/api/v1/space/delete",
 *        tags = {"Space"},
 *        summary = "Xóa space",
 *        description = "Xóa space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "spaceIds[]",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id space"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Delete(
 *        path = "/api/v1/space/delete-account",
 *        tags = {"Space"},
 *        summary = "Xóa thành viên trong space",
 *        description = "Xóa thành viên trong space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "spaceId",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của space"
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách id của account cần xóa"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/space/admin/update",
 *        tags = {"Space"},
 *        summary = "Cập nhật admin trong space",
 *        description = "Cập nhật admin trong space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "space_id",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của space"
 *      ),
 *     @OA\Parameter(
 *          name = "account_id",
 *            required = true,
 *            in = "query",
 *           @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của account"
 *      ),
 *     @OA\Parameter(
 *          name = "is_admin",
 *            required = true,
 *            in = "query",
 *           @OA\Schema(
 *              type="number",
 *          ),
 *            description = "Trạng thái: 1: admin|0: không"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/account-work-life/space/{id}",
 *        tags = {"Space"},
 *        summary = "Search account bản work và life",
 *        description = "Search account bản work và life theo space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "id",
 *            required = true,
 *            in = "path",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của space"
 *      ),
 *     @OA\Parameter(
 *          name = "search",
 *            in = "query",
 *           @OA\Schema(
 *              type="string",
 *          ),
 *            description = "search theo username của account căn cứ vào space "
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
// END SPACE

//APPLICATION
/**
 * @OA\Get(
 *        path = "/api/v1/application/list",
 *        tags = {"Application"},
 *        summary = "Danh sách các ứng dụng",
 *        description = "Danh sách các ứng dụng",
 *      @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/file/list",
 *        tags = {"Application"},
 *        summary = "Danh sách file của account",
 *        description = "Danh sách file của account",
 *            @OA\Parameter(
 *          name="search",
 *          description="key word cần search",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application/list-default",
 *        tags = {"Application"},
 *        summary = "Danh sách các ứng dụng mặc định",
 *        description = "Danh sách các ứng dụng mặc định",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/application/create",
 *        tags = {"Application"},
 *        summary = "Tạo mới ứng dụng",
 *        description = "Tạo mới ứng dụng",
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *    @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="Link Android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="Link IOS",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="Link URL",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="account_id",
 *          description="Id của người được gán",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_start",
 *          description="Ngày bắt đầu có hiệu lực ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_end",
 *          description="Ngày kết thúc hiệu lực ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="Id của ảnh icon ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_group_id",
 *          description="Id của nhóm ứng dụng (Nếu chọn nhóm ứng dụng)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_type",
 *          description="Kiểu để biết thuộc kiểu nào, thêm nhóm mới hoặc chọn vào nhóm cũ (0 => nhóm cũ, 1 => nhóm mới. Nếu không tích chọn thì không cần gửi lên)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_store_ids[]",
 *          description="Id của ứng dụng nếu lấy từ kho, tạo mới thì không cần truyền",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="array",
 *              @OA\Items(type="integer"),
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name_group",
 *          description="Tên của nhóm ứng dụng (Nếu tạo mới)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image",
 *          description="Ảnh dạng file khi đăng lên cùng ứng dụng (Là icon)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *      *     @OA\Parameter(
 *          name="file",
 *          description="file upload lên application",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "time_slot[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "time_start",
 *                     type="string",
 *                 ),
 *                 @OA\Property(
 *                     property = "time_end",
 *                     type="string",
 *                 ),
 *                 @OA\Property(
 *                     property = "weekdays",
 *                     type="array",
 *			           @OA\Items(
 *                          type = "integer",
 *                     ),
 *                 ),
 *                 @OA\Property(
 *                     property = "clicks",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách các khung giờ tương tác ứng dụng weekday (Là ngày trong tuần và gửi lên dạng số)"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application/detail",
 *        tags = {"Application"},
 *        summary = "Chi tiết ứng dụng",
 *        description = "Chi tiết ứng dụng",
 *      @OA\Parameter(
 *          name="id",
 *          description="Id của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application/click-app",
 *        tags = {"Application"},
 *        summary = "Click mở ứng dụng",
 *        description = "Click mở ứng dụng",
 *      @OA\Parameter(
 *          name="id",
 *          description="Id của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/application/update",
 *        tags = {"Application"},
 *        summary = "Cập nhật ứng dụng",
 *        description = "Cập nhật ứng dụng",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="Link Android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="Link IOS",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="Link URL",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="account_id",
 *          description="Id của người được gán",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_start",
 *          description="Ngày bắt đầu có hiệu lực ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_end",
 *          description="Ngày kết thúc hiệu lực ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="Id của ảnh icon ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_accept",
 *          description="Trường để duyệt ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_group_id",
 *          description="Id của nhóm ứng dụng (Nếu chọn nhóm ứng dụng)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="group_type",
 *          description="Kiểu để biết thuộc kiểu nào, thêm nhóm mới hoặc chọn vào nhóm cũ (0 => nhóm cũ, 1 => nhóm mới. Nếu không tích chọn thì không cần gửi lên)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name_group",
 *          description="Tên của nhóm ứng dụng (Nếu tạo mới)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image",
 *          description="Ảnh dạng file khi đăng lên cùng ứng dụng (Là icon)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *      *     @OA\Parameter(
 *          name="file",
 *          description="file upload application",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "time_slot[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "application_advanced_setting_id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "time_start",
 *                     type="string",
 *                 ),
 *                 @OA\Property(
 *                     property = "time_end",
 *                     type="string",
 *                 ),
 *                 @OA\Property(
 *                     property = "weekdays",
 *                     type="array",
 *			           @OA\Items(
 *                          type = "integer",
 *                     ),
 *                 ),
 *                 @OA\Property(
 *                     property = "clicks",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách các khung giờ tương tác ứng dụng weekday (Là ngày trong tuần và gửi lên dạng số)"
 *      ),
 *    @OA\Parameter(
 *          name="is_approve",
 *          description="(type approve: 1) - khi duyệt ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/application/delete",
 *        tags = {"Application"},
 *        summary = "Xóa các ứng dụng",
 *        description = "Xóa các ứng dụng",
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các ứng dụng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application/list-application-in-room",
 *        tags = {"Application"},
 *        summary = "Các ứng dụng đơn lẻ trong room",
 *        description = "Các ứng dụng đơn lẻ trong room",
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application/list-application-for-quick-bar",
 *        tags = {"Application"},
 *        summary = "Các ứng dụng cho quick bar",
 *        description = "Các ứng dụng cho quick bar",
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END APPLICATION

//POST
/**
 * @OA\Get(
 *        path = "/api/v1/post/list",
 *        tags = {"Post"},
 *        summary = "Danh sách các bài đăng (mặc định 10 bài)",
 *        description = "Danh sách các bài đăng",
 *      @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="page",
 *          description="Số của trang muốn lấy dữ liệu",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi cần lấy",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="order",
 *          description="Cột sắp xếp: id, name..",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="direction",
 *          description="Kiểu sắp xếp: desc, asc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/post/create",
 *        tags = {"Post"},
 *        summary = "Tạo mới ứng dụng",
 *        description = "Tạo mới ứng dụng",
 *     @OA\Parameter(
 *          name="content",
 *          description="Nội dung bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "image_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của ảnh chọn từ bộ sưu tập vào bài đăng"
 *      ),
 *     @OA\Parameter(
 *          name = "images[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "file",
 *               ),
 *          ),
 *            description = "Danh sách các ảnh đăng từ máy lên vào bài đăng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/post/update",
 *        tags = {"Post"},
 *        summary = "Cập nhật bài đăng",
 *        description = "Cập nhật bài đăng",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="content",
 *          description="Nội dung bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "image_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của ảnh chọn từ bộ sưu tập vào bài đăng"
 *      ),
 *     @OA\Parameter(
 *          name = "images[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "file",
 *               ),
 *          ),
 *            description = "Danh sách các ảnh đăng từ máy lên vào bài đăng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/post/delete",
 *        tags = {"Post"},
 *        summary = "Xóa bài đăng",
 *        description = "Xóa bài đăng",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/post/react",
 *        tags = {"Post"},
 *        summary = "Thả cảm xúc bài đăng",
 *        description = "Cập nhật ứng dụng",
 *     @OA\Parameter(
 *          name="post_id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="emotion_id",
 *          description="Id của cảm xúc",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/post/list-react",
 *        tags = {"Post"},
 *        summary = "Danh sách biểu cảm của bài đăng",
 *        description = "Danh sách biểu cảm của bài đăng",
 *     @OA\Parameter(
 *          name="post_id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END POST

//START COMMENT
/**
 * @OA\Get(
 *        path = "/api/v1/comment/list",
 *        tags = {"Comment"},
 *        summary = "Danh sách các bình luận trong bài đăng (Hiện chỉ có thể comment text và bình luận chỉ có 2 cấp trả lời)",
 *        description = "Danh sách các bình luận trong bài đăng (Hiện chỉ có thể comment text và bình luận chỉ có 2 cấp trả lời)",
 *     @OA\Parameter(
 *          name="post_id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/comment/create",
 *        tags = {"Comment"},
 *        summary = "Bình luận",
 *        description = "Bình luận",
 *     @OA\Parameter(
 *          name="content",
 *          description="Nội dung bình luận",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="post_id",
 *          description="Id của bài đăng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="parent_id",
 *          description="Id của bình luận cha nếu có",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

//END COMMENT

//APPLICATION GROUP
/**
 * @OA\Get(
 *        path = "/api/v1/group-application/list",
 *        tags = {"Group Application"},
 *        summary = "Danh sách các nhóm ứng dụng",
 *        description = "Danh sách các nhóm ứng dụng",
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/group-application/create",
 *        tags = {"Group Application"},
 *        summary = "Tạo mới nhóm ứng dụng",
 *        description = "Tạo mới nhóm ứng dụng",
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của nhóm ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "app_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các ứng dụng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/group-application/detail",
 *        tags = {"Group Application"},
 *        summary = "Chi tiết nhóm ứng dụng",
 *        description = "Chi tiết nhóm ứng dụng",
 *      @OA\Parameter(
 *          name="id",
 *          description="Id của nhóm ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/group-application/update",
 *        tags = {"Group Application"},
 *        summary = "Cập nhật nhóm ứng dụng",
 *        description = "Cập nhật nhóm ứng dụng",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của nhóm ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của nhóm ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "app_ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các ứng dụng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/group-application/delete",
 *        tags = {"Group Application"},
 *        summary = "Xóa nhóm ứng dụng",
 *        description = "Xóa nhóm ứng dụng",
 *     @OA\Parameter(
 *          name="room_id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="Id của space",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "ids[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các nhóm ứng dụng"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END APPLICATION GROUP

//APPLICATION STORE
/**
 * @OA\Get(
 *        path = "/api/v1/application-store/count-total",
 *        tags = {"Application Store"},
 *        summary = "Đếm ứng dụng trong kho",
 *        description = "Đếm ứng dụng trong kho",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/application-store",
 *        tags = {"Application Store"},
 *        summary = "Danh sách ứng dụng trong kho",
 *        description = "Danh sách ứng dụng trong kho",
 *     @OA\Parameter(
 *          name="page",
 *          description="trang",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghỉ",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_system",
 *          description="Ứng dụng từ kho hệ thống-- null: lấy cả hệ thống và của người dùng, true: láy của hệ thống, false : lấy của người dùng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="boolean"
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/application-store",
 *        tags = {"Application Store"},
 *        summary = "Tạo mới ứng dụng vào store",
 *        description = "Tạo mới ứng dụng vào store",
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="slug",
 *          description="Slug của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="Link Android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="Link IOS",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="Link URL",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="Id ảnh của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\RequestBody(
 *          request="ApplicationStore",
 *          required=false,
 *          description="Upload ảnh application",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  @OA\Property(property="application_image", type="string", format="binary")
 *              )
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/application-store/{id}",
 *        tags = {"Application Store"},
 *        summary = "Cập nhật ứng dụng vào store",
 *        description = "Cập nhật ứng dụng vào store",
 *     @OA\Parameter(
 *          name="id",
 *          description="id của ứng dụng",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="slug",
 *          description="Slug của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="Link Android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="Link IOS",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="Link URL",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="Id ảnh của ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\RequestBody(
 *          request="ApplicationStore",
 *          required=false,
 *          description="Upload ảnh application",
 *          @OA\MediaType(
 *              mediaType="multipart/form-data",
 *              @OA\Schema(
 *                  @OA\Property(property="application_image", type="string", format="binary")
 *              )
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete (
 *        path = "/api/v1/application-store",
 *        tags = {"Application Store"},
 *        summary = "Xóa nhiều ứng dụng trong store",
 *        description = "Xóa nhiều ứng dụng trong store",
 *     @OA\Parameter(
 *          name="ids[]",
 *          description="id của ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Delete (
 *        path = "/api/v1/application-store/{id}",
 *        tags = {"Application Store"},
 *        summary = "Xóa ứng dụng trong store",
 *        description = "Xóa ứng dụng trong store",
 *     @OA\Parameter(
 *          name="id",
 *          description="id của ứng dụng",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *       {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get  (
 *        path = "/api/v1/application-store/{id}",
 *        tags = {"Application Store"},
 *        summary = "Chi tiết ứng dụng trong store",
 *        description = "Chi tiết ứng dụng trong store",
 *     @OA\Parameter(
 *          name="id",
 *          description="id của ứng dụng",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *             "Platform": {}
 *         },
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
//END APPLICATION STORE

// ROOM

/**
 * @OA\Get(
 *        path = "/api/v1/room/list/{idRoom}/account",
 *        tags = {"Room"},
 *        summary = "Danh sách tất cả account trong room khi gán link",
 *        description = "Danh sách tất cả account trong room khi gán link",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="idRoom",
 *          description="id của room",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="search",
 *          description="từ khóa tìm kiếm",
 *          required=false,
 *          in="path",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/room/in-space",
 *        tags = {"Room"},
 *        summary = "Danh sách tất cả room trong space",
 *        description = "danh sách tất cả room trong space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="spaceId",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/room/account/list",
 *        tags = {"Room"},
 *        summary = "Danh sách tất cả account trong room",
 *        description = "Danh sách tất cả account trong room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="page",
 *          description="Trang hiện tại",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          description="Số bản ghi trong 1 page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="spaceId",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="roomId",
 *          description="id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="keyWork",
 *          description="keyWork tìm kiếm",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/room/create",
 *        tags = {"Room"},
 *        summary = "Tạo mới room",
 *        description = "Tạo mới room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="roomName",
 *          description="Tên room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="Loại room: 1: Công khai | 2: Cá nhân",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="isPin",
 *          description="Ghim: 1: có | 0: không",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="space id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space (Type = 1: Account bên Dones Pro - Type = 2: Account bên Dones)"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/room/room-sort",
 *        tags = {"Room"},
 *        summary = "Sắp xếp room",
 *        description = "Sắp xếp room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\RequestBody(
 *    required=true,
 *    description="Thứ tự sắp xếp",
 *    @OA\JsonContent(
 *       required={"spaceId"},
 *      @OA\Property(property="spaceId", type="number", example="4"),
 *       required={"sortRoom"},
 *      @OA\Property(property="sortRoom", type="object", example={{"room_id": 4,"position": 2},{"room_id": 5,"position": 1}}),
 *    ),
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/room/update",
 *        tags = {"Room"},
 *        summary = "Sửa room",
 *        description = "Sửa room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="roomId",
 *          description="id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="roomName",
 *          description="Tên room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="Loại room: 1: Công khai | 2: Cá nhân",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="isPin",
 *          description="Ghim: 1: có | 0: không",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="space id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space (Type = 1: Account bên Dones Pro - Type = 2: Account bên Dones)"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Delete(
 * path="/api/v1/room/delete/{roomId}",
 * summary="Xóa room",
 * description="Xóa room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * tags={"Room"},
 *     @OA\Parameter(
 *          name = "roomId",
 *            required = true,
 *            in = "path",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "Danh sách id thành viên trong space"
 *      ),
 * 	@OA\Response(response=200,description="Success"),
 * 	@OA\Response(response=400,description="Bad request"),
 * 	@OA\Response(response=404,description="Page Not Found"),
 * 	@OA\Response(response=500,description="System Error")
 * )
 *
 */

/**
 * @OA\Delete(
 * path="/api/v1/room/account/delete",
 * summary="Xóa account trong room",
 * description="Xóa account trong room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * tags={"Room"},
 *     @OA\Parameter(
 *          name = "roomId",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của room"
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space (Type = 1: Account bên Dones Pro - Type = 2: Account bên Dones)"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 	@OA\Response(response=200,description="Success"),
 * 	@OA\Response(response=400,description="Bad request"),
 * 	@OA\Response(response=404,description="Page Not Found"),
 * 	@OA\Response(response=500,description="System Error")
 * )
 *
 */
/**
 * @OA\Delete(
 *        path = "/api/v1/room/account/leave/{id}",
 *        tags = {"Room"},
 *        summary = "rời khỏi room",
 *        description = "rời khỏi room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *      },
 *     @OA\Parameter(
 *          name="id",
 *          description="id của room",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/room/update-room-and-application",
 *        tags = {"Room"},
 *        summary = "Cập nhật lại room và vị trí của ứng dụng trong room",
 *        description = "Cập nhật lại room và vị trí của ứng dụng trong room",
 *     @OA\Parameter(
 *          name="id",
 *          description="Id của room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên room",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="Loại room: 1: Công khai | 2: Cá nhân",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="is_pin",
 *          description="Ghim: 1: có | 0: không",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="space id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space"
 *      ),
 *     @OA\Parameter(
 *          name = "applications[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "position",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Các ứng dụng cập nhật lại vị trí (type là chỉ nhóm hay ứng dụng đơn lẻ: 1 => Ứng dụng đơn lẻ , 2 => nhóm ứng dụng)"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/room/account/add",
 *        tags = {"Room"},
 *        summary = "Thêm account vào room",
 *        description = "Thêm account vào room",
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="Id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="roomId",
 *          description="id của room",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "object",
 *                 @OA\Property(
 *                     property = "id",
 *                     type="integer",
 *                 ),
 *                 @OA\Property(
 *                     property = "type",
 *                     type="integer",
 *                 ),
 *               ),
 *          ),
 *            description = "Danh sách id thành viên trong space (Type = 1: Account bên Dones Pro - Type = 2: Account bên Dones)"
 *      ),
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/room/admin/update",
 *        tags = {"Room"},
 *        summary = "Cập nhật admin trong room",
 *        description = "Cập nhật admin trong room",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "room_id",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của room"
 *      ),
 *     @OA\Parameter(
 *          name = "account_id",
 *            required = true,
 *            in = "query",
 *           @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id của account"
 *      ),
 *     @OA\Parameter(
 *          name = "is_admin",
 *            required = true,
 *            in = "query",
 *           @OA\Schema(
 *              type="number",
 *          ),
 *            description = "Trạng thái: 1: admin|0: không"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

// END ROOM

// QUICK BAR
/**
 * @OA\Get(
 *        path = "/api/v1/quickbar/in-space",
 *        tags = {"Quick Bar"},
 *        summary = "Danh sách tất cả quick bar trong space",
 *        description = "danh sách tất cả quick bar trong space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *      @OA\Parameter(
 *          name="spaceId",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number",
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/quickbar/create",
 *        tags = {"Quick Bar"},
 *        summary = "Tạo mới Quick bar",
 *        description = "Tạo mới Quick bar",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="is_new",
 *          description="Thêm mới ứng dụng hay không? 0: không | 1: Thêm mới",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="enable_all",
 *          description="Hiển thị cho tất cả mọi người. 1 | 0 | 2: Hiển thị với cá nhân hoặc nhóm",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name = "application_id[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của ứng dụng"
 *      ),
 *     @OA\Parameter(
 *          name = "groupIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của nhóm tài khoản, sử dụng khi enable_all = 2"
 *      ),
 *     @OA\Parameter(
 *          name = "accountIds[]",
 *            required = false,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của tài khoản, sử dụng khi enable_all = 2"
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="is_new = 1, Tên ứng dụng",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="is_new = 1, Link android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="is_new = 1, Link iOs",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="is_new = 1, Link Url",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="is_new = 1, Trường hợp chọn ảnh trong bộ sưu tập",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image",
 *          description="Ảnh dạng file khi đăng lên cùng ứng dụng (Là icon)",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="file"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/quickbar/sort",
 *        tags = {"Quick Bar"},
 *        summary = "Sắp xếp quick bar",
 *        description = "Sắp xếp quick bar",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * @OA\RequestBody(
 *    required=true,
 *    description="Thứ tự sắp xếp",
 *    @OA\JsonContent(
 *       required={"sortQuickBar"},
 *      @OA\Property(property="sortQuickBar", type="object", example={{"id": 4,"position": 2},{"id": 5,"position": 1}}),
 *    ),
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Post(
 *        path = "/api/v1/quickbar/update",
 *        tags = {"Quick Bar"},
 *        summary = "Sửa quick bar",
 *        description = "Sửa quick bar",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="application_id",
 *          description="id của ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type link: 1 | type file: 0",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="type",
 *          description="type ứng dụng: quickbar: 1",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="id của space",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="Tên ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="enable_all",
 *          description="Bật cho tất cả, chỉ root",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="number"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_android",
 *          description="Link android",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_ios",
 *          description="Link iOs",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="link_url",
 *          description="Link Url",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_image_id",
 *          description="is_new = 1, Trường hợp chọn ảnh trong bộ sưu tập",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * @OA\RequestBody(
 *   request="application_image",
 *   required=false,
 *   description="Thêm media vào bộ sưu tập",
 *   @OA\MediaType(
 *     mediaType="multipart/form-data",
 *     @OA\Schema(
 *          @OA\Property(property="application_image", type="string", format="binary"),
 *      )
 *   )
 * ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Delete(
 * path="/api/v1/quickbar/delete",
 * summary="Xóa quick bar",
 * description="Xóa quick bar",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * tags={"Quick Bar"},
 *     @OA\Parameter(
 *          name = "id",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="number",
 *          ),
 *            description = "id quick bar muốn xóa"
 *      ),
 * 	@OA\Response(response=200,description="Success"),
 * 	@OA\Response(response=400,description="Bad request"),
 * 	@OA\Response(response=404,description="Page Not Found"),
 * 	@OA\Response(response=500,description="System Error")
 * )
 *
 */
// END QUICK BAR

// Statistic
/**
 * @OA\Get(
 *        path = "/api/v1/statistic/total",
 *        tags = {"Statistic"},
 *        summary = "Tổng account, space, room, application, lượt truy cập space",
 *        description = "Tổng account, space, room, application, lượt truy cập space",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="spaceId",
 *          description="spaceId",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="roomId",
 *          description="roomId",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="startDate",
 *          description="startDate",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="endDate",
 *          description="endDate",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/statistic/account",
 *        tags = {"Statistic"},
 *        summary = "Danh sách số lượng space, room mà người dùng đang tham gia",
 *        description = "Danh sách số lượng space, room mà người dùng đang tham gia",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="userName",
 *          description="userName",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/statistic/statistic-app",
 *        tags = {"Statistic"},
 *        summary = "Thống kê app",
 *        description = "Thống kê sử dụng app",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="application name",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="room id",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="space id",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_start",
 *          description="date start",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_end",
 *          description="date end",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Get(
 *        path = "/api/v1/statistic/statistic-app-by-day",
 *        tags = {"Statistic"},
 *        summary = "Thống kê sự dụng theo ngày",
 *        description = "Thống kê sự dụng theo ngày",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_id",
 *          description="mã ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_start",
 *          description="date start",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_end",
 *          description="date end",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/statistic/statistic-app-by-day-detail",
 *        tags = {"Statistic"},
 *        summary = "Chi tiết Thống kê sự dụng theo ngày của 1 ứng dụng",
 *        description = "Chi tiết Thống kê sự dụng theo ngày của 1 ứng dụng",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="name",
 *          description="name",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="application_id",
 *          description="mã ứng dụng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_at",
 *          description="date at",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

/**
 * @OA\Get(
 *        path = "/api/v1/statistic/statistic-account-by-day",
 *        tags = {"Statistic"},
 *        summary = "Thống kê sự dụng theo ngày của người dùng",
 *        description = "Thống kê sự dụng theo ngày của người dùng",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="space_id",
 *          description="space_id",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="room_id",
 *          description="room_id",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="account_id",
 *          description="mã người dùng",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_start",
 *          description="date_start",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="date_end",
 *          description="date_end",
 *          required=true,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
// END STATICTIS

// NOTIFICATIONS
/**
 * @OA\Get(
 *        path = "/api/v1/notification/list",
 *        tags = {"Notifications"},
 *        summary = "Danh sách thông báo",
 *        description = "Danh sách thông báo",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="page",
 *          description="page",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="limit",
 *          description="limit",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *     @OA\Parameter(
 *          name="read_all",
 *          description="Xem tất cả danh sách - không phân trang | define: 1",
 *          required=false,
 *          in="query",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */
/**
 * @OA\Post(
 *        path = "/api/v1/notification/update",
 *        tags = {"Notifications"},
 *        summary = "Cập nhật trạng thái thông báo",
 *        description = "Cập nhật trạng thái thông báo",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "notificationIds[]",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="array",
 *			    @OA\Items(
 *                   type = "integer",
 *               ),
 *          ),
 *            description = "Danh sách id của các thông báo"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */


/**
 * @OA\Post(
 *        path = "/api/v1/notification/send-to-account",
 *        tags = {"Notifications"},
 *        summary = "Gửi thông báo đến user",
 *        description = "Gửi thông báo đến user",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name = "account_ids",
 *            required = true,
 *            in = "query",
 *          @OA\Schema(
 *              type="string"
 *          ),
 *            description = "Danh sách id của user"
 *      ),
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 */

// END NOTIFICATIONS

// TEST

/**
 * @OA\Get(
 *        path = "/api/v1/login-history",
 *        tags = {"Login History"},
 *        summary = "Lịch sử đăng nhập",
 *        description = "Lịch sử đăng nhập",
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 * 		@OA\Response(response=200,description="Success"),
 * 		@OA\Response(response=400,description="Bad request"),
 * 		@OA\Response(response=404,description="Page Not Found"),
 * 		@OA\Response(response=500,description="System Error")
 * )
 *
 * @OA\Get(
 *     path="/api/v1/logout-device/{id}",
 *     summary="Đăng xuất khỏi thiết bị",
 *     tags = {"Login History"},
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Parameter(
 *          name="id",
 *          description="id của của lịch sử đăng nhập",
 *          required=true,
 *          in="path",
 *          @OA\Schema(
 *              type="integer",
 *          )
 *      ),
 *     @OA\Response(response=200,description="Success"),
 * )
 * @OA\Get(
 *     path="/api/v1/logout-device-all",
 *     summary="Đăng xuất khỏi tất cả các thiết bị",
 *     tags = {"Login History"},
 *      security={
 *         {
 *             "Bearer": {}
 *         },
 *          {
 *             "default": {}
 *         },
 *          {
 *              "Platform": {}
 *          }
 *      },
 *     @OA\Response(response=200,description="Success"),
 * )
 */
// END TEST
class SwaggerController extends Controller
{
}
