<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\GroupApplication\CreateRequest;
use App\Http\Requests\Api\GroupApplication\DeleteRequest;
use App\Http\Requests\Api\GroupApplication\DetailRequest;
use App\Http\Requests\Api\GroupApplication\UpdateRequest;
use App\Http\Requests\Api\InfoAccount\ChangePasswordRequest;
use App\Http\Requests\Api\InfoAccount\UpdateAccountRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\BaseRequest;
use App\Models\Postgres\ApplicationGroup;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\ApplicationGroupServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use App\Services\Postgres\RoomServiceInterface;
use  DB;
use App\Repositories\Postgres\NotificationRepositoryInterface;

class ApiGroupApplicationController extends Controller
{
    protected $applicationGroupService;
    protected $roomService;
    protected $notificationRepository;

    public function __construct(
        ApplicationGroupServiceInterface $applicationGroupService,
        RoomServiceInterface $roomService,
        NotificationRepositoryInterface $notificationRepository
    ) {
        $this->applicationGroupService = $applicationGroupService;
        $this->roomService = $roomService;
        $this->notificationRepository = $notificationRepository;
    }

    public function index(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $filter = [];
        $this->makeFilter($request, $filter);
        $filter['deleted_at'] = true;
        $filter['organization_id'] = $accountInfo['organization_id'];
        $groupApplications = $this->applicationGroupService->getAllGroupApplication($filter);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $groupApplications);
    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'space_id',
                'room_id'
            ]
        );

        $appIds = $request->get('app_ids');
        $accountInfo = $request->get('accountInfo');
        try {
            DB::beginTransaction();

            $data['is_active'] = ApplicationGroup::IS_ACTIVE;
            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];

            $isExists = $this->roomService->isRoom(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isExists)) {
                return eResponse::response(STATUS_API_NOT_DATA, __('notification.system.data-not-found'));
            }

            $isOwnRoom = $this->roomService->checkOutMasterRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isOwnRoom)) {
                return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
            }

            $applicationGroup = $this->applicationGroupService->addApplicationGroup($appIds, $data, ApplicationGroup::NON_GET_APPLICATION_GROUP, $accountInfo);
            if (!empty($applicationGroup)) {
                DB::commit();
                $this->notificationRepository->pushNotiCreateGroupApp($accountInfo, $data['room_id'], $data['space_id'], $data['name']);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function detail(DetailRequest $request)
    {
        $id = $request->get('id');
        $accountInfo = $request->get('accountInfo');
        $applicationGroup = $this->applicationGroupService->getDetailApplicationGroup(['id' => (int)$id, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applicationGroup);
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'space_id',
                'room_id'
            ]
        );

        $id = $request->get('id');
        $appIds = $request->get('app_ids');
        $accountInfo = $request->get('accountInfo');
        try {
            DB::beginTransaction();

            $data['is_active'] = ApplicationGroup::IS_ACTIVE;
            $data['organization_id'] = $accountInfo['organization_id'];

            $isExists = $this->roomService->isRoom(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isExists)) {
                return eResponse::response(STATUS_API_NOT_DATA, __('notification.system.data-not-found'));
            }

            $isOwnRoom = $this->roomService->checkOutMasterRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isOwnRoom)) {
                return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
            }

            $applicationGroup = $this->applicationGroupService->updateApplicationGroup($id, $appIds, $data, $accountInfo);
            if (!empty($applicationGroup)) {
                DB::commit();
                $this->notificationRepository->pushNotiUpdateGroupApp($accountInfo, $data['room_id'], $data['space_id'], $data['name']);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            DB::rollback();
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function delete(DeleteRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $ids = eFunction::arrayInteger($request->get('ids'));
        $roomId = $request->get('room_id');
        $spaceId = $request->get('space_id');
        try {
            DB::beginTransaction();
            if (!empty($ids)) {
                $appGroupName = ApplicationGroup::whereIn('id', $ids)->get()->pluck('name')->toArray();
                $applicationGroup = $this->applicationGroupService->deleteApplicationGroup($ids, $spaceId, $roomId, $accountInfo);
                if (!empty($applicationGroup)) {
                    DB::commit();
                    $this->notificationRepository->pushNotiDeleteGroupApp($accountInfo, $roomId, $spaceId, $appGroupName);
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
                }
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    private function makeFilter($request, &$filter)
    {
        if ($request->has('room_id')) {
            $filter['room_id'] = $request->get('room_id');
        }

        if ($request->has('space_id')) {
            $filter['space_id'] = $request->get('space_id');
        }
    }
}
