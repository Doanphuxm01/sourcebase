<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\InfoAccount\ChangeLanguageRequest;
use App\Http\Requests\Api\InfoAccount\ChangePasswordRequest;
use App\Http\Requests\Api\InfoAccount\UpdateAccountRequest;
use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Account;
use App\Models\Postgres\Image;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\ImageServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use  DB;
use App\Http\Requests\Api\Account\DeviceTokenRequest;
use PhpParser\Node\Expr\Empty_;
use Hash;


class ApiInfoAccountController extends Controller
{
    protected $accountService;
    protected $imageService;
    protected $organizationService;

    public function __construct(AccountServiceInterface $accountService, OrganizationServiceInterface $organizationService, ImageServiceInterface $imageService)
    {
        $this->accountService = $accountService;
        $this->imageService = $imageService;
        $this->organizationService = $organizationService;
    }

    public function index(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $account = $this->accountService->getDetailAccount(['id' => (int)$accountInfo['id'], 'is_active' => Account::IS_ACTIVE, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $account);
    }

    public function update(UpdateAccountRequest $request)
    {

        $data = $request->only(
            [
                'name',
                'username',
                'birthday',
                'phone_number',
                'position_id',
                'profile_image_id',
                'background_image_id',
                'link_address'
            ]
        );

        $data['username'] = 'null';
        $accountInfo = $request->get('accountInfo');
        $organization_id = $accountInfo['organization_id'];
        $organizationName = $request->get('organization_name');
        $profileImageAvatar = $request->file('profile_image_avatar');
        $profileImageBackground = $request->file('profile_image_background');
        $data['birthday'] = !empty($data['birthday']) ? $data['birthday'] : null;
        try {
            DB::beginTransaction();
            if (!empty($accountInfo['is_root']) && !empty($organizationName)) {
                $organization = $this->organizationService->updateOrganization($organization_id, $organizationName);
                if (empty($organization)) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-fail'));
                }
            }

            if (!empty($profileImageAvatar)) {
                $data['profile_image_id'] = $this->imageService->uploadImageForProfile(Account::AVATAR, $profileImageAvatar, $accountInfo, Image::COLLECTION_TYPE_AVATAR);
            }

            if (!empty($profileImageBackground)) {
                $data['background_image_id'] = $this->imageService->uploadImageForProfile(Account::BACKGROUND, $profileImageBackground, $accountInfo, Image::COLLECTION_TYPE_BACKGROUND);
            }

            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $data['keyPlatform'] = $keyPlatform;

            $account = $this->accountService->updateAccount((int)$accountInfo['id'], $data, Account::GET_ACCOUNT_INFO);
            if (!empty($account)) {
                DB::commit();
                $token = $request->bearerToken();
                $account['token'] = $token;
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), $account);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $password = $request->get('password');
        $newPassword = $request->get('new_password');
        if ($password == $newPassword) {
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors.password'));
        }
        try {
            DB::beginTransaction();
            $account = $this->accountService->updatePasswordAccount((int)$accountInfo['id'], $password, $newPassword);
            if ($account['error_code'] == 0) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-change-password-success'));
            }

            if ($account['error_code'] == 2) {
                return eResponse::response(STATUS_API_FALSE, __('notification.api-form-change-password-current'));
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-change-password-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function changeLanguage(ChangeLanguageRequest $request)
    {
        $key = $request->get('key');
        $accountInfo = $request->get('accountInfo');
        if (empty(Account::LANGUAGE[$key])) {
            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        }
        try {
            DB::beginTransaction();
            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }
            $account = $this->accountService->updateAccount((int)$accountInfo['id'], ['language' => $key, 'keyPlatform' => $keyPlatform], Account::NON_GET_ACCOUNT_INFO);
            if (!empty($account)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), ['language' => $key]);
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function updateDeviceToken(DeviceTokenRequest $request)
    {
        try {
            $deviceToken = $request->get('device_token');
            $deviceType = $request->get('device_type');
            $data = $this->accountService->updateDeviceTokenForAccount($deviceToken, $deviceType);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-data-success'), $data);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }
}
