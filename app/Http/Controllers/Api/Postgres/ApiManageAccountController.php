<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Account\ChangeStatusRequest;
use App\Http\Requests\Api\Account\CreateRequest;
use App\Http\Requests\Api\Account\DeleteRequest;
use App\Http\Requests\Api\Account\DetailRequest;
use App\Http\Requests\Api\Account\UpdateRequest;
use App\Http\Requests\Api\InfoAccount\ChangePasswordRequest;
use App\Http\Requests\Api\InfoAccount\UpdateAccountRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\BaseRequest;
use App\Jobs\SendMailAccountActive;
use App\Models\Postgres\Account;
use App\Models\Postgres\Image;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\ImageServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use  DB;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\RoomRepositoryInterface;
use App\Repositories\Postgres\AccountRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use Illuminate\Support\Facades\Log;

class ApiManageAccountController extends Controller
{
    protected $accountService;
    protected $imageService;
    protected $organizationService;
    protected $spaceRepository;
    protected $roomRepository;
    protected $accountRepository;

    public function __construct(
        AccountServiceInterface $accountService,
        OrganizationServiceInterface $organizationService,
        ImageServiceInterface $imageService,
        SpaceRepositoryInterface $spaceRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        AccountRepositoryInterface $accountRepository,
        RoomRepositoryInterface $roomRepository
    ) {
        $this->accountService = $accountService;
        $this->imageService = $imageService;
        $this->organizationService = $organizationService;
        $this->spaceRepository = $spaceRepository;
        $this->roomRepository = $roomRepository;
        $this->accountRepository = $accountRepository;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
    }

    public function index(PaginationRequest $request)
    {
        try {

            $filter = [];
            $limit = $request->limit();
            $accountInfo = $request->get('accountInfo');

            $this->makeFilter($request, $filter);
            $filter['deleted_at'] = true;
            $filter['organization_id'] = $accountInfo['organization_id'];
            // $filter['type_account'] = Account::TYPE_ACCOUNT_STAFF;
            $filter['not']['is_root'] = Account::IS_NOT_ROOT;
            $accounts = $this->accountService->getAllAccount($limit, $filter);
            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $accounts);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'username',
                'name',
                'phone_number',
                'email',
                'password',
                'decentralization_id',
                'profile_image_id'
            ]
        );
        if($data['decentralization_id'] < 0) {
            $data['is_admin'] = Account::IS_ADMIN;
            $data['decentralization_id'] = null;
        }
        $data['password'] = !empty($data['password']) ? $data['password'] : eFunction::generateRandomPassword(15) . '@';
        $data['username'] = $data['email'];
        $data['type_account'] = Account::TYPE_ACCOUNT_STAFF;
        $data['is_check_active'] = Account::IS_ACTIVE;
        $accountInfo = $request->get('accountInfo');
        $profileImageAvatar = $request->file('profile_image_avatar');
        $managerIds = eFunction::arrayInteger($request->get('manager_ids'));

        try {
            DB::beginTransaction();

            $data['is_active'] = Account::IS_ACTIVE;
            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];
            if (!empty($profileImageAvatar)) {
                $data['profile_image_id'] = $this->imageService->uploadImageForProfile(Account::AVATAR, $profileImageAvatar, $accountInfo, Image::COLLECTION_TYPE_AVATAR);
            }

            $data['is_first_login'] = Account::FIRST_LOGIN;

            $account = $this->accountService->addAccount($data, $managerIds, $accountInfo);
            if (!empty($account)) {
                // $data = $this->spaceRepository->addAcountSpaceDefault($account->id, $accountInfo);
                // if (!empty($data['rooms'])) {
                //     $account->rooms()->syncWithOutDetaching($data['rooms']);
                //     $account->rooms()->updateExistingPivot(
                //         $data['rooms'],
                //         [
                //             'organization_id' => $accountInfo['organization_id'],
                //             'space_id' => $data['space_id'],
                //         ]
                //     );
                // }

                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function detail(DetailRequest $request)
    {
        $id = $request->get('id');
        $accountInfo = $request->get('accountInfo');
        $account = $this->accountService->getDetailAccount(['id' => (int)$id, 'is_active' => Account::IS_ACTIVE, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $account);
    }

    public function getDetailUser($id)
    {
        $account = Account::with(['mainSpace', 'profileImage', 'backgroundImage'])->find($id);
        $data = $account->toArray();
        if (!$account->space_id) {
            $account->load('spaceAccount');
            $account->load('spaceAccountLifeWork');
            $getSpaceId = Arr::first($account['spaceAccount']);
            $getSpaceWorkLifeId = Arr::first($account['spaceAccountLifeWork']);
            if ($getSpaceId) {
                $data['main_space'] = $getSpaceId ? $getSpaceId['space_id'] : null;
            } else {
                $data['main_space'] = $getSpaceWorkLifeId ? $getSpaceWorkLifeId['space_id'] : null;
            }
        } else {
            $data['main_space'] = $data['main_space']['id'];
        }
        return $data;
    }

    private function getKeyPlatform($request)
    {
        // Check thiết bị đăng nhập
        $platform = $request->header('Platform', '');
        $keyPlatform = eFunction::checkPlatformLogin($platform);

        if (empty($keyPlatform)) {
            return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
        }

        return $keyPlatform;
    }

    public function updateMainSpace(Request $request)
    {
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);

            $attributes = $request->only(
                'main_space'
            );
            if (!empty($account)) {
                $accountInfo = [
                    'id' => $account['id']
                ];
                $this->accountRepository->nUpdate(['id' => $accountInfo['id']], $attributes);
                $result = $this->getDetailUser($accountInfo['id']);
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
        }
        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), []);
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only(
            [
                'username',
                'name',
                'phone_number',
                'email',
                'decentralization_id',
                'profile_image_id'
            ]
        );

        $id = $request->get('id');
        $accountInfo = $request->get('accountInfo');
        $profileImageAvatar = $request->file('profile_image_avatar');
        $managerIds = eFunction::arrayInteger($request->get('manager_ids'));

        try {
            DB::beginTransaction();

            if ($request->has('password') && !empty($request->get('password'))) {
                $data['password'] = $request->get('password');
                // $data['api_web_access_token'] = ''; // Nếu đổi pass thì log out tài khoản
                // $data['api_ios_access_token'] = ''; // Nếu đổi pass thì log out tài khoản
                // $data['api_android_access_token'] = ''; // Nếu đổi pass thì log out tài khoản
                // $data['api_web_app_access_token'] = ''; // Nếu đổi pass thì log out tài khoản
            }

            if (!empty($profileImageAvatar)) {
                $data['profile_image_id'] = $this->imageService->uploadImageForProfile(Account::AVATAR, $profileImageAvatar, $accountInfo, Image::COLLECTION_TYPE_AVATAR);
            }

            $data['is_active'] = Account::IS_ACTIVE;
            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];
            $managerIds = array_diff($managerIds, [$id]);
            $account = $this->accountService->updateAccountWithManager($id, $data, $managerIds, $accountInfo);
            if (!empty($account)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function delete(DeleteRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $ids = eFunction::arrayInteger($request->get('ids'));
        try {
            DB::beginTransaction();
            if (!empty($ids)) {
                $accounts = $this->accountService->deleteAccount($ids, $accountInfo);
                if (!empty($accounts)) {
                    $deleteAccEveryWhere = $this->spaceRepository->deleteAllAccount($ids, $accountInfo);
                    if ($deleteAccEveryWhere) {
                        DB::commit();
                        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
                    }
                }
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function changeStatus(ChangeStatusRequest $request)
    {
        $id = $request->get('id');
        $isActive = $request->get('is_active');
        $accountInfo = $request->get('accountInfo');
        try {
            if ($id == $accountInfo['id']) {
                return eResponse::response(STATUS_API_ERROR, __('notification.api-action-error'), []);
            }

            DB::beginTransaction();
            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $accounts = $this->accountService->updateAccount($id, [
                'is_active' => !empty($isActive) && (int)$isActive === Account::IS_ACTIVE ? Account::IS_ACTIVE : Account::IS_DISABLE,
                'keyPlatform' => $keyPlatform
            ], Account::NON_GET_ACCOUNT_INFO);

            if (!empty($accounts)) {
                $this->accountRepository->resetTokenAllAccountByFilter(['id' => (int)$id, 'delete_at' => true]);
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    private function makeFilter($request, &$filter)
    {
        if ($request->has('key_word')) {
            $filter['key_word'] = $request->get('key_word');
        }

        if ($request->has('decentralization_id')) {
            $filter['decentralization_id'] = $request->get('decentralization_id');
        }

        if ($request->has('space_id')) {
            $filter['space_id'] = $request->get('space_id');
        }

        if ($request->has('is_active')) {
            $filter['is_active'] = $request->get('is_active');
        }

        if ($request->has('type_account')) {
            $filter['type_account'] = $request->get('type_account');
        }
    }

    public function listCustomer()
    {
        $search = request()->input('search', null);
        $listAccount = $this->accountRepository->getAllAccountCustomer($search);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $listAccount);
    }

    public function approveJoinAll(Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $data = $request->only([
                'id',
                'is_accept', // 1: duyet || 0: khong duyet
                'approve_all' // 1: duyet tat ca || 0: duyet 1 cai
            ]);
            $result = $this->accountRepository->approveJoinAllInAccount($data);
//            if(!empty($result['active'])) {
//                $accountActive = $this->accountRepository->getListAccountForId($result['active']);
//                // gui email active thanh cong
//                // send job
//                SendMailAccountActive::dispatch($accountActive, Account::ACCOUNT_APPROVAL);
//            }
            if (!empty($result)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail'));
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function listAccountWaitApprove(Request $request)
    {
        $data = $request->only([
            'order_by',
            'key_word',
            'page',
            'limit',
        ]);
        $result = $this->accountRepository->listAccountWaitApprove($data);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
    }

    public function updateCustomer($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $data = $request->only([
                'rank',
            ]);
            $result = $this->accountRepository->updateCustomer($data, $id);
            if (!empty($result)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail'));
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }
    public function detailCustomer($id, Request $request)
    {
        $id = $this->accountRepository->getDetailCustomer($id);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), $id);
    }

    public function adminCustomer(Request $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only([
                'id',
                'is_admin',
            ]);
            $result = $this->accountRepository->setAdminInCustomer($data);
            if (!empty($result)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail'));
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }
}
