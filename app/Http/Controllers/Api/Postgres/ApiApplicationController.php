<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Application\AllApplicationRequest;
use App\Http\Requests\Api\Application\CreateRequest;
use App\Http\Requests\Api\Application\DeleteRequest;
use App\Http\Requests\Api\Application\DetailRequest;
use App\Http\Requests\Api\Application\ListApplicationRequest;
use App\Http\Requests\Api\Application\UpdateRequest;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\InfoAccount\ChangePasswordRequest;
use App\Http\Requests\Api\InfoAccount\UpdateAccountRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationGroup;
use App\Models\Postgres\Image;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\ApplicationGroupServiceInterface;
use App\Services\Postgres\ApplicationServiceInterface;
use App\Services\Postgres\ImageServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use App\Services\Postgres\RoomServiceInterface;
use  DB;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\ApplicationStoreRepositoryInterface;
use App\Models\Postgres\ApplicationStore;

class ApiApplicationController extends Controller
{
    protected $accountService;
    protected $imageService;
    protected $imageRepository;
    protected $roomService;
    protected $applicationService;
    protected $applicationGroupService;
    protected $organizationService;
    protected $spaceRepository;
    protected $quickBarRepository;
    protected $notificationRepository;
    protected $applicationRepository;
    protected $applicationStoreRepository;

    public function __construct(
        AccountServiceInterface $accountService,
        ImageRepositoryInterface $imageRepository,
        RoomServiceInterface $roomService,
        ApplicationServiceInterface $applicationService,
        OrganizationServiceInterface $organizationService,
        ImageServiceInterface $imageService,
        ApplicationGroupServiceInterface $applicationGroupService,
        QuickBarRepositoryInterface $quickBarRepository,
        SpaceRepositoryInterface $spaceRepository,
        NotificationRepositoryInterface $notificationRepository,
        ApplicationStoreRepositoryInterface $applicationStoreRepository,
        ApplicationRepositoryInterface $applicationRepository
    ) {
        $this->accountService = $accountService;
        $this->imageRepository = $imageRepository;
        $this->imageService = $imageService;
        $this->roomService = $roomService;
        $this->applicationService = $applicationService;
        $this->applicationGroupService = $applicationGroupService;
        $this->organizationService = $organizationService;
        $this->spaceRepository = $spaceRepository;
        $this->quickBarRepository = $quickBarRepository;
        $this->notificationRepository = $notificationRepository;
        $this->applicationRepository = $applicationRepository;
        $this->applicationStoreRepository = $applicationStoreRepository;
    }

    // public function index(PaginationRequest $request)
    public function index(AllApplicationRequest $request)
    {
        try {
            $filter = [];
            $accountInfo = $request->get('accountInfo');
            $this->makeFilter($request, $filter);
            $filter['deleted_at'] = true;
            $filter['organization_id'] = $accountInfo['organization_id'];

            $applications = $this->applicationService->getListApplicationByFilter($filter, $accountInfo);

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    // public function index(PaginationRequest $request)
    public function listAppDefault()
    {
        try {
            $applications = $this->applicationRepository->getAppDefault();

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'link_android',
                'link_ios',
                'link_url',
                'account_id',
                'date_start',
                'date_end',
                'room_id',
                'space_id',
                'application_image_id',
                'application_group_id',
                'application_store_ids',
            ]
        );

        $timeSlot = $request->get('time_slot');
        $applicationImage = $request->file('application_image');
        $accountInfo = $request->get('accountInfo');
        try {
            DB::beginTransaction();

            $data['organization_id'] = $accountInfo['organization_id'];
            $data['created_by'] = $accountInfo['id'];
            $data['type'] = Application::TYPE_APP_NORMAL;
            $isExists = $this->roomService->isRoom(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isExists)) {
                return eResponse::response(STATUS_API_NOT_DATA, __('notification.system.data-not-found'));
            }

            $isOwnRoom = $this->roomService->checkOutMasterRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (!empty($applicationImage)) {
                $data['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $accountInfo, Image::COLLECTION_TYPE_APP);
            }

            if ($request->has('group_type')) {
                $groupType = $request->get('group_type');
                if ((int)$groupType === Application::GROUP_TYPE_HAS_GROUP && empty($data['application_group_id'])) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail-not-select-group'));
                }

                if ((int)$groupType === Application::GROUP_TYPE_NEW_GROUP && !$request->has('name_group')) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail-mot-found-name-group'));
                }

                if ((int)$groupType === Application::GROUP_TYPE_NEW_GROUP) {
                    if (empty($isOwnRoom)) {
                        return eResponse::response(STATUS_API_FALSE, __('notification.api-not-permission-create-group-application'));
                    }

                    $dataGroup['name'] = $request->get('name_group');
                    $dataGroup['slug'] = $request->get('slug_name_group');
                    $dataGroup['room_id'] = $data['room_id'];
                    $dataGroup['space_id'] = $data['space_id'];
                    $dataGroup['is_active'] = ApplicationGroup::IS_ACTIVE;
                    $dataGroup['created_by'] = $accountInfo['id'];
                    $dataGroup['organization_id'] = $accountInfo['organization_id'];

                    $applicationGroup = $this->applicationGroupService->addApplicationGroup([], $dataGroup, ApplicationGroup::GET_APPLICATION_GROUP, $accountInfo);
                    if (empty($applicationGroup)) {
                        return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-application-fail'));
                    }

                    $data['application_group_id'] = $applicationGroup['id'];
                }
            }

            if (!empty($data['link_url'])) {
                $url = eFunction::getHost($data['link_url']);
                $url = eFunction::zaloCustomGroupUrl($data['link_url']);
                $data['x_frame'] = eFunction::getXFrameOptions($url);
                $data['domain'] = eFunction::getDomain($data['link_url']);
            }

            $data['is_accept'] = !empty($isOwnRoom) ? Application::ACCEPT : Application::NOT_ACCEPT;
            if ($request->has('application_store_ids')) {
                $application = [];
                foreach ($request->get('application_store_ids', []) as $asid) {
                    $data['application_store_id'] = $asid;
                    // $appStore = ApplicationStore::where('id', $asid)->first();
                    // if (!empty($appStore) && $appStore->is_system == Application::IS_SYSTEM) {
                    //     $data['is_accept'] = Application::ACCEPT;
                    // }
                    $applications[] = $this->applicationService->addApplication($timeSlot, $data, $accountInfo);
                }
                if (!empty($applications)) {
                    // Thông báo
                    $createdByRoom = $this->roomService->getCreatedByRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'deleted_at' => true]);
                    // $checkSpaceType = $this->spaceRepository->checkSpaceType(null, $request['space_id']);
                    // if ($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
                    //     foreach ($applications as $app) {
                    //         $this->notificationRepository->pushNotiCreateNewAppWorkLife($accountInfo, $app, $createdByRoom);
                    //     }
                    // } else {
                    foreach ($applications as $app) {
                        $this->notificationRepository->pushNotiCreateNewApp($accountInfo, $app, $createdByRoom, null);
                    }
                    // }
                }
            } else {
                if ($request->hasFile('file')) {
                    $file = $this->applicationService->uploadFile($request->all(), $accountInfo);
                    if ($file == false) {
                        return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
                    }
                    /* insert file to table image*/
                    $data['application_file_id'] = $file;
                }
                $aso = $this->applicationStoreRepository->create($data);
                $data['application_store_id'] = $aso->id;
                $application = $this->applicationService->addApplication($timeSlot, $data, $accountInfo);
                if (!empty($application)) {
                    // Thông báo
                    $createdByRoom = $this->roomService->getCreatedByRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'deleted_at' => true]);
                    // $checkSpaceType = $this->spaceRepository->checkSpaceType(null, $request['space_id']);
                    // if ($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
                    //     $this->notificationRepository->pushNotiCreateNewAppWorkLife($accountInfo, $application, $createdByRoom);
                    // } else {
                    $this->notificationRepository->pushNotiCreateNewApp($accountInfo, $application, $createdByRoom, null);
                    // }
                }
            }
            // $application = $this->applicationService->addApplication($timeSlot, $data, $accountInfo);
            // if (!empty($application)) {
            //     DB::commit();
            //     // Thông báo
            //     $createdByRoom = $this->roomService->getCreatedByRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            //     $checkTypeSpace = $this->spaceRepository->checkSpaceType(null, $request['space_id']);
            //     if($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
            //         $this->notificationRepository->pushNotiCreateNewAppWorkLife($accountInfo, $application, $createdByRoom);
            //     } else {
            //         $this->notificationRepository->pushNotiCreateNewApp($accountInfo, $application, $createdByRoom);
            //     }
            //     return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            // }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
        } catch (\Exception $e) {
            \Log::info($e->getMessage(), [$e->getLine(), $e->getFile()]);
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function detail(DetailRequest $request)
    {
        $id = $request->get('id');
        $filter = [];
        $accountInfo = $request->get('accountInfo');
        $this->makeFilter($request, $filter);
        $filter['id'] = $id;
        $filter['deleted_at'] = true;
        $filter['organization_id'] = $accountInfo['organization_id'];
        $application = $this->applicationService->getDetailApplication($filter, $accountInfo);
        if (isset($application['application_store']['file_path'])) {
            $application['application_store']['file_path'] = config('app.url_app') . $application['application_store']['file_path'];
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $application);
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'link_android',
                'link_ios',
                'link_url',
                'account_id',
                'date_start',
                'date_end',
                'room_id',
                'space_id',
                'application_image_id',
                'application_group_id',
                'application_store_id',
                'enable_all',
            ]
        );
        if (!isset($data['date_start']) && !isset($data['date_end'])) {
            $data['date_start'] = $data['date_end'] = null;
        }

        $id = $request->get('id');
        $timeSlot = $request->get('time_slot');
        $applicationImage = $request->file('application_image');
        $accountInfo = $request->get('accountInfo');
        $isAccept = $request->get('is_accept');
        $typeApplicaton = $request->get('type');
        $flagAccept = false;
        try {
            DB::beginTransaction();

            $isExists = $this->roomService->isRoom(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            if (empty($isExists)) {
                return eResponse::response(STATUS_API_NOT_DATA, __('notification.system.data-not-found'));
            }

            $isOwnRoom = $this->roomService->checkOutMasterRoom($accountInfo['id'], ['space_id' => $data['space_id'], 'id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
            $application = $this->applicationService->getDetailCreatedByApplication(['id' => $id, 'space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true], $accountInfo);
            // if (empty($application) || (empty($isOwnRoom) && $accountInfo['id'] !== @(int)$application['created_by']['id'])){
            //     return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
            // }

            if (!empty($isOwnRoom) && $application['is_accept'] === Application::NOT_ACCEPT) { //Là chủ room vào ứng dụng chưa đc accept
                if (empty($isAccept)) {
                    $this->notificationRepository->pushNotiRejectApp($accountInfo, $application);
                    $this->applicationService->deleteApplication($id, $data['room_id'], $data['space_id'], $accountInfo);
                    DB::commit();
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-not-accept'), []);
                }

                $flagAccept = true;
                $data['is_accept'] = Application::ACCEPT;
            }

            $data['organization_id'] = $accountInfo['organization_id'];
            $data['type'] = Application::TYPE_APP_NORMAL;
            if (!empty($applicationImage)) {
                $data['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $accountInfo, Image::COLLECTION_TYPE_APP);
            }

            if ($request->has('group_type')) {
                $groupType = $request->get('group_type');
                if ($groupType === Application::GROUP_TYPE_HAS_GROUP && empty($data['application_group_id'])) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail-not-select-group'));
                }

                if ($groupType === Application::GROUP_TYPE_NEW_GROUP && !$request->has('name_group')) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail-mot-found-name-group'));
                }

                if ($groupType === Application::GROUP_TYPE_NEW_GROUP) {
                    if (empty($isOwnRoom)) {
                        return eResponse::response(STATUS_API_FALSE, __('notification.api-not-permission-create-group-application'));
                    }

                    $dataGroup['name'] = $request->get('name_group');
                    $dataGroup['slug'] = $request->get('slug_name_group');
                    $dataGroup['room_id'] = $data['room_id'];
                    $dataGroup['space_id'] = $data['space_id'];
                    $dataGroup['is_active'] = ApplicationGroup::IS_ACTIVE;
                    $dataGroup['created_by'] = $accountInfo['id'];
                    $dataGroup['organization_id'] = $data['organization_id'];

                    $applicationGroup = $this->applicationGroupService->addApplicationGroup([], $dataGroup, ApplicationGroup::GET_APPLICATION_GROUP, $accountInfo);
                    if (empty($applicationGroup)) {
                        return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-application-fail'));
                    }

                    $data['application_group_id'] = $applicationGroup['id'];
                }
            }

            if (!empty($data['link_url']) && $typeApplicaton == Application::TYPE_LINK) {
                $url = eFunction::getHost($data['link_url']);
                $url = eFunction::zaloCustomGroupUrl($data['link_url']);
                $data['x_frame'] = eFunction::getXFrameOptions($url);
                $data['domain'] = eFunction::getDomain($data['link_url']);
                $this->applicationService->deleteFileTransferFilesViaLink($id, Application::DB_APPLICATION);
            }

            $appOld = $this->applicationRepository->getOneObjectApplicationByFilter(['id' => (int)$id, 'deleted_at' => true]);
            // xoá file khi application đang ở dạng file chuyển qua dạng link
            if ($request->hasFile('file')) {
                $this->applicationService->deleteFileTransferFilesViaLink($id, Application::DB_APPLICATION);
                $file = $this->applicationService->uploadFile($request, $accountInfo);
                if ($file == false) {
                    return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
                }
                $data['application_file_id'] = $file;
            }
            $application = $this->applicationService->updateApplication($id, $timeSlot, $data, $accountInfo);
            $applicationStore = $this->applicationStoreRepository->find($application->application_store_id);
            $idFileOld = $applicationStore['application_file_id'];
            if (!empty($data['link_url']) == false && !empty($request->hasFile('file')) == false) {
                $data['application_file_id'] = $idFileOld;
            }
            $applicationStore = $this->applicationStoreRepository->update($applicationStore, $data);
            if ($request->hasFile('file')) {
                $this->applicationService->getPatchFileApplication($idFileOld);
            }
            if (!empty($application)) {
                // update enable
                if (isset($data['enable_all'])) {
                    if ($data['enable_all'] == 1) {
                        $listAccountInSpace = $this->spaceRepository->listAccountInSpaceOtherAccount($data['space_id'], $accountInfo['id']);
                        if (!empty($listAccountInSpace)) {
                            foreach ($listAccountInSpace as $el) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $application->id,
                                    'space_id' => $data['space_id'],
                                    'enable_all' => 1,
                                ];
                            }
                        }
                        if (!empty($quickBar)) {
                            $this->quickBarRepository->deleteQuickBarUsingEnableAll($data['space_id'], $application->id, $accountInfo['id']);
                            $this->quickBarRepository->insertMulti($quickBar, $accountInfo);
                            $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $application->id, $data['enable_all']);
                        }
                    } else {
                        $this->quickBarRepository->deleteQuickBarUsingEnable($data['space_id'], $accountInfo['id'], $application->id);
                        $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $application->id, $data['enable_all']);
                    }
                }
                DB::commit();
                // $checkTypeSpace = $this->spaceRepository->checkSpaceType(null, $request['space_id']);
                if ($flagAccept) {
                    // if ($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
                    //     $this->notificationRepository->pushNotiApproveAppWorkLife($accountInfo, $application);
                    // } else {
                    $this->notificationRepository->pushNotiApproveApp($accountInfo, $application);
                    // }
                } else {
                    // if ($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
                    //     if ($application->is_accept == Application::ACCEPT) {
                    //         $this->notificationRepository->pushNotiUpdateAppWorkLife($accountInfo, $application, $appOld);
                    //     }
                    // } else {
                    if ($application->is_accept == Application::ACCEPT) {
                        $this->notificationRepository->pushNotiUpdateApp($accountInfo, $application, $appOld);
                    }
                    // }
                }
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function delete(DeleteRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $ids = eFunction::arrayInteger($request->get('ids'));
        $roomId = $request->get('room_id');
        $spaceId = $request->get('space_id');
        try {
            DB::beginTransaction();
            if (!empty($ids)) {
                // $spaceType = $this->spaceRepository->checkSpaceType(null, $spaceId);
                // if ($spaceType == Space::TYPE_SPACE_CUSTOMER) {
                //     $this->notificationRepository->pushNotiDeleteAppWorkLife($accountInfo, $ids);
                // } else {
                $this->notificationRepository->pushNotiDeleteApp($accountInfo, $ids);
                // }

                $application = $this->applicationService->deleteApplication($ids, $roomId, $spaceId, $accountInfo);
                if (!empty($application)) {
                    DB::commit();
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
                }
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function listApplicationInRoom(ListApplicationRequest $request)
    {
        try {
            $filter = [];
            $accountInfo = $request->get('accountInfo');
            $this->makeFilter($request, $filter);
            $filter['deleted_at'] = true;
            $filter['application_group_id_null'] = true;
            $filter['organization_id'] = $accountInfo['organization_id'];

            $applications = $this->applicationService->getListApplicationInRoomByFilter($filter);
            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function clickApplication(DetailRequest $request)
    {
        $id = $request->get('id');
        $filter = [];
        $accountInfo = $request->get('accountInfo');
        $this->makeFilter($request, $filter);
        $filter['id'] = $id;
        $filter['deleted_at'] = true;
        $filter['organization_id'] = $accountInfo['organization_id'];
        try {
            $application = $this->applicationStoreRepository->nView(['id' => $id]); //, 'created_by'=> $accountInfo['id']
            if ($application) {
                $application->load('applicationImage');
                $application->load('applicationFile');
                $application['application_type'] = Application::TYPE_LINK;
                if (!empty($application->applicationFile)) {
                    $application['application_type'] = Application::TYPE_FILE;
                }
            }

            if (!empty($filter['room_id'])) {
                $detailAppInRoom = Application::where('room_id', $filter['room_id'])->where('application_store_id', $id)->whereNull('deleted_at')->first();
                if ($detailAppInRoom) {
                    $filter['id'] = $detailAppInRoom->id;
                    $this->applicationService->getDetailApplicationForClick($filter, $accountInfo);
                    //socket
                    $this->applicationService->compareClickApplication($filter['id'], $filter['room_id'], $filter['space_id'], $accountInfo);
                }
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $application);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function getAllApplicationForQuickBar(AllApplicationRequest $request)
    {
        try {
            $filter = [];
            $accountInfo = $request->get('accountInfo');
            $this->makeFilter($request, $filter);
            $filter['deleted_at'] = true;
            $filter['organization_id'] = $accountInfo['organization_id'];

            $applications = $this->applicationService->getAllApplicationForQuickBar($filter, $accountInfo);

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    private function makeFilter($request, &$filter)
    {
        if ($request->has('room_id')) {
            $filter['room_id'] = $request->get('room_id');
        }

        if ($request->has('space_id')) {
            $filter['space_id'] = $request->get('space_id');
        }
    }
}
