<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Decentralization\CreateRequest;
use App\Http\Requests\Api\Decentralization\DeleteRequest;
use App\Http\Requests\Api\Decentralization\DetailRequest;
use App\Http\Requests\Api\Decentralization\UpdateRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Account;
use App\Models\Postgres\Decentralization;
use App\Repositories\Postgres\DecentralizationPermissionRepositoryInterface;
use App\Repositories\Postgres\DecentralizationRepositoryInterface;
use App\Services\Postgres\PermissionServiceInterface;
use  DB;


class ApiPermissionController extends Controller
{
    protected $permissionService;
    protected $decentralizationRepository;
    protected $decentralizationPermissionRepository;

    public function __construct(DecentralizationRepositoryInterface $decentralizationRepository, DecentralizationPermissionRepositoryInterface $decentralizationPermissionRepository, PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
        $this->decentralizationRepository = $decentralizationRepository;
        $this->decentralizationPermissionRepository = $decentralizationPermissionRepository;
    }

    public function index(PaginationRequest $request)
    {
        try{
            $filter = [];
            $limit = $request->limit();
            $accountInfo = $request->get('accountInfo');

            $this->makeFilter($request, $filter);
            $filter['deleted_at'] = true;
            $filter['organization_id'] = $accountInfo['organization_id'];
            $decentralizations = $this->decentralizationRepository->getListDecentralizationByFilter($limit, $filter);
            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $decentralizations);

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }

    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug'
            ]
        );

        $accountInfo = $request->get('accountInfo');
        $permissions = $request->get('permissions');
        if (empty($permissions)){
            return eResponse::responsePagination(STATUS_API_FALSE, __('notification.system.not-found-config'));
        }

        try{
            DB::beginTransaction();
            $decentralization = $this->permissionService->addDecentralization($data, $permissions, $accountInfo);
            if (!empty($decentralization)){
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }

    }

    public function detail(DetailRequest $request)
    {
        $id = $request->get('id');
        $accountInfo = $request->get('accountInfo');
        $decentralization = $this->decentralizationRepository->getOneArrayDecentralizationWithPermissionByFilter(['id' => (int)$id, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $decentralization);

    }

    public function update(UpdateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $data = $request->only(
            [
                'name',
                'slug'
            ]
        );

        $id = $request->get('id');
        $permissions = $request->get('permissions');
        if (empty($permissions)){
            return eResponse::responsePagination(STATUS_API_FALSE, __('notification.system.not-found-config'));
        }

        try{
            DB::beginTransaction();
            $decentralization = $this->permissionService->updateDecentralization($id, $data, $permissions, $accountInfo);
            if (!empty($decentralization)){
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function delete(DeleteRequest $request)
    {

        $accountInfo = $request->get('accountInfo');
        $ids = eFunction::arrayInteger($request->get('ids'));
        try{
            DB::beginTransaction();
            if (!empty($ids)){
                $decentralization = $this->permissionService->deleteDecentralization($ids, $accountInfo);
                if (!empty($decentralization)){
                    DB::commit();
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
                }
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function getAllDecentralizations(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $decentralizations = $this->decentralizationRepository->getAllDecentralizationByFilter(['is_active' => Decentralization::IS_ACTIVE, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $decentralizations);

    }

    private function makeFilter($request, &$filter)
    {
        if ($request->has('key_word')) {
            $filter['key_word'] = $request->get('key_word');
        }

        if ($request->has('startDate')) {
            $filter['start_date'] = $request->get('startDate');
        }

        if ($request->has('endDate')) {
            $filter['end_date'] = $request->get('endDate');
        }
    }

    public function listCustomer()
    {
//        $listAccount = Account::
    }
}
