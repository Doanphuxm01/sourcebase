<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Http\Controllers\Controller;
use App\Models\Postgres\Account;
use Illuminate\Http\Request;
use App\Elibs\eFunction;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Elibs\eResponse;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use Illuminate\Support\Facades\Http;

class ApiPushNotification extends Controller
{
    protected $notificationRepository;
    protected $applicationRepository;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        ApplicationRepositoryInterface $applicationRepository
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->applicationRepository = $applicationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $accountInfo = $request->get('accountInfo');
            $notification = $this->notificationRepository->showNotification($accountInfo, $request->all());
            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $notification);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function test()
    {
        try {
            $now = now();
            $date = $now->format('Y-m-d');
            $dayNumberInWeek = $now->format('N'); // Monday: 1 -> 7

            $app = $this->applicationRepository->filterAppAvancedSettingSendMail($date, $dayNumberInWeek);

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $app);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function updateStatusRead(Request $request)
    {
        try {
            // $accountInfo = $request->get('accountInfo');
            $this->notificationRepository->updateStatusNotification($request->all());
            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-update-data-success'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//         $device_tokens = Account::whereNotNull('device_token_ios')->pluck('device_token_ios')->all();
         $res = \UserNotificationHelper::sendWithAccountId($request->get('account_ids'),[$request->all()]);
         return eResponse::response(STATUS_API_SUCCESS,__('notification.api-form-update-data-success'), $res);
    }

    /**
     * send to user a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sendToAccount(Request $request)
    {
//         $device_tokens = Account::whereNotNull('device_token_ios')->pluck('device_token_ios')->all();
        $res = eFunction::pushNotifictions([
            'event'=>'add_account_in_space',
            'created_by_id' => 5,
            'created_by_name' => 'test',
            'avatar' => null,
            'space_id' => 1,
            'space_name' => 'test sp name ne'
        ],$request->get('account_ids'));
//         $res = \UserNotificationHelper::sendWithAccountId($request->get('account_ids'),[$request->all()]);
         return eResponse::response(STATUS_API_SUCCESS,__('notification.api-form-update-data-success'), $res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
