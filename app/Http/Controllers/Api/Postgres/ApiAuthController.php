<?php

namespace App\Http\Controllers\Api\Postgres;


use App\Elibs\Debug;
use App\Elibs\eCache;
use App\Elibs\eFunction;

use App\Http\Requests\Api\Account\SendCodeActiveAccount;
use App\Http\Requests\Api\Auth\GetMailRequest;
use App\Http\Requests\Api\Auth\GetResetPasswordRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\SignInRequest;
use App\Http\Requests\Api\Auth\SignInWithGoogleRequest;
use App\Http\Requests\Api\Auth\SignUpRequest;
use App\Http\Requests\Api\Auth\UpdateRequest;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\Request;
use App\Models\Base;
use App\Models\Postgres\Account;
use App\Http\Controllers\Controller;
use App\Elibs\eResponse;
use App\Models\Postgres\Permission;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\Eloquent\NotificationRepository;
use App\Repositories\Postgres\Eloquent\OrganizationRepository;
use App\Repositories\Postgres\OrganizationRepositoryInterface;
use App\Repositories\Postgres\PackageRepositoryInterface;
use App\Repositories\Postgres\PermissionRepositoryInterface;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\DecentralizationServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use App\Services\Postgres\PackageServiceInterface;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redis;
use Jenssegers\Agent\Facades\Agent;
use Illuminate\Support\Str;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use App\Repositories\Postgres\DecentralizationRepositoryInterface;
use App\Elibs\eCrypt;
use Illuminate\Support\Arr;
use Illuminate\Http\Request as IlluminateRequest;
use App\Models\Postgres\Organization;
use App\Services\Postgres\PermissionServiceInterface;

class ApiAuthController extends Controller
{
    protected $accountRepository;
    protected $accountService;
    protected $organizationRepository;
    protected $packageRepository;
    protected $organizationService;
    protected $decentralizationService;
    protected $packageService;
    protected $spaceRepository;
    protected $decentralizationRepository;
    protected $permissionService;
    protected $permissionRepository;
    protected $notificationRepository;

    /**
     * @var AccountAccessTokensRepositoryInterface
     */
    protected $accountAccessTokensRepository;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        OrganizationRepositoryInterface $organizationRepository,
        PackageRepositoryInterface $packageRepository,
        PackageServiceInterface $packageService,
        OrganizationServiceInterface $organizationService,
        DecentralizationServiceInterface $decentralizationService,
        AccountServiceInterface $accountService,
        DecentralizationRepositoryInterface $decentralizationRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        PermissionServiceInterface $permissionService,
        PermissionRepositoryInterface $permissionRepository,
        SpaceRepositoryInterface $spaceRepository,
        NotificationRepository $notificationRepository
    )
    {
        $this->accountRepository = $accountRepository;
        $this->packageRepository = $packageRepository;
        $this->accountService = $accountService;
        $this->packageService = $packageService;
        $this->decentralizationService = $decentralizationService;
        $this->organizationService = $organizationService;
        $this->organizationRepository = $organizationRepository;
        $this->spaceRepository = $spaceRepository;
        $this->decentralizationRepository = $decentralizationRepository;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
        $this->permissionService = $permissionService;
        $this->permissionRepository = $permissionRepository;
        $this->notificationRepository = $notificationRepository;
    }

    private function checkExpDate($organization_id)
    {
        if (!empty($organization_id)) {
            $checkExp = eFunction::getOrganizationExpiredDate($organization_id, 'organization_expiration_');
            if (empty($checkExp)) {
                return false;
            }
        }
        return true;
    }

    public function loginHistory(IlluminateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $lstHistory = $this->accountRepository->listLoginHistory($accountInfo);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $lstHistory);
    }

    public function logoutDeivce(IlluminateRequest $request, $id)
    {
        $accountInfo = $request->get('accountInfo');
        $this->accountRepository->logoutDevice($accountInfo, $id);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
    }

    public function logoutDeivceAll(IlluminateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $this->accountRepository->logoutDeviceAll($accountInfo);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
    }


    public function index(SignUpRequest $request)
    {

        $data = $request->only(
            [
                'name',
                'email',
                'username',
                'password',
                'phone_number',
                'device_name',
                'link_address'
            ]
        );

        $dataToken = [
            'ip' => $request->ip(),
            'location' => !empty(eFunction::getLocationWithIp($request->ip())) ? eFunction::getLocationWithIp($request->ip()) : null,
            'device_name' => !empty($data['device_name']) ? $data['device_name'] : null,
            'user_agent' => $request->header('User-Agent') ?? null,
        ];

        try {
            DB::beginTransaction();

            $data['email'] = strtolower($data['email']);
            $data['username'] = strtolower($data['email']);
//            $data['language'] = !empty($data['language']) ? $data['language'] : Account::LANGUAGE['vi']['key'];
            $data['is_active'] = Account::IS_ACTIVE;
            $data['is_root'] = Account::IS_NOT_ROOT;
            $data['type_account'] = Account::TYPE_ACCOUNT_CLIENT;
            $data['organization_id'] = $this->organizationRepository->getIdOrganizationMandu(Organization::SLUG);

            // Check thiết bị đăng nhập
            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $data['platform'] = Account::KEY_SIGN_IN[$keyPlatform];

            // $account = $this->accountService->signUpByAPI($data);
            $account = $this->accountAccessTokensRepository->createTokenAccount($data, $dataToken);
            if (empty($account)) {
                return eResponse::response(STATUS_API_FALSE, __('notification.system.create-fail-account'), []);
            }
            // send code check active tài khoản
            $randomNumberCode = eFunction::randomNumber(6);
            $keyRedis = Account::keyRedisOtpForEmail($data['email']);
            $account['url'] = $randomNumberCode;
            $account['code'] = $randomNumberCode;
            eFunction::setRedisWithExpiredTime($keyRedis, \GuzzleHttp\json_encode($account), Base::TIME_SIGN_UP);
            $this->accountService->sendRabbitMQForMail($account->toArray(), Account::VERIFICATION_ACCOUNT, '', '', '');
            DB::commit();
            // socket to account permission account
            $permissionManage = Permission::CONTROLLER_KEY['ApiManageAccountController'];
            $getAllAccountToPermission = $this->permissionRepository->getAllAccountToPermission($permissionManage);
            $requestData['accountIds'] = $getAllAccountToPermission;
            $this->notificationRepository->pushNotiAccountInvite($account, $requestData, [], []);
            //output
            $output = $this->outputSignIn($account, Account::IS_ROOT, $keyPlatform);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.system.create-successful-account'), $output);
        } catch (\Exception $e) {
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only(
            [
                'position_id',
                'organization_name',
                'scale',
                'major_id',
            ]
        );

        $id = $request->get('id');

        try {
            DB::beginTransaction();
            $organization = $this->organizationService->createOrganization($data);
            if (empty($organization)) {
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.create-fail-organization'));
            }


            $space = $this->spaceRepository->createSpaceAfterRegister($id, $data, $organization);
            if (empty($space)) {
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.create-fail-space'));
            }
            $data['space_id'] = !empty($space['id']) ? $space['id'] : null;
            $account = $this->accountService->updateAccountWithOrganization($id, $data, $organization);
            if (!empty($account)) {
                $rootInfo = $this->accountRepository->getOneObjectAccountByFilter(['id' => $id]);

                $permissions = [
                    0 => [
                        0 =>
                            array(
                                'permission_id' => 1,
                                'read' => '0',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                        1 =>
                            array(
                                'permission_id' => 2,
                                'read' => '1',
                                'create' => '1',
                                'update' => '1',
                                'delete' => '1',
                                'decentralization_permission_id' => NULL,
                            ),
                        2 =>
                            array(
                                'permission_id' => 3,
                                'read' => '0',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                        3 =>
                            array(
                                'permission_id' => 4,
                                'read' => '1',
                                'create' => '1',
                                'update' => '1',
                                'delete' => '1',
                                'decentralization_permission_id' => NULL,
                            ),
                        4 =>
                            array(
                                'permission_id' => 5,
                                'read' => '1',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                    ],
                    1 => [
                        0 =>
                            array(
                                'permission_id' => 1,
                                'read' => '0',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                        1 =>
                            array(
                                'permission_id' => 2,
                                'read' => '1',
                                'create' => '1',
                                'update' => '1',
                                'delete' => '1',
                                'decentralization_permission_id' => NULL,
                            ),
                        2 =>
                            array(
                                'permission_id' => 3,
                                'read' => '0',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                        3 =>
                            array(
                                'permission_id' => 4,
                                'read' => '1',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                        4 =>
                            array(
                                'permission_id' => 5,
                                'read' => '0',
                                'create' => '0',
                                'update' => '0',
                                'delete' => '0',
                                'decentralization_permission_id' => NULL,
                            ),
                    ],
                ];
                $dataDecen = [
                    0 => [
                        'name' => 'admin',
                        'slug' => 'admin'
                    ],
                    1 => [
                        'name' => 'member',
                        'slug' => 'member'
                    ],
                ];

                $this->permissionService->addDecentralization($dataDecen[0], $permissions[0], $rootInfo);
                $this->permissionService->addDecentralization($dataDecen[1], $permissions[1], $rootInfo);
                DB::commit();
                $data = [
                    'name' => !empty($rootInfo->name) ? $rootInfo->name : '',
                    'email' => !empty($rootInfo->email) ? $rootInfo->email : '',
                    'phone' => !empty($rootInfo->phone_number) ? $rootInfo->phone_number : ''
                ];
                $tpl = [
                    'pattern' => Account::PATTERN[Account::NEW_ORGANIZATION],
                    'data' => eCrypt::encryptAES(json_encode($data))
                ];
                $sendMailService = $rootInfo->toArray();
                $sendMailService['name'] = !empty($rootInfo->name) ? $rootInfo->name : '';
                //                $this->accountService->sendRabbitMQForMail($sendMailService, Account::NEW_ACCOUNT, '', '', '');
                //                eFunction::sendMessageQueueFormSendEmail($tpl, Account::QUEUE_NAME_SEND_EMAIL);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), [
                    'is_active' => Account::IS_ACTIVE,
                    'organization_id' => $organization['id'],
                    'space_id' => $space['id']
                ]);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.create-fail-account'), []);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function signIn(SignInRequest $request)
    {
        $data = $request->only(
            [
                'username',
                //                'email',
                'password',
                'device_name'
            ]
        );

        $dataToken = [
            'ip' => $request->ip(),
            'location' => !empty(eFunction::getLocationWithIp($request->ip())) ? eFunction::getLocationWithIp($request->ip()) : null,
            'device_name' => !empty($data['device_name']) ? $data['device_name'] : null,
            'user_agent' => $request->header('User-Agent') ?? null,
        ];
        $data['username'] = strtolower($data['username']);
        //        $isMe = $this->accountService->findAccountForEmail($data['email']);
        //        if ($isMe == false) {
        //            return eResponse::response(STATUS_API_FALSE, __('notification.system.account-not-found'));
        //        }
        //        $data['username'] = $isMe['username'];
        try {
            // $accountExists = $this->accountRepository->getOneArrayAccountByFilter(['email' => $data['email']]);
            $accountExists = $this->accountRepository->getOneArrayAccountByFilter(['email' => $data['username']]);
            if (empty($accountExists)) {
                return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.system.account-not-found'));
            }

            $numberLoginFailed = eFunction::getThrottleLogin($accountExists['id'], Account::STRING_REDIS);
            if (!empty($numberLoginFailed) && $numberLoginFailed >= eFunction::MAX_TIME_LOGIN) {
                return eResponse::response(STATUS_API_EXPIRED_TIME, __('notification.system.blocked-account'), ['expired' => eFunction::getThrottleLoginExpiredTime($accountExists['id'], Account::STRING_REDIS)]);
            }

            // Check thiết bị đăng nhập
            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $data['platform'] = Account::KEY_SIGN_IN[$keyPlatform];

            // Tạo token
            $account = $this->accountService->signInByAPI($data);

            if (empty($account)) {
                $times = eFunction::setThrottleLogin($accountExists['id'], Account::STRING_REDIS);
                return eResponse::response(STATUS_API_FALSE, __('notification.system.blocked-account-when-login-failed', [
                    "number" => $times,
                    "max_number" => eFunction::MAX_TIME_LOGIN
                ]), [
                    "times" => $times,
                    "max_times" => eFunction::MAX_TIME_LOGIN
                ]);
            }

            $account = $this->accountAccessTokensRepository->createTokenAccount($accountExists, $dataToken);

            if (!is_null($account['is_active'])) {
                if ($account['is_active'] == Account::IS_DISABLE) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.system.space-account-blocked-login-failed'));
                }
            }

            if (!is_null($account['is_check_active'])) {
                if ($account['is_check_active'] == Account::IS_DISABLE) {
                    $this->sendCodeActiveAccountCore($account['email']);
                    // $output = $this->outputSignIn($account, @$account['is_root'], $keyPlatform);
                    // return eResponse::response(STATUS_API_SUCCESS, __('account.active.success'), $output);
                    // return eResponse::response(STATUS_API_SUCCESS, __('notification.system.log-in-successfully'), $output);
                }
            }

            // if (!empty($account->organization_id)) {
            //     $checkRedisOr = $this->checkExpDate($account->organization_id);
            //     if (empty($checkRedisOr)) {
            //         return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.api-organization-expiration'));
            //     }
            // }

            if (empty($account->space_id) && !empty($account->organization_id)) {
                // $space = $this->spaceRepository->getOneArraySpaceByFilter(['organization_id' => $account->organization_id, 'is_default' => Space::DEFAULT_SPACE]);
                $space = $this->spaceRepository->getOneSpaceOfAccount(@$account->id);
                if (!empty($space)) {
                    $this->accountRepository->update($account, ['space_id' => $space->space_id]);
                }
            }

            if ($numberLoginFailed) {
                eFunction::delThrottleLogin($accountExists['id']);
            }

            $output = $this->outputSignIn($account, @$account['is_root'], $keyPlatform);

            // if (!empty($output['organization_id'])) {
            //     $checkExp = eFunction::getOrganizationExpiredDate($output['organization_id'], 'organization_expiration_');
            //     if (empty($checkExp)) {
            //         return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.api-organization-expiration'));
            //     }
            // }

            eFunction::delThrottleLogin($accountExists['id'], Account::STRING_REDIS);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.system.log-in-successfully'), $output);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function signInWithGoogle(SignInWithGoogleRequest $request)
    {
        $data = $request->only(
            [
                'google_id',
                'name',
                'email',
                'device_name'
            ]
        );
        $dataToken = [
            'ip' => $request->ip(),
            'location' => !empty(eFunction::getLocationWithIp($request->ip())) ? eFunction::getLocationWithIp($request->ip()) : null,
            'device_name' => !empty($data['device_name']) ? $data['device_name'] : null,
            'user_agent' => $request->header('User-Agent') ?? null,
        ];

        try {
            DB::beginTransaction();
            // Check thiết bị đăng nhập
            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $data['platform'] = Account::KEY_SIGN_IN[$keyPlatform];
            $accountExists = $this->accountService->checkEmailLogin($data);
            if (!empty($accountExists)) {
                if (!empty($accountExists->organization_id)) {
//                    $checkRedisOr = $this->checkExpDate($accountExists->organization_id);
//                    if (empty($checkRedisOr)) {
//                        return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.api-organization-expiration'));
//                    }
                }
                if ($accountExists->is_active == Account::IS_DISABLE) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.system.space-account-blocked-login-failed'));
                }
//                Account::where('id', $accountExists['id'])->whereNull('deleted_at')->update(['is_check_active' => Account::IS_DISABLE]);
                $account = $this->accountAccessTokensRepository->createTokenAccount($accountExists, $dataToken);
//                $account['is_check_active'] = Account::IS_DISABLE;
                $output = $this->outputSignIn($account, @$account['is_root'], $keyPlatform);
                eFunction::delThrottleLogin($accountExists['id'], Account::STRING_REDIS);
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.log-in-successfully'), $output);
            }
            $charactersString = substr(str_shuffle(str_repeat("!@#$%^&*()", 5)), 0, 5);
            $password = eFunction::generateRandomPassword(15) . $charactersString;

            $data['username'] = $data['email'];
            $data['password'] = $password;
            $data['language'] = !empty($data['language']) ? $data['language'] : Account::LANGUAGE['vi']['key'];
            $data['is_active'] = Account::IS_ACTIVE;
            $data['is_root'] = Account::IS_NOT_ROOT;
            $data['is_check_active'] = Account::IS_DISABLE;
            $data['type_account'] = Account::TYPE_ACCOUNT_CLIENT;
            $data['organization_id'] = $this->organizationRepository->getIdOrganizationMandu(Organization::SLUG);
            // $account = $this->accountService->signUpByAPI($data);
            $account = $this->accountAccessTokensRepository->createTokenAccount($data, $dataToken);
            if (empty($account)) {
                return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('notification.system.create-fail-account'), []);
            }

            if (empty($account->space_id) && !empty($account->organization_id)) {
                // $space = $this->spaceRepository->getOneArraySpaceByFilter(['organization_id' => $account->organization_id, 'is_default' => Space::DEFAULT_SPACE]);
                // if (!empty($space)) {
                //     $this->accountRepository->update($account, ['space_id' => $space['id']]);
                // }
                $space = $this->spaceRepository->getOneSpaceOfAccount(@$account->id);
                if (!empty($space)) {
                    $this->accountRepository->update($account, ['space_id' => $space->space_id]);
                }
            }
            DB::commit();
            $output = $this->outputSignIn($account, @$account['is_root'], $keyPlatform);
            $this->accountService->sendRabbitMQForMail($account->toArray(), Account::NEW_ACCOUNT_GOOGLE, $password, '', '');

            // if (!empty($output['organization_id'])) {
            //     $checkExp = eFunction::getOrganizationExpiredDate($output['organization_id'], 'organization_expiration_');
            //     if (empty($checkExp)) {
            //         return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.api-organization-expiration'));
            //     }
            // }

            return eResponse::response(STATUS_API_SUCCESS, __('notification.system.create-successful-account'), $output);
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function signOut(BaseRequest $request)
    {
        try {
            DB::beginTransaction();
            $token = $request->bearerToken();

            $platform = $request->header('Platform', '');
            $keyPlatform = eFunction::checkPlatformLogin($platform);

            if (empty($keyPlatform)) {
                return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
            }

            $signOut = $this->accountService->signOutAccount($token, $keyPlatform);
            if (!empty($signOut)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.log-out-successfully'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());

            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function mailForResetPasswordAccount(GetMailRequest $request)
    {
        $email = Str::lower($request->get('email'));
        // gủi otp check
        $send = $this->accountService->sendEmailToResetPasswordAccount(strtolower($email));
        if (!empty($send)) {
            return eResponse::response(STATUS_API_SUCCESS, __('notification.system.send-mail-success'), []);
        }
        return eResponse::response(STATUS_API_FALSE, __('notification.system.email-not-found'));
    }

    public function checkOtpResetPasswordAccount(BaseRequest $request)
    {
        $code = $request->get('code');
        $email = $request->get('email');
        $keyRedis = Account::keyRedisOtpForEmailForResetPassWord($email);
        $data = eFunction::getRedis($keyRedis);
        if (!is_null($data)) {
            $lsObj = \GuzzleHttp\json_decode($data);
            if ($lsObj->code == $code) {
                return eResponse::response(STATUS_API_SUCCESS, __('account.otp.oke'), $lsObj);
            }
        }
        return eResponse::response(STATUS_API_FALSE, __('account.otp.fail'), $email);
    }

    public function tokenForResetPassword(GetResetPasswordRequest $request)
    {
        $token = $request->get('token');
        $cacheExists = eCache::get($token);
        if (empty($cacheExists)) {
            return eResponse::response(STATUS_API_FALSE, __('notification.system.token-expired'));
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.system.accept-access'), ['token' => $token]);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            DB::beginTransaction();
            $id = $request->get('id');
            $email = Str::lower($request->get('email'));
            $password = $request->get('password');
            $array = [
                'id' => $id,
                'email' => $email
            ];
            $resetAccount = $this->accountService->resetPasswordForAccount($array, $password);
            if (!empty($resetAccount)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.reset-password-success'));
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.token-expired'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    private function getMainSpace($id)
    {
        $account = Account::find($id);
        $data = $account->toArray();
        if (!$account->main_space) {
            $account->load('spaceAccount');
            $getSpaceId = Arr::first($account['spaceAccount']);
            if ($getSpaceId) {
                $data['main_space'] = $getSpaceId ? $getSpaceId['space_id'] : null;
            }
        } else {
            $data['main_space'] = $account->main_space;
        }
        return $data['main_space'];
    }

    private function outputSignIn($account, $isRoot, $keyPlatform)
    {
        // $permissions = $this->decentralizationService->getPermissions(@$account->decentralization_id, $isRoot);
        // $decentralization = $this->decentralizationRepository->getOneArrayDecentralizationWithPermissionByFilter(['id' => (int)$account->decentralization_id, 'deleted_at' => true]);
        // $accountArray = $account->toArray();
        // $mainSpace = $this->getMainSpace($account->id);
        // $output = [
        //     'id' => $account->id,
        //     'name' => $account->name,
        //     'email' => $account->email,
        //     "is_active" => $account->is_active,
        //     "is_root" => $account->is_root,
        //     "username" => $account->username,
        //     "organization_id" => !empty($account->organization_id) ? $account->organization_id : '',
        //     "decentralization_id" => $account->decentralization_id,
        //     "decentralization" => [
        //         "id" => !empty($decentralization) ? $decentralization['id'] : '',
        //         "name" => !empty($decentralization) ? $decentralization['name'] : ''
        //     ],
        //     "phone_number" => $account->phone_number,
        //     'language' => $account->language,
        //     // 'token' => $accountArray[Account::KEY_SIGN_IN[$keyPlatform]],
        //     'token' => $account->token,
        //     'is_leader' => $account->is_leader,
        //     'space_id' => $account->space_id,
        //     'is_first_login' => $account->is_first_login,
        //     'permissions' => $permissions,
        //     'avatar' => !empty($account->profileImage->source) && file_exists(public_path() . '/' . $account->profileImage->source) ? asset($account->profileImage->source) : '',
        //     'cover' => !empty($account->backgroundImage->source) && file_exists(public_path() . '/' . $account->backgroundImage->source) ? asset($account->backgroundImage->source) : '',
        //     'main_space' => !empty($account->main_space) ? $account->main_space : $mainSpace,
        // ];

        // $organization = Organization::where('id', $account['organization_id'])->first();

        // if ($organization) {
        //     $now = new \DateTime(now());
        //     $date = new \DateTime($organization->exp_date);

        //     $exp_date = $now->diff($date)->format("%r%a");
        // } else {
        //     $exp_date = null;
        // }

        $permissions = $this->decentralizationService->getPermissions(@$account['decentralization_id'], $account['is_root']);
        $decentralization = $this->decentralizationRepository->getOneArrayDecentralizationWithPermissionByFilter(['id' => (int)$account['decentralization_id'], 'deleted_at' => true]);
        $mainSpace = $this->getMainSpace($account['id']);
        $output = [
            'id' => $account['id'],
            'name' => $account['name'],
            'email' => $account['email'],
            "is_active" => $account['is_active'],
            "is_check_active" => $account['is_check_active'],
            "is_root" => $account['is_root'],
            "username" => $account['username'],
            "organization_id" => !empty($account['organization_id']) ? $account['organization_id'] : '',
            "decentralization_id" => $account['decentralization_id'],
            "decentralization" => [
                "id" => !empty($decentralization) ? $decentralization['id'] : '',
                "name" => !empty($decentralization) ? $decentralization['name'] : ''
            ],
            "phone_number" => $account['phone_number'],
            'language' => $account['language'],
            // 'token' => $accountArray[Account::KEY_SIGN_IN[$keyPlatform]],
            'token' => $account['token'],
            'is_leader' => $account['is_leader'],
            'space_id' => $account['space_id'],
            'is_first_login' => $account['is_first_login'],
            'permissions' => $permissions,
            'avatar' => !empty($account['avatar']) && file_exists(public_path() . '/' . $account['avatar']) ? asset($account['avatar']) : '',
            'cover' => !empty($account['cover']) && file_exists(public_path() . '/' . $account['cover']) ? asset($account['cover']) : '',
            'main_space' => !empty($account['main_space']) ? $account['main_space'] : $mainSpace,
            'type_account' => $account['type_account'],
            // 'exp_date' => $exp_date,
        ];

        return $output;
    }

    public function activeAccount(SendCodeActiveAccount $request)
    {
        $email = $request['email'];
        $resetPassword = $request->input('is_reset_password', Account::RESET_NO_PASSWORD_KEY_API);
        $code = $request['code'];
        try {
            DB::beginTransaction();
            $keyRedis = Account::keyRedisOtpForEmail($email);
            $data = eFunction::getRedis($keyRedis);
            $lsObj = [];
            if (!is_null($data)) {
                $lsObj = \GuzzleHttp\json_decode($data);
                if ($lsObj->code == $code) {
                    // start trường hợp check otp của reset password
                    if ($resetPassword == Account::RESET_YES_PASSWORD_KEY_API) {
                        $tpl = [
                            'id' => $lsObj->id,
                            'email' => $lsObj->email,
                        ];
                        eFunction::delRedis($keyRedis);
                        DB::commit();
                        return eResponse::response(STATUS_API_SUCCESS, __('account.otp.oke'), $tpl);
                    }
                    //end
                    $checkAccount = $this->accountService->activeAccount($lsObj);
                    if ($checkAccount === false) {
                        return eResponse::response(STATUS_API_FALSE, __('account.active.fail'), $lsObj);
                    }
                    if ($checkAccount === true) {
                        $array = [
                            'email' => $lsObj->email,
                            'url' => $lsObj->code,
                            'name' => $lsObj->name,
                            'username' => $lsObj->username,
                            'password' => '',
                        ];
//                        $this->accountService->sendRabbitMQForMail($array, Account::NEW_ACCOUNT, $array['password'], '', '');
                        $this->accountService->sendRabbitMQForMail($array, Account::NEW_ACCOUNT_GOOGLE, $array['password'], '', '');
                        // cho vào login luôn
                        // Check thiết bị đăng nhập
                        $platform = $request->header('Platform', '');
                        $keyPlatform = eFunction::checkPlatformLogin($platform);

                        if (empty($keyPlatform)) {
                            return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
                        }
                        $dataToken = [
                            'ip' => $request->ip(),
                            'location' => !empty(eFunction::getLocationWithIp($request->ip())) ? eFunction::getLocationWithIp($request->ip()) : null,
                            'device_name' => !empty($data['device_name']) ? $data['device_name'] : null,
                            'user_agent' => $request->header('User-Agent') ?? null,
                        ];
                        $accountExists = $this->accountRepository->getOneArrayAccountByFilter(['email' => $email]);
                        $account = $this->accountAccessTokensRepository->createTokenAccount($accountExists, $dataToken);
                        $output = $this->outputSignIn($account, @$account['is_root'], $keyPlatform);
                        eFunction::delThrottleLogin($accountExists['id'], Account::STRING_REDIS);
                        // send mail chào mừng
                        $data = [
                            'name' => !empty($account['name']) ? $account['name'] : '',
                            'email' => !empty($account['email']) ? $account['email'] : '',
                            'phone' => !empty($account['phone_number']) ? $account['phone_number'] : ''
                        ];
                        $tpl = [
                            'pattern' => Account::PATTERN[Account::NEW_ORGANIZATION],
                            'data' => eCrypt::encryptAES(json_encode($data))
                        ];
//                        $this->accountService->sendRabbitMQForMail($account, Account::NEW_ACCOUNT, '', '', '');
//                        eFunction::sendMessageQueueFormSendEmail($tpl, Account::QUEUE_NAME_SEND_EMAIL);
                        // end login vào luôn
                        eFunction::delRedis($keyRedis);
                        DB::commit();
                        return eResponse::response(STATUS_API_SUCCESS, __('account.active.success'), $output);
                    }
                } else {
                    return eResponse::response(STATUS_API_FALSE, __('account.otp.fail.time'), $lsObj);
                }
            }
            if ($resetPassword == Account::RESET_YES_PASSWORD_KEY_API) {
                return eResponse::response(STATUS_API_FALSE, __('account.otp.fail'), $email);
            }
            return eResponse::response(STATUS_API_FALSE, __('account.otp.fail.time'), $lsObj);
            DB::commit();
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_FALSE, __('account.active.fail'), $code);
        }
    }

    public function sendCodeActiveAccount(SendCodeActiveAccount $request)
    {
        $email = $request['email'];
        $keyRedis = Account::keyRedisOtpForEmail($email);
        //        $keyOldRedis = eFunction::getRedis($keyRedis);
        $resetPassword = $request->input('is_reset_password', Account::RESET_NO_PASSWORD_KEY_API);
        $idMeLogin = $this->accountService->findAccountForEmail($email);
        if ($idMeLogin === false) {
            return eResponse::response(STATUS_API_FALSE, __('account.active.fail'), $idMeLogin);
        }
        $name = $request['name'];
        $randomNumberCode = eFunction::randomNumber(6);
        $lsObj['email'] = $email;
        $lsObj['id'] = $idMeLogin['id'];
        $lsObj['name'] = $name;
        $lsObj['username'] = $name;
        $lsObj['code'] = $randomNumberCode;
        $lsObj['url'] = $randomNumberCode;
        //        if (!empty($keyOldRedis)) {
        //            $timeTllOld = eFunction::getRedisExpiredTime($keyRedis);
        //            if ($timeTllOld > 180) {
        //                return eResponse::response(STATUS_API_FALSE, __('account.send-code-active.waining.time'));
        //            }
        //        }
        eFunction::setRedisWithExpiredTime($keyRedis, \GuzzleHttp\json_encode($lsObj), Base::TIME_SIGN_UP);
        $lsObj['password'] = (isset(request()->password) ? request()->password : '');
        if ($resetPassword == Account::RESET_YES_PASSWORD_KEY_API) {
            $send = $this->accountService->sendEmailToResetPasswordAccount(strtolower($email));
            if (!empty($send)) {
                return eResponse::response(STATUS_API_SUCCESS, __('notification.system.send-mail-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.system.email-not-found'));
        }
        $this->accountService->sendRabbitMQForMail($lsObj, Account::VERIFICATION_ACCOUNT, $lsObj['password'], '', '');
        return eResponse::response(STATUS_API_SUCCESS, __('account.send-code-active.success'), $lsObj);
    }

    public function sendCodeActiveAccountCore($email)
    {
        $idMeLogin = $this->accountService->findAccountForEmail($email);
        if ($idMeLogin === false) {
            return eResponse::response(STATUS_API_FALSE, __('account.active.fail'), $idMeLogin);
        }
        $name = $idMeLogin['name'];
        $randomNumberCode = eFunction::randomNumber(6);
        $lsObj['email'] = $email;
        $lsObj['id'] = $idMeLogin['id'];
        $lsObj['name'] = $name;
        $lsObj['username'] = $name;
        $lsObj['code'] = $randomNumberCode;
        $lsObj['url'] = $randomNumberCode;
        $keyRedis = Account::keyRedisOtpForEmail($email);
        eFunction::setRedisWithExpiredTime($keyRedis, \GuzzleHttp\json_encode($lsObj), Base::TIME_SIGN_UP);
        $this->accountService->sendRabbitMQForMail($lsObj, Account::VERIFICATION_ACCOUNT, '', '', '');
        return eResponse::response(STATUS_API_SUCCESS, __('account.send-code-active.success'), $lsObj);
    }
}
