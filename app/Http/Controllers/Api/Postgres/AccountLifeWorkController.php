<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Models\Postgres\Account;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use Illuminate\Http\Request;
use App\Services\Postgres\AccountLifeWorkServiceInterface;

class AccountLifeWorkController extends Controller
{
    private $serviceAccountLifeWork;
    private $spaceRepository;
    private $accountRepository;

    public function __construct(
        AccountLifeWorkServiceInterface $serviceAccountLifeWork,
        SpaceRepositoryInterface $spaceRepository,
        AccountRepositoryInterface $accountRepository
    )
    {
        $this->serviceAccountLifeWork = $serviceAccountLifeWork;
        $this->spaceRepository = $spaceRepository;
        $this->accountRepository = $accountRepository;
    }

    public function searchAccount($id, Request $request)
    {
        $token = $request->bearerToken();
        $search = $request->filled('search') ? $request->get('search') : '';
        if (!empty($token)) {
            $keyPlatform = $this->getKeyPlatform($request);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            if (!empty($account)) {
                // kiểm tra space đang là dạng nào
                $checkAccountInSpace = $this->spaceRepository->checkSpaceType($account['id'], $id);

                if (empty($checkAccountInSpace)) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('space.create.spaceId'), []);
                }
                $filterAccount = $this->serviceAccountLifeWork->filterByAccountService($id, $checkAccountInSpace, $search);
                if ($checkAccountInSpace === Space::TYPE_SPACE_CUSTOMER) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $filterAccount);
                } else {
//                    $result = [];
//                    foreach ($filterAccount as $key => $item) {
//                        $result[] = $item['account'];
//                    }
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $filterAccount);
                }
            }
        }

    }

    private function getKeyPlatform($request)
    {
        // Check thiết bị đăng nhập
        $platform = $request->header('Platform', '');
        $keyPlatform = eFunction::checkPlatformLogin($platform);

        if (empty($keyPlatform)) {
            return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
        }

        return $keyPlatform;
    }
}
