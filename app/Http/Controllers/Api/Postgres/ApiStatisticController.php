<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\InfoAccount\ChangePasswordRequest;
use App\Http\Requests\Api\InfoAccount\UpdateAccountRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\Api\Statistic\StatisticAccountByDayRequest;
use App\Http\Requests\Api\Statistic\StatisticAppByDayRequest;
use App\Models\Postgres\Application;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use Carbon\Carbon;
use  DB;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\RoomRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\StatisticApplicationRepositoryInterface;
use App\Repositories\Postgres\StatisticAccountRepositoryInterface;
use Illuminate\Http\Request;

class ApiStatisticController extends Controller
{
    protected $accountService;
    protected $organizationService;
    protected $accountRepository;
    protected $roomRepository;
    protected $spaceRepository;
    protected $applicationRepository;

    public function __construct(
        AccountServiceInterface $accountService,
        OrganizationServiceInterface $organizationService,
        AccountRepositoryInterface $accountRepository,
        RoomRepositoryInterface $roomRepository,
        SpaceRepositoryInterface $spaceRepository,
        ApplicationRepositoryInterface $applicationRepository
    ) {
        $this->accountService = $accountService;
        $this->organizationService = $organizationService;
        $this->accountRepository = $accountRepository;
        $this->roomRepository = $roomRepository;
        $this->spaceRepository = $spaceRepository;
        $this->applicationRepository = $applicationRepository;
    }

    public function index(PaginationRequest $request)
    { }

    public function visit(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->spaceRepository->updateVisit(4, $accountInfo);
    }

    public function listCount(Request $request)
    {
        $getRequest = $request->only([
            'spaceId', 'roomId', 'startDate', 'endDate'
        ]);

        $accountInfo = $request->get('accountInfo');

        $data = $this->spaceRepository->statictis($getRequest, $accountInfo);

        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }

    public function countSpaceRoomForAccount(Request $request)
    {
        $accountInfo = $request->get('accountInfo');

        $data = $this->accountRepository->countSpaceAndRoom($request->all(), $accountInfo);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }

    public function statisticApp(Request $request){
        $filters = $request->only([
            "page",
            "limit",
            "date_at",
            "date_start",
            "date_end",
            "space_id",
            "room_id",
            "name", // application name
        ]);
        $filters['organization_id'] = $request->accountInfo['organization_id'];
        $data = $this->applicationRepository->statisticApplication($filters);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }

    public function statisticAppByDay(StatisticAppByDayRequest $request){
        $page = (int)$request->get('page',1);
        $page = $page-1;
        $limit = (int)$request->get('limit',30);
        $date_start = $request->get('date_start');
        $date_end = $request->get('date_end');
        $filters = [
            'organization_id' => $request->accountInfo['organization_id'],
            'application_id' => $request->get('application_id'),
            'date_start' => $date_start,
            'date_end' => $date_end,
        ];
        $filters['organization_id'] = $request->accountInfo['organization_id'];
        $result = $this->applicationRepository->statisticApplicationByDay($filters);
        $totalRecord = count($result);
        $totalPage = ceil($totalRecord/$limit);
        $chunks = array_chunk($result,$limit);
        if($page >= $totalPage){
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
        }
        $data = [
            'total' => $totalPage,
            'per_page' => $limit,
            'current_page' => $page+1,
            'last_page' => $totalPage,
            'data' => $chunks[$page]
        ];
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }

    public function statisticAppByDayDetail(Request $request){
        $filters = $request->only([
            'application_id',
            'date_at',
            'name',
        ]);
        $filters['organization_id'] = $request->accountInfo['organization_id'];
        $filters['limit'] = $request->get('limit',30);
        $data = $this->applicationRepository->statisticApplicationByDayDetail($filters);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }
    public function statisticAccountByDay(StatisticAccountByDayRequest $request){
        $filters = $request->only([
            'account_id',
            'date_start',
            'date_end',
            'space_id',
            'room_id',
        ]);
        $filters['organization_id'] = $request->accountInfo['organization_id'];
        $filters['limit'] = $request->get('limit',30);
        $data = $this->applicationRepository->statisticAccountByDay($filters);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
    }
}
