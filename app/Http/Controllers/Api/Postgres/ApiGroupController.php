<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Postgres\GroupRepositoryInterface;
use App\Http\Requests\Api\Group\GroupInsertRequest;
use App\Http\Requests\Api\Group\GroupDeleteRequest;
use Exception;
use App\Elibs\eResponse;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Api\Group\GroupUpdateRequest;

class ApiGroupController extends Controller
{
    private $groupRepository;

    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    public function index(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->groupRepository->listGroupWithPaginate($request->all(), $accountInfo);
    }

    public function store(GroupInsertRequest $request)
    {
        try {
            $accountInfo = $request->get('accountInfo');
            return $this->groupRepository->createGroup($request->all(), $accountInfo);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function show($id)
    {
        return $this->groupRepository->findId($id);
    }

    public function update(GroupUpdateRequest $request, $id)
    {
        try {
            $accountInfo = $request->get('accountInfo');
            return $this->groupRepository->updateGroup($id, $request->all(), $accountInfo);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function destroy($id)
    {
        try {
            return $this->groupRepository->deleteGroup($id);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteMany(GroupDeleteRequest $request)
    {
        try {
            return $this->groupRepository->deleteMany($request->all());
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }
}
