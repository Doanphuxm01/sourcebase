<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\Postgres\ApplicationStore;
use App\Models\Postgres\LifeNotifications;
use App\Models\Postgres\Space;
use Illuminate\Http\Request;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Models\Postgres\Application;
use App\Elibs\eResponse;
use Illuminate\Support\Facades\DB;
use App\Services\Postgres\ApplicationServiceInterface;
use App\Services\Postgres\OrganizationServiceInterface;
use App\Services\Postgres\ImageServiceInterface;
use App\Models\Postgres\Image;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\Api\QuickBar\QuickBarCreateRequest;
use App\Http\Requests\Api\QuickBar\QuickBarUpdateRequest;
use App\Http\Requests\Api\QuickBar\QuickBarDeleteRequest;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Models\Postgres\QuickBar;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\ApplicationStoreRepositoryInterface;
use App\Repositories\Postgres\GroupRepositoryInterface;

class ApiQuickBarController extends Controller
{
    protected $quickBarRepository;
    protected $applicationService;
    protected $organizationService;
    protected $imageService;
    protected $spaceRepository;
    protected $notificationRepository;
    protected $applicationRepository;
    protected $applicationStoreRepository;
    protected $groupRepository;

    public function __construct(
        QuickBarRepositoryInterface $quickBarRepository,
        ApplicationServiceInterface $applicationService,
        OrganizationServiceInterface $organizationService,
        ImageServiceInterface $imageService,
        SpaceRepositoryInterface $spaceRepository,
        NotificationRepositoryInterface $notificationRepository,
        ApplicationStoreRepositoryInterface $applicationStoreRepository,
        GroupRepositoryInterface $groupRepository,
        ApplicationRepositoryInterface $applicationRepository
    ) {
        $this->quickBarRepository = $quickBarRepository;
        $this->applicationService = $applicationService;
        $this->organizationService = $organizationService;
        $this->imageService = $imageService;
        $this->spaceRepository = $spaceRepository;
        $this->notificationRepository = $notificationRepository;
        $this->applicationRepository = $applicationRepository;
        $this->applicationStoreRepository = $applicationStoreRepository;
        $this->groupRepository = $groupRepository;
    }

    public function index(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $data = $request->only('spaceId');

        return $this->quickBarRepository->listQuickBarSpaceId($data, $accountInfo);
    }

    public function detail($id, Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $result = $this->quickBarRepository->quickbarDetail($id, $accountInfo);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-success'), $result);
    }

    public function create(QuickBarCreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $dateToday = date('Y-m-d H:i:s');
            // thêm mới từ quibar
            if ($request->is_new) {
                $data = $request->only(
                    [
                        'name',
                        'slug',
                        'link_android',
                        'link_ios',
                        'link_url',
                        'space_id',
                        'enable_all',
                        'application_image_id',
                        'application_file_id'
                    ]
                );
                $applicationImage = $request->file('application_image');
                $data['organization_id'] = $accountInfo['organization_id'];
                $data['created_by'] = $accountInfo['id'];
                $data['type'] = Application::TYPE_APP_BAR;
                if (!empty($applicationImage)) {
                    $data['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $accountInfo, Image::COLLECTION_TYPE_APP);
                }
                if ($request->file) {
                    $file = $this->applicationService->uploadFile($request, $accountInfo);
                    if ($file == false) {
                        return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
                    }
                    $data['application_file_id'] = !empty($file) ? $file : '';
                }

                $application = $this->applicationStoreRepository->create($data);
                $lsSendDataOpenApp = [
                    'application_store' => $this->applicationStoreRepository->getApplicationStore($application->id, $accountInfo),
                ];

                // $application = $this->applicationService->addApplication([], $data, $accountInfo);
                if (!empty($application)) {
                    if ($data['enable_all'] == QuickBar::ENABLE_ALL) {
                        $listAccountInSpace = $this->spaceRepository->listAccountInSpace($data['space_id']);
                        if (!empty($listAccountInSpace)) {
                            foreach ($listAccountInSpace as $el) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $application->id,
                                    'space_id' => (int)$data['space_id'],
                                    'enable_all' => QuickBar::ENABLE_ALL,
                                    'created_at' => $dateToday,
                                    'updated_at' => $dateToday,
                                    'created_by' => $accountInfo['id'],
                                ];
                            }
                            $this->notificationRepository->pushNotiQuickBarEnableAll($accountInfo, $application->id, $listAccountInSpace, $data['space_id'], $lsSendDataOpenApp);
                        }
                    } elseif ($data['enable_all'] == QuickBar::ENABLE_SELECT) {
                        $groupIds = $request->get('groupIds');
                        $accountIds = $request->get('accountIds');
                        array_push($accountIds, $accountInfo['id']);
                        // Nếu truyền group id lên thì check account có trong space thuộc group
                        $accountInGroup = [];
                        if (!empty($groupIds)) {
                            $accountInGroup = $this->groupRepository->listAccountGroupInSpace($groupIds, $data['space_id']);
                        }

                        // Merge 2 array id account và bỏ trùng
                        $accountAddQuickBar = array_unique(array_merge($accountIds, $accountInGroup));
                        if (!empty($accountAddQuickBar)) {
                            foreach ($accountAddQuickBar as $el) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $application->id,
                                    'space_id' => (int)$data['space_id'],
                                    'enable_all' => QuickBar::ENABLE_SELECT,
                                    'created_at' => $dateToday,
                                    'updated_at' => $dateToday,
                                    'created_by' => $accountInfo['id'],
                                ];
                            }
                            $this->notificationRepository->pushNotiQuickBarEnableAll($accountInfo, $application->id, $accountAddQuickBar, $data['space_id'], $lsSendDataOpenApp);
                        }
                    } else {
                        $quickBar = [
                            'account_id' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                            'application_id' => $application->id,
                            'space_id' => $data['space_id'],
                            'enable_all' => QuickBar::ENABLE_ONE,
                            'created_at' => $dateToday,
                            'updated_at' => $dateToday,
                            'created_by' => $accountInfo['id'],
                        ];
                    }

                    if (!empty($quickBar)) {
                        $this->quickBarRepository->insertMulti($quickBar, $accountInfo);
                    }

                    DB::commit();
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
                }

                return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
            } // thêm mới từ room
            else {
                $requestAll = $request->all();
                $applicationIds = $requestAll['application_id'];
                // check ứng dụng đã có từ kho có sẵn
                $quickbarExist = $this->quickBarRepository->getIdAppInQuickbar($applicationIds, $request->space_id, $accountInfo);

                if (!empty($quickbarExist)) {
                    $applicationIds = array_diff($applicationIds, $quickbarExist);
                }

                if (empty($applicationIds)) {
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
                }

                sort($applicationIds);
                $quickBar = [];
                if ($request->enable_all == QuickBar::ENABLE_ALL) {
                    $listAccountInSpace = $this->spaceRepository->listAccountInSpace($request->space_id);
                    if (!empty($listAccountInSpace)) {
                        foreach ($listAccountInSpace as $el) {
                            foreach ($applicationIds as $value) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $value,
                                    'space_id' => $request->space_id,
                                    'enable_all' => $request->enable_all,
                                    'created_at' => $dateToday,
                                    'updated_at' => $dateToday,
                                    'created_by' => $accountInfo['id'],
                                ];
                            }
                        }
                        foreach ($applicationIds as $value) {
                            $lsSendDataOpenApp = [
                                'application_store' => $this->applicationStoreRepository->getApplicationStore($value, $accountInfo),
                            ];
                            $this->notificationRepository->pushNotiQuickBarEnableAll($accountInfo, $value, $listAccountInSpace, $request->space_id, $lsSendDataOpenApp);
                        }
                    }
                } elseif ($request->enable_all == QuickBar::ENABLE_SELECT) {
                    $groupIds = $request->get('groupIds');
                    $accountIds = $request->get('accountIds');
                    array_push($accountIds, $accountInfo['id']);
                    // Nếu truyền group id lên thì check account có trong space thuộc group
                    $accountInGroup = [];
                    if (!empty($groupIds)) {
                        $accountInGroup = $this->groupRepository->listAccountGroupInSpace($groupIds, $requestAll['space_id']);
                    }

                    // Merge 2 array id account và bỏ trùng
                    $accountAddQuickBar = array_unique(array_merge($accountIds, $accountInGroup));
                    if (!empty($accountAddQuickBar)) {
                        foreach ($accountAddQuickBar as $el) {
                            foreach ($applicationIds as $value) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $value,
                                    'space_id' => $request->space_id,
                                    'enable_all' => $request->enable_all,
                                    'created_at' => $dateToday,
                                    'updated_at' => $dateToday,
                                    'created_by' => $accountInfo['id'],
                                ];
                            }
                        }
                        foreach ($applicationIds as $value) {
                            $lsSendDataOpenApp = [
                                'application_store' => $this->applicationStoreRepository->getApplicationStore($value, $accountInfo),
                            ];
                            $this->notificationRepository->pushNotiQuickBarEnableAll($accountInfo, $value, $accountAddQuickBar, $request->space_id, $lsSendDataOpenApp);
                        }
                    }
                } else {
                    foreach ($applicationIds as $value) {
                        $quickBar[] = [
                            'account_id' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                            'application_id' => $value,
                            'space_id' => $request->space_id,
                            'enable_all' => 0,
                            'created_at' => $dateToday,
                            'updated_at' => $dateToday,
                            'created_by' => $accountInfo['id'],
                        ];
                    }
                }
                $this->quickBarRepository->insertMulti($quickBar, $accountInfo);
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }
        } catch (Exception $e) {
            Log::info($e->getMessage());
            Debug::sendNotification($e);
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function update(QuickBarUpdateRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'type',
                'link_android',
                'link_ios',
                'link_url',
                'space_id',
                'enable_all',
                'application_image_id',
            ]
        );
        try {
            DB::beginTransaction();
            $id = $request->get('application_id');
            $accountInfo = $request->get('accountInfo');

            $checkOwnQuickbar = $this->quickBarRepository->checkOwnQuickbar($id, $accountInfo, $data['space_id']);

            if (empty($checkOwnQuickbar)) {
                return eResponse::response(STATUS_API_FALSE, __('notification.api-has-permission-quickbar'));
            }

            $data['organization_id'] = $accountInfo['organization_id'];
            $data['created_by'] = $accountInfo['id'];

            if ($request->hasFile('application_image')) {
                $applicationImage = $request->file('application_image');
                $data['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $accountInfo, Image::COLLECTION_TYPE_APP);
            }

            // Lấy thông tin application quickbar cũ
            $quickBarData = $this->quickBarRepository->getOneQuickBar($id, $data['space_id'], $accountInfo);
            if (!empty($quickBarData)) {
                $checkEnable = $quickBarData->enable_all;
            }
            $listAccountInSpace = $this->spaceRepository->listAccountInSpaceOtherAccount($data['space_id'], $accountInfo['id']);
            $accountAddQuickBar = [];
            if (!empty($data['link_url'])) {
                $this->applicationService->deleteFileTransferFilesViaLink($id, Application::DB_APPLICATION_STORE);
            }
            if ($request->hasFile('file') == false) {
                $idFileOldToApplicationStore = $this->applicationStoreRepository->find($id);
                $data['application_file_id'] = $idFileOldToApplicationStore->application_file_id;
            }
            if ($request->hasFile('file')) {
                // xoá file khi quibar đang ở dạng file chuyển qua dạng link
                $file = $this->applicationService->uploadFile($request->all(), $accountInfo);
                if ($file == false) {
                    return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
                }
                $data['application_file_id'] = $file;
            }

            $data_app = [
                'name' => $data['name'],
                'slug' => $data['slug'],
                'type' => $data['type'],
                'link_android' => !empty($data['link_android']) ? $data['link_android'] : null,
                'link_ios' => !empty($data['link_ios']) ? $data['link_ios'] : null,
                'link_url' => !empty($data['link_url']) ? $data['link_url'] : null,
                'application_image_id' => !empty($data['application_image_id']) ? $data['application_image_id'] : null,
                'application_file_id' => !empty($data['application_file_id']) ? $data['application_file_id'] : null,
                'organization_id' => $accountInfo['organization_id'],
            ];
            $imageOld = ApplicationStore::where('id', $id)->first();
            $idImageOld = $imageOld->application_file_id;
            $application = $this->applicationStoreRepository->nUpdate(['id' => $id], $data_app);
            if ($request->hasFile('file')) {
                $this->applicationService->getPatchFileApplication($idImageOld);
            }
            if (!empty($application)) {
                if (isset($data['enable_all'])) {
                    // nếu bật enable all cho tất cả thành viên trong space
                    if ($data['enable_all'] == QuickBar::ENABLE_ALL) {
                        if (!empty($listAccountInSpace)) {
                            // khi có ls account trong space
                            foreach ($listAccountInSpace as $el) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $id,
                                    'space_id' => $data['space_id'],
                                    'enable_all' => QuickBar::ENABLE_ALL,
                                ];
                            }
                        } else {
                            //khi ko ls account trong space
                            $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $id, $data['enable_all']);
                        }
                        // }
                        if (!empty($quickBar)) {
                            // Xóa quickbar cũ
                            $this->quickBarRepository->deleteQuickBarUsingEnable($data['space_id'], $accountInfo['id'], $id);
                            // Thêm mới
                            $this->quickBarRepository->insertMulti($quickBar, $accountInfo);
                            // update
                            $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $id, $data['enable_all']);
                        }
                    } elseif ($data['enable_all'] == QuickBar::ENABLE_SELECT) {
                        $groupIds = $request->get('groupIds');
                        $accountIds = $request->get('accountIds');
                        array_push($accountIds, $accountInfo['id']);
                        // Nếu truyền group id lên thì check account có trong space thuộc group
                        $accountInGroup = [];
                        if (!empty($groupIds)) {
                            $accountInGroup = $this->groupRepository->listAccountGroupInSpace($groupIds, $data['space_id']);
                        }

                        // Merge 2 array id account và bỏ trùng
                        $accountAddQuickBar = array_unique(array_merge($accountIds, $accountInGroup));
                        if (!empty($accountAddQuickBar)) {
                            // khi có ls account trong space
                            foreach ($accountAddQuickBar as $el) {
                                $quickBar[] = [
                                    'account_id' => $el,
                                    'organization_id' => $accountInfo['organization_id'],
                                    'application_id' => $id,
                                    'space_id' => $data['space_id'],
                                    'enable_all' => QuickBar::ENABLE_SELECT,
                                ];
                            }
                        } else {
                            //khi ko ls account trong space
                            $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $id, $data['enable_all']);
                        }
                        // }
                        if (!empty($quickBar)) {
                            // Xóa quickbar cũ
                            $this->quickBarRepository->deleteQuickBarUsingEnable($data['space_id'], $accountInfo['id'], $id);
                            // Thêm mới
                            $this->quickBarRepository->insertMulti($quickBar, $accountInfo);
                            // update
                            $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $id, $data['enable_all']);
                        }
                    } else {
                        // Bắn thông báo
                        $this->quickBarRepository->deleteQuickBarUsingEnable($data['space_id'], $accountInfo, $id);
                        $this->quickBarRepository->updateEnable($data['space_id'], $accountInfo['id'], $id, $data['enable_all']);
                    }
                }
                if ($data['enable_all'] == QuickBar::ENABLE_ALL && $checkEnable == QuickBar::ENABLE_ALL) {
                    // Nếu là ứng dụng enable all thì thông báo cập nhật
                    $this->notificationRepository->pushNotiUpdateQuickbar($accountInfo, $id, $listAccountInSpace, $request->space_id, []);
                } else if ($data['enable_all'] == QuickBar::ENABLE_ALL && $checkEnable == QuickBar::ENABLE_ONE) {
                    // Nếu chuyển thành enable all thì thông báo thêm mới
                    $this->notificationRepository->pushNotiQuickBarEnableAll($accountInfo, $id, $listAccountInSpace, $request->space_id, null);
                } else if ($data['enable_all'] == QuickBar::ENABLE_ONE && $checkEnable == QuickBar::ENABLE_ALL) {
                    // Nếu chuyển về no enable thì thông báo xóa
                    $this->notificationRepository->pushNotiDeleteQuickBar($accountInfo, $id, $listAccountInSpace, $request->space_id);
                } else if ($data['enable_all'] == QuickBar::ENABLE_ONE && $checkEnable == QuickBar::ENABLE_ONE) {
                    // Thông báo xóa
                    $this->notificationRepository->pushNotiDeleteQuickBar($accountInfo, $id, $listAccountInSpace, $request->space_id);
                } else if ($data['enable_all'] == QuickBar::ENABLE_SELECT && !empty($accountAddQuickBar)) {
                    // Thông báo cập nhật
                    $this->notificationRepository->pushNotiUpdateQuickbar($accountInfo, $id, $accountAddQuickBar, $request->space_id, []);
                }
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function delete(QuickBarDeleteRequest $request)
    {
        $quickBarId = $request->only('id');
        $accountInfo = $request->get('accountInfo');
        // Thông báo
        $quickBar = QuickBar::where('id', $request->id)->first();
        if (!empty($quickBar) && $quickBar->enable_all == QuickBar::ENABLE_ALL) {
            $listAccountInSpace = $this->spaceRepository->listAccountInSpaceOtherAccount($quickBar->space_id, $accountInfo['id']);
            $this->notificationRepository->pushNotiDeleteQuickBar($accountInfo, $quickBar->application_id, $listAccountInSpace, $quickBar->space_id);
        } else if (!empty($quickBar) && $quickBar->enable_all == QuickBar::ENABLE_SELECT) {
            // Lọc danh sách người sử dụng quickbar
            $listAccountUsingQuickbar = $this->quickBarRepository->listAccountUsingQuickbarOtherAccount($quickBar->space_id, $quickBar->application_id, $accountInfo);
            $this->notificationRepository->pushNotiDeleteQuickBar($accountInfo, $quickBar->application_id, $listAccountUsingQuickbar, $quickBar->space_id);
        }
        /* xoá file */
        if ($quickBar) {
            $idImage = QuickBar::where('id', $quickBarId)->with('application.applicationFile')->first();
            if (!empty($idImage->application->applicationFile)) {
                $this->applicationService->getPatchFileApplication($idImage->application->applicationFile->id);
            }
        }
        return $this->quickBarRepository->deleteQuickBar($quickBarId, $accountInfo);
    }

    public function sort(Request $request)
    {
        return $this->quickBarRepository->sort($request->all());
    }
}
