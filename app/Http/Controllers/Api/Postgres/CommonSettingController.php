<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Models\Postgres\Account;
use App\Models\Postgres\Permission;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Services\Postgres\DecentralizationServiceInterface;
use Illuminate\Http\Request;

class CommonSettingController extends Controller
{

    protected $accountRepository;
    protected $decentralizationService;
    protected $accountAccessTokensRepository;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        DecentralizationServiceInterface $decentralizationService
    ) {
        $this->accountRepository = $accountRepository;
        $this->decentralizationService = $decentralizationService;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
    }

    public function moduleIndex(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$accountInfo['id'], 'deleted_at' => true]);
        $permissions = $this->decentralizationService->getPermissions(@$account['decentralization_id'], $account['is_root']);
        if (!empty($permissions)) {
            $data['permissions'] = $permissions;
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
    }
}
