<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\Api\Post\CreateRequest;
use App\Http\Requests\Api\Post\DeleteRequest;
use App\Http\Requests\Api\Post\DetailReactRequest;
use App\Http\Requests\Api\Post\ReactRequest;
use App\Http\Requests\Api\Post\UpdateRequest;
use App\Models\Postgres\Image;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Repositories\Postgres\PostEmotionRepositoryInterface;
use App\Services\Postgres\FileUploadServiceInterface;
use App\Services\Postgres\PostEmotionServiceInterface;
use App\Services\Postgres\PostServiceInterface;
use  DB;


class ApiPostController extends Controller
{
    protected $postEmotionService;
    protected $postEmotionRepository;
    protected $postService;
    protected $imageRepository;
    protected $fileUploadService;

    public function __construct(PostServiceInterface $postService, PostEmotionServiceInterface $postEmotionService, PostEmotionRepositoryInterface $postEmotionRepository, ImageRepositoryInterface $imageRepository, FileUploadServiceInterface $fileUploadService)
    {
        $this->postEmotionRepository = $postEmotionRepository;
        $this->imageRepository = $imageRepository;
        $this->postService = $postService;
        $this->postEmotionService = $postEmotionService;
        $this->fileUploadService = $fileUploadService;
    }

    public function index(PaginationRequest $request)
    {
        try{
            if (!$request->has('space_id')){
                return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
            }

            $limit = $request->limit();
            $filter = [];
            $accountInfo = $request->get('accountInfo');
            $filter['deleted_at'] = true;
            $filter['space_id'] = $request->get('space_id');
            $filter['organization_id'] = $accountInfo['organization_id'];

            $applications = $this->postService->getListPostByFilter($limit, $filter, $accountInfo);

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'content',
                'space_id',
            ]
        );

        $accountInfo = $request->get('accountInfo');
        $images = $request->file('images');
        $imageIds = $request->get('image_ids', []);

        try{
            DB::beginTransaction();

            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];

            if (!empty($images)){
                $arrImages = [];
                foreach ($images as $image){
                    $arrImages[] = $this->fileUploadService->upload('post', $image,  $accountInfo, Image::COLLECTION_TYPE_POST);
                }

                if (!empty($arrImages)){
                    $ids = [];
                    foreach ($arrImages as $image){
                        $ids[] = $this->imageRepository->insertMultiGetID($image);
                    }

                    if (!empty($ids)){
                        $imageIds = array_merge($imageIds, $ids);
                    }
                }
            }

            $post = $this->postService->addPost($data, $imageIds, $accountInfo);

            if (!empty($post)){
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function update(UpdateRequest $request)
    {
        $data = $request->only(
            [
                'content',
                'space_id',
            ]
        );

        $accountInfo = $request->get('accountInfo');
        $images = $request->file('images');
        $imageIds = $request->get('image_ids', []);
        $id = $request->get('id');

        try{
            DB::beginTransaction();

            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];

            if (!empty($images)){
                $arrImages = [];
                foreach ($images as $image){
                    $arrImages[] = $this->fileUploadService->upload('post', $image,  $accountInfo, Image::COLLECTION_TYPE_POST);
                }

                if (!empty($arrImages)){
                    $ids = [];
                    foreach ($arrImages as $image){
                        $ids[] = $this->imageRepository->insertMultiGetID($image);
                    }

                    if (!empty($ids)){
                        $imageIds = array_merge($imageIds, $ids);
                    }
                }
            }

            $post = $this->postService->updatePost($id, $data, $imageIds, $accountInfo);

            if (!empty($post)){
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function react(ReactRequest $request)
    {
        $data = $request->only(
            [
                'post_id',
                'emotion_id',
                'space_id',
            ]
        );

        $accountInfo = $request->get('accountInfo');

        try{
            DB::beginTransaction();

            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];

            $this->postEmotionRepository->create($data);

            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function listReact(DetailReactRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $filter['space_id'] = $request->get('post_id');
        $filter['post_id'] = $request->get('space_id');
        $filter['organization_id'] = $accountInfo['organization_id'];

        $listPostEmotion = $this->postEmotionService->getAllReact($filter);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $listPostEmotion);
    }

    public function delete(DeleteRequest $request)
    {
        $id = $request->get('id');
        $spaceId = $request->get('space_id');
        $accountInfo = $request->get('accountInfo');

        try{
            DB::beginTransaction();
            $posts = $this->postService->deletePost($id, $spaceId, $accountInfo);
            if (!empty($posts)){
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
            }

            return eResponse::response(STATUS_API_FALSE, __('notification.system.data-not-found'));

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }
}
