<?php

namespace App\Http\Controllers\Api\Postgres;


use App\Elibs\eCache;
use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Http\Controllers\Controller;
use App\Elibs\eResponse;
use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Account;
use App\Models\Postgres\Major;
use App\Models\Postgres\Organization;
use App\Models\Postgres\Permission;
use App\Models\Postgres\Position;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\EmotionRepositoryInterface;
use App\Repositories\Postgres\GroupRepositoryInterface;
use App\Repositories\Postgres\MajorRepositoryInterface;
use App\Repositories\Postgres\PermissionRepositoryInterface;
use App\Repositories\Postgres\PositionRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Services\Postgres\AccountLifeWorkServiceInterface;
use App\Services\Postgres\AccountServiceInterface;
use Composer\Cache;
use  DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class ApiDefaultController extends Controller
{
    protected $majorRepository;
    protected $permissionRepository;
    protected $positionRepository;
    protected $emotionRepository;
    protected $accountService;
    protected $groupRepository;
    protected $spaceRepository;
    private $serviceAccountLifeWork;

    public function __construct(AccountLifeWorkServiceInterface $serviceAccountLifeWork, SpaceRepositoryInterface $spaceRepository, MajorRepositoryInterface $majorRepository, GroupRepositoryInterface $groupRepository, AccountServiceInterface $accountService, EmotionRepositoryInterface $emotionRepository, PermissionRepositoryInterface $permissionRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->accountService = $accountService;
        $this->majorRepository = $majorRepository;
        $this->permissionRepository = $permissionRepository;
        $this->positionRepository = $positionRepository;
        $this->emotionRepository = $emotionRepository;
        $this->groupRepository = $groupRepository;
        $this->spaceRepository = $spaceRepository;
        $this->serviceAccountLifeWork = $serviceAccountLifeWork;
    }

    public function getPosition(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $positions = $this->positionRepository->getAllPositionByFilter(['is_active' => Position::IS_ACTIVE, 'deleted_at' => true]);
        if (!empty($positions) && $accountInfo['language'] !== Account::LANGUAGE_VI) {
            foreach ($positions as $k => $position) {
                $positions[$k]['name'] = Position::LANGUAGE_POSITION[$position['slug']]['en'];
            }
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $positions);
    }

    public function getScale()
    {
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), Organization::SCALE);
    }

    public function getMajor(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $filter['deleted_at'] = true;
        $filter['is_active'] = Major::IS_ACTIVE;
        if ($request->has('key_word')) {
            $filter['key_word'] = $request->get('key_word');
        }

        $majors = $this->majorRepository->getAllMajorByFilter($filter);
        if (!empty($majors) && $accountInfo['language'] !== Account::LANGUAGE_VI) {
            foreach ($majors as $k => $major) {
                $majors[$k]['name'] = Major::LANGUAGE_MAJOR[$major['slug']]['en'];
            }
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $majors);
    }

    public function getPermission(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $permissions = $this->permissionRepository->getAllPermissionByFilter(['is_active' => Permission::IS_ACTIVE, 'deleted_at' => true]);
        if (!empty($permissions) && $accountInfo['language'] !== Account::LANGUAGE_VI) {
            foreach ($permissions as $k => $permission) {
                $permissions[$k]['name'] = Permission::LANGUAGE_PERMISSION[$permission['key']]['en'];
            }
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $permissions);
    }

    public function getLanguage()
    {
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), Account::LANGUAGE);
    }

    public function getWeekdays(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), Account::WEEKDAYS[$accountInfo['language']]);
    }

    public function getEmotion()
    {
        $emotions = $this->emotionRepository->getAllEmotionByFilter([]);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $emotions);
    }

    public function getInfoNewVersion()
    {
        return eResponse::responseNONECRYPT(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), [
            'info' => '
                Bản cập nhật mới có một số tính năng như :
                \n + Bài đăng
                \n + Bình luận
                \n + Thêm mới vào room
            '
        ]);
    }

    public function getAllAccount(BaseRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $filter = [];
        $this->makeFilter($request, $filter);
        $filter['is_active'] = Account::IS_ACTIVE;
        $filter['is_check_active'] = Account::IS_ACTIVE;
        $filter['organization_id'] = $accountInfo['organization_id'];
        $filter['deleted_at'] = true;
        $filter['not']['is_root'] = true;
        /**
         * @params $space_id   => để loại bỏ những account đã có trong space
         * @params $space_type => để filter ra account theo type của space
         * @params $search     => để search thôi
         * */
        $spaceId = (int)$request->filled('space_id') ? $request->get('space_id') : '';
        $spaceType = (int)$request->filled('space_type') ? $request->get('space_type') : '';
        $search = $request->get('search') ?? '';
        $lsArrayAccount = [];
        $accounts = [];
        if (!empty($search)) {
            $filter['key_word'] = $search;
        }
//        if ($spaceId) {
//
//            // tồn tại spaceId thì trừ bỏ mấy cái thằng account đã có trong space đi thôi
//            // $listAccountInSpace = $this->spaceRepository->listAccountInSpaceLifeWork($spaceId);
////            $listAccountInSpace = $this->spaceRepository->listAccountLifeIdInSpaceLifeWork($spaceId);
////            $lsArrayAccount = $listAccountInSpace;
//            $checkTypeSpace = $this->spaceRepository->checkSpaceType(null, (int)$spaceId);
//            if ($checkTypeSpace === Space::TYPE_SPACE_CUSTOMER) {
//                if($spaceType == Account::DONES_PRO) {
//                    $accounts = $this->accountService->getAccountBelongToOrganization($lsArrayAccount, null, $checkTypeSpace, $request->all(), $accountInfo);
//                } else {
//                    $accounts = $this->serviceAccountLifeWork->filterByAccountServiceRemoveAccount($lsArrayAccount, null, $checkTypeSpace, $search, $accountInfo);
//                }
//            }
//            if ($checkTypeSpace === Space::TYPE_SPACE_INTERNAL) {
//                $filter['type_account'] = Account::TYPE_ACCOUNT_STAFF;
//                $accounts = $this->accountService->getAllAccountForSelect($filter);
//                $collect = collect($accounts)->paginate(10)->toArray();
//                sort($collect['data']);
//                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $collect);
//            }
//            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $accounts);
//        }
        if ($spaceType || $spaceId) {
            if($spaceId) {
                $spaceAccount = $this->spaceRepository->listAccountInSpace($spaceId);
            }
            if ($spaceType == Space::TYPE_SPACE_CUSTOMER) {
//                $accounts = $this->serviceAccountLifeWork->filterByAccountServiceRemoveAccount($lsArrayAccount, null, $spaceType, $search, $accountInfo);
                $accounts = $this->accountService->getAllAccountForSelect($filter);
                if(!empty($spaceAccount)) {
                    $collect = collect($accounts)->whereNotIn('id', $spaceAccount)->paginate(10)->toArray();
                } else {
                    $collect = collect($accounts)->paginate(10)->toArray();
                }
                sort($collect['data']);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $collect);
            } else {
                $filter['type_account'] = Account::TYPE_ACCOUNT_STAFF;
                $accounts = $this->accountService->getAllAccountForSelect($filter);
                if(!empty($spaceAccount)) {
                    $collect = collect($accounts)->whereNotIn('id', $spaceAccount)->paginate(10)->toArray();
                } else {
                    $collect = collect($accounts)->paginate(10)->toArray();
                }
                sort($collect['data']);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $collect);
            }
        }
        /**
         * lấy all account bên nội bộ
         */
        $accounts = $this->accountService->getAllAccountForSelect($filter);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $accounts);
    }

    public function list(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->groupRepository->listGroup($accountInfo);
    }

    private function makeFilter($request, &$filter)
    {
        if ($request->has('key_word')) {
            $filter['key_word'] = $request->get('key_word');
        }

//        if ($request->has('decentralization_id')) {
//            $filter['decentralization_id'] = $request->get('decentralization_id');
//        }

        if ($request->has('space_id')) {
            $filter['space_id'] = $request->get('space_id');
        }

        if ($request->has('is_active')) {
            $filter['is_active'] = $request->get('is_active');
        }
    }
}
