<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Comment\CreateRequest;
use App\Http\Requests\Api\PaginationRequest;
use App\Repositories\Postgres\CommentRepositoryInterface;
use App\Services\Postgres\CommentServiceInterface;
use  DB;


class ApiCommentController extends Controller
{
    protected $commentService;
    protected $commentRepository;

    public function __construct(CommentServiceInterface $commentService, CommentRepositoryInterface $commentRepository)
    {
        $this->commentRepository = $commentRepository;
        $this->commentService = $commentService;
    }

    public function index(PaginationRequest $request)
    {
        try {
            if (!$request->has('space_id') || !$request->has('post_id')) {
                return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
            }

            $filter = [];
            $accountInfo = $request->get('accountInfo');
            $filter['deleted_at'] = true;
            $filter['space_id'] = $request->get('space_id');
            $filter['post_id'] = $request->get('post_id');
            $filter['organization_id'] = $accountInfo['organization_id'];

            $applications = $this->commentService->getListCommentByFilter($filter, $accountInfo);

            return eResponse::responsePagination(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $applications);

        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function create(CreateRequest $request)
    {
        $data = $request->only(
            [
                'post_id',
                'space_id',
                'content',
                'parent_id'
            ]
        );

        $accountInfo = $request->get('accountInfo');

        try{
            DB::beginTransaction();

            $data['created_by'] = $accountInfo['id'];
            $data['organization_id'] = $accountInfo['organization_id'];

            $this->commentRepository->create($data);

            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), []);

        }catch(\Exception $e){
            \Log::info($e->getMessage());
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }
}
