<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Models\Postgres\ApplicationStore;
use App\Services\Postgres\ApplicationServiceInterface;
use Illuminate\Http\Request;
use App\Repositories\Postgres\ApplicationStoreRepositoryInterface;
use App\Services\Postgres\ImageServiceInterface;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Elibs\eFunction;
use App\Models\Postgres\Application;
use App\Models\Postgres\Image;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\ApplicationStore\CreateRequest;

class ApiApplicationStoreController extends Controller
{
    protected $applicationStoreRepository;
    protected $imageService;
    protected $applicationRepository;
    protected $quickbarRepository;
    protected $applicationService;

    //
    public function __construct(
        ApplicationStoreRepositoryInterface $applicationStoreRepository,
        ImageServiceInterface $imageService,
        QuickBarRepositoryInterface $quickbarRepository,
        ApplicationRepositoryInterface $applicationRepository,
        ApplicationServiceInterface $applicationService
    ) {
        $this->applicationStoreRepository = $applicationStoreRepository;
        $this->imageService = $imageService;
        $this->applicationRepository = $applicationRepository;
        $this->quickbarRepository = $quickbarRepository;
        $this->applicationService = $applicationService;
    }

    public function list(Request $request): \Illuminate\Http\JsonResponse
    {
        $accountInfo = $request->get('accountInfo');
        $attributes = $request->only(
            [
                'page',
                'limit',
                'name',
            ]
        );
        if ($request->has('is_system')) {
            $attributes['is_system'] = $request->boolean('is_system');
        }
        $attributes['created_by'] = $accountInfo['id'];
        // $attributes['organization_id'] = $accountInfo['organization_id'];
        $applicationStore = $this->applicationStoreRepository->nList($attributes);
        $applicationStore->load('applicationImage');
        // map remove type application store
        if (isset($attributes['limit'])) {
            $applicationStore = $applicationStore->toArray();
            $applicationStore['data'] = array_map(function ($val) {
                unset($val['type']);
                return $val;
            }, $applicationStore['data']);
        } else {
            $applicationStore = $applicationStore->map(function ($val) {
                unset($val->type);
                return $val;
            });
        }

        return res($applicationStore);
    }

    public function create(CreateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $attributes = $request->only(
            [
                'name',
                'slug',
                'link_android',
                'link_ios',
                'link_url',
                'application_image_id',
            ]
        );
        if (request()->type == Application::TYPE_FILE) {
            unset($attributes['link_url']);
        }
        $attributes['created_by'] = $accountInfo['id'];
        $attributes['organization_id'] = $accountInfo['organization_id'];
        $applicationImage = $request->file('application_image');
        if ($request->hasFile('application_image')) {
            $attributes['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $request->get('accountInfo'), Image::COLLECTION_TYPE_APP);
        }
        if ($request->hasFile('file')) {
            $file = $this->applicationService->uploadFile($request, $accountInfo);
            if ($file == false) {
                return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
            }
            $attributes['application_file_id'] = !empty($file) ? $file : '';
        }
        if (!empty($attributes['link_url'])) {
            $url = eFunction::getHost($attributes['link_url']);
            $url = eFunction::zaloCustomGroupUrl($attributes['link_url']);
            $attributes['x_frame'] = eFunction::getXFrameOptions($url);
            $attributes['domain'] = eFunction::getDomain($attributes['link_url']);
        }
        $applicationStore = $this->applicationStoreRepository->nCreate($attributes);
        $applicationStore->refresh();
        return res($applicationStore);
    }

    public function update(CreateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $attributes = $request->only(
            [
                'name',
                'slug',
                'link_android',
                'link_ios',
                'link_url',
                'application_image_id',
            ]
        );
        if ($request->type == Application::TYPE_FILE) {
            unset($attributes['link_url']);
        }
        $attributes['created_by'] = $accountInfo['id'];
        $attributes['organization_id'] = $accountInfo['organization_id'];
        $applicationImage = $request->file('application_image');
        if ($request->hasFile('application_image')) {
            $attributes['application_image_id'] = $this->imageService->uploadImageForProfile(Application::AVATAR_APPLICATION, $applicationImage, $request->get('accountInfo'), Image::COLLECTION_TYPE_APP);
        }
        // xoá file khi application store đang ở dạng file chuyển qua dạng link
        if (!empty($attributes['link_url'])) {
            $this->applicationService->deleteFileTransferFilesViaLink($request->id, Application::DB_APPLICATION_STORE);
            $url = eFunction::getHost($attributes['link_url']);
            $url = eFunction::zaloCustomGroupUrl($attributes['link_url']);
            $attributes['x_frame'] = eFunction::getXFrameOptions($url);
            $attributes['domain'] = eFunction::getDomain($attributes['link_url']);
        }
        if ($request->hasFile('file')) {
            $file = $this->applicationService->uploadFile($request->all(), $accountInfo);
            if ($file == false) {
                return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
            }
            $attributes['application_file_id'] = $file;
        }
        $imageOld = ApplicationStore::where('id', $request->id)->first();
        $idImageOld = $imageOld->application_file_id;
        $applicationStore = $this->applicationStoreRepository->nUpdate(['id' => $request->id], $attributes);
        $applicationStore = $this->applicationStoreRepository->nView(['id' => $request->id]);
        if ($request->hasFile('file')) {
            $this->applicationService->getPatchFileApplication($idImageOld);
        }
        return res($applicationStore);
    }

    public function delete($id): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();
            $applicationStore = $this->applicationStoreRepository->nView(['id' => $id]); //,'created_by' => Auth::id()
            if ($applicationStore) {
                $applicationStore->delete();
            }
            $idFile = $applicationStore['application_file_id'];
            $this->applicationRepository->getBlankModel()->where('id', $id)->delete();
            /* xoá file */
            if ($idFile) {
                $this->applicationService->getPatchFileApplication($idFile);
            }
            return res($applicationStore);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            Debug::sendNotification($e);
            DB::rollback();
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }

    }

    public function deleteMultiple(Request $request): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'ids' => 'required|array',
            'ids.*' => 'integer'
        ]);
        $number_app_store = $this->applicationStoreRepository->getBlankModel()->whereIn('id', $request->get('ids'))->delete();
        $number_app = $this->applicationRepository->getBlankModel()->whereIn('id', $request->get('ids'))->delete();
        // Delete quickbar
        $this->quickbarRepository->deleteQuickbarApp($request->get('ids'));
        return res([$number_app_store, $number_app]);
    }

    public function view($id): \Illuminate\Http\JsonResponse
    {
        $applicationStore = $this->applicationStoreRepository->nView(['id' => $id]); //,'created_by' => Auth::id()
        if ($applicationStore) {
            $applicationStore->load('applicationImage');
            $applicationStore->load('applicationFile');
            $applicationStore['application_type'] = Application::TYPE_LINK;
            if (!empty($applicationStore->applicationFile)) {
                $applicationStore['application_type'] = Application::TYPE_FILE;
            }
        }
        return res($applicationStore);
    }

    public function countTotal(Request $request): \Illuminate\Http\JsonResponse
    {
        $accountInfo = $request->get('accountInfo');
        $count = $this->applicationStoreRepository->countTotal($accountInfo); //,'created_by' => Auth::id()
        return res($count);
    }
}
