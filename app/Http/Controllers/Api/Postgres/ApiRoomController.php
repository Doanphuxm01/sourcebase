<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Room\SaveApplicationAndRoomRequest;
use App\Models\Postgres\Room;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Services\Postgres\AccountLifeWorkServiceInterface;
use App\Services\Postgres\RoomServiceInterface;
use Illuminate\Http\Request;
use App\Repositories\Postgres\RoomRepositoryInterface;
use App\Http\Requests\Api\Room\RoomCreateRequest;
use App\Http\Requests\Api\Room\RoomUpdateRequest;
use App\Http\Requests\Api\Room\RoomSortRequest;
use  DB;
use App\Http\Requests\Api\Room\RoomAddAccountRequest as AppRoomAddAccountRequest;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Http\Requests\Api\Room\RoomUpdateAdminRequest;

class ApiRoomController extends Controller
{
    private $roomRepository;
    protected $roomService;
    protected $notificationRepository;
    protected $spaceRepository;

    public function __construct(
        RoomRepositoryInterface $roomRepository,
        RoomServiceInterface $roomService,
        NotificationRepositoryInterface $notificationRepository,
        SpaceRepositoryInterface $spaceRepository
    ) {
        $this->roomRepository = $roomRepository;
        $this->roomService = $roomService;
        $this->notificationRepository = $notificationRepository;
        $this->spaceRepository = $spaceRepository;
    }

    public function accountLeave($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $id = $this->roomRepository->accountLeaveRoom($id, $accountInfo);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
        } catch (Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-fail'), []);
        }
    }

    public function index(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $room = $this->roomRepository->listRoomInSpacePaginate($request->all(), $accountInfo);
        if (!empty($room)) {
            $room = $this->roomService->checkClickInRoom($room, $accountInfo);
        }

        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $room);
    }

    public function listAllRoom(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->roomRepository->listAllRoom($request->all(), $accountInfo);
    }

    public function listAccountInRoom($idRoom, Request $request)
    {
        $search = $request->get('search') ?? '';
        // $checkRoomIntypeSpace = $this->roomRepository->getRoomTypeInSpace($idRoom);
        $result = [];
        // if (!empty($checkRoomIntypeSpace)) {
        //     if ($checkRoomIntypeSpace['type'] == 2) {
        // $accounts = $this->spaceRepository->listAccountLifeIdInSpaceLifeWork($checkRoomIntypeSpace['space_id']);
        // $accounts = $this->spaceRepository->searchAccountInSpaceWorkLife($checkRoomIntypeSpace['space_id'], $search);
        // $accounts1 = $this->serviceAccountLifeWork->filterByAccountServiceRemoveAccount($accounts, null, $checkRoomIntypeSpace['type'], $search);
        // $result = collect($accounts)->paginate(10)->toArray();
        // } else {
        $accounts = $this->roomRepository->listAccountInRoom($idRoom, $search);
        // }
        $result = collect($accounts)->paginate(10)->toArray();
        sort($result['data']);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
        // }
        // return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
    }

    public function sort(RoomSortRequest $request)
    {
        return $this->roomRepository->sort($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoomCreateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        // $checkTypeSpace = $this->spaceRepository->checkSpaceType(null, $request['spaceId']);
        // if ($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
        //     return $this->roomRepository->createRoomWorkLife($request->all(), $accountInfo);
        // } else {
        return $this->roomRepository->createRoom($request->all(), $accountInfo);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function addAccount(AppRoomAddAccountRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->roomRepository->addAccount($request->all(), $accountInfo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoomUpdateRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->roomRepository->updateRoom($request->all(), $accountInfo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        // $checkTypeSpace = $this->roomRepository->getRoomTypeInSpace($id);
        // if ($checkTypeSpace['type'] == Space::TYPE_SPACE_CUSTOMER) {
        //     $lsAccountLife = $this->spaceRepository->listAccountLifeIdInSpaceLifeWork($checkTypeSpace['space_id']);
        //     return $this->roomRepository->deleteRoomWorkLife($id, $accountInfo, $lsAccountLife);
        // } else {
        return $this->roomRepository->deleteRoom($id, $accountInfo);
        // }
    }

    public function updateRoomAndAllApplication(SaveApplicationAndRoomRequest $request)
    {
        $data = $request->only(
            [
                'name',
                'slug',
                'type',
                'is_pin',
            ]
        );

        $id = $request->get('id');
        $spaceId = $request->get('space_id');
        $applications = $request->get('applications');
        $accountInfo = $request->get('accountInfo');
        $accountIds = $request->get('accountIds');

        try {
            DB::beginTransaction();
            $isOwnRoom = $this->roomService->checkOutMasterRoom(
                $accountInfo['id'],
                ['space_id' => $spaceId, 'id' => $id, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]
            );
            if (empty($isOwnRoom)) {
                return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
            }
            $data['is_pin'] = (int)$data['is_pin'] === Room::IS_PIN ? Room::IS_PIN : Room::NOT_PIN;
            $data['type'] = (int)$data['type'] === Room::TYPE_ROOM_PUBLIC ? Room::TYPE_ROOM_PUBLIC : Room::TYPE_ROOM_PRIVATE;
            // $checkTypeSpace = $this->spaceRepository->checkSpaceType(null, $spaceId);
            // if ($checkTypeSpace == 2) {
            //     $room = $this->roomService->updateRoomAndAllApplicationWorkLife($id, $spaceId, $applications, $data, $accountInfo, $accountIds);
            // } else {
            $room = $this->roomService->updateRoomAndAllApplication($id, $spaceId, $applications, $data, $accountInfo, $accountIds);
            // }
            if (!empty($room)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_ERROR, __('notification.system.errors'), []);
        }
    }

    public function deleteAccount(Request $request)
    {
        // $checkTypeSpaceInRoom = $this->roomRepository->getRoomTypeInSpace($request['roomId']);
        $accountInfo = $request->get('accountInfo');
        // if ($checkTypeSpaceInRoom['type'] == Space::TYPE_SPACE_CUSTOMER) {
        //     return $this->roomRepository->deleteAccountInRoomWorkLife($request->all(), $accountInfo);
        // } else {
        return $this->roomRepository->deleteAccountInRoom($request->all(), $accountInfo);
        // }
    }

    public function listAccount(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->roomRepository->paginateAccountInRoom($request->all(), $accountInfo);
    }

    public function updateAdmin(RoomUpdateAdminRequest $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            // $roomType = $this->roomRepository->getRoomTypeInSpace($request['room_id']);
            // if ($roomType['type'] == Space::TYPE_SPACE_CUSTOMER) {
            //     $room = $this->roomRepository->updateAdminInRoomWorkLife($request->all(), $accountInfo);
            // }
            // if ($roomType['type'] == Space::TYPE_SPACE_INTERNAL) {
            $room = $this->roomRepository->updateAdminInRoom($request->all(), $accountInfo);
            // }
            if (!empty($room)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
            DB::rollback();
            Debug::sendNotification($e);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }
}
