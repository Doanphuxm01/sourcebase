<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Postgres\MajorRepositoryInterface;
use App\Elibs\eResponse;

class ApiMajorController extends Controller
{
    private $majorRepository;

    public function __construct(MajorRepositoryInterface $majorRepository)
    {
        $this->majorRepository = $majorRepository;
    }
    public function index(Request $request)
    {
        $major = $this->majorRepository->getAllMajorByFilter($request->all());
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $major);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
