<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Elibs\eResponse;
use App\Services\Postgres\FileUploadServiceInterface;
use Illuminate\Support\Facades\Response;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Http\Requests\Api\Image\CollectionUploadRequest;
use App\Http\Requests\Api\Image\CollectionDeleteRequest;
use App\Http\Requests\Api\Image\CollectionViewRequest;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\Image;

class ApiImageController extends Controller
{
    private $fileUploadService;
    private $imageRepository;

    public function __construct(
        FileUploadServiceInterface $fileUploadService,
        ImageRepositoryInterface $imageRepository
    ) {
        $this->fileUploadService = $fileUploadService;
        $this->imageRepository = $imageRepository;
    }

    public function index(CollectionViewRequest $request)
    {
        $accountInfo = $request->get('accountInfo');
        $images = $this->imageRepository->listCollectionWithPaginate($request->all(), $accountInfo);

        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $images);
    }

    public function listIconLibrary(Request $request)
    {
        $images = $this->imageRepository->listCollectionAll($request->all());

        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $images);
    }

    public function collectionUpload(CollectionUploadRequest $request)
    {
        try {
            $accountInfo = $request->get('accountInfo');
            $file = $request->file('file');
            $collectionType = $request->collectionType ?? Image::COLLECTION_TYPE_DEFAULT;
            $fileNew = $this->fileUploadService->upload('store_collection', $file,  $accountInfo, $collectionType);
            $data = $this->imageRepository->create($fileNew);
            return eResponse::response(STATUS_API_SUCCESS, __('image.upload.success'), $data);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('image.upload.fail'));
        }
    }

    public function deleteCollection(CollectionDeleteRequest $request)
    {
        try {
            $image = $this->imageRepository->deleteCollection($request->all());
            if (!empty($image)) {
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), []);
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function typeCollection()
    {
        $data = [
            ['id' => 1, 'type' => 'Tất cả'],
            ['id' => 2, 'type' => 'Ảnh space'],
            ['id' => 3, 'type' => 'Ảnh ứng dụng'],
            ['id' => 4, 'type' => 'Ảnh timeline'],
            ['id' => 5, 'type' => 'Ảnh đại diện'],
        ];
        return eResponse::response(STATUS_API_SUCCESS, null, $data);
    }

    public function listFile(Request $request)
    {
        $search = str_replace(' ', '-', $request->search);
        $token = $request->bearerToken();
        if (!empty($token)) {
            $accountInfo = $request->get('accountInfo');
            $fileApplication = $this->imageRepository->listFileApplication($accountInfo['id'], $search);
            return eResponse::response(STATUS_API_SUCCESS, __('file.list'), $fileApplication);
        }
        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);

    }
}
