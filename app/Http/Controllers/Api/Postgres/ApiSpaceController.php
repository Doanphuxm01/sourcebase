<?php

namespace App\Http\Controllers\Api\Postgres;

use App\Elibs\Debug;
use App\Elibs\eCache;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PaginationRequest;
use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Space;
use App\Services\Postgres\AccountLifeWorkServiceInterface;
use  DB;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\Image;
use App\Services\Postgres\FileUploadServiceInterface;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Http\Requests\Api\Space\SpaceCreateRequest;
use App\Http\Requests\Api\Space\SpaceUpdateRequest;
use App\Http\Requests\Api\Space\SpaceDeleteRequest;
use App\Http\Requests\Api\Space\SpaceAccountDeleteRequest;
use App\Http\Requests\Api\Space\SpaceUpdateAccountStatusRequest;
use App\Http\Requests\Api\Space\SpaceDetailRequest;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Models\Postgres\Notification;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Http\Requests\Api\Space\SpaceUpdateAdmin;
use App\Models\Postgres\Account;
use App\Services\Postgres\DecentralizationServiceInterface;
use App\Models\Postgres\Permission;
use App\Http\Requests\Api\Space\SpaceNoPerRequest;
use Throwable;
use App\Http\Requests\Api\Space\ApproveJoinSpaceRequest;
use App\Http\Requests\Api\Space\UpdateShowSpaceRequest;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;

class ApiSpaceController extends Controller
{
    protected $fileUploadService;
    protected $imageRepository;
    protected $spaceRepository;
    protected $accountRepository;
    protected $notificationRepository;
    protected $decentralizationService;
    protected $serviceAccountLifeWork;
    protected $quickBarRepository;
    /**
     * @var AccountAccessTokensRepositoryInterface
     */
    protected $accountAccessTokensRepository;

    public function __construct(
        FileUploadServiceInterface $fileUploadService,
        ImageRepositoryInterface $imageRepository,
        SpaceRepositoryInterface $spaceRepository,
        AccountRepositoryInterface $accountRepository,
        DecentralizationServiceInterface $decentralizationService,
        NotificationRepositoryInterface $notificationRepository,
        QuickBarRepositoryInterface $quickBarRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        AccountLifeWorkServiceInterface $serviceAccountLifeWork
    ) {
        $this->fileUploadService = $fileUploadService;
        $this->imageRepository = $imageRepository;
        $this->spaceRepository = $spaceRepository;
        $this->accountRepository = $accountRepository;
        $this->notificationRepository = $notificationRepository;
        $this->decentralizationService = $decentralizationService;
        $this->serviceAccountLifeWork = $serviceAccountLifeWork;
        $this->quickBarRepository = $quickBarRepository;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
    }

    // private function checkHasPermission($decentralization_id, $isRoot, $nameModule, $namePer)
    // {
    //     $permissions = $this->decentralizationService->getPermissions(@$decentralization_id, $isRoot);
    //     if (!empty($permissions)) {
    //         if ($permissions[$nameModule][$namePer] == Permission::HAS_PERMISSION) {
    //             return true;
    //         } else {
    //             return eResponse::response(STATUS_API_ERROR, __('notification.system.not-have-access'), []);
    //         }
    //     }

    //     return eResponse::response(STATUS_API_ERROR, __('notification.system.not-have-access'), []);
    // }

    public function accountLeave($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $data = $this->spaceRepository->accountLeaveSpace($id, $accountInfo);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
        } catch (Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-fail'), []);
        }
    }

    public function listSpaceWaitApprove(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $result = $this->spaceRepository->listSpaceWaitApprove($accountInfo);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
    }

    public function listAccountWaitApprove(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        $data = $request->only([
            'id',
            'order_by',
            'key_word',
            'page',
            'limit',
        ]);
        $result = $this->spaceRepository->listAccountWaitApprove($accountInfo, $data);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
    }

    private function getKeyPlatform($request)
    {
        // Check thiết bị đăng nhập
        $platform = $request->header('Platform', '');
        $keyPlatform = eFunction::checkPlatformLogin($platform);

        if (empty($keyPlatform)) {
            return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
        }

        return $keyPlatform;
    }

    public function index(Request $request)
    {
        $accountInfo = $request->get('accountInfo');
        return $this->spaceRepository->getAllListSpace($request->all(), $accountInfo);
    }

    public function searchRoomApplication(Request $request)
    {
        $keyWord = $request->only(['name', 'space_id']);
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);

            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);

            if (!empty($account)) {
                $data = $this->spaceRepository->searchRoomApplication($keyWord, $account);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
            }
        }
        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);
    }

    public function indexNoPermission(Request $request)
    {
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);
            if (!empty($account)) {
                //                return $this->spaceRepository->listSpace($request->all(), $account);
                return $this->spaceRepository->getAllListSpace($request->all(), $account);
            }
        }

        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);
    }

    public function listAccount($id)
    {
        //        $key = 'SpaceListAccount:'.$id;
        //        if($cache = eCache::getRequests($key)){
        //            return $cache;
        //        }
        $data = $this->spaceRepository->listAccount($id, null, null);
        //        eCache::setRequests($key, $data);
        return $data;
    }

    public function listAccountNoPermission($id, Request $request)
    {
        $search =  $request->get('search') ?? '';
        $type =  $request->get('type_account') ?? null;
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);

            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);
            $checkAccountInSpace = $this->spaceRepository->checkSpaceType($account_id, $id);
            if (empty($checkAccountInSpace)) {
                return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('space.create.spaceId'), []);
            }
            if (!empty($account_id)) {
                // if ($checkAccountInSpace === Space::TYPE_SPACE_CUSTOMER) {
                //     $accounts = $this->spaceRepository->listAccountWorkLife($id, $search);
                //     Log::info($accounts);
                //     $result = collect($accounts)->paginate(10)->toArray();
                //     sort($result['data']);
                //     return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
                // }
                $checkAccountInSpace = $this->spaceRepository->checkAccountInSpace($account_id, $id);
                if (empty($checkAccountInSpace)) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('space.create.spaceId'), []);
                }
                $accounts = $this->spaceRepository->listAccount($id, $search, $type);
                $result = collect($accounts)->paginate(10)->toArray();
                sort($result['data']);
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
            }
        }

        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);
    }

    public function create(SpaceCreateRequest $request)
    {

        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $collectionType = $request->collectionType ?? Image::COLLECTION_TYPE_SPACE;
                $fileNew = $this->fileUploadService->upload('store_collection', $file, $accountInfo, $collectionType);
                $image = $this->imageRepository->create($fileNew);
                $collectionId = $image['id'];
            } else if ($request->collectionId) {
                $collectionId = $request->collectionId;
                $this->imageRepository->updateCollectionType($collectionId, Image::COLLECTION_TYPE_SPACE);
            } else {
                $collectionId = null;
            }
            $data = $this->spaceRepository->createSpace($request->all(), $accountInfo, $collectionId);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), $data);
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function addAccount(Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $space = $this->spaceRepository->addAccount($request->all(), $accountInfo);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function update(SpaceUpdateRequest $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            //check permission
            // $this->checkHasPermission(@$accountInfo['decentralization_id'], $accountInfo['id'], 'space', 'update');
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $collectionType = $request->collectionType ?? Image::COLLECTION_TYPE_SPACE;
                $fileNew = $this->fileUploadService->upload('store_collection', $file, $accountInfo, $collectionType);
                $image = $this->imageRepository->create($fileNew);
                $collectionId = $image['id'];
            } else {
                if ($request->collectionId) {
                    $collectionId = $request->collectionId;
                    $this->imageRepository->updateCollectionType($collectionId, Image::COLLECTION_TYPE_SPACE);
                } else {
                    $collectionId = null;
                }
            }
            $id = $request['spaceId'];
            // $spaceType = $this->spaceRepository->checkSpaceType(null, $id);
            // if ($spaceType != $request['type']) {
            //     return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
            // }
            // if ($spaceType === Space::TYPE_SPACE_CUSTOMER) {
            //     $data = $this->spaceRepository->updateSpaceLifeWork($request->all(), $accountInfo, $collectionId);
            // } else {
            $data = $this->spaceRepository->updateSpace($request->all(), $accountInfo, $collectionId);
            // }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), $data);
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function isAdminUpdate(SpaceUpdateAdmin $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            // $id = $request['space_id'];
            // $spaceType = $this->spaceRepository->checkSpaceType(null, $id);
            // if ($spaceType === Space::TYPE_SPACE_CUSTOMER) {
            //     $status = $this->spaceRepository->updateAdminInSpaceLifeWork($request->all(), $accountInfo);
            // } else {
            $status = $this->spaceRepository->updateAdminInSpace($request->all(), $accountInfo);
            // }
            if (!empty($status)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function delete(SpaceDeleteRequest $request)
    {
        $accountInfo = $request->get('accountInfo');

        //check permission
        // $this->checkHasPermission(@$accountInfo['decentralization_id'], $accountInfo['id'], 'space', 'delete');

        return $this->spaceRepository->deleteSpace($request->all(), $accountInfo);
    }

    public function deleteAccount(SpaceAccountDeleteRequest $request)
    {
        $accountInfo = $request->get('accountInfo');

        //check permission
        $id = $request['spaceId'];
        // $this->checkHasPermission(@$accountInfo['decentralization_id'], $accountInfo['id'], 'space', 'update');
        // $spaceType = $this->spaceRepository->checkSpaceType(null, $id);
        // if ($spaceType === Space::TYPE_SPACE_CUSTOMER) {
        //     return $this->spaceRepository->deleteAccountInSpaceLifeWork($request->all(), $accountInfo);
        // } else {
        return $this->spaceRepository->deleteAccountInSpace($request->all(), $accountInfo);
        // }
    }

    public function listGroupInSpace($id)
    {
        return $this->spaceRepository->listGroupInSpace($id, null);
    }

    public function listGroupInSpaceNoPermission($id, Request $request)
    {
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);

            if (!empty($account)) {
                $checkAccountInSpace = $this->spaceRepository->checkAccountInSpace($account['id'], $id);
                if (empty($checkAccountInSpace)) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('space.create.spaceId'), []);
                }
                $type = $request->get('type');
                $result = $this->spaceRepository->listGroupInSpace($id, $type);
                if (!empty($result)) {
                    $data = collect($result)->paginate(10)->toArray();
                    return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $data);
                }
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), []);
            }
        }

        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);
    }

    public function findById($id, Request $request)
    {
        // $spaceType = $this->spaceRepository->checkSpaceType(null, $id);
        // if ($spaceType === Space::TYPE_SPACE_CUSTOMER) {
        //     return $this->spaceRepository->finByIdLifeWork($id, $request->all());
        // } else {
        return $this->spaceRepository->findById($id, $request->all());
        // }
        // $accountInfo = $request->get('accountInfo');
        // $this->spaceRepository->updateVisit($id, $accountInfo);

    }

    public function findByIdNoPermission($id, Request $request)
    {
        $token = $request->bearerToken();
        if (!empty($token)) {
            // $keyPlatform = $this->getKeyPlatform($request);
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);

            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);

            if (!empty($account)) {
                $id = $this->spaceRepository->checkAccountInSpace($account['id'], $id);
                if (empty($id)) {
                    return eResponse::response(STATUS_API_INPUT_VALIDATOR, __('space.create.spaceId'), []);
                }
                $this->spaceRepository->updateVisit($id, $account);
                $requestData = $request->all();
                $requestData['account_id'] = $account['id'];
                $requestData['accountInfo'] = [
                    'id' => $account['id'],
                    'name' => $account['name'],
                    'main_space' => $account['main_space'],
                ];
                $result = $this->spaceRepository->findBySpaceIdAndQuickbar($id, $requestData);

                if (!empty($result)) {
                    $accountInfo = [
                        'id' => $account['id'],
                        'organization_id' => $account['organization_id'],
                    ];
                    $data['spaceId'] = $id;
                    $quickbar = $this->quickBarRepository->listQuickBarSpaceIdRe($data, $accountInfo);
                    $result['quickbar'] = $quickbar;
                }
                // $checkSpaceType = $this->spaceRepository->checkSpaceType(null, $id);
                // if ($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
                //     $totalAccount = $this->spaceRepository->totalAccountSpaceCustom($id, $accountInfo);
                //     if ($totalAccount != false) {
                //         $result->account_count = $totalAccount;
                //     }
                // }
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $result);
            }
        }

        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['token' => $token]);
    }

    public function listRoom($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            // $this->spaceRepository->updateVisit($id, $accountInfo);
            DB::commit();
            return $this->spaceRepository->listRoomOfSpace($id);
        } catch (Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    // Duyệt vào space khách hàng
    public function approveJoin(ApproveJoinSpaceRequest $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $data = $request->only([
                'id',
                'account_life_id',
                'is_accept',
            ]);
            $result = $this->spaceRepository->approveJoinInSpace($data, $accountInfo);
            if (!empty($result)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail'));
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    // Duyệt all vào space khách hàng
    public function approveJoinAll(Request $request)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $request->get('accountInfo');
            $data = $request->only([
                'ids',
                'is_accept',
            ]);
            $result = $this->spaceRepository->approveJoinAllInSpace($data, $accountInfo);
            if (!empty($result)) {
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-fail'));
        } catch (\Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    // Hiển thị space với cộng đồng
    public function updateShowSpace($id, UpdateShowSpaceRequest $request)
    {
        $data = $request->only(['is_show']);
        $accountInfo = $request->get('accountInfo');
        try {
            DB::beginTransaction();
            $getModel = Space::findOrFail($id);
            $this->spaceRepository->update($getModel, $data);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), []);
        } catch (Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-fail'), []);
        }
    }
}
