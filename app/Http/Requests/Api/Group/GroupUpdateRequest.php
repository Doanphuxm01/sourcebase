<?php

namespace App\Http\Requests\Api\Group;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Api\BaseRequest;

class GroupUpdateRequest extends BaseRequest
{
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $id = ($this->method() == 'POST') ? $this->get('id') : 0;
        $rules = [
            'slug' => 'required|string|unique:groups,slug,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'accountIds' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'accountIds.*' => __('group.request.account'),
        ];
    }
}
