<?php

namespace App\Http\Requests\Api\Group;

use App\Http\Requests\BaseRequest;

class GroupDeleteRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'groupIds' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'groupIds.*' => __('group.request.groupIds'),
        ];
    }
}
