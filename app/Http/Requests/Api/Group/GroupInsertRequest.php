<?php

namespace App\Http\Requests\Api\Group;

use App\Http\Requests\BaseRequest;

class GroupInsertRequest extends BaseRequest
{
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $rules = [
            // 'groupName' => 'required|string|max:50|unique:groups,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            // 'slug' => 'required|string|unique:groups,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'accountIds' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            // 'groupName.string' => __('group.request.valid'),
            // 'groupName.required' => __('group.request.name'),
            // 'groupName.max' => __('group.request.max'),
            // 'groupName.unique' => __('group.request.unique'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'accountIds.*' => __('group.request.account'),
        ];
    }
}
