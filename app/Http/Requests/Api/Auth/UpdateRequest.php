<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class UpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'required|exists:accounts,id',
            'position_id' => 'required|exists:positions,id',
            'organization_name' => 'required|string',
            'scale' => 'required|integer',
            'major_id' => 'required|exists:majors,id',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'position_id.required' => __('notification.api-form-required'),
            'position_id.exists' => __('notification.api-form-not-exists'),

            'organization_name.required' => __('notification.api-form-required'),
            'organization_name.string' => __('notification.api-form-string'),

            'scale.required' => __('notification.api-form-required'),
            'scale.integer' => __('notification.api-form-integer'),

            'major_id.required' => __('notification.api-form-required'),
            'major_id.exists' => __('notification.api-form-not-exists'),
        ];
    }
}
