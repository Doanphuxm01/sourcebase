<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class GetMailRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|exists:accounts,email',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'email.required' => __('notification.api-form-required'),
            'email.exists' => __('notification.system.email-not-found'),
        ];
    }

    public function getValidatorInstance()
    {
        $this->merge([
            'email' => strtolower($this->get('email')),
        ]);
        return parent::getValidatorInstance();
    }
}
