<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class ResetPasswordRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
//            'token' =>'required',
            'id' => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
//            'token.required' => __('notification.api-form-required'),

            'password.required' => __('notification.api-form-required'),
            'password.min' => __('notification.api-form-min'),
        ];
    }
}
