<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class GetResetPasswordRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'token' =>'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'token.required' => __('notification.api-form-required'),
        ];
    }
}
