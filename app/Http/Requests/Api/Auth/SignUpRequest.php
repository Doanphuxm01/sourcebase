<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class SignUpRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|unique:accounts,email,NULL,id,deleted_at,NULL',
            'name' => 'required|string',
            'username' => 'string',
            'password' => 'required|min:6',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),

            'email.required' => __('notification.api-form-required'),
            'email.email' => __('notification.api-form-email'),
            'email.unique' => __('notification.api-form-unique'),

            'username.required' => __('notification.api-form-required'),
            'username.email' => __('notification.api-form-email'),
            'username.unique' => __('notification.api-form-unique'),

            'password.required' => __('notification.api-form-required'),
            'password.min' => __('notification.api-form-min'),
        ];
    }

    public function getValidatorInstance()
    {
        $this->merge([
            'email' =>    strtolower($this->get('email')),
        ]);
        return parent::getValidatorInstance();
    }
}
