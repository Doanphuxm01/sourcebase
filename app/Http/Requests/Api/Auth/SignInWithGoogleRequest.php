<?php
namespace App\Http\Requests\Api\Auth;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class SignInWithGoogleRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'google_id' => 'required|string',
            'email' => 'required',
            'name' => 'required|string',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'google_id.required' => __('notification.api-form-required'),
            'google_id.string' => __('notification.api-form-string'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),

            'email.required' => __('notification.api-form-required'),
        ];
    }
}
