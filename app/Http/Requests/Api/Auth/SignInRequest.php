<?php
namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\RequestSignIn;

class SignInRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'username' => 'required|email',
//            'email' => 'required|email',
            'password' => 'required',
            // 'device_name' =>'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'username.required' => __('notification.api-form-required'),
//            'email.required' => __('notification.api-form-required'),
//            'username.email' => __('notification.api-form-email'),

            'password.required' => __('notification.api-form-required'),
            // 'device_name.required' => __('notification.api-form-required'),
        ];
    }
}
