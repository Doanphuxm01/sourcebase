<?php
namespace App\Http\Requests\Api\Application;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;
use App\Models\Log;
use App\Models\Postgres\Application;

class UpdateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $id = ($this->method() == 'POST') ? $this->get('id') : 0;
        $spaceId = $this->get('space_id');
        $roomId = $this->get('room_id');
        $link_url = $this->get('link_url');
        $link_android = $this->get('link_android');
        $link_ios = $this->get('link_ios');
        $typeFile = (int)request()->type;
        $isApprove = (int)request()->is_approve;
        $platform = $this->header('Platform', '');
        $rules = [
            'id' => 'integer|required|exists:applications,id',
            'name' => 'required|string|unique:applications,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL',
            'slug' => 'required|string|unique:applications,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL',
            'space_id' => 'integer|required|exists:spaces,id',
            'room_id' => 'integer|required|exists:rooms,id',
        ];
        $uri = request()->route();
        if ($uri->uri == 'api/v1/application/update') {
            unset($rules['name']);
            unset($rules['slug']);
        }
        if ($typeFile == Application::TYPE_FILE) {
            if ($uri->uri == 'api/v1/application/update') {
                switch ($platform) {
                    case 'android':
                    case 'ios':
                        $rules['file'] = 'max:' . Application::FILE_MAX . '';
                        break;
                    default:
                        if ($isApprove == Application::APPROVE) {
                            $rules['file'] = '';
                        } else {
//                            $rules['file'] = 'required|max:' . Application::FILE_MAX . '';
                            $rules['file'] = 'max:' . Application::FILE_MAX . '';
                        }
                        break;
                }

            }
//            else {
//                $rules['file'] = 'required|max:' . Application::FILE_MAX . '';
//            }
        } else {
            if (empty($link_url) && empty($link_android) && empty($link_ios) && empty($application_store_ids) && empty($account_id)) {
                switch ($platform) {
                    case 'ios':
                        $rules['link_ios'] = 'required';
                        break;

                    case 'android':
                        $rules['link_android'] = 'required';
                        break;

                    default:
                        $rules['link_url'] = 'required';
                        break;
                }
            }
        }
        $hasFileBackground = BaseRequest::hasFile('application_image');
        if ($hasFileBackground) {
            $rules['application_image'] = 'image|max:5120';
        }

        if (BaseRequest::has('application_image_id')) {
            $rules['application_image_id'] = 'integer|exists:images,id';
        }

        if (BaseRequest::has('application_group_id')) {
            $rules['application_group_id'] = 'integer|exists:application_groups,id';
        }

        if (BaseRequest::has('time_slot')) {
            $rules['time_slot'] = 'array';
        }

        if (BaseRequest::has('name_group')) {
            $rules['name_group'] = 'required|string|unique:application_groups,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL';
            $rules['slug_name_group'] = 'required|string|unique:application_groups,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL';
        }
        \Log::info([
            'rule' => $rules,
            'request' => request()->all()
        ]);
        return $rules;
    }

    public function messages()
    {
        return [
            'file.max' => __('notification.api-form-file-max-size'),

            'id.integer' => __('notification.api-form-integer'),
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'room_id.integer' => __('notification.api-form-integer'),
            'room_id.required' => __('notification.api-form-required'),
            'room_id.exists' => __('notification.api-form-not-exists'),

            'application_group_id.integer' => __('notification.api-form-integer'),
            'application_group_id.exists' => __('notification.api-form-not-exists'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'name_group.string' => __('notification.api-form-string'),
            'name_group.unique' => __('notification.api-form-unique'),

            'slug_name_group.string' => __('notification.api-form-string'),
            'slug_name_group.unique' => __('notification.api-form-unique'),

            'application_image_id.integer' => __('notification.api-form-integer'),
            'application_image_id.required' => __('notification.api-form-required'),
            'application_image_id.exists' => __('notification.api-form-not-exists'),

            'application_image.image' => __('notification.api-form-must-image'),
            'application_image.max' => __('notification.api-form-max'),

            'time_slot.array' => __('notification.api-form-array'),

            'link_ios.required' => __('notification.api-form-required'),
            'link_android.required' => __('notification.api-form-required'),
            'link_url.required' => __('notification.api-form-required'),
        ];
    }
}
