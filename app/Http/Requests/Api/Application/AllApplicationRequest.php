<?php
namespace App\Http\Requests\Api\Application;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;
use App\Http\Requests\Api\PaginationRequest;

class AllApplicationRequest extends PaginationRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'space_id' => 'integer|required|exists:spaces,id',
            'room_id' => 'integer|required|exists:rooms,id',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'room_id.integer' => __('notification.api-form-integer'),
            'room_id.required' => __('notification.api-form-required'),
            'room_id.exists' => __('notification.api-form-not-exists'),

        ];
    }
}
