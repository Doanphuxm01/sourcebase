<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;

class SpaceDeleteRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'spaceIds' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'spaceIds.*' => __('space.create.spaceId'),
        ];
    }
}
