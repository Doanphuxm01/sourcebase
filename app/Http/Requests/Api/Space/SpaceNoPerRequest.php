<?php

namespace App\Http\Requests\Api\Space;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;

class SpaceNoPerRequest extends BaseRequest
{
    public function rules()
    {
        $id = $this->get('id');

        $rules = [
            'id' => 'required|exists:spaces,id',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.*' => __('space.create.spaceId'),
        ];
    }
}
