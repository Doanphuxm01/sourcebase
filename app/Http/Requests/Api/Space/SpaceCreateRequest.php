<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;
use App\Models\Postgres\Space;
use App\Models\Postgres\Image;

class SpaceCreateRequest extends BaseRequest
{
    public function rules()
    {
        if ($this->hasFile('file')) {
            $typeFile = $this->file->getClientMimeType();
            if ($typeFile == Image::MP4) {
                $max = 52428;
            } else {
                $max = 5120;
            }
        } else {
            $max = 5120;
        }
        $accountInfo = $this->get('accountInfo');
        $rules = [
            'spaceName' => 'required|string|unique:spaces,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'type' => ['numeric', Rule::in([Space::TYPE_SPACE_INTERNAL, Space::TYPE_SPACE_CUSTOMER])],
            'slug' => 'required|string|unique:spaces,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'majorId' => 'required|numeric',
            'collectionId' => 'numeric',
            'accountIds' => 'array',
            'groupIds' => 'array',
            'file' => 'mimes:jpg,bmp,png,mp4,mov,ogg,qt|max:' . $max,
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'spaceName.*' => __('space.create.spaceName'),
            'type.*' => __('space.create.type'),
            'majorId.*' => __('space.create.majorId'),
            'isCollection.*' => __('space.create.isCollection'),
            'collectionId.*' => __('space.create.collectionId'),
            'accountIds.*' => __('space.create.accountIds'),
            'groupIds.*' => __('space.create.groupIds'),
            'file.*' => __('space.create.file'),
            'is_show.required' => __('notification.api-form-required'),
            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),
        ];
    }
}
