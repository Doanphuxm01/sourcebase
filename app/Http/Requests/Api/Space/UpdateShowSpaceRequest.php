<?php

namespace App\Http\Requests\Api\Space;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class UpdateShowSpaceRequest extends BaseRequest
{
    public function rules()
    {
        $id = $this->get('id');

        $rules = [
            'id' => 'required|exists:spaces,id',
            'is_show' => ['numeric', Rule::in([0, 1])],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.*' => __('space.create.spaceId'),
            'is_show.*' => __('notification.api-form-required'),
        ];
    }
}
