<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;

class SpaceUpdateAdmin extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'space_id' => 'required|numeric',
            'account_id' => 'required|numeric',
            'is_admin' => 'required|numeric',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'space_id.*' => __('notification.api-form-required'),
            'account_id.*' => __('notification.api-form-required'),
            'is_admin.*' => __('notification.api-form-required'),
        ];
    }
}
