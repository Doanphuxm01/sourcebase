<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Space;
use Illuminate\Validation\Rule;

class SpaceUpdateAccountStatusRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'spaceId' => 'integer|required|exists:spaces,id',
            'accountId ' => 'required|numeric',
            'isBlocked' => ['numeric', Rule::in([Space::NOT_BLOCKED, Space::IS_BLOCKED])],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'spaceId.*' => __('space.create.spaceId'),
            'accountId.*' => __('space.create.accountIds'),
            'isBlocked.*' => __('space.create.isBlocked'),
        ];
    }
}
