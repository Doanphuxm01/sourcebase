<?php

namespace App\Http\Requests\Api\Space;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\BaseRequest;

class ApproveJoinSpaceRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'id' => 'integer|required|exists:spaces,id',
            'account_life_id' => 'integer|required|exists:life_accounts,id',
            'is_accept' => 'integer|required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.*' => __('notification.api-form-not-exists'),
            'account_life_id.*' => __('notification.api-form-not-exists'),
            'is_accept.*' => __('notification.api-form-not-exists'),
        ];
    }
}
