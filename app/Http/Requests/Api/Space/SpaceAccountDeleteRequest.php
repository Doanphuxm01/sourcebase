<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;

class SpaceAccountDeleteRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'spaceId' => 'integer|required|exists:spaces,id',
            'accountIds' => 'array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'spaceId.*' => __('space.create.spaceId'),
            'accountIds.*' => __('space.create.accountIds'),
        ];
    }
}
