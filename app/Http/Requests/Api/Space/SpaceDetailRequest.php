<?php

namespace App\Http\Requests\Api\Space;

use App\Http\Requests\BaseRequest;

class SpaceDetailRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'spaceId ' => 'required|numeric',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'spaceId.*' => __('space.create.spaceId'),
        ];
    }
}
