<?php

namespace App\Http\Requests\Api\Statistic;

use Illuminate\Foundation\Http\FormRequest;

class StatisticAccountByDayRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page' => 'integer|min:1',
            'limit' => 'integer|min:1',
            'account_id' => 'required|integer',
            'space_id' => 'integer',
            'room_id' => 'integer',
            'date_start' => 'required|string',
            'date_end' => 'required|string',
        ];
    }
}
