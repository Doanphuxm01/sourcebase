<?php

namespace App\Http\Requests\Api\Statistic;

use Illuminate\Foundation\Http\FormRequest;

class StatisticAppByDayRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'page' => 'integer|min:0',
            'limit' => 'integer|min:1',
            'application_id' => 'required|integer',
            'date_start' => 'required|string',
            'date_end' => 'required|string',
        ];
    }
}
