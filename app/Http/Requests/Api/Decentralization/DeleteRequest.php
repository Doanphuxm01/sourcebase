<?php
namespace App\Http\Requests\Api\Decentralization;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class DeleteRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'ids' => 'array|required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'ids.array' => __('notification.api-form-array'),
            'ids.required' => __('notification.api-form-required'),
        ];
    }
}
