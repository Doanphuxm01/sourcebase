<?php
namespace App\Http\Requests\Api\Decentralization;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class CreateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');

        $rules = [
            'name' => 'required|string|unique:decentralizations,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'slug' => 'required|string|unique:decentralizations,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',deleted_at,NULL',
            'permissions' => 'array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'permissions.array' => __('notification.api-form-array'),
        ];
    }
}
