<?php
namespace App\Http\Requests\Api\Image;

use App\Http\Requests\Api\BaseRequest;
use App\Models\Postgres\Image;
use Illuminate\Validation\Rule;

class CollectionUploadRequest extends BaseRequest
{
    public function rules()
    {
        $typeFile = $this->file->getClientMimeType();
        if ($typeFile == Image::MP4) {
            $max = 52428;
        } else {
            $max = 5120;
        }
        $rules = [
            // 'file' => 'required|mimes:jpg,bmp,png,mp4,mov,ogg,qt|max:' . $max,
            'file' => 'required|mimes:jpg,bmp,png,gif|max:' . $max,
            'collectionType' => ['numeric', Rule::in([Image::COLLECTION_TYPE_DEFAULT, Image::COLLECTION_TYPE_SPACE, Image::COLLECTION_TYPE_APP, Image::COLLECTION_TYPE_TIMELINE, Image::COLLECTION_TYPE_AVATAR, Image::COLLECTION_TYPE_BACKGROUND])],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'file.*' => __('image.file'),
            'collectionType.*' => __('image.type'),
        ];
    }
}
