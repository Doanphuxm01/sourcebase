<?php

namespace App\Http\Requests\Api\Image;

use App\Http\Requests\BaseRequest;

class CollectionDeleteRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'colectionIds' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'colectionIds.*' => __('image.delete.id'),
        ];
    }
}
