<?php

namespace App\Http\Requests\Api\Image;

use App\Http\Requests\BaseRequest;

class CollectionViewRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'startDate' => 'date',
            'endDate' => 'date',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'startDate.date' => __('image.date'),
            'endDate.date' => __('image.date'),
        ];
    }
}
