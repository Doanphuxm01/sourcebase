<?php
namespace App\Http\Requests\Api\InfoAccount;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class ChangeLanguageRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'key' =>'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'key.required' => __('notification.api-form-required'),
        ];
    }
}
