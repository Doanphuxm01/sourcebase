<?php
namespace App\Http\Requests\Api\InfoAccount;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class UpdateAccountRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');

        $rules = [
            'name' => 'required|string',
            'username' => 'string',
            // 'username' => 'required|string|unique:accounts,username,' . $accountInfo['id'] . ',id,deleted_at,NULL',
//            'organization_name' => 'required|string',
        ];

        $hasFileAvatar = BaseRequest::hasFile('profile_image_avatar');
        if($hasFileAvatar){
            $rules['profile_image_avatar'] = 'image|max:5120';
        }

        $hasFileBackground = BaseRequest::hasFile('profile_image_background');
        if($hasFileBackground){
            $rules['profile_image_background'] = 'image|max:5120';
        }

        if (BaseRequest::has('profile_image_id')){
            $rules['profile_image_id'] = 'integer|exists:images,id';
        }

        if (BaseRequest::has('background_image_id')){
            $rules['background_image_id'] = 'integer|exists:images,id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),

            'username.required' => __('notification.api-form-required'),
            'username.string' => __('notification.api-form-string'),
            'username.unique' => __('notification.api-form-unique'),

            'profile_image_id.integer' => __('notification.api-form-integer'),
            'profile_image_id.exists' => __('notification.api-form-not-exists'),

            'background_image_id.integer' => __('notification.api-form-integer'),
            'background_image_id.exists' => __('notification.api-form-not-exists'),

            'profile_image_avatar.image' => __('notification.api-form-must-image'),
            'profile_image_avatar.max' => __('notification.api-form-max'),

            'profile_image_background.image' => __('notification.api-form-must-image'),
            'profile_image_background.max' => __('notification.api-form-max'),

            'organization_name.required' => __('notification.api-form-required'),
            'organization_name.string' => __('notification.api-form-string'),
        ];
    }
}
