<?php
namespace App\Http\Requests\Api\InfoAccount;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class ChangePasswordRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'password' =>'required',
            'new_password' =>'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*]).{8,}$/',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'password.required' => __('notification.api-form-required'),

            'new_password.required' => __('notification.api-form-required'),
            'new_password.regex' => __('notification.api-form-regex'),
        ];
    }
}
