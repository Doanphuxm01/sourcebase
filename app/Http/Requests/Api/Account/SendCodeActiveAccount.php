<?php

namespace App\Http\Requests\Api\Account;

use App\Http\Requests\Api\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class SendCodeActiveAccount extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required',
        ];
        if (request()->is('api/v1/active-account')) {
            $rules['code'] = 'required';
        }
        return $rules;
    }
}
