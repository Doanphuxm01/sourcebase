<?php
namespace App\Http\Requests\Api\Account;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class CreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // 'username' => 'required|string|unique:accounts,username,NULL,id,deleted_at,NULL',
            'username' => 'string',
            'name' => 'required|string',
            'email' => 'required|email|unique:accounts,email,NULL,id,deleted_at,NULL',
            'password' =>'min:6',
            'decentralization_id' => 'integer|required|exists:decentralizations,id',
            'manager_ids' => 'array'
        ];
        if(request()->decentralization_id < 0) {
            unset($rules['decentralization_id']);
        }

        $hasFile = BaseRequest::hasFile('profile_image_avatar');
        if($hasFile){
            $rules['profile_image_avatar'] = 'image|max:5120';
        }

        if (BaseRequest::has('profile_image_id')){
            $rules['profile_image_id'] = 'integer|exists:images,id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'username.required' => __('notification.api-form-required'),
            'username.email' => __('notification.api-form-email'),
            'username.unique' => __('notification.api-form-unique'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),

            'email.required' => __('notification.api-form-required'),
            'email.string' => __('notification.api-form-string'),
            'email.unique' => __('notification.api-form-unique'),

            'background_image_id.integer' => __('notification.api-form-integer'),
            'background_image_id.exists' => __('notification.api-form-not-exists'),

            'password.required' => __('notification.api-form-required'),
            'password.min' => __('notification.api-form-min'),

            'decentralization_id.integer' => __('notification.api-form-integer'),
            'decentralization_id.required' => __('notification.api-form-required'),
            'decentralization_id.exists' => __('notification.api-form-not-exists'),

            'profile_image_avatar.image' => __('notification.api-form-must-image'),
            'profile_image_avatar.max' => __('notification.api-form-max'),

            'manager_ids.array' => __('notification.api-form-array'),

        ];
    }
}
