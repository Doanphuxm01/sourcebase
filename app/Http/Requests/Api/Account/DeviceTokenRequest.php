<?php

namespace App\Http\Requests\Api\Account;

use App\Http\Requests\Api\BaseRequest;
use App\Models\Postgres\Account;

class DeviceTokenRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_type' => 'integer|required|in:'.Account::TYPE_SIGN_IN_IOS.",".Account::TYPE_SIGN_IN_ANDROID,
            'device_token' => 'required|string'
        ];
    }
}
