<?php
namespace App\Http\Requests\Api\GroupApplication;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class UpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $id = ($this->method() == 'POST') ? $this->get('id') : 0;
        $spaceId =  $this->get('space_id');
        $roomId =  $this->get('room_id');

        $rules = [
            'id' => 'integer|required|exists:application_groups,id',
            'space_id' => 'integer|required|exists:spaces,id',
            'room_id' => 'integer|required|exists:rooms,id',
            'name' => 'required|string|unique:application_groups,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL',
            // 'slug' => 'required|string|unique:application_groups,slug,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',room_id,' . $roomId . ',deleted_at,NULL',
            'app_ids' => 'array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.integer' => __('notification.api-form-integer'),
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'room_id.integer' => __('notification.api-form-integer'),
            'room_id.required' => __('notification.api-form-required'),
            'room_id.exists' => __('notification.api-form-not-exists'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            // 'slug.required' => __('notification.api-form-required'),
            // 'slug.string' => __('notification.api-form-string'),
            // 'slug.unique' => __('notification.api-form-unique'),

            'app_ids.array' => __('notification.api-form-array'),
        ];
    }
}
