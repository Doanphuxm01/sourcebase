<?php

namespace App\Http\Requests\Api\Room;

use App\Http\Requests\BaseRequest;

class RoomUpdateAdminRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'room_id' => 'required|numeric',
            'account_id' => 'required|numeric',
            'is_admin' => 'required|numeric',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'room_id.*' => __('notification.api-form-required'),
            'account_id.*' => __('notification.api-form-required'),
            'is_admin.*' => __('notification.api-form-required'),
        ];
    }
}
