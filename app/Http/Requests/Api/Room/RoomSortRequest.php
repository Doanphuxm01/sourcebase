<?php

namespace App\Http\Requests\Api\Room;

use App\Http\Requests\BaseRequest;

class RoomSortRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'sortRoom' => 'required|array',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'sortRoom.*' => __('room.request.sort'),
        ];
    }
}
