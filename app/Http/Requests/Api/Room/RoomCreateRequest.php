<?php

namespace App\Http\Requests\Api\Room;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;
use App\Models\Postgres\Room;

class RoomCreateRequest extends BaseRequest
{
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $spaceId = $this->get('spaceId');
        $rules = [
            'spaceId' => 'integer|required|exists:spaces,id',
            'roomName' => 'required|string|unique:rooms,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',deleted_at,NULL',
            'type' => ['numeric', Rule::in([Room::TYPE_ROOM_PRIVATE, Room::TYPE_ROOM_PUBLIC])],
            'slug' => 'required|string|unique:rooms,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',deleted_at,NULL',
            'accountIds' => 'array',
            'isPin' => ['numeric', Rule::in([Room::IS_PIN, Room::NOT_PIN])],
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'roomName.*' => __('room.request.name'),
            'type.*' => __('room.request.type'),
            'accountIds.*' => __('room.request.accountId'),
            'spaceId.*' => __('room.request.spaceId'),
            'isPin.*' => __('room.request.pin'),
            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),
        ];
    }
}
