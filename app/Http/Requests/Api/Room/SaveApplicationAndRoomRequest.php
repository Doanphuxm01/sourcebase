<?php

namespace App\Http\Requests\Api\Room;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;
use App\Models\Postgres\Room;

class SaveApplicationAndRoomRequest extends BaseRequest
{
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $id = ($this->method() == 'POST') ? $this->get('id') : 0;
        $spaceId = $this->get('space_id');

        $rules = [
            'id' => 'integer|required|exists:rooms,id',
            'name' => 'required|string|unique:rooms,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',deleted_at,NULL',
            'slug' => 'required|string|unique:rooms,slug,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',space_id,' . $spaceId . ',deleted_at,NULL',
            'type' => 'required|integer',
            'is_pin' => 'required|integer',
            'space_id' => 'integer|required|exists:spaces,id',
            'applications' => 'array'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.integer' => __('notification.api-form-integer'),
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'type.required' => __('notification.api-form-required'),
            'type.integer' => __('notification.api-form-integer'),

            'is_pin.required' => __('notification.api-form-required'),
            'is_pin.integer' => __('notification.api-form-integer'),

            'applications.array' => __('notification.api-form-array'),
        ];
    }
}
