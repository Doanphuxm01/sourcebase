<?php

namespace App\Http\Requests\Api\Room;

use App\Http\Requests\BaseRequest;

class RoomAddAccountRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'accountIds' => 'required|array',
            'roomId' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'accountIds.*' => __('room.request.accountId'),
            'roomId.*' => __('notification.api-form-required'),
        ];
    }
}
