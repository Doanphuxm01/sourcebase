<?php

namespace App\Http\Requests\Api\QuickBar;

use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Application;

class QuickBarCreateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $isNew = $this->get('is_new');
        $spaceId = $this->get('space_id');
        $link_url = $this->get('link_url');
        $link_android = $this->get('link_android');
        $link_ios = $this->get('link_ios');
        $typeFile = (int)request()->type;

        if ($isNew) {
            $rules = [
                'name' => 'required|string|unique:application_stores,name,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
                'slug' => 'required|string|unique:application_stores,slug,NULL,id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
                'space_id' => 'integer|required|exists:spaces,id',
            ];
            if ($typeFile == Application::TYPE_FILE) {
                $rules['file'] = 'required|max:' . Application::FILE_MAX . '';
            } else {
                if (empty($link_url) && empty($link_android) && empty($link_ios) && empty($application_store_ids)) {
                    $platform = $this->header('Platform', '');
                    switch ($platform) {
                        case 'ios':
                            $rules['link_ios'] = 'required';
                            break;

                        case 'android':
                            $rules['link_android'] = 'required';
                            break;

                        default:
                            $rules['link_url'] = 'required';
                            break;
                    }
                }
            }
            $hasFileBackground = BaseRequest::hasFile('application_image');
            if ($hasFileBackground) {
                $rules['application_image'] = 'image|max:5120';
            }

            if (BaseRequest::has('application_image_id')) {
                $rules['application_image_id'] = 'integer|exists:images,id';
            }
        } else {
            $rules = [
                'space_id' => 'integer|required|exists:spaces,id',
                'application_id' => 'array|required|exists:application_stores,id',
            ];
        }
        \Log::info([
            'rule' => $rules,
            'request' => request()->all()
        ]);
        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'application_id.integer' => __('notification.api-form-integer'),
            'application_id.required' => __('notification.api-form-required'),
            'application_id.exists' => __('notification.api-form-not-exists'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'application_image_id.integer' => __('notification.api-form-integer'),
            'application_image_id.required' => __('notification.api-form-required'),
            'application_image_id.exists' => __('notification.api-form-not-exists'),

            'application_image.image' => __('notification.api-form-must-image'),
            'application_image.max' => __('notification.api-form-max'),

            'is_new.integer' => __('notification.api-form-integer'),
            'is_new.required' => __('notification.api-form-required'),

            // 'link_url.required' => __('notification.api-form-required'),
            // 'link_ios.required' => __('notification.api-form-required'),
            // 'link_android.required' => __('notification.api-form-required'),
        ];
    }
}
