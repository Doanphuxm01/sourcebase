<?php

namespace App\Http\Requests\Api\QuickBar;

use App\Http\Requests\BaseRequest;

class QuickBarDeleteRequest extends BaseRequest
{
    public function rules()
    {

        $rules = [
            'id' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.required' => __('notification.api-form-required'),
        ];
    }
}
