<?php

namespace App\Http\Requests\Api\QuickBar;

use App\Http\Requests\BaseRequest;
use App\Models\Postgres\Application;

class QuickBarUpdateRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $id = ($this->method() == 'POST') ? $this->get('application_id') : 0;
        $spaceId = $this->get('space_id');
        $typeFile = (int)request()->type;
        $rules = [
            // 'name' => 'required|string|unique:application_stores,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
            // 'slug' => 'required|string|unique:application_stores,slug,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
            'name' => 'required|string|unique:application_stores,name,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',type,1,deleted_at,NULL',
            'slug' => 'required|string|unique:application_stores,slug,' . $id . ',id,organization_id,' . $accountInfo['organization_id'] . ',created_by,' . $accountInfo['id'] . ',type,1,deleted_at,NULL',
            'space_id' => 'integer|required|exists:spaces,id',
        ];
        $uri = request()->route();
        if ($uri->uri == 'api/v1/quickbar/update') {
            unset($rules['name']);
            unset($rules['slug']);
        }
        if ($typeFile == Application::TYPE_FILE) {
            $rules['file'] = 'max:' . Application::FILE_MAX . '';
        }

        $hasFileBackground = BaseRequest::hasFile('application_image');
        if ($hasFileBackground) {
            $rules['application_image'] = 'image|max:5120';
        }

        if (BaseRequest::has('application_image_id')) {
            $rules['application_image_id'] = 'integer|exists:images,id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'file.max' => __('notification.api-form-file-max-size'),
            'file.required' => __('notification.api-form-file-required'),

            'id.integer' => __('notification.api-form-integer'),
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'application_image_id.integer' => __('notification.api-form-integer'),
            'application_image_id.required' => __('notification.api-form-required'),
            'application_image_id.exists' => __('notification.api-form-not-exists'),

            'application_image.image' => __('notification.api-form-must-image'),
            'application_image.max' => __('notification.api-form-max'),

        ];
    }
}
