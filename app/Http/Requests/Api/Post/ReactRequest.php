<?php
namespace App\Http\Requests\Api\Post;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class ReactRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' => 'integer|required|exists:posts,id',
            'emotion_id' => 'integer|required|exists:emotions,id',
            'space_id' => 'integer|required|exists:spaces,id',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'id.integer' => __('notification.api-form-integer'),
            'id.required' => __('notification.api-form-required'),
            'id.exists' => __('notification.api-form-not-exists'),

            'emotion_id.integer' => __('notification.api-form-integer'),
            'emotion_id.required' => __('notification.api-form-required'),
            'emotion_id.exists' => __('notification.api-form-not-exists'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),
        ];
    }
}
