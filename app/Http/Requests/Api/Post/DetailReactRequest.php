<?php
namespace App\Http\Requests\Api\Post;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class DetailReactRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'post_id' => 'integer|required|exists:posts,id',
            'space_id' => 'integer|required|exists:spaces,id',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'post_id.integer' => __('notification.api-form-integer'),
            'post_id.required' => __('notification.api-form-required'),
            'post_id.exists' => __('notification.api-form-not-exists'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),
        ];
    }
}
