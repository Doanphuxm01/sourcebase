<?php
namespace App\Http\Requests\Api\Comment;

use App\Elibs\eFunction;
use App\Http\Requests\Api\BaseRequest;
use App\Http\Requests\Api\Request;

class CreateRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'post_id' => 'integer|required|exists:spaces,id',
            'space_id' => 'integer|required|exists:spaces,id',
            'content' => 'required|string',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'post_id.integer' => __('notification.api-form-integer'),
            'post_id.required' => __('notification.api-form-required'),
            'post_id.exists' => __('notification.api-form-not-exists'),

            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists' => __('notification.api-form-not-exists'),


            'content.integer' => __('notification.api-form-string'),
            'content.required' => __('notification.api-form-required'),
        ];
    }
}
