<?php
namespace App\Http\Requests\Api;

use App\Http\Requests\Api\Request;

class BaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

}
