<?php

namespace App\Http\Requests\ApplicationStore;

use App\Models\Postgres\Application;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Api\BaseRequest;
use Illuminate\Support\Facades\Route;

class CreateRequest extends BaseRequest
{
    public function rules()
    {
        $accountInfo = $this->get('accountInfo');
        $link_url = $this->get('link_url');
        $link_android = $this->get('link_android');
        $link_ios = $this->get('link_ios');

        $application_store_ids = $this->get('application_store_ids');
        $typeFile = (int)request()->type;
        $rules = [
            'name' => 'string|required|unique:application_stores,name,NULL,id,created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
            'slug' => 'string|required|unique:application_stores,slug,NULL,id,created_by,' . $accountInfo['id'] . ',deleted_at,NULL',
        ];
        $uri = request()->route();
        if ($uri->uri == 'api/v1/application-store/{id}') {
            unset($rules['name']);
            unset($rules['slug']);
            $rules = [];
        }
        if ($typeFile == Application::TYPE_FILE) {
            $rules['file'] = 'required|max:' . Application::FILE_MAX . '';
        }
        if ($typeFile == Application::TYPE_LINK) {
            if (empty($link_url) && empty($link_android) && empty($link_ios) && empty($application_store_ids)) {
                $platform = $this->header('Platform', '');
                switch ($platform) {
                    case 'ios':
                        $rules['link_ios'] = 'required';
                        break;

                    case 'android':
                        $rules['link_android'] = 'required';
                        break;

                    default:
                        $rules['link_url'] = 'required';
                        break;
                }
            }
        }
        $hasFileBackground = BaseRequest::hasFile('application_image');
        if ($hasFileBackground) {
            $rules['application_image'] = 'image|max:5120';
        }

        if (BaseRequest::has('application_image_id')) {
            $rules['application_image_id'] = 'integer|exists:images,id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => __('notification.api-form-required'),
            'name.string' => __('notification.api-form-string'),
            'name.unique' => __('notification.api-form-unique '),

            'file.max' => __('notification.api-form-file-max-size'),
            'file.required' => __('notification.api-form-file-required'),
            'space_id.integer' => __('notification.api-form-integer'),
            'space_id.required' => __('notification.api-form-required'),
            'space_id.exists ' => __('notification.api-form-not-exists'),

            'room_id.integer' => __('notification.api-form-integer'),
            'room_id.exists' => __('notification.api-form-not-exists'),

            'application_group_id.integer' => __('notification.api-form-integer'),
            'application_group_id.exists' => __('notification.api-form-not-exists'),

            'slug.required' => __('notification.api-form-required'),
            'slug.string' => __('notification.api-form-string'),
            'slug.unique' => __('notification.api-form-unique'),

            'name_group.string' => __('notification.api-form-string'),
            'name_group.unique' => __('notification.api-form-unique'),

            'slug_name_group.string' => __('notification.api-form-string'),
            'slug_name_group.unique' => __('notification.api-form-unique'),

            'application_image_id.integer' => __('notification.api-form-integer'),
            'application_image_id.required' => __('notification.api-form-required'),
            'application_image_id.exists' => __('notification.api-form-not-exists'),

            'application_image.image' => __('notification.api-form-must-image'),
            'application_image.max' => __('notification.api-form-max'),

            'link_ios.required' => __('notification.api-form-required'),
            'link_android.required' => __('notification.api-form-required'),
            'link_url.required' => __('notification.api-form-required'),

        ];
    }
}
