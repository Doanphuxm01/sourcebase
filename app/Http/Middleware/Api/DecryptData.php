<?php

namespace App\Http\Middleware\Api;

use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Elibs\eResponse;
use Closure;
use Illuminate\Support\Facades\App;

class DecryptData
{
    protected $accountRepository;

    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->get('data');
        if (!empty($data)) {
            $data = json_decode(eCrypt::decryptAES($data), true);
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $request->merge([$key => $value]);
                }
                return $next($request);
            }
        } else {
            $file = $request->hasFile('file');
            $hasFileAvatar = $request->hasFile('profile_image_avatar');
            $hasFileBackground = $request->hasFile('profile_image_background');
            if (!empty($hasFileAvatar) || !empty($hasFileBackground) || !empty($file)) {
                return $next($request);
            }
        }

        $method = $request->getRealMethod();
        $router = $request->route()->getActionName();
        $nameAction = explode('@', $router);
        $swagger = $request->header('swagger', '');
        //Nếu là ở local hoặc là vào bằng swagger hoặc là phương thức get thì cho vào lấy dữ liệu khi không mã hóa
        if (
            App::environment('local')
            || !empty($swagger)
            || $method === Account::METHOD_GET
            || $method === Account::METHOD_DELETE
            || $nameAction[1] === Account::SIGN_OUT
        ) {
            return $next($request);
        }

        return eResponse::response(STATUS_API_NOT_DATA, __('notification.system.data-not-found'), []);
    }
}
