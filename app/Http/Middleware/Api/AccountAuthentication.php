<?php

namespace App\Http\Middleware\Api;

use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Models\Postgres\Permission;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use App\Elibs\eResponse;
use Closure;
use Illuminate\Support\Facades\App;
use App\Services\Postgres\DecentralizationServiceInterface;
use Illuminate\Support\Arr;

class AccountAuthentication
{
    protected $accountRepository;
    protected $decentralizationService;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        DecentralizationServiceInterface $decentralizationService
    ) {
        $this->accountRepository = $accountRepository;
        $this->decentralizationService = $decentralizationService;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isAccess = false;
        $token = $request->bearerToken();
        $method = $request->getRealMethod();
        $data = $request->get('data');
        $file = $request->hasFile('file');
        $swagger = $request->header('swagger', '');
        $hasFileAvatar = $request->hasFile('profile_image_avatar');
        $hasFileBackground = $request->hasFile('profile_image_background');
        // Check thiết bị đăng nhập
        $platform = $request->header('Platform', '');
        $keyPlatform = eFunction::checkPlatformLogin($platform);

        if (empty($keyPlatform)) {
            return eResponse::response(STATUS_API_ERROR, __('notification.system.platform'), []);
        }

        if (App::environment('local') || !empty($swagger) || (!App::environment('local') && !empty($data)) || $method === Account::METHOD_GET || $method === Account::METHOD_DELETE || !empty($hasFileAvatar) || !empty($hasFileBackground) || !empty($file)) {
            $isAccess = true;
        }

        if (!empty($token) && !empty($isAccess)) {
            // $account = $this->accountRepository->getOneArrayAccountForLoginByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token, 'deleted_at' => true]);
            $account_id = $this->accountAccessTokensRepository->getIdForToken($token);
            $account = $this->accountRepository->getOneArrayAccountForLoginByFilter(['id' => (int)$account_id, 'deleted_at' => true]);
            if (!empty($account) && !empty($account['id'])) {
                $hasPermission = false;
                $router = $request->route()->getActionName();
                $method = $request->getRealMethod();
                $nameAction = explode('@', $router);
                if (!empty($method) && !empty($nameAction[0])) {
                    //Check quyền cho tài khoản
                    if ((int)$account['is_root'] === Account::IS_ROOT) {
                        $hasPermission = true;
                    } else {
                        $controller = explode('\\', $nameAction[0]);
                        if (!empty($controller)) {
                            if (in_array(end($controller), Permission::LIST_CONTROLLER)) {
                                if (!empty($account['decentralization']) && !empty($account['decentralization']['decentralization_permissions'])) {
                                    $key = Permission::CONTROLLER_KEY[end($controller)];
                                    $decentralizationPermissions = collect($account['decentralization']['decentralization_permissions'])->keyBy('key')->toArray();
                                    if (!empty($key) && !empty($decentralizationPermissions[$key])) {
                                        $permission = $decentralizationPermissions[$key];
                                        if ($method === Permission::METHOD_POST && ($request->has('id') || $request->has('spaceId'))) {
                                            $method = Permission::METHOD_UPDATE; // check
                                        }

                                        $function = Permission::LIST_METHOD[$method];
                                        if (!empty($permission[$function])) {
                                            $hasPermission = true;
                                        }
                                    }
                                }
                            } else {
                                $hasPermission = true;
                            }
                        }
                    }
                }

                if (empty($hasPermission)) {
                    return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
                }

                $accountInfo = [
                    'id' => @$account['id'],
                    'name' => @$account['name'],
                    'avatar' => !empty($account['profile_image']) && !empty($account['profile_image']['source']) && file_exists(public_path() . '/' . $account['profile_image']['source']) ? asset($account['profile_image']['source']) : '',
                    'decentralization_id' => !empty($account['decentralization_id']) ? $account['decentralization_id'] : null,
                    'organization_id' => !empty($account['organization_id']) ? $account['organization_id'] : null,
                    'position_id' => !empty($account['position_id']) ? $account['position_id'] : null,
                    'language' => $account['language'],
                    'is_root' => $account['is_root'],
                    'type_account' => $account['type_account'],
                    'main_space' => $account['main_space']
                ];

                if (!$accountInfo['main_space']) {
                    $getAccountDetail = Account::find($account['id']);
                    $getAccountDetail->load('spaceAccount');
                    $getSpaceId = Arr::first($getAccountDetail['spaceAccount']);
                    $accountInfo['main_space'] = $getSpaceId ? $getSpaceId['space_id'] : null;
                }

                if (!empty($account['decentralization_id'])) {
                    $permissions = $this->decentralizationService->getPermissions($account['decentralization_id'], $account['is_root']);
                    $accountInfo['permissions'] = $permissions;
                } else {
                    $accountInfo['permissions'] = '';
                }

                $request->merge(compact('accountInfo'));

                if ($request->has('name')) {
                    $slug = eFunction::generateSlug($request->get('name'), '-');
                    $request->merge(compact('slug'));
                }

                if ($request->has('spaceName')) {
                    $slug = eFunction::generateSlug($request->get('spaceName'), '-');
                    $request->merge(compact('slug'));
                }

                if ($request->has('roomName')) {
                    $slug = eFunction::generateSlug($request->get('roomName'), '-');
                    $request->merge(compact('slug'));
                }

                if ($request->has('groupName')) {
                    $slug = eFunction::generateSlug($request->get('groupName'), '-');
                    $request->merge(compact('slug'));
                }

                if ($request->has('name_group')) {
                    $slug_name_group = eFunction::generateSlug($request->get('name_group'), '-');
                    $request->merge(compact('slug_name_group'));
                }

                if ($request->has('username')) {
                    $username = strtolower($request->get('username'));
                    $request->merge(compact('username'));
                }

                if ($request->has('email')) {
                    $email = strtolower($request->get('email'));
                    $request->merge(compact('email'));
                }

                if (!empty($accountInfo['language'])) {
                    $file = Account::LANGUAGE[$accountInfo['language']]['key'];
                    App::setLocale($file);
                }

                return $next($request);
            }
        }

        return eResponse::response(STATUS_API_TOKEN_FALSE, __('notification.system.token-not-found'), ['is_Access' => $isAccess, 'token' => $token]);
    }
}
