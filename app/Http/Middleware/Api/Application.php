<?php

namespace App\Http\Middleware\Api;

use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Models\Postgres\Permission;
use App\Models\Postgres\Room;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Elibs\eResponse;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\RoomAccountRepositoryInterface;
use App\Repositories\Postgres\RoomRepositoryInterface;
use Closure;
use Illuminate\Support\Facades\App;

class Application
{
    protected $roomRepository;
    protected $applicationRepository;
    protected $roomAccountRepository;

    public function __construct(RoomRepositoryInterface $roomRepository, ApplicationRepositoryInterface $applicationRepository, RoomAccountRepositoryInterface $roomAccountRepository)
    {
        $this->roomRepository = $roomRepository;
        $this->roomAccountRepository = $roomAccountRepository;
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accountInfo = $request->get('accountInfo');
        $roomId = $request->get('room_id');
        $spaceId = $request->get('space_id');
        if (!empty($accountInfo['id']) && !empty($spaceId)){
            if (!empty($roomId)){
                $room = $this->roomRepository->getOneArrayRoomByFilter(['id' => $roomId, 'space_id' => $spaceId, 'deleted_at' => true]);
                if (!empty($room)){
                    if ((int)$room['type'] === Room::TYPE_ROOM_PUBLIC){
                        return $next($request);
                    }

                    if ((int)$room['type'] === Room::TYPE_ROOM_PRIVATE){
                        $roomAccount = $this->roomAccountRepository->getOneArrayRoomAccountByFilter(['account_id' => $accountInfo['id'], 'id' => $roomId, 'space_id' => $spaceId]);
                        if (!empty($roomAccount)){
                            return $next($request);
                        }
                    }
                }
            }else{
                $id = $request->get('id');
                $application = $this->applicationRepository->getOneArrayApplicationWithAdvancedByFilter(['id' => (int)$id, 'deleted_at' => true], $accountInfo);
                if (!empty($application) && $application['type'] === \App\Models\Postgres\Application::TYPE_APP_BAR){
                    return $next($request);
                }
            }
        }

        return eResponse::response(STATUS_API_SUCCESS, __('notification.system.not-have-access'), []);
    }
}
