<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $language = $request->lang;
        if (!session()->has('locale')) {
            switch ($language) {
                case 'en':
                    $file = 'en';
                    break;
                default:
                    $file = 'vi';
                    break;
            }
            session()->put('locale', $file);
            app()->setLocale(session()->get('locale'));
            return $next($request);
        } else {
            $language = 'vi';
            session()->put('locale', $language);
            app()->setLocale(session()->get('locale'));
            return $next($request);
        }
    }
}
