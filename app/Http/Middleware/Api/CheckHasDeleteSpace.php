<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Http\Request;
use App\Models\Postgres\Space;
use App\Elibs\eResponse;

class CheckHasDeleteSpace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $accountInfo = $request->get('accountInfo');
        $spaceId = $request->get('spaceIds');
        $result = Space::whereIn('id', $spaceId)->get()->pluck('created_by')->unique()->values()->toArray();
        $count = count($result);
        if ($count == 1 && $accountInfo['id'] == $result[0]) {
            return $next($request);
        }

        return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
    }
}
