<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Http\Request;
use App\Elibs\eResponse;
use App\Models\Postgres\SpaceAccount;
use App\Models\Postgres\SpaceAccountLifeWork;

class CheckAccountUpdateSpace
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $accountInfo = $request->get('accountInfo');
        $id = $request->get('spaceId');
        $checkSpaceWork = SpaceAccount::where('account_id', $accountInfo['id'])->where('space_id', $id)->where('is_admin', SpaceAccount::IS_ADMIN)->first();
        $checkSpaceWorkLife = SpaceAccountLifeWork::where('account_id', $accountInfo['id'])->where('space_id', $id)->where('is_admin', SpaceAccount::IS_ADMIN)->first();

        if (!empty($checkSpaceWork) || !empty($checkSpaceWorkLife)) {
            return $next($request);
        }

        return eResponse::response(STATUS_API_ERROR, __('notification.api-no-permission'));
    }
}
