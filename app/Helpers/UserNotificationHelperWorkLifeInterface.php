<?php

namespace App\Helpers;

interface UserNotificationHelperWorkLifeInterface
{
    /**
     * @param array $device_tokens
     * @param array $notification
     * @param array $data
     */
    public function sendFCM($device_tokens, $notification, $data);
    /**
     * @param array $account_ids
     * @param array $data
     * @param array|null $notification
     */
    public function sendWithAccountId($account_ids, $data, $notification = null);
}
