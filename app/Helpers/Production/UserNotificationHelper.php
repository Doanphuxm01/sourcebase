<?php

namespace App\Helpers\Production;

use App\Helpers\UserNotificationHelperInterface;
use App\Models\Postgres\Account;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class UserNotificationHelper implements UserNotificationHelperInterface
{
    protected $url;
    protected $serverKey;
    protected $headers;
    protected $notification;

    public function __construct()
    {
        $this->url = config('firebase.url');
        $this->serverKey = config('firebase.server_key');
        $this->headers = [
            "Authorization" => 'key=' . $this->serverKey,
            'Content-Type' => 'application/json',
        ];
        $this->notification = [
            "title" => env('APP_NAME'),
            "body" => __('notification.system.notifications-app'),
        ];
    }

    public function sendFCM($device_tokens = [], $notification = [], $data = [])
    {
        if (count($device_tokens) <= 0) {
            return true;
        }
        if (count($notification) <= 0) {
            $notification = $this->notification;
        }
        $params = [
            "registration_ids" => $device_tokens,
            "notification" => $notification,
            "data" => [
                "payload" => $data
            ]
        ];
        $res = Http::withHeaders($this->headers)->post($this->url, $params);
        return $res->json();
    }

    public function sendWithAccountId($accounts_ids, $data, $notification = null)
    {
        $deviceTokens = [];
        $accounts = [];
        if (is_array($accounts_ids)) {
            $accounts = Account::whereIn('id', $accounts_ids)->get();
        } else {
            $account = Account::find($accounts_ids);
            $accounts = [$account];
        }
        foreach ($accounts as $account) {
            if (!empty($account->device_token_ios)) {
                $deviceTokens[] = $account->device_token_ios;
            }
            if (!empty($account->device_token_android)) {
                $deviceTokens[] = $account->device_token_android;
            }
        }
        return $this->sendFCM($deviceTokens, $notification? $notification : $this->notification, $data);
    }
}
