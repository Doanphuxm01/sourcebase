<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface OrganizationServiceInterface extends BaseServiceInterface
{
    public function createOrganization($data);

    public function updateOrganization($id, $name);
}
