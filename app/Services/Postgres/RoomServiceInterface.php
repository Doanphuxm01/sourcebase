<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface RoomServiceInterface extends BaseServiceInterface
{
    public function checkOutMasterRoom($accountId, $filter);

    public function getCreatedByRoom($accountId, $filter);

    public function isRoom($filter);

    public function updateRoomAndAllApplication($id, $spaceId, $applications, $data, $accountInfo, $accountIds);

    public function updateRoomAndAllApplicationWorkLife($id, $spaceId, $applications, $data, $accountInfo, $accountIds);

    public function checkClickInRoom($room, $accountInfo);
}
