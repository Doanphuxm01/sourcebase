<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface PermissionServiceInterface extends BaseServiceInterface
{
    public function addDecentralization($data, $permissions, $accountInfo);

    public function updateDecentralization($id, $data, $permissions, $accountInfo);

    public function deleteDecentralization($ids, $accountInfo);

    public function getListDecentralization($limit, $filter, $accountInfo);
}
