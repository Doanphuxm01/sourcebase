<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface PostEmotionServiceInterface extends BaseServiceInterface
{
    public function getAllReact($filter);
}
