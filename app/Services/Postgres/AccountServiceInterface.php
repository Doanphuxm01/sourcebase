<?php namespace App\Services\Postgres;

use App\Services\AuthenticationServiceInterface;
use App\Services\BaseServiceInterface;

interface AccountServiceInterface extends AuthenticationServiceInterface
{
    public function getDetailAccount($filter);

    public function updateAccountWithOrganization($id, $data, $organization);

    public function updateAccount($id, $data, $type);

    public function signOutAccount($token, $keyPlatform);

    public function updatePasswordAccount($id, $password, $newPassword);

    public function sendEmailToResetPasswordAccount($email);

    public function resetPasswordForAccount($token, $password);

    public function getAllAccount($limit, $filter);

    public function addAccount($data, $managerIds, $accountInfo);

    public function deleteAccount($ids, $accountInfo);

    public function getAllAccountForSelect($filter);

    public function getAccountBelongToOrganization($lsAccount, $id, $type, $request, $infoAccount);

    public function updateAccountWithManager($id, $data, $managerIds, $accountInfo);

    public function checkEmailLogin($data);

    public function updateDeviceTokenForAccount($deviceToken, $deviceType);

    public function activeAccount($lsObj);

    public function findAccountForEmail($emil);

    public function findAccountForId($id);

}
