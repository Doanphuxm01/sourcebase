<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface FileUploadServiceInterface extends BaseServiceInterface
{
    public function upload($configKey, $file, $storeAccount, $collectionType);
}
