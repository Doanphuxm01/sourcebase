<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface PostServiceInterface extends BaseServiceInterface
{
    public function deletePost($id, $spaceId, $accountInfo);

    public function addPost($data, $imageIds, $accountInfo);

    public function updatePost($id, $data, $imageIds, $accountInfo);

    public function getListPostByFilter($limit, $filter, $accountInfo);
}
