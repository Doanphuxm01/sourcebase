<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface ApplicationGroupServiceInterface extends BaseServiceInterface
{
    public function addApplicationGroup($appIds, $data, $type, $accountInfo);

    public function getDetailApplicationGroup($filter);

    public function deleteApplicationGroup($ids, $spaceId, $roomId, $accountInfo);

    public function updateApplicationGroup($id, $appIds, $data, $accountInfo);

    public function getAllGroupApplication($filter);
}
