<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface ImageServiceInterface extends BaseServiceInterface
{
    public function uploadFile($request);

    public function uploadImageForProfile($configKey, $file, $accountInfo, $collectionType);
}
