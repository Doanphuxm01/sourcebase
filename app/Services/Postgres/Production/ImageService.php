<?php namespace App\Services\Postgres\Production;


use App\Repositories\Postgres\ImageRepositoryInterface;
use \App\Services\Postgres\ImageServiceInterface;
use App\Services\Postgres\FileUploadServiceInterface;


use App\Services\Production\BaseService;
use Carbon\Carbon;

class ImageService extends BaseService implements ImageServiceInterface
{
    protected $imageRepository;
    protected $fileUploadService;

    public function __construct(ImageRepositoryInterface $imageRepository, FileUploadServiceInterface $fileUploadService)
    {
        $this->imageRepository = $imageRepository;
        $this->fileUploadService = $fileUploadService;
    }
    public function uploadFile($request)
    {
        $adminUser = $request->get('accountInfo');
        if ($request->hasFile('file')) {
            $fileName = $request->fileName;
            $organizationId = $request->organizationId;
            $file = $request->file('file');
            $mimes = $file->getMimeType();
            $extension = $file->getClientOriginalExtension();
            $size = $file->getSize();
            if ($mimes == 'video/mp4') {
                $duration = 255;
            } else {
                [$width, $height] = getimagesize($file);
                $dimension = 255;
            }
            $type = $request->type ?? 0;

            $fileNameMd5 =  $this->generateFileName($fileName, time(), $extension);
            $file->move(public_path() . '/files/', $fileNameMd5);
            $data = [
                'name' => $fileName,
                'md5_file' => $fileNameMd5,
                'source' => '/files/' . $fileNameMd5,
                'source_thumb' => '/files/' . $fileNameMd5,
                'organization_id' => $organizationId,
                'type' => $type,
                'file_size' => $size,
                'width' => $width ?? 0,
                'height' => $height ?? 0,
                'mimes' => $mimes,
                'duration' => $duration ?? 255,
                'dimension' => $dimension ?? 255,
                'created_by' =>  $adminUser['id'],
            ];
            return $data;
        } else {
            return false;
        }
    }

    private function generateFileName($seed, $postFix, $ext)
    {
        $filename = md5($seed);
        if (!empty($postFix)) {
            $filename .= '_' . $postFix;
        }
        if (!empty($ext)) {
            $filename .= '.' . $ext;
        }
        return $filename;
    }

    public function uploadImageForProfile($configKey, $file, $accountInfo, $collectionType)
    {
        $image = $this->fileUploadService->upload($configKey, $file, $accountInfo, $collectionType);
        if (!empty($image)){
            $image = $this->imageRepository->create($image);
            return $image->id;
        }

        return false;
    }
}
