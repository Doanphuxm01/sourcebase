<?php namespace App\Services\Postgres\Production;

use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationGroup;
use App\Models\Postgres\ApplicationLocation;
use App\Repositories\Postgres\ApplicationGroupLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationGroupRepositoryInterface;
use App\Repositories\Postgres\ApplicationLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use \App\Services\Postgres\ApplicationGroupServiceInterface;

use App\Services\Production\BaseService;
use Illuminate\Database\Eloquent\Model;

class ApplicationGroupService extends BaseService implements ApplicationGroupServiceInterface
{
    protected $applicationGroupRepository;
    protected $applicationGroupLocationRepository;
    protected $applicationLocationRepository;
    protected $applicationRepository;

    public function __construct(
        ApplicationLocationRepositoryInterface $applicationLocationRepository,
        ApplicationGroupRepositoryInterface $applicationGroupRepository,
        ApplicationGroupLocationRepositoryInterface $applicationGroupLocationRepository,
        ApplicationRepositoryInterface $applicationRepository
    ) {
        $this->applicationLocationRepository = $applicationLocationRepository;
        $this->applicationGroupRepository = $applicationGroupRepository;
        $this->applicationGroupLocationRepository = $applicationGroupLocationRepository;
        $this->applicationRepository = $applicationRepository;
    }
    public function addApplicationGroup($appIds, $data, $type, $accountInfo)
    {
        $applicationGroup = $this->applicationGroupRepository->create($data);
        if (empty($applicationGroup)) {
            return false;
        }

        $applicationLocations = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
        if (!empty($applicationLocations)) {
            $this->applicationLocationRepository->create([
                'organization_id' => $accountInfo['organization_id'],
                'account_id' => $accountInfo['id'],
                'application_id' => null,
                'application_group_id' => $applicationGroup->id,
                'type' => ApplicationLocation::TYPE_APPLICATION_GROUP,
                'space_id' => $data['space_id'],
                'room_id' => $data['room_id'],
                'position' => count($applicationLocations) + 1
            ]);
        } else {
            $this->applicationLocationRepository->create([
                'organization_id' => $accountInfo['organization_id'],
                'account_id' => $accountInfo['id'],
                'application_id' => null,
                'application_group_id' => $applicationGroup->id,
                'type' => ApplicationLocation::TYPE_APPLICATION_GROUP,
                'space_id' => $data['space_id'],
                'room_id' => $data['room_id'],
                'position' => 1
            ]);
        }

        if (!empty($appIds)) {
            $this->addApplicationToApplicationGroup($applicationGroup->id, $appIds);
            $this->addApplicationGroupLocation($applicationGroup->id, $data['space_id'], $data['room_id'], $appIds, $accountInfo);
            $applicationLocationForSorts = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $data['space_id'], 'application_group_id' => null, 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
            if (!empty($applicationLocationForSorts)) {
                $arrApplicationLocations = [];
                foreach ($applicationLocationForSorts as $k => $applicationLocationForSort) {
                    $arrApplicationLocations[] = [
                        'id' => $applicationLocationForSort['id'],
                        'position' => $k + 1
                    ];
                }

                if (!empty($arrApplicationLocations)) {
                    $applicationGroupInstance = new ApplicationLocation();
                    $indexApplicationGroup = 'id';
                    \Batch::update($applicationGroupInstance, $arrApplicationLocations, $indexApplicationGroup);
                }
            }
        }

        if ($type === ApplicationGroup::GET_APPLICATION_GROUP) {
            return $applicationGroup->toArray();
        }

        return true;
    }

    public function addApplicationToApplicationGroup($id, $appIds)
    {
        $applications = $this->applicationRepository->getAllApplicationByFilter(['id' => $appIds, 'deleted_at' => true]);
        if (!empty($applications)) {
            $arrApplication = [];
            foreach ($applications as $application) {
                $arrApplication[] = [
                    'id' => $application['id'],
                    'application_group_id' => $id
                ];
            }

            if (!empty($arrApplication)) {
                $applicationInstance = new Application();
                $indexApplication = 'id';
                \Batch::update($applicationInstance, $arrApplication, $indexApplication);
            }
        }
    }

    public function addApplicationGroupLocation($id, $spaceId, $roomId, $appIds, $accountInfo)
    {
        $this->applicationGroupLocationRepository->deleteAllApplicationGroupLocationByFilter(['application_group_id' => $id, 'space_id' => $spaceId, 'room_id' => $roomId]);
        $arrApplicationGroupLocation = [];
        foreach ($appIds as $k => $appId) {
            $arrApplicationGroupLocation[] = [
                'application_id' => $appId,
                'application_group_id' => $id,
                'space_id' => $spaceId,
                'room_id' => $roomId,
                'position' => $k + 1,
                'organization_id' => $accountInfo['organization_id'],
                'account_id' => $accountInfo['id'],
            ];
        }

        if (!empty($arrApplicationGroupLocation)) {
            $this->applicationGroupLocationRepository->insertMulti($arrApplicationGroupLocation);
        }
    }

    public function getDetailApplicationGroup($filter)
    {
        $applicationGroup = $this->applicationGroupRepository->getOneArrayApplicationGroupByFilter($filter);
        if (!empty($applicationGroup) && !empty($applicationGroup['applications'])) {
            $arrApplications = [];
            foreach ($applicationGroup['applications'] as $application) {
                $arrApplications[] = [
                    'id' => $application['id'],
                    'name' => @$application['name'],
                    'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                    'link_android' => @$application['link_android'],
                    'link_ios' => @$application['link_ios'],
                    'link_url' => @$application['link_url'],
                    'is_accept' => @$application['is_accept'],
                    'type' => Application::TYPE_VIEW_SINGLE,
                    'application_store' => @$application['application_store'],
                ];
            }

            if (!empty($arrApplications)) {
                $applicationGroup['applications'] = $arrApplications;
            }
        }
        return $applicationGroup;
    }

    public function updateApplicationGroup($id, $appIds, $data, $accountInfo)
    {
        $applicationGroup = $this->applicationGroupRepository->getOneObjectApplicationGroupByFilter(['id' => (int)$id, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        if (empty($applicationGroup)) {
            return false;
        }

        $this->applicationGroupRepository->update($applicationGroup, $data);
        if (!empty($appIds)) {
            $applications = $this->applicationRepository->getAllApplicationByFilter(['application_group_id' => $id, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
            if (!empty($applications)) {
                $this->addApplicationGroupLocation($id, $data['space_id'], $data['room_id'], $appIds, $accountInfo);

                $appIdsAdded = collect($applications)->pluck('id')->values()->toArray();
                $arrAdd = array_diff($appIds, $appIdsAdded);
                $arrDel = array_diff($appIdsAdded, $appIds);
                if (!empty($arrAdd)) {
                    $this->addApplicationToApplicationGroup($id, $arrAdd);
                }
                if (!empty($arrDel)) {
                    $this->applicationRepository->deleteApplicationFromApplicationGroupByFilter(['id' => $arrDel, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
                }
            } else {
                $this->applicationRepository->deleteApplicationFromApplicationGroupByFilter(['application_group_id' => $id, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
                $this->addApplicationToApplicationGroup($id, $appIds);
            }
        } else {
            $this->applicationRepository->deleteApplicationFromApplicationGroupByFilter(['application_group_id' => $id, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        }

        return true;
    }

    public function deleteApplicationGroup($ids, $spaceId, $roomId, $accountInfo)
    {
        $arrApplicationLocations = $arrNewApplicationLocations = [];
        $applications = $this->applicationRepository->getAllApplicationByFilter(['application_group_id' => $ids, 'space_id' => $spaceId, 'room_id' => $roomId, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationGroupRepository->deleteAllApplicationGroupByFilter(['id' => $ids, 'space_id' => $spaceId, 'room_id' => $roomId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationRepository->deleteApplicationFromApplicationGroupByFilter(['application_group_id' => $ids, 'space_id' => $spaceId, 'room_id' => $roomId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationGroupLocationRepository->deleteAllApplicationGroupLocationByFilter(['application_group_id' => $ids, 'space_id' => $spaceId, 'room_id' => $roomId, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationLocationRepository->deleteAllApplicationLocationByFilter(['application_group_id' => $ids, 'space_id' => $spaceId, 'room_id' => $roomId, 'organization_id' => $accountInfo['organization_id']]);
        $applicationLocations = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $spaceId, 'room_id' => $roomId, 'organization_id' => $accountInfo['organization_id']]);
        if (!empty($applicationLocations)) {
            foreach ($applicationLocations as $k => $applicationLocation) {
                $arrApplicationLocations[] = [
                    'id' => $applicationLocation['id'],
                    'position' => $k + 1
                ];
            }
        }
        if (!empty($applications)) {
            foreach ($applications as $application) {
                $arrNewApplicationLocations[] = [
                    'organization_id' => $accountInfo['organization_id'],
                    'account_id' => $accountInfo['id'],
                    'application_id' => $application['id'],
                    'type' => ApplicationLocation::TYPE_APPLICATION,
                    'space_id' => $spaceId,
                    'room_id' => $roomId,
                    'position' => count($applicationLocations) + $k + 1
                ];
            }
        }

        if (!empty($arrApplicationLocations)) {
            $applicationGroupInstance = new ApplicationLocation();
            $indexApplicationGroup = 'id';
            \Batch::update($applicationGroupInstance, $arrApplicationLocations, $indexApplicationGroup);
        }

        if (!empty($arrNewApplicationLocations)) {
            $this->applicationLocationRepository->insertMulti($arrNewApplicationLocations);
        }

        return true;
    }

    public function getAllGroupApplication($filter)
    {
        $groupApplications = $this->applicationGroupRepository->getAllGroupApplicationByFilter($filter);
        return $groupApplications;
    }
}
