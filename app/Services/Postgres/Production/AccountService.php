<?php namespace App\Services\Postgres\Production;

use App\Elibs\Debug;
use App\Elibs\eCache;
use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Models\Base;
use App\Models\Postgres\Account;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\ManagementAccountRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use App\Services\Postgres\AccountServiceInterface;
use App\Services\Production\AuthenticationService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Hash;

class AccountService extends AuthenticationService implements AccountServiceInterface
{
    protected $accountRepository;
    protected $spaceRepository;
    protected $managementAccountRepository;

    public function __construct(
        AccountRepositoryInterface $accountRepository,
        SpaceRepositoryInterface $spaceRepository,
        AccountAccessTokensRepositoryInterface $accountAccessTokensRepository,
        ManagementAccountRepositoryInterface $managementAccountRepository
    )
    {
        $this->spaceRepository = $spaceRepository;
        $this->authenticationRepository = $accountRepository;
        $this->accountAccessTokensRepository = $accountAccessTokensRepository;
        $this->managementAccountRepository = $managementAccountRepository;
    }

    public function getGuardName()
    {
        return 'admins';
    }

    public function getDetailAccount($filter)
    {
        $account = $this->authenticationRepository->getOneArrayAccountWithModelByFilter($filter);
        if (!empty($account)) {
            $account['profile_image'] = !empty($account['profile_image']['source']) && file_exists(public_path() . '/' . $account['profile_image']['source']) ? asset($account['profile_image']['source']) : '';
            $account['background_image'] = !empty($account['background_image']['source']) && file_exists(public_path() . '/' . $account['background_image']['source']) ? asset($account['background_image']['source']) : '';
            $account['organization_name'] = !empty($account['organization']['name']) ? $account['organization']['name'] : null;
            $account['package_name'] = !empty($account['organization']['package']['name']) ? $account['organization']['package']['name'] : null;

            return $account;
        }

        return [];
    }

    public function getAllAccountForSelect($filter)
    {
        $arrAccounts = [];
        $accounts = $this->authenticationRepository->getAllAccountForSelectByFilter($filter);
        if (!empty($accounts)) {
            //            $arrAccounts[] = [
            //                'id' => Account::ALL_ACCOUNT,
            //                'name' => 'All'
            //            ];

            foreach ($accounts as $k => $account) {
                $arrAccounts[$k + 1]['id'] = !empty($account['id']) ? $account['id'] : '';
                $arrAccounts[$k + 1]['name'] = !empty($account['name']) ? $account['name'] : '';
                $arrAccounts[$k + 1]['email'] = !empty($account['email']) ? $account['email'] : '';
                $arrAccounts[$k + 1]['phone_number'] = !empty($account['phone_number']) ? $account['phone_number'] : '';
                $arrAccounts[$k + 1]['type_account'] = empty($account['type_account']) ? $account['type_account'] : '';
                if (!empty($account['profile_image'])) {
                    $arrAccounts[$k + 1]['profile_image']['id'] = !empty($account['profile_image']['id']) ? $account['profile_image']['id'] : '';
                    $arrAccounts[$k + 1]['profile_image']['name'] = !empty($account['profile_image']['name']) ? asset($account['profile_image']['name']) : '';
                    $arrAccounts[$k + 1]['profile_image']['md5_file'] = !empty($account['profile_image']['md5_file']) ? asset($account['profile_image']['md5_file']) : '';
                    $arrAccounts[$k + 1]['profile_image']['source'] = !empty($account['profile_image']['source']) && file_exists(public_path() . '/' . $account['profile_image']['source']) ? asset($account['profile_image']['source']) : '';
                    $arrAccounts[$k + 1]['profile_image']['source_thumb'] = !empty($account['profile_image']['source_thumb']) ? asset($account['profile_image']['source_thumb']) : '';
                } else {
                    $arrAccounts[$k + 1]['profile_image'] = null;
                }
//                $arrAccounts[$k + 1]['position'] = !empty($account['position']['name']) ? $account['position']['name'] : '';
//                $arrAccounts[$k + 1]['managers'] = !empty($account['managers']) ? $account['managers'] : [];
//                $arrAccounts[$k + 1]['type'] = Account::DONES_PRO;
            }
        }
        return $arrAccounts;
    }

    public function updateAccountWithOrganization($id, $data, $organization)
    {
        $params = [
            'is_active' => Account::IS_ACTIVE,
            'position_id' => $data['position_id'],
            'organization_id' => $organization['id'],
            'space_id' => $data['space_id'],
        ];

        $newAccount = $this->updateAccount($id, $params, Account::NON_GET_ACCOUNT_INFO);

        return $newAccount;
    }

    public function signOutAccount($token, $keyPlatform)
    {
        // $account = $this->authenticationRepository->getOneObjectAccountByFilter([Account::KEY_SIGN_IN[$keyPlatform] => $token]);
        // if (!empty($account)) {
        //     $this->authenticationRepository->update($account, [Account::KEY_SIGN_IN[$keyPlatform] => '']);
        //     return true;
        // }
        $this->accountAccessTokensRepository->deleteToken($token);
        return true;
    }

    public function updateAccount($id, $data, $type)
    {
        if (!empty($data['keyPlatform'])) {
            $keyPlatform = $data['keyPlatform'];
            unset($data['keyPlatform']);
        }

        $account = $this->authenticationRepository->updateAllAccountByFilter(['id' => (int)$id, 'deleted_at' => true], $data);
        if (empty($account)) {
            return false;
        }

        if ($type === Account::GET_ACCOUNT_INFO) {
            $account = $this->authenticationRepository->getOneArrayAccountWithManagerByFilter(['id' => (int)$id, 'deleted_at' => true]);

            if (!empty($account)) {
                $arrAccount = [
                    'id' => $account['id'],
                    'name' => $account['name'],
                    'email' => $account['email'],
                    "is_active" => $account['is_active'],
                    "is_root" => $account['is_root'],
                    "username" => $account['username'],
                    "organization_id" => $account['organization_id'],
                    "phone_number" => $account['phone_number'],
                    'language' => $account['language'],
                    // 'token' => !empty($account[Account::KEY_SIGN_IN[$keyPlatform]]) ? $account[Account::KEY_SIGN_IN[$keyPlatform]] : '',
                    'avatar' => !empty($account['profile_image']['source']) && file_exists(public_path() . '/' . $account['profile_image']['source']) ? asset($account['profile_image']['source']) : '',
                    'cover' => !empty($account['background_image']['source']) && file_exists(public_path() . '/' . $account['background_image']['source']) ? asset($account['background_image']['source']) : '',
                    'position' => !empty($account['position']['name']) ? $account['position']['name'] : '',
                    'organization' => !empty($account['organization']['name']) ? $account['organization']['name'] : '',
                    'space_id' => !empty($account['space_id']) ? $account['space_id'] : '',
                ];

                return $arrAccount;
            }
        }

        return true;
    }

    public function updatePasswordAccount($id, $password, $newPassword)
    {
        $account = $this->authenticationRepository->getOneObjectAccountByFilter(['id' => (int)$id, 'deleted_at' => true, 'is_active' => Account::IS_ACTIVE]);

        if (empty($account)) {
            return [
                'error_code' => 1,
                'message' => 'Tài khoản không tồn tại'
            ];
        }

        $check_pass = \Hash::check($password, $account->password);
        if (!$check_pass) {
            return [
                'error_code' => 2,
                'message' => 'Mật khẩu hiện tại không chính xác'
            ];
        }

        $this->authenticationRepository->update($account, [
            'password' => $newPassword,
            'token' => '',
            // 'api_web_access_token' => '',
            // 'api_web_app_access_token' => '',
            // 'api_ios_access_token' => '',
            // 'api_android_access_token' => '',
            'is_first_login' => Account::LOGIN_NORM
        ]);
        $this->sendRabbitMQForMail($account->toArray(), Account::CHANGE_ACCOUNT, '', '', '');

        return [
            'error_code' => 0,
            'message' => 'Success'
        ];
    }

    public function sendEmailToResetPasswordAccount($email)
    {
        $account = $this->authenticationRepository->getOneArrayAccountByFilter(['email' => $email, 'deleted_at' => true]);
        if (empty($account)) {
            return false;
        }
        // send code check active tài khoản\
        $randomNumberCode = eFunction::randomNumber(6);
        $keyRedis = Account::keyRedisOtpForEmail($email);
        $account['url'] = $randomNumberCode;
        $account['code'] = $randomNumberCode;
        $token = Str::random(60);
        $account['token'] = $token;
        $value = eCrypt::encryptAES($account['id'] . Account::KEY_RESET_PASSWORD . $account['email'] . Account::KEY_RESET_PASSWORD . strtotime('now'));
        eCache::add($token, $value, Account::KEY_RESET_PASSWORD);
        eFunction::setRedisWithExpiredTime($keyRedis, \GuzzleHttp\json_encode($account), Base::TIME_SIGN_UP);

        self::sendRabbitMQForMail($account, Account::RESET_PASSWORD, '', $token, '');
        return $token;
    }

    public function sendRabbitMQForMail($account, $type, $password, $token, $createdBy)
    {
        if (empty($account['email'])) {
            return false;
        }

        $data = [
            'rootName' => $createdBy,
            'email' => $account['email'],
            'name' => $account['name'],
            'username' => $account['username'],
            'password' => $password,
//            'url' => (int)$type === Account::RESET_PASSWORD ? config('const.app_url') . 'reset-password?token=' . $token : (!empty($account['url']) ? $account['url'] : config('const.app_url'))
            'url' => (int)$type === Account::RESET_PASSWORD ? $account['url'] : (!empty($account['url']) ? $account['url'] : config('const.app_url'))
        ];
        $tpl = [
            'pattern' => Account::PATTERN[$type],
            'data' => eCrypt::encryptAES(json_encode($data))
        ];

        eFunction::sendMessageQueueFormSendEmail($tpl, Account::QUEUE_NAME_SEND_EMAIL);
        return true;
    }

    public function resetPasswordForAccount($array, $password)
    {
//        $cacheExists = eCache::get($token);
//        if (empty($cacheExists)) {
//            return false;
//        }
//
//        $decrypt = eCrypt::decryptAES($cacheExists);
//        $arr = explode(Account::KEY_RESET_PASSWORD, $decrypt);
//        dd($arr);
//        if (empty($arr)) {
//            return false;
//        }
//        $accountExists = $this->authenticationRepository->getOneObjectAccountByFilter(['id' => (int)$arr[0], 'email' => $arr[1], 'deleted_at' => true]);
        $accountExists = $this->authenticationRepository->getOneObjectAccountByFilter(['id' => (int)$array['id'], 'email' => $array['email'], 'deleted_at' => true]);
        if (empty($accountExists)) {
            return false;
        }

        $account = $this->authenticationRepository->update($accountExists, ['api_web_access_token' => '', 'api_ios_access_token' => '', 'api_android_access_token' => '', 'api_web_app_access_token' => '', 'password' => $password]);
        //clear token for user
        $this->accountAccessTokensRepository->clearAllTokenForUser($accountExists['id']);
        if (empty($account)) {
            return false;
        }

//        eFunction::delRedis($token);

        return true;
    }

    public function getAllAccount($limit, $filter)
    {
        $accounts = $this->authenticationRepository->getListAccountByFilter($limit, $filter);
        if (!empty($accounts['data'])) {
            foreach ($accounts['data'] as $k => $account) {
                $accounts['data'][$k]['avatar'] = !empty($account['profile_image']['source']) && file_exists(public_path() . '/' . $account['profile_image']['source']) ? asset($account['profile_image']['source']) : null;
            }
        }
        return $accounts;
    }

    public function addAccount($data, $managerIds, $accountInfo)
    {
        $account = $this->authenticationRepository->create($data);
        if (!empty($account)) {
            $this->sendRabbitMQForMail($account->toArray(), Account::ACTIVE_ACCOUNT, $data['password'], '', $accountInfo['name']);
            if (!empty($managerIds)) {
                $arrManagers = [];
                if (in_array(Account::ALL_ACCOUNT, $managerIds)) {
                    $filter['is_active'] = Account::IS_ACTIVE;
                    $filter['organization_id'] = $accountInfo['organization_id'];
                    $filter['deleted_at'] = true;
                    $accounts = $this->authenticationRepository->getAllAccountForSelectByFilter($filter);
                    $this->authenticationRepository->updateAllAccountByFilter($filter, ['is_leader' => Account::IS_LEADER]);
                    if (!empty($accounts)) {
                        foreach ($accounts as $acc) {
                            $arrManagers[] = [
                                'manager_id' => (int)$acc['id'],
                                'account_id' => $account->id,
                                'created_by' => $accountInfo['id'],
                                'organization_id' => $accountInfo['organization_id'],
                                'created_at' => eFunction::getDateTimeNow(),
                                'updated_at' => eFunction::getDateTimeNow(),
                            ];
                        }
                    }
                } else {
                    $accounts = $this->authenticationRepository->countAllAccountByFilter(['id' => $managerIds, 'deleted_at' => true, 'is_active' => Account::IS_ACTIVE]);
                    if ($accounts !== count($managerIds)) {
                        return false;
                    }

                    $this->authenticationRepository->updateAllAccountByFilter(['id' => $managerIds, 'deleted_at' => true, 'is_active' => Account::IS_ACTIVE], ['is_leader' => Account::IS_LEADER]);
                    foreach ($managerIds as $idC) {
                        $arrManagers[] = [
                            'manager_id' => (int)$idC,
                            'account_id' => $account->id,
                            'created_by' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                            'created_at' => eFunction::getDateTimeNow(),
                            'updated_at' => eFunction::getDateTimeNow(),
                        ];
                    }
                }

                if (!empty($arrManagers)) {
                    $this->managementAccountRepository->insertMulti($arrManagers);
                }
            }
        }

        return $account;
    }

    public function deleteAccount($ids, $accountInfo)
    {
        $accounts = $this->authenticationRepository->getAllAccountByFilter(['id' => $ids, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        if (empty($accounts)) {
            return false;
        }

        $accountIds = collect($accounts)->pluck('id')->values()->toArray();
        $this->authenticationRepository->deleteAllAccountByFilter(['id' => $ids, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        $this->managementAccountRepository->deleteAllManagementAccountByFilter(['account_id' => $accountIds, 'organization_id' => $accountInfo['organization_id']]);
        return true;
    }

    public function updateAccountWithManager($id, $data, $managerIds, $accountInfo)
    {
        $account = $this->authenticationRepository->getOneObjectAccountByFilter(['id' => $id, 'is_active' => Account::IS_ACTIVE, 'deleted_at' => true]);
        if (empty($account)) {
            return false;
        }
        $newAccount = $this->authenticationRepository->update($account, $data);
        if (!empty($newAccount)) {
            if (!empty($managerIds)) {
                if (in_array(Account::ALL_ACCOUNT, $managerIds)) {
                    // $filter['is_active'] = Account::IS_ACTIVE;
                    $filter['organization_id'] = $accountInfo['organization_id'];
                    $filter['deleted_at'] = true;
                    $accounts = $this->authenticationRepository->getAllAccountForSelectByFilter($filter);
                    if (!empty($accounts)) {
                        $managementAccounts = $this->managementAccountRepository->getAllManagementAccountByFilter(['account_id' => (int)$id]);
                        if (!empty($managementAccounts)) {
                            $managerAccountIds = collect($managementAccounts)->pluck('manager_id')->filter()->unique()->values()->toArray();
                            $accounts = collect($accounts)->filter(function ($q) use ($managerAccountIds, $id) {
                                return (int)$q['id'] !== (int)$id && !in_array($q['id'], $managerAccountIds);
                            });
                        }

                        foreach ($accounts as $acc) {
                            $arrManagers[] = [
                                'manager_id' => (int)$acc['id'],
                                'account_id' => $account->id,
                                'created_by' => $accountInfo['id'],
                                'organization_id' => $accountInfo['organization_id'],
                                'created_at' => eFunction::getDateTimeNow(),
                                'updated_at' => eFunction::getDateTimeNow(),
                            ];
                        }
                    }
                } else {
                    $accounts = $this->authenticationRepository->countAllAccountByFilter(['id' => $managerIds, 'deleted_at' => true]);
                    if ($accounts !== count($managerIds)) {
                        return false;
                    }

                    $arrManagers = [];
                    $managementAccounts = $this->managementAccountRepository->getAllManagementAccountByFilter(['account_id' => (int)$id]);
                    if (!empty($managementAccounts)) {
                        $oldIds = collect($managementAccounts)->pluck('manager_id')->filter()->unique()->values()->toArray();

                        $arrNew = array_diff($managerIds, $oldIds);
                        $arrDel = array_diff($oldIds, $managerIds);

                        if (!empty($arrNew)) {
                            foreach ($arrNew as $idC) {
                                $arrManagers[] = [
                                    'manager_id' => (int)$idC,
                                    'account_id' => (int)$id,
                                    'created_by' => $accountInfo['id'],
                                    'organization_id' => $accountInfo['organization_id'],
                                    'created_at' => eFunction::getDateTimeNow(),
                                    'updated_at' => eFunction::getDateTimeNow(),
                                ];
                            }
                        }

                        if (!empty($arrDel)) {
                            $this->managementAccountRepository->deleteAllManagementAccountByFilter(['account_id' => (int)$id, 'manager_id' => $arrDel]);
                        }
                    } else {
                        foreach ($managerIds as $idC) {
                            $arrManagers[] = [
                                'manager_id' => (int)$idC,
                                'account_id' => (int)$id,
                                'created_by' => $accountInfo['id'],
                                'organization_id' => $accountInfo['organization_id'],
                                'created_at' => eFunction::getDateTimeNow(),
                                'updated_at' => eFunction::getDateTimeNow(),
                            ];
                        }
                    }

                    if (!empty($arrManagers)) {
                        $this->managementAccountRepository->insertMulti($arrManagers);
                    }
                }
            } else {
                $this->managementAccountRepository->deleteAllManagementAccountByFilter(['account_id' => (int)$id]);
            }
        }

        return true;
    }

    public function updateDeviceTokenForAccount($deviceToken, $deviceType)
    {
        $field = $deviceType == Account::TYPE_SIGN_IN_IOS ? 'device_token_ios' : 'device_token_android';
        $data = $this->authenticationRepository->updateAllAccountByFilter(
            ["id" => request()->get('accountInfo')['id']],
            [
                $field => $deviceToken,
                "device_type" => $deviceType
            ]
        );
        return $data;
    }

    public function checkEmailLogin($data)
    {
        $accountExists = $this->authenticationRepository->getOneObjectAccountByFilter(['google_id' => $data['google_id'], 'email' => $data['email'], 'deleted_at' => true]);
        if (empty($accountExists)) {
            $accountExists = $this->authenticationRepository->getOneObjectAccountByFilter(['email' => $data['email'], 'deleted_at' => true]);
        }

        if (!empty($accountExists)) {
            $this->authenticationRepository->update($accountExists, ['google_id' => $data['google_id']]);
            $accountExists['platform'] = $data['platform'];
            // $accountExists = $this->setAPIAccessToken($accountExists);
            if (!empty($accountExists)) {
                return $accountExists;
            }
        }

        return [];
    }

    public function getAccountBelongToOrganization($lsAccount, $id, $type, $request, $infoAccount)
    {
        try {
            $where = [
                'is_active' => Account::IS_ACTIVE,
                'organization_id' => $infoAccount['organization_id'],
            ];
            $listObj = Account::where($where)->where('id', '<>', $infoAccount['id']);
            $search = $request['search'] ?? '';
            if (!empty($search)) {
                // nếu search theo từ khóa
                // $listObj = $listObj->where(DB::raw('vn_unaccent(name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                $listObj = $listObj->where(function ($query) use ($search) {
                    $query->orWhere(DB::raw('vn_unaccent(name)'), 'like', '%' . Str::lower(Str::ascii($search) . '%'))
                        ->orWhere('email', 'like', '%' . Str::lower(Str::ascii($search) . '%'));
                });
            }

            // nếu sarch all
            $listObj = $listObj->with('profileImage', function ($q) {
                $q->select('id', 'name', 'source_thumb', 'source');
            });

            $listObj = $listObj->select('id', 'name', 'username', 'email', 'profile_image_id')->get()->toArray();
            $query = array_map(function ($el) {
                $el['type'] = Account::DONES_PRO;
                if (!empty($el['profile_image'])) {
                    if (is_array($el['profile_image'])) {
                        // $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
                        $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? config('systems.URL_IMAGE_LIFE') . ($el['profile_image']['source_thumb']) : '';
                        $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
                    }
                }
                return $el;
            }, $listObj);
            $result = collect($query)->paginate(10)->toArray();

            sort($result['data']);
            return $result;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function activeAccount($lsObj)
    {
        $isMe = Account::where('id', $lsObj->id)->first();
        if (!empty($isMe)) {
//            Account::where('id', $lsObj->id)->update(['is_check_active' => Account::IS_ACTIVE]);
            Account::where('id', $lsObj->id)->update(['is_check_active' => Account::IS_DISABLE]);
            return true;
        }
        return false;
    }

    public function findAccountForEmail($mail)
    {
        $query = Account::where('email', $mail)->first();
        if (!empty($query)) {
            return $query->toArray();
        }
        return false;
    }

    public function findAccountForId($id)
    {
        $query = Account::where('id', $id)->first();
        if (!empty($query)) {
            return $query->toArray();
        }
        return false;
    }
}
