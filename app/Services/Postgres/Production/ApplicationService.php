<?php namespace App\Services\Postgres\Production;

use App\Elibs\Debug;
use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Jobs\UploadFileApplication;
use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationAdvancedSetting;
use App\Models\Postgres\ApplicationGroupLocation;
use App\Models\Postgres\ApplicationLocation;
use App\Models\Postgres\ApplicationStore;
use App\Models\Postgres\Image;
use App\Models\Postgres\StatisticAccount;
use App\Models\Postgres\StatisticApplication;
use App\Repositories\Postgres\ApplicationAdvancedSettingRepositoryInterface;
use App\Repositories\Postgres\ApplicationGroupLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationGroupRepositoryInterface;
use App\Repositories\Postgres\ApplicationLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\Eloquent\NotificationRepository;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Repositories\Postgres\NotificationRepositoryInterface;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Repositories\Postgres\StatisticAccountRepositoryInterface;
use App\Repositories\Postgres\StatisticApplicationRepositoryInterface;
use \App\Services\Postgres\ApplicationServiceInterface;

use App\Services\Production\BaseService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use tests\models\Postgres\ApplicationGroupLocationTest;
use phpDocumentor\Reflection\File;
use function PHPUnit\Framework\fileExists;

class ApplicationService extends BaseService implements ApplicationServiceInterface
{
    protected $applicationGroupRepository;
    protected $applicationAdvancedSettingRepository;
    protected $applicationRepository;
    protected $quickBarRepository;
    protected $statisticApplicationRepository;
    protected $statisticAccountRepository;
    protected $applicationLocationRepository;
    protected $applicationGroupLocationRepository;
    protected $imageRepository;
    protected $notificationRepository;

    public function __construct(
        ApplicationGroupRepositoryInterface $applicationGroupRepository,
        ApplicationAdvancedSettingRepositoryInterface $applicationAdvancedSettingRepository,
        ApplicationLocationRepositoryInterface $applicationLocationRepository,
        ApplicationGroupLocationRepositoryInterface $applicationGroupLocationRepository,
        QuickBarRepositoryInterface $quickBarRepository,
        StatisticApplicationRepositoryInterface $statisticApplicationRepository,
        StatisticAccountRepositoryInterface $statisticAccountRepository,
        ApplicationRepositoryInterface $applicationRepository,
        ImageRepositoryInterface $imageRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->applicationAdvancedSettingRepository = $applicationAdvancedSettingRepository;
        $this->applicationGroupRepository = $applicationGroupRepository;
        $this->applicationLocationRepository = $applicationLocationRepository;
        $this->applicationGroupLocationRepository = $applicationGroupLocationRepository;
        $this->quickBarRepository = $quickBarRepository;
        $this->statisticAccountRepository = $statisticAccountRepository;
        $this->statisticApplicationRepository = $statisticApplicationRepository;
        $this->applicationRepository = $applicationRepository;
        $this->imageRepository = $imageRepository;
        $this->notificationRepository = $notificationRepository;
    }

    public function addApplication($timeSlot, $data, $accountInfo)
    {
        $application = $this->applicationRepository->create($data);
        if (!empty($application)) {
            if ((int)$data['type'] !== Application::TYPE_APP_BAR) {
                $this->addLocation($application->id, ApplicationLocation::TYPE_ADD_NEW, 0, $data, $accountInfo);
            }

            if (!empty($timeSlot)) {
                $arrAdvanced = [];
                foreach ($timeSlot as $time) {
                    // $time = json_decode($time, true);
                    if (!empty($time['weekdays']) && is_array($time['weekdays'])) {
                        $arrAdvanced[] = [
                            'application_id' => $application->id,
                            'time_start' => $time['time_start'],
                            'time_end' => $time['time_end'],
                            'monday' => in_array(ApplicationAdvancedSetting::MONDAY, $time['weekdays']) ? 1 : 0,
                            'tuesday' => in_array(ApplicationAdvancedSetting::TUESDAY, $time['weekdays']) ? 1 : 0,
                            'wednesday' => in_array(ApplicationAdvancedSetting::WEDNESDAY, $time['weekdays']) ? 1 : 0,
                            'thursday' => in_array(ApplicationAdvancedSetting::THURSDAY, $time['weekdays']) ? 1 : 0,
                            'friday' => in_array(ApplicationAdvancedSetting::FRIDAY, $time['weekdays']) ? 1 : 0,
                            'saturday' => in_array(ApplicationAdvancedSetting::SATURDAY, $time['weekdays']) ? 1 : 0,
                            'sunday' => in_array(ApplicationAdvancedSetting::SUNDAY, $time['weekdays']) ? 1 : 0,
                            'clicks' => $time['clicks'],
                            'created_by' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                        ];
                    }
                }

                if (!empty($arrAdvanced)) {
                    $this->applicationAdvancedSettingRepository->createMulti($arrAdvanced);
                }
            }
        }

        return $application;
    }

    public function getDetailCreatedByApplication($filter, $accountInfo)
    {
        $application = $this->applicationRepository->getOneArrayApplicationWithAdvancedByFilter($filter, $accountInfo);
        if (!empty($application)) {
            return $application;
        }

        return [];
    }

    public function getDetailApplication($filter, $accountInfo)
    {
        $application = $this->applicationRepository->getOneArrayApplicationWithAdvancedByFilter($filter, $accountInfo);
        if (!empty($application)) {
            if (!empty($application['advanced_setting'])) {
                $arrAdvancedSetting = [];
                foreach ($application['advanced_setting'] as $setting) {
                    $arrWeekdays = [];
                    if (!empty($setting['monday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::MONDAY);
                    }

                    if (!empty($setting['tuesday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::TUESDAY);
                    }

                    if (!empty($setting['wednesday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::WEDNESDAY);
                    }

                    if (!empty($setting['thursday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::THURSDAY);
                    }

                    if (!empty($setting['friday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::FRIDAY);
                    }

                    if (!empty($setting['saturday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::SATURDAY);
                    }

                    if (!empty($setting['sunday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::SUNDAY);
                    }

                    $arrAdvancedSetting[] = [
                        "id" => $setting['id'],
                        'application_id' => $setting['application_id'],
                        'time_start' => $setting['time_start'],
                        'time_end' => $setting['time_end'],
                        'weekdays' => $arrWeekdays,
                        'clicks' => $setting['clicks'],
                    ];
                }

                $application['advanced_setting'] = $arrAdvancedSetting;
            }

            $application['avatar'] = !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '';
        }
        $application['application_type'] = Application::TYPE_LINK;
        if (!empty($application['application_store']['application_file'])) {
            $application['application_type'] = Application::TYPE_FILE;
        }
        return $application;
    }

    public function getDetailApplicationForClick($filter, $accountInfo)
    {
        $filter['app_library'] = true;
        $application = $this->applicationRepository->getOneArrayApplicationWithAdvancedByFilter($filter, $accountInfo);

        if (!empty($application)) {
            $strToTime = strtotime('now');
            $today = getdate($strToTime);
            $date = date('Y-m-d', $strToTime);
            $filterAdvanced['application_id'] = (int)$filter['id'];
            $filterAdvanced[ApplicationAdvancedSetting::ARRAY_WEEKDAYS[$today['wday']]] = ApplicationAdvancedSetting::VALUE_WEEKDAYS;
            //Xem ứng dụng này có setting nâng cao không
            $applicationAdvancedSettings = $this->applicationAdvancedSettingRepository->getAllApplicationAdvancedSettingByFilter($filterAdvanced);
            if (!empty($applicationAdvancedSettings)) {
                $arrApplicationAdvancedSetting = [];
                foreach ($applicationAdvancedSettings as $applicationAdvancedSetting) {
                    if ($strToTime >= strtotime($date . ' ' . $applicationAdvancedSetting['time_start']) && $strToTime <= strtotime($date . ' ' . $applicationAdvancedSetting['time_end'])) {
                        //1 thời điểm chỉ có thể có 1 setting nâng cao
                        $arrApplicationAdvancedSetting = $applicationAdvancedSetting;
                    }
                }

                if (!empty($arrApplicationAdvancedSetting)) { //Nếu có setting nâng cao thì sẽ gán thêm filter trong statistic application
                    $filterStatisticApplication['application_advanced_setting_id'] = $arrApplicationAdvancedSetting['id'];
                }
            }

            $filterStatisticApplication['date_at'] = $date;
            $filterStatisticApplication['application_id'] = $filter['id'];

            $statisticApplicationToday = $this->statisticApplicationRepository->getOneObjectStatisticApplicationByFilter($filterStatisticApplication);
            if (!empty($statisticApplicationToday)) { //Nếu đã có thông kê ứng dụng này vowsis filter trong ngày hôm nay thì cộng thêm lượt click là 1
                $this->statisticApplicationRepository->update($statisticApplicationToday, [
                    'clicks' => $statisticApplicationToday->clicks + 1
                ]);
            } else { //Nếu không có thì tạo mới
                $statisticApplicationToday = $this->statisticApplicationRepository->create([
                    'application_advanced_setting_id' => !empty($filterStatisticApplication['application_advanced_setting_id']) ? $filterStatisticApplication['application_advanced_setting_id'] : null,
                    'date_at' => $date,
                    'application_id' => $filter['id'],
                    'organization_id' => $accountInfo['organization_id'],
                    'room_id' => !empty($filter['room_id']) ? $filter['room_id'] : null,
                    'space_id' => !empty($filter['space_id']) ? $filter['space_id'] : null,
                    'clicks' => 1
                ]);
            }

            if (!empty($statisticApplicationToday)) { //Cập nhật lại thống kê account clip trong ngày với id của statistic application
                $statisticAccount = $this->statisticAccountRepository->getOneObjectStatisticAccountByFilter([
                    'account_id' => $accountInfo['id'],
                    'application_id' => $filter['id'],
                    'statistic_application_id' => $statisticApplicationToday->id,
                    'date_at' => $date
                ]);
                if (!empty($statisticAccount)) {
                    $this->statisticAccountRepository->update($statisticAccount, [
                        'clicks' => $statisticAccount->clicks + 1
                    ]);
                } else {
                    $this->statisticAccountRepository->create([
                        'statistic_application_id' => $statisticApplicationToday->id,
                        'date_at' => $date,
                        'account_id' => $accountInfo['id'],
                        'application_id' => $filter['id'],
                        'organization_id' => $accountInfo['organization_id'],
                        'clicks' => 1
                    ]);
                }
            }

            if (!empty($application['advanced_setting'])) {
                $arrAdvancedSetting = [];
                foreach ($application['advanced_setting'] as $setting) {
                    $arrWeekdays = [];
                    if (!empty($setting['monday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::MONDAY);
                    }

                    if (!empty($setting['tuesday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::TUESDAY);
                    }

                    if (!empty($setting['wednesday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::WEDNESDAY);
                    }

                    if (!empty($setting['thursday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::THURSDAY);
                    }

                    if (!empty($setting['friday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::FRIDAY);
                    }

                    if (!empty($setting['saturday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::SATURDAY);
                    }

                    if (!empty($setting['sunday'])) {
                        array_push($arrWeekdays, ApplicationAdvancedSetting::SUNDAY);
                    }

                    $arrAdvancedSetting[] = [
                        "id" => $setting['id'],
                        'application_id' => $setting['application_id'],
                        'time_start' => $setting['time_start'],
                        'time_end' => $setting['time_end'],
                        'weekdays' => $arrWeekdays,
                        'clicks' => $setting['clicks'],
                    ];
                }

                $application['advanced_setting'] = $arrAdvancedSetting;
            }
            $application['avatar'] = !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '';
        }

        return $application;
    }

    public function updateApplication($id, $timeSlot, $data, $accountInfo)
    {
        $application = $this->applicationRepository->getOneObjectApplicationByFilter(['id' => (int)$id, 'deleted_at' => true]);
        if (empty($application)) {
            return false;
        }

        $idApplicationGroupBefore = !empty($application->application_group_id) ? $application->application_group_id : false;
        $this->applicationRepository->update($application, $data);
        if ((int)$data['type'] !== Application::TYPE_APP_BAR) {
            $applicationLocation = $this->applicationLocationRepository->getOneArrayApplicationLocationByFilter(['application_id' => $id, 'room_id' => $data['room_id'], 'space_id' => $data['space_id']]);
            if (empty($applicationLocation)) {
                $this->addLocation((int)$id, ApplicationLocation::TYPE_UPDATE, $idApplicationGroupBefore, $data, $accountInfo);
            }
        }

        if (!empty($timeSlot)) {
            $arrAdvancedNew = $arrAdvancedNOld = [];
            $advancedIds = collect($timeSlot)->pluck('application_advanced_setting_id')->filter()->unique()->values()->toArray();
            if (!empty($advancedIds)) {
                $advancedSetting = $this->applicationAdvancedSettingRepository->getAllApplicationAdvancedSettingByFilter(['application_id' => (int)$id]);
                if (!empty($advancedSetting)) {
                    $advancedSettingIds = collect($advancedSetting)->pluck('id')->values()->toArray();
                    $arrDel = array_diff($advancedSettingIds, $advancedIds);
                    if (!empty($arrDel)) {
                        $this->applicationAdvancedSettingRepository->deleteAllApplicationAdvancedSettingByFilter(['id' => $arrDel]);
                    }
                }
            }

            foreach ($timeSlot as $time) {
                // $time = json_decode($time, true);
                if (!empty($time['weekdays'])) {
                    if (!empty($time['application_advanced_setting_id'])) {
                        $arrAdvancedNOld[] = [
                            'id' => $time['application_advanced_setting_id'],
                            'time_start' => $time['time_start'],
                            'time_end' => $time['time_end'],
                            'monday' => in_array(ApplicationAdvancedSetting::MONDAY, $time['weekdays']) ? 1 : 0,
                            'tuesday' => in_array(ApplicationAdvancedSetting::TUESDAY, $time['weekdays']) ? 1 : 0,
                            'wednesday' => in_array(ApplicationAdvancedSetting::WEDNESDAY, $time['weekdays']) ? 1 : 0,
                            'thursday' => in_array(ApplicationAdvancedSetting::THURSDAY, $time['weekdays']) ? 1 : 0,
                            'friday' => in_array(ApplicationAdvancedSetting::FRIDAY, $time['weekdays']) ? 1 : 0,
                            'saturday' => in_array(ApplicationAdvancedSetting::SATURDAY, $time['weekdays']) ? 1 : 0,
                            'sunday' => in_array(ApplicationAdvancedSetting::SUNDAY, $time['weekdays']) ? 1 : 0,
                            'clicks' => $time['clicks'],
                            'created_by' => $accountInfo['id'],
                        ];
                    } else {
                        $arrAdvancedNew[] = [
                            'application_id' => $application->id,
                            'time_start' => $time['time_start'],
                            'time_end' => $time['time_end'],
                            'monday' => in_array(ApplicationAdvancedSetting::MONDAY, $time['weekdays']) ? 1 : 0,
                            'tuesday' => in_array(ApplicationAdvancedSetting::TUESDAY, $time['weekdays']) ? 1 : 0,
                            'wednesday' => in_array(ApplicationAdvancedSetting::WEDNESDAY, $time['weekdays']) ? 1 : 0,
                            'thursday' => in_array(ApplicationAdvancedSetting::THURSDAY, $time['weekdays']) ? 1 : 0,
                            'friday' => in_array(ApplicationAdvancedSetting::FRIDAY, $time['weekdays']) ? 1 : 0,
                            'saturday' => in_array(ApplicationAdvancedSetting::SATURDAY, $time['weekdays']) ? 1 : 0,
                            'sunday' => in_array(ApplicationAdvancedSetting::SUNDAY, $time['weekdays']) ? 1 : 0,
                            'clicks' => $time['clicks'],
                            'created_by' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                        ];
                    }
                }
            }

            if (!empty($arrAdvancedNew)) {
                $this->applicationAdvancedSettingRepository->createMulti($arrAdvancedNew);
            }

            if (!empty($arrAdvancedNOld)) {
                $applicationAdvancedSettingInstance = new ApplicationAdvancedSetting();
                $indexApplicationAdvancedSetting = 'id';
                \Batch::update($applicationAdvancedSettingInstance, $arrAdvancedNOld, $indexApplicationAdvancedSetting);
            }
        } else {
            $this->applicationAdvancedSettingRepository->deleteAllApplicationAdvancedSettingByFilter(['application_id' => $application->id]);
        }

        return $application;
    }

    public function deleteApplication($ids, $roomId, $spaceId, $accountInfo)
    {
        $isSortRoom = false;
        $arrApplicationGroupId = [];
        $applications = $this->applicationRepository->getAllApplicationByFilter(['id' => $ids, 'room_id' => $roomId, 'space_id' => $spaceId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        if (!empty($applications)) {
            foreach ($applications as $k => $application) {
                if (empty($application['application_group_id'])) {
                    $isSortRoom = true;
                } else {
                    $arrApplicationGroupId[] = $application['application_group_id'];
                }
            }
        }

        $this->applicationRepository->deleteApplicationByFilter(['id' => $ids, 'room_id' => $roomId, 'space_id' => $spaceId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationLocationRepository->deleteAllApplicationLocationByFilter(['application_id' => $ids, 'room_id' => $roomId, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id']]);
        $this->applicationGroupLocationRepository->deleteAllApplicationGroupLocationByFilter(['application_id' => $ids, 'room_id' => $roomId, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id']]);
        // nếu xóa file

        if (!empty($isSortRoom)) {
            $applicationStillWorkings = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['room_id' => $roomId, 'space_id' => $spaceId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
            if (!empty($applicationStillWorkings)) {
                $arrApplications = [];
                foreach ($applicationStillWorkings as $applicationStillWorking) {
                    $arrApplications[] = [
                        'id' => $applicationStillWorking['id'],
                        // 'position' => $k + 1
                    ];
                }

                if (!empty($arrApplications)) {
                    $applicationInstance = new ApplicationLocation();
                    $indexApplication = 'id';
                    \Batch::update($applicationInstance, $arrApplications, $indexApplication);
                }
            }
        }

        if (!empty($arrApplicationGroupId)) {
            $applicationGroups = $this->applicationGroupLocationRepository->getAllApplicationGroupLocationByFilter(['application_group_id' => $arrApplicationGroupId[0], 'room_id' => $roomId, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id']]);
            if (!empty($applicationGroups)) {
                $arrApplicationGroups = [];
                foreach ($applicationGroups as $k => $applicationGroup) {
                    $arrApplicationGroups[] = [
                        'id' => $applicationGroup['id'],
                        // 'position' => $k + 1
                    ];
                }

                if (!empty($arrApplicationGroups)) {
                    $applicationGroupInstance = new ApplicationGroupLocation();
                    $indexApplicationGroup = 'id';
                    \Batch::update($applicationGroupInstance, $arrApplicationGroups, $indexApplicationGroup);
                }
            }
        }

        return true;
    }

    public function getListApplicationByFilter($filter, $accountInfo)
    {
        $isSingle = true;
        $arrApplications = [];
        $applications = $this->applicationRepository->getAllApplicationWithImageByFilter($filter);
        if (!empty($applications)) {
            $strToTime = strtotime('now');
            $today = getdate($strToTime);
            $date = date('Y-m-d', $strToTime);
            $time = date('H:i:s', $strToTime);
            $groupApplicationIds = collect($applications)->pluck('application_group_id')->unique()->filter()->values()->toArray();
            $applicationIds = collect($applications)->pluck('id')->values()->toArray();
            $filterAdvanced['application_id'] = $applicationIds;
            $filterAdvanced[ApplicationAdvancedSetting::ARRAY_WEEKDAYS[$today['wday']]] = ApplicationAdvancedSetting::VALUE_WEEKDAYS;
            $applicationAdvancedSettings = $this->applicationAdvancedSettingRepository->getAllApplicationAdvancedSettingByFilter($filterAdvanced);
            if (!empty($applicationAdvancedSettings)) {
                $arrApplicationAdvancedSettings = [];
                foreach ($applicationAdvancedSettings as $applicationAdvancedSetting) {
                    if (!empty($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']])) {
                        if (
                            !empty($applicationAdvancedSetting['time_end']) &&
                            !empty($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']]['time_start']) &&
                            !empty($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']]['time_end']) &&
                            strtotime($applicationAdvancedSetting['time_end']) < strtotime($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']]['time_end']) &&
                            strtotime($applicationAdvancedSetting['time_start']) <= strtotime($time) &&
                            strtotime($applicationAdvancedSetting['time_end']) >= strtotime($time)
                        ) {
                            $arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']] = $applicationAdvancedSetting;
                        }
                    } else {
                        $arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']] = $applicationAdvancedSetting;
                    }
                }
                if (!empty($arrApplicationAdvancedSettings)) {
                    $filterStatisticApplication['application_advanced_setting_id'] = collect($arrApplicationAdvancedSettings)->pluck('id')->values()->toArray();
                    $filterStatisticApplication['room_id'] = $filter['room_id'];
                    $filterStatisticApplication['space_id'] = $filter['space_id'];
                    $filterStatisticApplication['date_at'] = $date;
                    $filterStatisticApplication['application_id'] = $applicationIds;
                    $statisticApplication = $this->statisticApplicationRepository->getAllStatisticApplicationByFilter($filterStatisticApplication);

                    $lstIdStatisticApplication = collect($statisticApplication)->pluck('id')->toArray();

                    $filterStatisticAccountApplication['date_at'] = $date;
                    $filterStatisticAccountApplication['statistic_application_id'] = $lstIdStatisticApplication;
                    $filterStatisticAccountApplication['account_id'] = $accountInfo['id'];
                    $statisticAccountRepository = $this->statisticAccountRepository->getAllObjectStatisticAccountByFilter($filterStatisticAccountApplication);

                    if (!empty($statisticAccountRepository) && !empty($statisticApplication)) {
                        $statisticApplication = collect($statisticApplication)->keyBy('application_id')->toArray();

                        $statisticAccountRepository = collect($statisticAccountRepository)->keyBy('application_id')->toArray();

                        foreach ($applications as $k => $application) {
                            if (
                                !empty($arrApplicationAdvancedSettings[$application['id']]) && !empty($statisticAccountRepository[$application['id']])
                                && !empty($statisticAccountRepository[$application['id']]['date_at']) && strtotime($statisticAccountRepository[$application['id']]['date_at']) === strtotime($date)
                            ) {
                                if (
                                    !empty($arrApplicationAdvancedSettings[$application['id']]['clicks']) &&
                                    !empty($statisticAccountRepository[$application['id']]['clicks']) &&
                                    $arrApplicationAdvancedSettings[$application['id']]['clicks'] <= $statisticAccountRepository[$application['id']]['clicks'] &&
                                    strtotime($arrApplicationAdvancedSettings[$application['id']]['time_start']) <= strtotime($time) &&
                                    strtotime($arrApplicationAdvancedSettings[$application['id']]['time_end']) >= strtotime($time)
                                ) {
                                    $applications[$k]['click_enough'] = Application::CLICK_NOT_SETTING; // Đủ lượt click
                                    // $applications[$k]['click_enough'] = Application::CLICK_ENOUGH;
                                } else if (
                                    !empty($arrApplicationAdvancedSettings[$application['id']]['time_start']) &&
                                    !empty($arrApplicationAdvancedSettings[$application['id']]['time_end']) &&
                                    // !empty($statisticAccountRepository[$application['id']]['clicks']) &&
                                    // $arrApplicationAdvancedSettings[$application['id']]['clicks'] <= $statisticAccountRepository[$application['id']]['clicks'] &&
                                    (strtotime($arrApplicationAdvancedSettings[$application['id']]['time_start']) > strtotime($time) ||
                                        strtotime($arrApplicationAdvancedSettings[$application['id']]['time_end']) < strtotime($time))
                                ) {
                                    // $applications[$k]['click_enough'] = Application::CLICK_NOT_ENOUGH;
                                    $applications[$k]['click_enough'] = Application::CLICK_NOT_SETTING;
                                } else {
                                    // $applications[$k]['click_enough'] = Application::CLICK_NOT_ENOUGH;
                                    $applications[$k]['click_enough'] = Application::CLICK_ENOUGH;
                                }
                            } else {
                                if (
                                    !empty($arrApplicationAdvancedSettings[$application['id']]) &&
                                    strtotime($arrApplicationAdvancedSettings[$application['id']]['time_start']) <= strtotime($time) &&
                                    strtotime($arrApplicationAdvancedSettings[$application['id']]['time_end']) >= strtotime($time)
                                ) {
                                    $applications[$k]['click_enough'] = Application::CLICK_ENOUGH;
                                }
                            }
                            // else if (
                            //     !empty($arrApplicationAdvancedSettings[$application['id']])
                            // ) {
                            //     \Log::info('3');
                            //     $applications[$k]['click_enough'] = Application::CLICK_NOT_SETTING;
                            // }
                        }
                    } else {
                        foreach ($applications as $k => $application) {
                            if (
                                !empty($arrApplicationAdvancedSettings[$application['id']]) &&
                                strtotime($arrApplicationAdvancedSettings[$application['id']]['time_start']) <= strtotime($time) &&
                                strtotime($arrApplicationAdvancedSettings[$application['id']]['time_end']) >= strtotime($time)
                            ) {
                                $applications[$k]['click_enough'] = Application::CLICK_ENOUGH; // Chưa đủ lượt click
                                // $applications[$k]['click_enough'] = Application::CLICK_NOT_ENOUGH;
                            } else {
                                $applications[$k]['click_enough'] = Application::CLICK_NOT_SETTING; // Không hiển thị !
                            }
                        }
                    }
                }
            }
            if (!empty($groupApplicationIds)) {
                $groupApplications = $this->applicationGroupRepository->getAllGroupApplicationByFilter(['id' => $groupApplicationIds, 'deleted_at' => true]);
                if (!empty($groupApplications)) {
                    $groupApplications = collect($groupApplications)->keyBy('id')->toArray();
                    foreach ($applications as $application) {
                        if (!empty($application['application_group_id']) && !empty($groupApplications[$application['application_group_id']])) {
                            if (!empty($arrApplications[$application['application_group_id']])) {
                                $arrApplications[$application['application_group_id']]['children'][] = [
                                    'id' => $application['id'],
                                    'name' => @$application['name'],
                                    'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                                    'link_android' => @$application['link_android'],
                                    'link_ios' => @$application['link_ios'],
                                    'link_url' => @$application['link_url'],
                                    'room_id' => @$application['room_id'],
                                    'click_enough' => !empty($application['click_enough']) ? $application['click_enough'] : Application::CLICK_NOT_SETTING,
                                    'created_by' => @$application['created_by'],
                                    'account_id' => @$application['account_id'],
                                    'is_accept' => @$application['is_accept'],
                                    'type' => Application::TYPE_VIEW_SINGLE,
                                    'x_frame' => @$application['x_frame'],
                                    'application_store' => @$application['application_store'],
                                ];
                            } else {
                                $arrApplications[$application['application_group_id']] = [
                                    'id' => $groupApplications[$application['application_group_id']]['id'],
                                    'name' => $groupApplications[$application['application_group_id']]['name'],
                                    'type' => Application::TYPE_VIEW_GROUP,
                                    'children' => [
                                        [
                                            'id' => $application['id'],
                                            'name' => @$application['name'],
                                            'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                                            'link_android' => @$application['link_android'],
                                            'account_id' => @$application['account_id'],
                                            'link_ios' => @$application['link_ios'],
                                            'created_by' => @$application['created_by'],
                                            'room_id' => @$application['room_id'],
                                            'click_enough' => !empty($application['click_enough']) ? $application['click_enough'] : Application::CLICK_NOT_SETTING,
                                            'link_url' => @$application['link_url'],
                                            'is_accept' => @$application['is_accept'],
                                            'type' => Application::TYPE_VIEW_SINGLE,
                                            'x_frame' => @$application['x_frame'],
                                            'application_store' => @$application['application_store'],
                                        ]
                                    ]
                                ];
                            }
                        }
                    }

                    foreach ($applications as $application) {
                        if (empty($application['application_group_id']) || empty($groupApplications[$application['application_group_id']])) {
                            $arrApplications[] = [
                                'id' => $application['id'],
                                'name' => $application['name'],
                                'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                                'link_android' => @$application['link_android'],
                                'link_ios' => @$application['link_ios'],
                                'created_by' => @$application['created_by'],
                                'room_id' => @$application['room_id'],
                                'click_enough' => !empty($application['click_enough']) ? $application['click_enough'] : Application::CLICK_NOT_SETTING,
                                'link_url' => @$application['link_url'],
                                'is_accept' => @$application['is_accept'],
                                'type' => Application::TYPE_VIEW_SINGLE,
                                'x_frame' => @$application['x_frame'],
                                'account_id' => @$application['account_id'],
                                'application_store' => @$application['application_store'],
                            ];
                        }
                    }

                    $isSingle = false;
                }
            }

            if (!empty($isSingle)) {
                foreach ($applications as $application) {
                    $arrApplications[] = [
                        'id' => $application['id'],
                        'name' => @$application['name'],
                        'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                        'link_android' => @$application['link_android'],
                        'link_ios' => @$application['link_ios'],
                        'link_url' => @$application['link_url'],
                        'account_id' => @$application['account_id'],
                        'room_id' => @$application['room_id'],
                        'created_by' => @$application['created_by'],
                        'click_enough' => !empty($application['click_enough']) ? $application['click_enough'] : Application::CLICK_NOT_SETTING,
                        'is_accept' => @$application['is_accept'],
                        'type' => Application::TYPE_VIEW_SINGLE,
                        'x_frame' => @$application['x_frame'],
                        'account_id' => @$application['account_id'],
                        'application_store' => @$application['application_store'],
                    ];
                }
            }

            if (!empty($filter['room_id']) && !empty($filter['space_id'])) {
                $applicationGroupLocations = [];
                $applicationLocations = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $filter['space_id'], 'room_id' => $filter['room_id'], 'organization_id' => $accountInfo['organization_id']]);
                if (!empty($groupApplicationIds)) {
                    $applicationGroupLocations = $this->applicationGroupLocationRepository->getAllApplicationGroupLocationByFilter(['application_group_id' => $groupApplicationIds, 'space_id' => $filter['space_id'], 'room_id' => $filter['room_id'], 'organization_id' => $accountInfo['organization_id']]);
                }

                if (!empty($applicationLocations) && !empty($arrApplications)) {
                    foreach ($applicationLocations as $applicationLocation) {
                        foreach ($arrApplications as $k => $app) {
                            if ($app['type'] === Application::TYPE_VIEW_SINGLE) {
                                if (!empty($applicationLocation['application_id']) && (int)$applicationLocation['application_id'] === (int)$app['id']) {
                                    $arrApplications[$k]['position'] = $applicationLocation['position'];
                                }
                            } else {
                                if (!empty($applicationLocation['application_group_id']) && (int)$applicationLocation['application_group_id'] === (int)$app['id']) {
                                    if (!empty($applicationGroupLocations)) {
                                        $applicationGroupLocationInGroup = collect($applicationGroupLocations)->filter(function ($q) use ($app) {
                                            return @(int)$q['application_group_id'] === (int)@$app['id'];
                                        })->values()->toArray();

                                        if (!empty($applicationGroupLocationInGroup) && !empty($app['children'])) {
                                            foreach ($applicationGroupLocationInGroup as $location) {
                                                foreach ($app['children'] as $ka => $a) {
                                                    if (!empty($location['application_id']) && (int)$location['application_id'] === (int)$a['id']) {
                                                        $arrApplications[$k]['children'][$ka]['position'] = $location['position'];
                                                    }
                                                }
                                            }

                                            $arrApplications[$k]['children'] = collect($arrApplications[$k]['children'])->sortBy('position')->values()->toArray();
                                        }
                                    }
                                    $arrApplications[$k]['position'] = $applicationLocation['position'];
                                }
                            }
                        }
                    }

                    $arrApplications = collect($arrApplications)->sortBy('position')->values()->toArray();
                }
            }
        }

        return $arrApplications;
    }

    public function compareClickApplication($idApplication, $roomId, $spaceId, $accountInfo)
    {
        $strToTime = strtotime('now');
        // $today = getdate($strToTime);
        // $date = date('Y-m-d', $strToTime);
        // $date_s = date('H:i:s');
        $applicationAdvancedSetting = ApplicationAdvancedSetting::Where('application_id', $idApplication)->get()->toArray();
        foreach ($applicationAdvancedSetting as $key => $item) {
            $timeStart = strtotime($item['time_start']) - strtotime('TODAY');
            $timeEnd = strtotime($item['time_end']) - strtotime('TODAY');
            $timeHourNow = strtotime(date('H:i:s')) - strtotime('TODAY');
            $weekdayNow = getdate($strToTime);
            if ($item[strtolower($weekdayNow['weekday'])] === ApplicationAdvancedSetting::VALUE_WEEKDAYS) { // kiểm tra xem ngày hôm nay có phải thuộc dạng setting được clik hay không
                if ($timeHourNow < $timeEnd && $timeHourNow > $timeStart) { // kiểm tra xem thời điểm hiện tại click có nằm trong khoảng thời gian time_start & time_end không
                    $tpl = $item;
                }
            }
        }
        if (!empty($tpl)) {
            $compare = StatisticApplication::where([
                'application_id' => $tpl['application_id'],
                'room_id' => $roomId,
                'space_id' => $spaceId,
                'date_at' => date('Y-m-d'),
                'application_advanced_setting_id' => $tpl['id'],
            ])->first();
            \Log::info(['statistic_applications' => $compare]);
            if (!empty($compare)) {
                $compare = $compare->toArray();
                $tpl['statistic_application'] = $compare;
                $statisticAccounts = StatisticAccount::where([
                    'statistic_application_id' => $compare['id'],
                    'account_id' => $accountInfo['id'],
                    'date_at' => date('Y-m-d'),
                ])->first();
                \Log::info(['statistic_accounts' => $statisticAccounts]);
                if (!empty($statisticAccounts)) {
                    $statisticAccounts = $statisticAccounts->toArray();
                    $tpl['statistic_accounts'] = $statisticAccounts;
                    $data = [];
                    \Log::info(['click_account' => $tpl['statistic_accounts']['clicks'], 'click_compare' => $tpl['clicks']]);
                    if ($tpl['statistic_accounts']['clicks'] == $tpl['clicks']) {
                        $data = [
                            'application_id' => $idApplication,
                            'room_id' => $roomId,
                            'space_id' => $spaceId,
                            'account_id' => $accountInfo['id'],
                            'is_warning' => StatisticApplication::IS_WARNING,
                        ];
                        $this->notificationRepository->pushNotiWarningApplication($data, $accountInfo);
                        return $data;
                    }
                    return false;
                }
            }

        }
        return false;
    }

    private function addLocation($applicationId, $type, $idApplicationGroupBefore, $data, $accountInfo)
    {
        $isSortApplicationGroup = false;
        if (empty($data['application_group_id'])) { //Khi mà app không vào nhóm ứng dụng
            $applicationLocations = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
            if ($type === ApplicationLocation::TYPE_ADD_NEW) {
                if (!empty($applicationLocations)) {
                    $this->applicationLocationRepository->create([
                        'organization_id' => $accountInfo['organization_id'],
                        'account_id' => $accountInfo['id'],
                        'application_id' => $applicationId,
                        'type' => ApplicationLocation::TYPE_APPLICATION,
                        'space_id' => $data['space_id'],
                        'room_id' => $data['room_id'],
                        'position' => count($applicationLocations) + 1
                    ]);
                } else {
                    $this->applicationLocationRepository->create([
                        'organization_id' => $accountInfo['organization_id'],
                        'account_id' => $accountInfo['id'],
                        'application_id' => $applicationId,
                        'type' => ApplicationLocation::TYPE_APPLICATION,
                        'space_id' => $data['space_id'],
                        'room_id' => $data['room_id'],
                        'position' => 1
                    ]);
                }
            } else if ($type === ApplicationLocation::TYPE_UPDATE && !empty($idApplicationGroupBefore) && !empty($applicationLocations)) {
                //Xóa vị trí app đso trong nhóm
                $this->applicationGroupLocationRepository->deleteAllApplicationGroupLocationByFilter(['application_group_id' => $idApplicationGroupBefore, 'application_id' => $applicationId, 'space_id' => $data['space_id'], 'room_id' => $data['room_id']]);
                //Cập nhật lại vị trí của các app còn lại trong nhóm
                $applicationGroupLocations = $this->applicationGroupLocationRepository->getAllApplicationGroupLocationByFilter(['application_group_id' => $idApplicationGroupBefore, 'space_id' => $data['space_id'], 'room_id' => $data['room_id']]);
                if (!empty($applicationGroupLocations)) {
                    $arrApplicationGroupLocations = [];
                    foreach ($applicationGroupLocations as $k => $applicationGroupLocation) {
                        $arrApplicationGroupLocations[] = [
                            'id' => $applicationGroupLocation['id'],
                            'position' => $k + 1
                        ];
                    }

                    if (!empty($arrApplicationGroupLocations)) {
                        $applicationGroupLocationInstance = new ApplicationGroupLocation();
                        $indexApplicationGroupLocation = 'id';
                        \Batch::update($applicationGroupLocationInstance, $arrApplicationGroupLocations, $indexApplicationGroupLocation);
                    }
                }

                //Thêm mới vị trí của app ra ngoài room
                $this->applicationLocationRepository->create([
                    'organization_id' => $accountInfo['organization_id'],
                    'account_id' => $accountInfo['id'],
                    'application_id' => $applicationId,
                    'type' => ApplicationLocation::TYPE_APPLICATION,
                    'space_id' => $data['space_id'],
                    'room_id' => $data['room_id'],
                    'position' => count($applicationLocations) + 1
                ]);
            }
        } else { //Khi app được thêm vào nhóm ứng dụng
            if ($type === ApplicationLocation::TYPE_ADD_NEW) {
                $isSortApplicationGroup = true;
            } else if ($type === ApplicationLocation::TYPE_UPDATE) {
                if (!empty($idApplicationGroupBefore)) { //Nếu mà trước khi cập nhật app ở trong nhóm
                    if ((int)$idApplicationGroupBefore !== (int)$data['application_group_id']) {
                        $isSortApplicationGroup = true;

                        $applicationGroupLocations = $this->applicationGroupLocationRepository->getAllApplicationGroupLocationByFilter(['application_group_id' => $idApplicationGroupBefore, 'space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
                        if (!empty($applicationGroupLocations)) {
                            $arrApplicationGroupLocations = [];
                            foreach ($applicationGroupLocations as $k => $applicationGroupLocation) {
                                $arrApplicationGroupLocations[] = [
                                    'id' => $applicationGroupLocation['id'],
                                    'position' => $k + 1
                                ];
                            }

                            if (!empty($arrApplicationGroupLocations)) {
                                $applicationGroupLocationInstance = new ApplicationGroupLocation();
                                $indexApplicationGroupLocation = 'id';
                                \Batch::update($applicationGroupLocationInstance, $arrApplicationGroupLocations, $indexApplicationGroupLocation);
                            }
                        }
                    }
                } else { //trước khi cập nhật app không ở trong nhóm
                    //Cập nhật lại ví trí của ứng dụng và nhóm ứng dụng trong room
                    $applicationLocations = $this->applicationLocationRepository->getAllApplicationLocationByFilter(['space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
                    if (!empty($applicationLocations)) {
                        $arrApplicationLocations = [];
                        foreach ($applicationLocations as $k => $applicationLocation) {
                            $arrApplicationLocations[] = [
                                'id' => $applicationLocation['id'],
                                'position' => $k + 1
                            ];
                        }

                        if (!empty($arrApplicationLocations)) {
                            $applicationLocationInstance = new ApplicationLocation();
                            $indexApplicationLocation = 'id';
                            \Batch::update($applicationLocationInstance, $arrApplicationLocations, $indexApplicationLocation);
                        }
                    }

                    //Thêm vị trí cho ứng dụng
                    $isSortApplicationGroup = true;
                }
            }
        }

        if ($isSortApplicationGroup) {
            $applicationGroupLocations = $this->applicationGroupLocationRepository->getAllApplicationGroupLocationByFilter(['application_group_id' => $data['application_group_id'], 'space_id' => $data['space_id'], 'room_id' => $data['room_id'], 'organization_id' => $accountInfo['organization_id']]);
            if (!empty($applicationGroupLocations)) {
                $this->applicationGroupLocationRepository->create([
                    'organization_id' => $accountInfo['organization_id'],
                    'account_id' => $accountInfo['id'],
                    'application_id' => $applicationId,
                    'application_group_id' => $data['application_group_id'],
                    'space_id' => $data['space_id'],
                    'room_id' => $data['room_id'],
                    'position' => count($applicationGroupLocations) + 1
                ]);
            } else {
                $this->applicationGroupLocationRepository->create([
                    'organization_id' => $accountInfo['organization_id'],
                    'account_id' => $accountInfo['id'],
                    'application_id' => $applicationId,
                    'application_group_id' => $data['application_group_id'],
                    'space_id' => $data['space_id'],
                    'room_id' => $data['room_id'],
                    'position' => 1
                ]);
            }
        }
    }

    public function getListApplicationInRoomByFilter($filter)
    {
        $arrApplications = [];
        $applications = $this->applicationRepository->getAllApplicationWithImageByFilter($filter);
        if (!empty($applications)) {
            foreach ($applications as $application) {
                $arrApplications[] = [
                    'id' => $application['id'],
                    'name' => @$application['name'],
                    'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                    'link_android' => @$application['link_android'],
                    'link_ios' => @$application['link_ios'],
                    'link_url' => @$application['link_url'],
                    'is_accept' => @$application['is_accept'],
                    'application_store' => @$application['application_store'],
                    'type' => Application::TYPE_VIEW_SINGLE,
                ];
            }
        }
        return $arrApplications;
    }

    public function getAllApplicationForQuickBar($filter, $accountInfo)
    {
        $arrApplications = [];
        $allApplicationInQuickBar = $this->quickBarRepository->getAllApplicationInQuickByFilter(['space_id' => $filter['space_id'], 'account_id' => $accountInfo['id'], 'organization_id' => $accountInfo['organization_id']]);
        if (!empty($allApplicationInQuickBar)) {
            $filter['application_not_in'] = collect($allApplicationInQuickBar)->pluck('application_id')->filter()->unique()->values()->toArray();
        }

        $filter['is_accept'] = Application::ACCEPT;

        $applications = $this->applicationRepository->getAllApplicationWithImageByFilter($filter);
        if (!empty($applications)) {
            foreach ($applications as $application) {
                $arrApplications[] = [
                    'id' => $application['id'],
                    'name' => @$application['name'],
                    'icon' => !empty($application['application_image']['source']) && file_exists(public_path() . '/' . $application['application_image']['source']) ? asset($application['application_image']['source']) : '',
                    'link_android' => @$application['link_android'],
                    'link_ios' => @$application['link_ios'],
                    'link_url' => @$application['link_url'],
                    'is_accept' => @$application['is_accept'],
                    'type' => Application::TYPE_VIEW_SINGLE,
                ];
            }
        }
        return $arrApplications;
    }

    public function uploadFile($request, $accountInfo)
    {
        if ($request['file']) {
            $file = $request['file'];
            $extensions = array('doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf');
            $result = array($file->getClientOriginalExtension());
            if (in_array($result[0], $extensions)) {
                $mime = $file->getClientOriginalExtension();
                $folder = uniqid() . '-' . now()->timestamp;
                $filename = $file->getClientOriginalName();
                $newPath = str_replace(' ', '-', $filename);
                $size = $file->getSize();
                //                UploadFileApplication::dispatch($file, $folder, $newPath);
                $svFile = $file->storeAs('public/file/' . $folder, $newPath);
                /* insert file to table image*/
                $lsfile = [
                    "file_mime" => $mime,
                    "file_size" => $size,
                    "is_file" => 1,
                    "file_path" => 'storage/file/' . $folder . '/' . $newPath,
                ];
                $saveObj = [
                    'name' => $filename,
                    'source_thumb' => $lsfile['file_path'],
                    'source' => $lsfile['file_path'],
                    'organization_id' => $accountInfo['organization_id'],
                    'file_size' => $size,
                    'mimes' => $lsfile['file_mime'],
                    'type' => Image::FILE_APPLICATION,
                    'created_by' => $accountInfo['id'],
                    'created_at' => eFunction::getDateTimeNow(),
                    'updated_at' => eFunction::getDateTimeNow(),
                ];
                return $this->imageRepository->insertMultiGetID($saveObj);
            } else {
                return false;
//                return eResponse::response(STATUS_API_FALSE, __('application.file.upload.fail.mimes'));
            }
        } else {
            return false;
        }
    }

    public function getPatchFileApplication($id)
    {
        $idImage = Image::find($id);
        if (!empty($idImage)) {
            $patch = $idImage->source;
            if (file_exists($patch)) {
                unlink($patch);
            }
            $delete = Image::where('id', $id)->delete();
            return true;
        }
        return false;
    }

    public function deleteFileTransferFilesViaLink($idApplication, $type)
    {
        if ($type == Application::DB_APPLICATION_STORE) {
            $applicationStore = ApplicationStore::where('id', $idApplication)->with('applicationFile')->first();
        }
        if ($type == Application::DB_APPLICATION) {
            $application = Application::where('id', $idApplication)->with('applicationStoreFile')->first();
        }
        if (!empty($application)) {
            $application = $application->toArray();
            if (!empty($application['application_store_file'])) {
                $deleteFile = $this->getPatchFileApplication($application['application_store_file']['application_file']['id']);
                if ($deleteFile == true) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        if (!empty($applicationStore)) {
            $applicationStore = $applicationStore->toArray();
            if (!empty($applicationStore['application_file'])) {
                $deleteFile = $this->getPatchFileApplication($applicationStore['application_file']['id']);
                if ($deleteFile == true) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }


}
