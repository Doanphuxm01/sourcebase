<?php namespace App\Services\Postgres\Production;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Models\Postgres\Account;
use App\Models\Postgres\DecentralizationPermission;
use App\Models\Postgres\Permission;
use App\Repositories\Postgres\AccountRepositoryInterface;
use App\Repositories\Postgres\DecentralizationPermissionRepositoryInterface;
use App\Repositories\Postgres\DecentralizationRepositoryInterface;
use App\Repositories\Postgres\PermissionRepositoryInterface;
use \App\Services\Postgres\PermissionServiceInterface;
use DB;
use App\Services\Production\BaseService;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\False_;

class PermissionService extends BaseService implements PermissionServiceInterface
{
    protected $decentralizationRepository;
    protected $decentralizationPermissionRepository;
    protected $permissionRepository;
    protected $accountRepository;

    public function __construct(
        DecentralizationRepositoryInterface $decentralizationRepository,
        DecentralizationPermissionRepositoryInterface $decentralizationPermissionRepository,
        PermissionRepositoryInterface $permissionRepository,
        AccountRepositoryInterface $accountRepository
    ) {
        $this->decentralizationRepository = $decentralizationRepository;
        $this->decentralizationPermissionRepository = $decentralizationPermissionRepository;
        $this->permissionRepository = $permissionRepository;
        $this->accountRepository = $accountRepository;
    }

    public function addDecentralization($data, $permissions, $accountInfo)
    {
        $data['created_by'] = $accountInfo['id'];
        $data['organization_id'] = $accountInfo['organization_id'];
        $decentralization = $this->decentralizationRepository->create($data);
        if (!empty($decentralization)) {
            $permissionIds = collect($permissions)->pluck('permission_id')->filter()->unique()->values()->toArray();
            if (!empty($permissionIds)) {
                $permissionCheck = $this->permissionRepository->countAllPermissionsByFilter(['id' => $permissionIds, 'deleted_at' => true]);
                if ($permissionCheck !== count($permissionIds)) {
                    return false;
                }

                $arrDecentralizationPermission = [];
                foreach ($permissions as $permission) {
                    if (!empty($permission['permission_id'])) {
                        $arrDecentralizationPermission[] = [
                            'permission_id' => (int)$permission['permission_id'],
                            'read' => !empty($permission['read']) ? Permission::IS_ENABLE : Permission::DISABLE,
                            'create' => !empty($permission['create']) ? Permission::IS_ENABLE : Permission::DISABLE,
                            'update' => !empty($permission['update']) ? Permission::IS_ENABLE : Permission::DISABLE,
                            'delete' => !empty($permission['delete']) ? Permission::IS_ENABLE : Permission::DISABLE,
                            'decentralization_id' => $decentralization->id,
                            'created_by' => $accountInfo['id'],
                            'organization_id' => $accountInfo['organization_id'],
                            'created_at' => eFunction::getDateTimeNow(),
                            'updated_at' => eFunction::getDateTimeNow(),
                        ];
                    }
                }

                if (!empty($arrDecentralizationPermission)) {
                    $this->decentralizationPermissionRepository->createMulti($arrDecentralizationPermission);
                }
            }
        }

        return true;
    }

    public function updateDecentralization($id, $data, $permissions, $accountInfo)
    {
        $decentralization = $this->decentralizationRepository->getOneObjectDecentralizationByFilter(['id' => (int)$id, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        if (empty($decentralization)) {
            return false;
        }

        $data['created_by'] = $accountInfo['id'];
        $newDecentralization = $this->decentralizationRepository->update($decentralization, $data);
        if (!empty($newDecentralization)) {
            $permissionIds = collect($permissions)->pluck('permission_id')->filter()->unique()->values()->toArray();
            $decentralizationPermissionIds = collect($permissions)->pluck('decentralization_permission_id')->filter()->unique()->values()->toArray();
            if (!empty($permissionIds) && !empty($decentralizationPermissionIds)) {
                $permissionCheck = $this->permissionRepository->countAllPermissionsByFilter(['id' => $permissionIds, 'deleted_at' => true]);
                if ($permissionCheck !== count($permissionIds)) {
                    return false;
                }
                $decentralizationPermissionCheck = $this->decentralizationPermissionRepository->countAllDecentralizationPermissionsByFilter(['id' => $decentralizationPermissionIds]);
                if ($decentralizationPermissionCheck !== count($decentralizationPermissionIds)) {
                    return false;
                }

                $arrDecentralizationPermission = $arrUpdateDecentralizationPermission = [];
                foreach ($permissions as $permission) {
                    if (!empty($permission['permission_id'])) {
                        if (!empty($permission['decentralization_permission_id'])) {
                            $arrUpdateDecentralizationPermission[] = [
                                'id' => (int)$permission['decentralization_permission_id'],
                                'permission_id' => (int)$permission['permission_id'],
                                'read' => !empty($permission['read']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'create' => !empty($permission['create']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'update' => !empty($permission['update']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'delete' => !empty($permission['delete']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'decentralization_id' => $newDecentralization->id,
                                'created_by' => $accountInfo['id'],
                                'organization_id' => $accountInfo['organization_id'],
                                'created_at' => eFunction::getDateTimeNow(),
                                'updated_at' => eFunction::getDateTimeNow(),
                            ];
                        } else {
                            $arrDecentralizationPermission[] = [
                                'permission_id' => (int)$permission['permission_id'],
                                'read' => !empty($permission['read']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'create' => !empty($permission['create']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'update' => !empty($permission['update']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'delete' => !empty($permission['delete']) ? Permission::IS_ENABLE : Permission::DISABLE,
                                'decentralization_id' => $newDecentralization->id,
                                'created_by' => $accountInfo['id'],
                                'organization_id' => $accountInfo['organization_id'],
                                'created_at' => eFunction::getDateTimeNow(),
                                'updated_at' => eFunction::getDateTimeNow(),
                            ];
                        }
                    }
                }

                if (!empty($arrUpdateDecentralizationPermission)) {
                    $index = 'id';
                    $permissionInstance = new DecentralizationPermission();
                    \Batch::update($permissionInstance, $arrUpdateDecentralizationPermission, $index);
                }

                if (!empty($arrDecentralizationPermission)) {
                    $this->decentralizationPermissionRepository->createMulti($arrDecentralizationPermission);
                }

                $this->accountRepository->resetTokenAllAccountByFilter(['decentralization_id' => (int)$id, 'delete_at' => true]);
            }
        }

        return true;
    }

    public function deleteDecentralization($ids, $accountInfo)
    {
        $decentralization = $this->decentralizationRepository->getAllDecentralizationByFilter(['id' => $ids, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        if (empty($decentralization)) {
            return false;
        }

        $idsDecentralizations = collect($decentralization)->pluck('id')->values()->toArray();
        $this->decentralizationRepository->deleteAllDecentralizationByFilter(['id' => $ids, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        $this->accountRepository->updateAllAccountByFilter(['decentralization_id' => $idsDecentralizations, 'deleted_at' => true], ['decentralization_id' => null, 'api_web_access_token' => null]);
        return true;
    }

    public function getListDecentralization($limit, $filter, $accountInfo)
    {
        $decentralizations = $this->decentralizationRepository->getListDecentralizationByFilter($limit, $filter);
        return $decentralizations;
    }
}
