<?php namespace App\Services\Postgres\Production;

use App\Elibs\eFunction;
use App\Elibs\eResponse;
use App\Models\Postgres\Organization;
use App\Models\Postgres\Package;
use App\Repositories\Postgres\OrganizationRepositoryInterface;
use App\Repositories\Postgres\PackageRepositoryInterface;
use App\Services\Postgres\OrganizationServiceInterface;

use DB;

use App\Services\Production\BaseService;

class OrganizationService extends BaseService implements OrganizationServiceInterface
{
    protected $organizationRepository;
    protected $packageRepository;

    public function __construct(OrganizationRepositoryInterface $organizationRepository, PackageRepositoryInterface $packageRepository)
    {
        $this->packageRepository = $packageRepository;
        $this->organizationRepository = $organizationRepository;
    }

    public function createOrganization($data)
    {
        $package = $this->packageRepository->getOneArrayPackageByFilter(['deleted_at' => true, 'is_trial' => Package::IS_TRIAL, 'is_active' => Package::IS_ACTIVE]);
        if (empty($package)) {
            return eResponse::response(STATUS_API_SUCCESS, __('notification.system.package-not-found'));
        }

        $arrOrganization = [
            'name' => $data['organization_name'],
            'slug' => eFunction::generateSlug($data['organization_name'], '-'),
            'scale' => $data['scale'],
            'major_id' => (int)$data['major_id'],
            'package_id' => $package['id'],
            'due_date' => now(),
            'exp_date' => now()->addDay(15),
        ];


        $organization = $this->organizationRepository->create($arrOrganization);
        if (empty($organization)) {
            return [];
        }

        eFunction::setExpirationDateOrganization($organization->id, Organization::REDIS_EXP_NAME, $organization->exp_date);

        return $organization->toArray();
    }

    public function updateOrganization($id, $name)
    {
        $organization = $this->organizationRepository->getOneObjectOrganizationByFilter(['deleted_at' => true, 'id' => (int)$id, 'is_active' => Organization::IS_ACTIVE]);
        if (empty($organization)) {
            return false;
        }

        $this->organizationRepository->update($organization, ['name' => $name]);
        return true;
    }
}
