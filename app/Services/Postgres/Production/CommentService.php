<?php namespace App\Services\Postgres\Production;

use App\Repositories\Postgres\CommentRepositoryInterface;
use \App\Services\Postgres\CommentServiceInterface;

use App\Services\Production\BaseService;

class CommentService extends BaseService implements CommentServiceInterface
{
    protected $commentRepository;

    public function __construct(
        CommentRepositoryInterface $commentRepository
    )
    {
        $this->commentRepository= $commentRepository;
    }

    public function getListCommentByFilter($filter, $accountInfo)
    {
        $arrComments = [];
        $comments = $this->commentRepository->getAllCommentWithAllByFilter($filter);
        if (!empty($comments)){
            foreach ($comments as $k => $comment){
                $comments[$k] = [
                    'id' => @$comment['id'],
                    'content' => @$comment['content'],
                    'space_id' => @$comment['space_id'],
                    'post_id' => @$comment['post_id'],
                    'parent_id' => @$comment['parent_id'],
                    'created_by' => [
                        'id' => !empty($post['created_by']['id']) ? $post['created_by']['id'] : null,
                        'avatar' => !empty($post['created_by']['profile_image']['source']) ? asset($post['created_by']['profile_image']['source']) : ""
                    ],
                ];
            }

            $arrComments = collect($comments)->filter(function ($q){
                return empty($q['parent_id']);
            })->values()->toArray();

            $arrSubComments = collect($comments)->filter(function ($q){
                return empty(!$q['parent_id']);
            })->values()->toArray();

            if (!empty($arrSubComments) && !empty($arrComments)){
                foreach ($arrComments as $k => $comment){
                    foreach ($arrSubComments as $cm){
                        if (!empty($cm['parent_id']) && !empty($comment['id']) && (int)$cm['parent_id'] === (int)$comment['id']){
                            $arrComments[$k]['sub_comment'][] = $cm;
                        }
                    }
                }
            }
        }

        return $arrComments;
    }
}
