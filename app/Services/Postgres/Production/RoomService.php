<?php

namespace App\Services\Postgres\Production;

use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationAdvancedSetting;
use App\Models\Postgres\ApplicationLocation;
use App\Models\Postgres\Space;
use App\Repositories\Postgres\ApplicationAdvancedSettingRepositoryInterface;
use App\Repositories\Postgres\ApplicationGroupLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationLocationRepositoryInterface;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\QuickBarRepositoryInterface;
use App\Repositories\Postgres\RoomRepositoryInterface;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Repositories\Postgres\StatisticAccountRepositoryInterface;
use App\Repositories\Postgres\StatisticApplicationRepositoryInterface;
use \App\Services\Postgres\RoomServiceInterface;

use App\Services\Production\BaseService;
use App\Models\Postgres\Room;
use App\Repositories\Postgres\NotificationRepositoryInterface;

class RoomService extends BaseService implements RoomServiceInterface
{
    protected $roomRepository;
    protected $applicationLocationRepository;
    protected $applicationAdvancedSettingRepository;
    protected $applicationRepository;
    protected $statisticApplicationRepository;
    protected $statisticAccountRepository;
    protected $notificationRepository;
    protected $spaceRepository;

    public function __construct(
        RoomRepositoryInterface $roomRepository,
        ApplicationAdvancedSettingRepositoryInterface $applicationAdvancedSettingRepository,
        StatisticApplicationRepositoryInterface $statisticApplicationRepository,
        StatisticAccountRepositoryInterface $statisticAccountRepository,
        ApplicationRepositoryInterface $applicationRepository,
        ApplicationLocationRepositoryInterface $applicationLocationRepository,
        NotificationRepositoryInterface $notificationRepository,
        SpaceRepositoryInterface $spaceRepository
    ) {
        $this->roomRepository = $roomRepository;
        $this->applicationLocationRepository = $applicationLocationRepository;
        $this->applicationAdvancedSettingRepository = $applicationAdvancedSettingRepository;
        $this->statisticAccountRepository = $statisticAccountRepository;
        $this->statisticApplicationRepository = $statisticApplicationRepository;
        $this->applicationRepository = $applicationRepository;
        $this->notificationRepository = $notificationRepository;
        $this->spaceRepository = $spaceRepository;
    }

    public function checkOutMasterRoom($accountId, $filter)
    {
        // Kiểm tra có phải người tạo room
        $room = $this->roomRepository->getOneArrayRoomByFilter($filter);
        // Kiểm tra có phải admin room
        // $checkSpaceType = $this->spaceRepository->checkSpaceType(null, $filter['space_id']);
        // if($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
        //     $isAdmin = $this->roomRepository->checkAdminRoomWorkLife($accountId, $filter);
        // } else {
        $isAdmin = $this->roomRepository->checkAdminRoom($accountId, $filter);
        // }
        if (!empty($room) && $room['created_by'] === (int)$accountId) {
            return  true;
        } else if (!empty($isAdmin)) {
            return  true;
        }

        return false;
    }

    public function getCreatedByRoom($accountId, $filter)
    {
        // check admin room
        $isAdmin = $this->roomRepository->checkAdminRoom($accountId, $filter);
        if (!empty($isAdmin)) {
            return $accountId;
        }

        $room = $this->roomRepository->getOneArrayRoomByFilter($filter);
        if (!empty($room) && $room['created_by']) {
            // id own room
            return $room['created_by'];
        }
    }

    public function isRoom($filter)
    {
        $room = $this->roomRepository->getOneArrayRoomByFilter($filter);
        if (!empty($room)) {
            return $room;
        }

        return false;
    }

    public function checkClickInRoom($room, $accountInfo)
    {
        $room = collect($room)->keyBy('id')->toArray();
        $roomIds = collect($room)->pluck('id')->values()->toArray();
        $applications = $this->applicationRepository->getAllApplicationByFilter(['room_id' => $roomIds, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        if (!empty($applications)) {
            $strToTime = strtotime('now');
            $today = getdate($strToTime);
            $date = date('Y-m-d', $strToTime);
            $applicationIds = collect($applications)->pluck('id')->values()->toArray();
            $filterAdvanced['application_id'] = $applicationIds;
            $filterAdvanced[ApplicationAdvancedSetting::ARRAY_WEEKDAYS[$today['wday']]] = ApplicationAdvancedSetting::VALUE_WEEKDAYS;
            $applicationAdvancedSettings = $this->applicationAdvancedSettingRepository->getAllApplicationAdvancedSettingByFilter($filterAdvanced);
            if (!empty($applicationAdvancedSettings)) {
                $arrApplicationAdvancedSettings = [];
                foreach ($applicationAdvancedSettings as $applicationAdvancedSetting) {
                    if (!empty($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']])) {
                        if (!empty($applicationAdvancedSetting['time_end']) && !empty($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']]['time_end']) && strtotime($applicationAdvancedSetting['time_end']) > strtotime($arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']]['time_end'])) {
                            $arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']] = $applicationAdvancedSetting;
                        }
                    } else {
                        $arrApplicationAdvancedSettings[$applicationAdvancedSetting['application_id']] = $applicationAdvancedSetting;
                    }
                }
                if (!empty($arrApplicationAdvancedSettings)) {
                    $filterStatisticApplication['application_advanced_setting_id'] = collect($arrApplicationAdvancedSettings)->pluck('id')->values()->toArray();
                    $filterStatisticApplication['room_id'] = $roomIds;
                    $filterStatisticApplication['date_at'] = $date;
                    $filterStatisticApplication['application_id'] = $applicationIds;

                    $statisticApplication = $this->statisticApplicationRepository->getAllStatisticApplicationByFilter($filterStatisticApplication);
                    if (!empty($statisticApplication)) {
                        $statisticApplication = collect($statisticApplication)->keyBy('application_id')->values()->toArray();
                        foreach ($applications as $application) {
                            if (
                                !empty($arrApplicationAdvancedSettings[$application['id']]) && !empty($statisticApplication[$application['id']]) && !empty($room[$application['room_id']])
                                && !empty($statisticApplication[$application['id']]['date_at']) && strtotime($statisticApplication[$application['id']]['date_at']) === strtotime($date)
                            ) {
                                if (
                                    !empty($arrApplicationAdvancedSettings[$application['id']]['clicks']) && !empty($statisticApplication[$application['id']]['clicks'])
                                    && $arrApplicationAdvancedSettings[$application['id']]['clicks'] > $statisticApplication[$application['id']]['clicks']
                                ) {
                                    $room[$application['room_id']]['click_enough'] = Application::CLICK_ENOUGH;
                                } else {
                                    $room[$application['room_id']]['click_enough'] = Application::CLICK_NOT_ENOUGH;
                                }
                            }
                        }
                    } else {
                        foreach ($applications as $application) {
                            if (!empty($room[$application['room_id']])) {
                                $room[$application['room_id']]['click_enough'] = Application::CLICK_NOT_ENOUGH;
                            }
                        }
                    }
                }
            }
        }

        $room = collect($room)->values()->toArray();
        return $room;
    }

    public function updateRoomAndAllApplication($id, $spaceId, $applications, $data, $accountInfo, $accountIds)
    {
        $room = $this->roomRepository->getOneObjectRoomByFilter(['id' => $id, 'space_id' => $spaceId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        if (empty($room)) {
            return false;
        }

        $oldRoomName = $room->name;

        $this->roomRepository->update($room, $data);
        $this->roomRepository->updateAccountRoomPublic($room, $spaceId, $data['type'], $accountInfo, $accountIds);

        if (!empty($applications)) {
            $arrApplicationLocations = [];
            foreach ($applications as $application) {
                if (!empty($application['type']) && !empty($application['id']) && !empty($application['position'])) {
                    if ($application['type'] === ApplicationLocation::TYPE_APPLICATION) {
                        $arrApplicationLocations[] = [
                            'organization_id' => $accountInfo['organization_id'],
                            'account_id' => $accountInfo['id'],
                            'application_id' => $application['id'],
                            'application_group_id' => null,
                            'type' => ApplicationLocation::TYPE_APPLICATION,
                            'space_id' => $spaceId,
                            'room_id' => $id,
                            'position' => $application['position']
                        ];
                    } else if ($application['type'] === ApplicationLocation::TYPE_APPLICATION_GROUP) {
                        $arrApplicationLocations[] = [
                            'organization_id' => $accountInfo['organization_id'],
                            'account_id' => $accountInfo['id'],
                            'application_id' => null,
                            'application_group_id' => $application['id'],
                            'type' => ApplicationLocation::TYPE_APPLICATION_GROUP,
                            'space_id' => $spaceId,
                            'room_id' => $id,
                            'position' => $application['position']
                        ];
                    }
                }
            }

            if (!empty($arrApplicationLocations)) {
                $this->applicationLocationRepository->deleteAllApplicationLocationByFilter(['space_id' => $spaceId, 'room_id' => $id, 'account_id' => $accountInfo['id'], 'organization_id' => $accountInfo['organization_id']]);
                $this->applicationLocationRepository->insertMulti($arrApplicationLocations);
            }
        }

        $this->notificationRepository->pushNotiUpdateRoom($accountInfo, $room, $oldRoomName);

        return true;
    }

    public function updateRoomAndAllApplicationWorkLife($id, $spaceId, $applications, $data, $accountInfo, $accountIds)
    {
        $room = $this->roomRepository->getOneObjectRoomByFilter(['id' => $id, 'space_id' => $spaceId, 'deleted_at' => true, 'organization_id' => $accountInfo['organization_id']]);
        if (empty($room)) {
            return false;
        }

        $oldRoomName = $room->name;

        $this->roomRepository->update($room, $data);
        $this->roomRepository->updateAccountRoomPublicWorkLife($room, $spaceId, $data['type'], $accountInfo, $accountIds);

        if (!empty($applications)) {
            $arrApplicationLocations = [];
            foreach ($applications as $application) {
                if (!empty($application['type']) && !empty($application['id']) && !empty($application['position'])) {
                    if ($application['type'] === ApplicationLocation::TYPE_APPLICATION) {
                        $arrApplicationLocations[] = [
                            'organization_id' => $accountInfo['organization_id'],
                            'account_id' => $accountInfo['id'],
                            'application_id' => $application['id'],
                            'application_group_id' => null,
                            'type' => ApplicationLocation::TYPE_APPLICATION,
                            'space_id' => $spaceId,
                            'room_id' => $id,
                            'position' => $application['position']
                        ];
                    } else if ($application['type'] === ApplicationLocation::TYPE_APPLICATION_GROUP) {
                        $arrApplicationLocations[] = [
                            'organization_id' => $accountInfo['organization_id'],
                            'account_id' => $accountInfo['id'],
                            'application_id' => null,
                            'application_group_id' => $application['id'],
                            'type' => ApplicationLocation::TYPE_APPLICATION_GROUP,
                            'space_id' => $spaceId,
                            'room_id' => $id,
                            'position' => $application['position']
                        ];
                    }
                }
            }

            if (!empty($arrApplicationLocations)) {
                $this->applicationLocationRepository->deleteAllApplicationLocationByFilter(['space_id' => $spaceId, 'room_id' => $id, 'account_id' => $accountInfo['id'], 'organization_id' => $accountInfo['organization_id']]);
                $this->applicationLocationRepository->insertMulti($arrApplicationLocations);
            }
        }
        $this->notificationRepository->pushNotiUpdateRoomWorkLife($accountInfo, $room, $oldRoomName);

        return true;
    }
}
