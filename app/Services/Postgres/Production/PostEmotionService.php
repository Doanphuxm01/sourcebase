<?php namespace App\Services\Postgres\Production;

use App\Repositories\Postgres\CommentRepositoryInterface;
use App\Repositories\Postgres\EmotionRepositoryInterface;
use App\Repositories\Postgres\PostEmotionRepositoryInterface;
use App\Repositories\Postgres\PostImageRepositoryInterface;
use App\Repositories\Postgres\PostRepositoryInterface;
use \App\Services\Postgres\PostEmotionServiceInterface;

use App\Services\Production\BaseService;

class PostEmotionService extends BaseService implements PostEmotionServiceInterface
{
    protected $postRepository;
    protected $emotionRepository;
    protected $commentRepository;
    protected $postImageRepository;
    protected $postEmotionRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        EmotionRepositoryInterface $emotionRepository,
        CommentRepositoryInterface $commentRepository,
        PostImageRepositoryInterface $postImageRepository,
        PostEmotionRepositoryInterface $postEmotionRepository
    )
    {
        $this->emotionRepository = $emotionRepository;
        $this->postRepository = $postRepository;
        $this->commentRepository= $commentRepository;
        $this->postImageRepository = $postImageRepository;
        $this->postEmotionRepository = $postEmotionRepository;
    }

    public function getAllReact($filter)
    {
        $arrEmotion = [];
        $detailPostEmotions = $this->postEmotionRepository->getAllEmotionByFilter($filter);
        if (!empty($detailPostEmotions)){
            foreach ($detailPostEmotions as $detailPostEmotion){
                $arrEmotion[$detailPostEmotion['emotion_id']][] = [
                    'id' => $detailPostEmotion['id'],
                    'post_id' => @$detailPostEmotion['post_id'],
                    'space_id' => @$detailPostEmotion['space_id'],
                    'account_id' => !empty($detailPostEmotion['created_by']['id']) ? $detailPostEmotion['created_by']['id'] : "",
                    'avatar' => !empty($detailPostEmotion['created_by']['profile_image']['source']) && file_exists(public_path() . '/' . $detailPostEmotion['created_by']['profile_image']['source']) ? asset($detailPostEmotion['created_by']['profile_image']['source']) : "",
                ];

            }
        }

        return $arrEmotion;
    }
}
