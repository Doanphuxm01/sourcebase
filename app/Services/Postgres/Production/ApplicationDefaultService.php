<?php namespace App\Services\Postgres\Production;

use \App\Services\Postgres\ApplicationDefaultServiceInterface;

use App\Services\Production\BaseService;

class ApplicationDefaultService extends BaseService implements ApplicationDefaultServiceInterface
{
    public function method()
    {
    }
}
