<?php namespace App\Services\Postgres\Production;

use App\Models\Postgres\Permission;
use App\Repositories\Postgres\DecentralizationRepositoryInterface;
use App\Repositories\Postgres\PermissionRepositoryInterface;
use \App\Services\Postgres\DecentralizationServiceInterface;

use App\Services\Production\BaseService;

class DecentralizationService extends BaseService implements DecentralizationServiceInterface
{
    protected $decentralizationRepository;
    protected $permissionRepository;

    public function __construct(
        DecentralizationRepositoryInterface $decentralizationRepository,
        PermissionRepositoryInterface $permissionRepository
    )
    {
        $this->decentralizationRepository = $decentralizationRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function getPermissions($id, $isRoot)
    {
        $arrPermissions = [];
        $allPermissions = $this->permissionRepository->getAllPermissionByFilter([]);
        $decentralization = $this->decentralizationRepository->getOneArrayDecentralizationWithPermissionByFilter(['id' => (int)@$id, 'deleted_at' => true]);
        $decentralizationPermissions = [];
        if (!empty($decentralization) && !empty($decentralization['decentralization_permissions'])){
            $decentralizationPermissions = collect($decentralization['decentralization_permissions'])->keyBy('key')->toArray();
        }
        if (!empty($allPermissions)){
            foreach ($allPermissions as $permission){
                $arrPermissions[$permission['key']] = [
                    'read' => !empty($decentralizationPermissions[$permission['key']]['read']) || $isRoot ? Permission::IS_ENABLE : Permission::DISABLE,
                    'create' => !empty($decentralizationPermissions[$permission['key']]['create']) || $isRoot ? Permission::IS_ENABLE: Permission::DISABLE,
                    'update' => !empty($decentralizationPermissions[$permission['key']]['update']) || $isRoot ? Permission::IS_ENABLE : Permission::DISABLE,
                    'delete' => !empty($decentralizationPermissions[$permission['key']]['delete']) || $isRoot ? Permission::IS_ENABLE : Permission::DISABLE,
                ];
            }
        }

        return $arrPermissions;
    }
}
