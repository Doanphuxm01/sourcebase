<?php namespace App\Services\Postgres\Production;

use App\Models\Postgres\Space;
use \App\Services\Postgres\AccountLifeWorkServiceInterface;

use App\Services\Production\BaseService;
use App\Repositories\Postgres\AccountLifeWorkRepositoryInterface;

class AccountLifeWorkService extends BaseService implements AccountLifeWorkServiceInterface
{
    protected $accountLifeWork;

    public function __construct(AccountLifeWorkRepositoryInterface $accountLifeWork)
    {
        $this->accountLifeWork = $accountLifeWork;
    }

    public function method()
    {

    }

    public function filterByAccountService($id, $type, $request)
    {
        if ($type === Space::TYPE_SPACE_INTERNAL) {
            return $this->accountLifeWork->filterByAccountWork($id, $type, $request);
        }

        if ($type === Space::TYPE_SPACE_CUSTOMER) {
            return $this->accountLifeWork->filterByAccountLife($id, $type, $request);
        }
    }

    public function filterByAccountServiceRemoveAccount($lsAccount, $id, $type, $request, $infoAccount)
    {
        if ((int)$type === Space::TYPE_SPACE_CUSTOMER) {
            return $this->accountLifeWork->getAccountLifeInWhere($lsAccount, $id, $type, $request, $infoAccount);
        }
    }
}
