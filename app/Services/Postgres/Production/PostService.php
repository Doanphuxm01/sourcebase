<?php namespace App\Services\Postgres\Production;

use App\Repositories\Postgres\CommentRepositoryInterface;
use App\Repositories\Postgres\PostEmotionRepositoryInterface;
use App\Repositories\Postgres\PostImageRepositoryInterface;
use App\Repositories\Postgres\PostRepositoryInterface;
use \App\Services\Postgres\PostServiceInterface;

use App\Services\Production\BaseService;

class PostService extends BaseService implements PostServiceInterface
{
    protected $postRepository;
    protected $commentRepository;
    protected $postImageRepository;
    protected $postEmotionRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        CommentRepositoryInterface $commentRepository,
        PostImageRepositoryInterface $postImageRepository,
        PostEmotionRepositoryInterface $postEmotionRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->commentRepository= $commentRepository;
        $this->postImageRepository = $postImageRepository;
        $this->postEmotionRepository = $postEmotionRepository;
    }

    public function getListPostByFilter($limit, $filter, $accountInfo)
    {
        $posts = $this->postRepository->getAllPostWithAllByFilter($limit, $filter);
        if (!empty($posts['data'])){
            $arPost = [];
            foreach ($posts['data'] as $post){
                $arrPostImages = [];
                if (!empty($post['post_image'])){
                    foreach ($post['post_image'] as $postImage){
                        if (!empty($postImage['source'])) {
                            $arrPostImages[] = asset($postImage['source']);
                        }
                    }
                }

                $arPost[] = [
                    'id' => $post['id'],
                    'content' => $post['content'],
                    'space_id' => $post['space_id'],
                    'created_by' => [
                        'id' => !empty($post['created_by']['id']) ? $post['created_by']['id'] : null,
                        'avatar' => !empty($post['created_by']['profile_image']['source']) ? asset($post['created_by']['profile_image']['source']) : ""
                    ],
                    'images' => $arrPostImages,
                    'comments' => !empty($post['postComment']) ? $post['postComment'] : [],
                    'emotions' => !empty($post['postEmotion']) ? $post['postEmotion'] : [],
                ];
            }

            $posts['data'] = $arPost;
        }

        return $posts;
    }

    public function addPost($data, $imageIds, $accountInfo)
    {
        $post = $this->postRepository->create($data);
        if (!empty($post) && !empty($imageIds)){
            $arrPostImage = [];
            foreach ($imageIds as $imageId){
                $arrPostImage[] = [
                    'post_id' => @$post->id,
                    'image_id' => (int)$imageId,
                    'space_id' => @$post->space_id,
                    'organization_id' => $accountInfo['organization_id'],
                ];
            }

            if (!empty($arrPostImage)){
                $this->postImageRepository->insertMulti($arrPostImage);
            }
        }

        return true;
    }

    public function updatePost($id, $data, $imageIds, $accountInfo)
    {
        $post = $this->postRepository->getOneObjectPostByFilter(['id' => $id, 'deleted_at' => true]);
        if (empty($post)){
            return false;
        }

        $updatePost = $this->postRepository->update($post, $data);
        if (!empty($updatePost) && !empty($imageIds)){
            $arrPostImage = [];
            foreach ($imageIds as $imageId){
                $arrPostImage[] = [
                    'post_id' => $post->id,
                    'image_id' => (int)$imageId,
                    'space_id' => $post->space_id,
                    'organization_id' => $accountInfo['organization_id'],
                ];
            }

            if (!empty($arrPostImage)){
                $this->postImageRepository->deleteAllPostImageByFilter(['post_id' => $post->id, 'space_id' => $post->space_id]);
                $this->postImageRepository->insertMulti($arrPostImage);
            }
        }

        return true;
    }


    public function deletePost($id, $spaceId, $accountInfo)
    {
        $this->postRepository->deleteAllPostByFilter(['id' => (int)$id, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        $this->commentRepository->deleteAllCommentByFilter(['post_id' => (int)$id, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id'], 'deleted_at' => true]);
        $this->postImageRepository->deleteAllPostImageByFilter(['post_id' => (int)$id, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id']]);
        $this->postEmotionRepository->deleteAllPostEmotionByFilter(['post_id' => (int)$id, 'space_id' => $spaceId, 'organization_id' => $accountInfo['organization_id']]);
    }
}
