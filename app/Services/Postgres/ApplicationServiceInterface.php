<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface ApplicationServiceInterface extends BaseServiceInterface
{
    public function addApplication($timeSlot, $data, $accountInfo);

    public function getDetailApplication($filter, $accountInfo);

    public function getDetailApplicationForClick($filter, $accountInfo);

    public function updateApplication($id, $timeSlot, $data, $accountInfo);

    public function deleteApplication($ids, $roomId, $spaceId, $accountInfo);

    public function getListApplicationByFilter($filter, $accountInfo);

    public function getListApplicationInRoomByFilter($filter);

    public function getDetailCreatedByApplication($filter, $accountInfo);

    public function getAllApplicationForQuickBar($filter, $accountInfo);

    public function uploadFile($request, $accountInfo);

    public function getPatchFileApplication($patch);

    public function deleteFileTransferFilesViaLink($idApplication, $type);

    public function compareClickApplication($idApplication, $roomId, $spaceId, $accountInfo);
}
