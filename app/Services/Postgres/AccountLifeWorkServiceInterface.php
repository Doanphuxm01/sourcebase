<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface AccountLifeWorkServiceInterface extends BaseServiceInterface
{
    public function filterByAccountService($id, $type, $request);

    public function filterByAccountServiceRemoveAccount($lsAccount, $id, $type, $request, $infoAccount);
}
