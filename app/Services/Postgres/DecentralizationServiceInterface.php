<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface DecentralizationServiceInterface extends BaseServiceInterface
{
    public function getPermissions($id, $isRoot);
}
