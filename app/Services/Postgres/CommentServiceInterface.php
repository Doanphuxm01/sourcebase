<?php namespace App\Services\Postgres;

use App\Services\BaseServiceInterface;

interface CommentServiceInterface extends BaseServiceInterface
{
    public function getListCommentByFilter($filter, $accountInfo);
}
