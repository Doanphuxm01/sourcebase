<?php

namespace App\Console\Commands;

use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Repositories\Postgres\AccountRepositoryInterface;
use Illuminate\Console\Command;

class DeleteTimeBlockAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:open {username}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open account when blocked';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $accountRepository;
    public function __construct(AccountRepositoryInterface $accountRepository)
    {
        parent::__construct();
        $this->accountRepository = $accountRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $username = $this->argument('username');
        if (!empty($username)){
            $account = $this->accountRepository->getOneArrayAccountByFilter(['username' => $username]);
            if (!empty($account)){
                eFunction::resetRedisWithExpiredTime($account['username'] . Account::STRING_REDIS);
                eFunction::resetRedisWithExpiredTime($account['email'] . Account::STRING_REDIS);
            }
        }
        return true;
    }
}
