<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\Image;

class AddMd5FileImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:md5';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listImges = Image::where('md5_file', '')->whereNull('deleted_at')->get();
        foreach ($listImges as $image) {
            if (file_exists(public_path($image->source))) {
                $update = Image::find($image->id);
                $update->md5_file = md5_file(public_path($update->source));
                $update->save();
            }
        }
        return Command::SUCCESS;
    }
}
