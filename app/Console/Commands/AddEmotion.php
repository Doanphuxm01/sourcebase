<?php

namespace App\Console\Commands;

use App\Elibs\eFunction;
use App\Repositories\Postgres\EmotionRepositoryInterface;
use Illuminate\Console\Command;

class AddEmotion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:Emotion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add emotions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $emotionRepository;
    public function __construct(EmotionRepositoryInterface $emotionRepository)
    {
        parent::__construct();
        $this->emotionRepository = $emotionRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $emotions = [
            [
                'name' => 'cry',
                'source' => 'images/emotions/cry.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
            [
                'name' => 'haha',
                'source' => 'images/emotions/haha.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
            [
                'name' => 'angry',
                'source' => 'images/emotions/angry.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
            [
                'name' => 'heart',
                'source' => 'images/emotions/heart.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
            [
                'name' => 'like',
                'source' => 'images/emotions/like.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
            [
                'name' => 'love',
                'source' => 'images/emotions/love.png',
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ],
        ];

        $this->emotionRepository->insertMulti($emotions);

        return true;
    }
}
