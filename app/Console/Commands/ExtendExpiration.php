<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Elibs\eFunction;
use Illuminate\Support\Facades\DB;
use App\Models\Postgres\Organization;

class ExtendExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extend:expiration {id} {day}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'extend:expiration organization_id day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            // Nhận tham số truyền vào
            $id = (int)$this->argument('id');
            $day = (int)$this->argument('day');

            // Gia hạn tổ chức
            $organization = Organization::find($id);
            $exp_date = \Carbon\Carbon::parse($organization->exp_date);
            if ($exp_date < now()) {
                $expDate = now()->addDay($day);
            } else {
                $expDate = $exp_date->addDay($day);
            }
            $organization->exp_date = $expDate;
            $organization->due_date = now();
            $organization->is_trial = Organization::IS_PRO;
            $organization->save();
            DB::commit();
            
            $days = now()->diffInDays($expDate);
            // Cache redis
            eFunction::setExpirationDateOrganizationAddDay($id, $days, $expDate);
            return true;
        } catch (\Exception $ex) {
            DB::rollBack();
            \Log::error($ex->getMessage());
            return false;
        }
    }
}
