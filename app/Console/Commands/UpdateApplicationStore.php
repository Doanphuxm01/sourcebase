<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationStore;
use Illuminate\Support\Facades\DB;
use App\Jobs\UpdateApplicationStore as UpdateAppJob;

class UpdateApplicationStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application-store:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update table application store in database';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            UpdateAppJob::dispatch();
            DB::commit();
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());;
            DB::rollback();
            return Command::FAILURE;
        }
    }
}
