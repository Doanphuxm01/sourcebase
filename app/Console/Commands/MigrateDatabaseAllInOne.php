<?php

namespace App\Console\Commands;

use App\Jobs\MergeDatabaseJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class MigrateDatabaseAllInOne extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:merge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Merge Database Dones in Dones Pro';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            
            \Artisan::call('migrate');
            MergeDatabaseJob::dispatch();

            DB::commit();
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            DB::rollBack();
            \Log::error($ex->getMessage());
            return Command::FAILURE;
        }
    }
}
