<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Models\Postgres\Account;
use App\Elibs\eCrypt;
use App\Elibs\eFunction;

class SendMailInteract extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-mail:interact';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $applicationRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ApplicationRepositoryInterface $applicationRepository)
    {
        parent::__construct();
        $this->applicationRepository = $applicationRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = now();
        $date = $now->format('Y-m-d');
        $dayNumberInWeek = $now->format('N'); // Monday: 1 -> 7

        $app = $this->applicationRepository->filterAppAvancedSettingSendMail($date, $dayNumberInWeek);

        if (!empty($app)) {
            foreach ($app as $data) {
                $tpl = [
                    'pattern' => Account::PATTERN[Account::APP_INTERACT],
                    'data' => eCrypt::encryptAES(json_encode($data))
                ];

                eFunction::sendMessageQueueFormSendEmail($tpl, Account::QUEUE_NAME_SEND_EMAIL);
            }
        }
        return true;
    }
}
