<?php

namespace App\Console\Commands;

use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Models\Postgres\Package;
use App\Models\Postgres\Position;
use App\Repositories\Postgres\MajorRepositoryInterface;
use App\Repositories\Postgres\PackageRepositoryInterface;
use App\Repositories\Postgres\PositionRepositoryInterface;
use Illuminate\Console\Command;

class AddDefaultData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'default:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add default data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $majorRepository;
    protected $packageRepository;
    protected $positionRepository;
    public function __construct(MajorRepositoryInterface $majorRepository, PackageRepositoryInterface $packageRepository, PositionRepositoryInterface $positionRepository)
    {
        parent::__construct();
        $this->majorRepository = $majorRepository;
        $this->packageRepository = $packageRepository;
        $this->positionRepository = $positionRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arrSpace = ['Giáo dục', 'Xe cộ', 'Ẩm thực', 'Âm nhạc', 'Thể thao', 'Du lịch', 'Giải trí', 'Nghệ thuật', 'Thẩm mỹ', 'Đồ họa', 'Thời trang', 'Bất động sản', 'Kiến trúc, xây dựng', 'Công nghệ', 'Luật', 'Báo chí', 'Khoa học', 'Xã hội', 'Truyền thông', 'Sức khỏe', 'Tài chính', 'Kinh tế', 'Y tế', 'Mua sắm'];
        $arrInsertSpace = [];
        foreach ($arrSpace as $space){
            $arrInsertSpace[] = [
                'name' => $space,
                'slug' => eFunction::generateSlug($space, '-'),
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        $arrPosition = [];
        foreach (Position::POSITION as $position){
            $arrPosition[] = [
                'name' => $position,
                'slug' => eFunction::generateSlug($position, '-'),
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        $this->packageRepository->create([
            'name' => 'Dùng thử',
            'slug' => eFunction::generateSlug('Dùng thử', '-'),
            'is_trial' => Package::IS_TRIAL,
            'point' => 50
        ]);

        if (!empty($arrInsertSpace)){
            $this->majorRepository->insertMulti($arrInsertSpace);
        }

        if (!empty($arrPosition)){
            $this->positionRepository->insertMulti($arrPosition);
        }

        return true;
    }
}
