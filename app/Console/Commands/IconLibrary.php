<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Postgres\ImageRepositoryInterface;
use App\Elibs\eFunction;
use App\Models\Postgres\Image;

class IconLibrary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:icon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Icon Default';
    protected $imageRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ImageRepositoryInterface $imageRepository)
    {
        $this->imageRepository = $imageRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->imageRepository->deleteIconDefault();

        $mydir = public_path('images/icon-lib');
        $myfiles = array_diff(scandir($mydir), array('.', '..'));
        sort($myfiles);

        $iconLibrary = [];

        foreach ($myfiles as $nameFile) {
            $iconLibrary[] = [
                'name' => eFunction::generateSlug($nameFile, '-') . '.png',
                'source_thumb' => 'images/icon-lib/' . $nameFile,
                'source' => 'images/icon-lib/' . $nameFile,
                'collection_type' => Image::COLLECTION_TYPE_LIBRARY,
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        $this->imageRepository->insertMulti($iconLibrary);

        return true;
    }
}
