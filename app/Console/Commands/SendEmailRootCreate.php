<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Elibs\eCrypt;
use Illuminate\Support\Facades\DB;

class SendEmailRootCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-mail:root-create {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send-mail:root-create email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            $email = $this->argument('email');
            $account = Account::where('email', $email)->whereNull('deleted_at')->first();
            if ($account) {
                $password = eFunction::generateRandomPassword(15) . '@';
                $account->password = \Hash::make($password);
                $account->save();
                DB::commit();
                $account = $account->toArray();
                $data = [
                    'rootname' => '',
                    'username' => $account['email'],
                    'email' => $account['email'],
                    'name' => $account['name'],
                    'url' => '',
                    'password' => $password,
                ];
                $tpl = [
                    'pattern' => Account::PATTERN[Account::ACTIVE_ACCOUNT],
                    'data' => eCrypt::encryptAES(json_encode($data))
                ];

                eFunction::sendMessageQueueFormSendEmail($tpl, Account::QUEUE_NAME_SEND_EMAIL);
            }
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            DB::rollback();
        }
    }
}
