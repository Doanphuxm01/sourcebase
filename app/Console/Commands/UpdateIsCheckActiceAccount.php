<?php

namespace App\Console\Commands;

use App\Models\Postgres\Account;
use Illuminate\Console\Command;

class UpdateIsCheckActiceAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:active_account_login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update account old to account active';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return Account::where('is_active', Account::IS_ACTIVE)->update(['is_check_active' => Account::IS_ACTIVE]);
    }
}
