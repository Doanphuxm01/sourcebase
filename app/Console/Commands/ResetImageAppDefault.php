<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\Application;
use App\Models\Postgres\Image;
use App\Elibs\eFunction;

class ResetImageAppDefault extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'image-app:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset image app default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $listAppDefault = Application::where('is_system', Application::IS_SYSTEM)->get()->toArray();
        if (!empty($listAppDefault)) {
            foreach ($listAppDefault as $value) {
                $image = Image::where('name', 'like', '%' . eFunction::generateSlug($value['name'], '-') . '%')->first();
                if (!empty($image)) {
                    $app = Application::find($value['id']);
                    $app->application_image_id = $image->id;
                    $app->save();
                }
            }
        }
        return true;
    }
}
