<?php

namespace App\Console\Commands;

use App\Elibs\eFunction;
use App\Repositories\Postgres\PermissionRepositoryInterface;
use Illuminate\Console\Command;

class AddPermission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add permission';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $permissionRepository;
    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        parent::__construct();
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->permissionRepository->deleteAllPermissionsByFilter(['deleted_at' => true]);

        $arrPermission = [
            'space' => 'Quản lý Space',
            'account' => 'Quản lý tài khoản',
            'group' => 'Quản lý nhóm',
            'customer' => 'Quản lý khách hàng',
            'statistic' => 'Báo cáo thống kê'
        ];

        $arrPers = [];
        foreach ($arrPermission as $key => $permission){
            $arrPers[] = [
                'name' => $permission,
                'key' => $key,
                'slug' => eFunction::generateSlug($permission, '-'),
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow()
            ];
        }

        $this->permissionRepository->createMulti($arrPers);
    }
}
