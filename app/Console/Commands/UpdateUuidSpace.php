<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\Space;

class UpdateUuidSpace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'space:add-uuid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add uuid in Space';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Space::all();
        Space::all();
        return Command::SUCCESS;
    }
}
