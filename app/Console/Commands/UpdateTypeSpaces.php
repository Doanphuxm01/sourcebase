<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\SpaceAccountLifeWork;
use App\Models\Postgres\Space;

class UpdateTypeSpaces extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'space:update-type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update type space';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $spaceIdTypeCustomer = SpaceAccountLifeWork::get()->pluck('space_id')->unique()->toArray();
        Space::whereNotIn('id', $spaceIdTypeCustomer)->update(['type' => Space::TYPE_SPACE_INTERNAL]);
        return Command::SUCCESS;
    }
}
