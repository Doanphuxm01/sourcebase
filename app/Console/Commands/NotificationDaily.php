<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Repositories\Postgres\ApplicationRepositoryInterface;
use App\Repositories\Postgres\NotificationRepositoryInterface;

class NotificationDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $applicationRepository;
    protected $notificationRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ApplicationRepositoryInterface $applicationRepository,
        NotificationRepositoryInterface $notificationRepository
    ) {
        parent::__construct();
        $this->applicationRepository = $applicationRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = now();
        $date = $now->format('Y-m-d');
        $timeCurrent = $now->format('H:i:s');
        $timeAddHour = $now->addHour()->format('H:i:s');
        $dayNumberInWeek = $now->format('N'); // Monday: 1 -> 7
        $application = $this->applicationRepository->filterAppAvancedSetting($date, $timeCurrent, $timeAddHour, $dayNumberInWeek);
        if (!empty($application)) {
            $this->notificationRepository->pushNotiInteractApp($application);
        }
        return true;
    }
}
