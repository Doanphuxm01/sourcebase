<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Models\Postgres\QuickBar;

class UpdateCretedByQuickBar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quick_bar:update_created_by';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update lable database created_by';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $quickBar = QuickBar::with('application')->get()->toArray();
        foreach ($quickBar as $key => $value) {
            if (!empty($value['application']['created_by'])) {
                QuickBar::where('id', $value['id'])->update(['created_by' => $value['application']['created_by']]);
            }
        }
        return 1;
    }
}
