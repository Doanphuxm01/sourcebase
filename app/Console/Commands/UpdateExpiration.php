<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\Postgres\OrganizationRepositoryInterface;
use App\Models\Postgres\Organization;
use App\Elibs\eFunction;

class UpdateExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $organizationRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrganizationRepositoryInterface $organizationRepository)
    {
        parent::__construct();
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = $this->organizationRepository->getAllObjectOrganization();
        if (!empty($data)) {
            $dataUpdate = array_map(function ($val) {
                if (empty($val['exp_date'])) {
                    $now = now();
                    $exp_date = \Carbon\Carbon::parse($val['created_at'])->addDay(15);
                    $val['due_date'] = $val['created_at'];
                    $val['exp_date'] = $exp_date;
                    if ($now->format('Y-m-d H:s:i') <= $exp_date->format('Y-m-d H:s:i')) {
                        $interval = $now->diff($exp_date);
                        $time = $interval->format('%d') * 24 * 60 * 60;
                        eFunction::setExpirationDateOrganizationCommand($val['id'], 'organization_expiration_', $val['exp_date'], $time);
                    }
                } else {
                    $now = now();
                    $exp_date = \Carbon\Carbon::parse($val['exp_date']);
                    if ($now->format('Y-m-d H:s:i') <= $exp_date->format('Y-m-d H:s:i')) {
                        $interval = $now->diff($exp_date);
                        $time = $interval->format('%d') * 24 * 60 * 60;
                        eFunction::setExpirationDateOrganizationCommand($val['id'], 'organization_expiration_', $val['exp_date'], $time);
                    }
                }
                return $val;
            }, $data->toArray());

            \Batch::update(new Organization(), $dataUpdate, 'id');
        }

        return true;
    }
}
