<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateDatabaseQuickbar;

class UpdateQuickbar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quickbars:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update quickbar database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UpdateDatabaseQuickbar::dispatch();
        return Command::SUCCESS;
    }
}
