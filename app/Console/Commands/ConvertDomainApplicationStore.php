<?php

namespace App\Console\Commands;

use App\Elibs\eFunction;
use App\Jobs\UpdateDomainApplicationStore;
use App\Models\Postgres\ApplicationStore;
use Illuminate\Console\Command;

class ConvertDomainApplicationStore extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:domain';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update link_url to domain application store';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        UpdateDomainApplicationStore::dispatch();
    }
}
