<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Postgres\Account;
use Illuminate\Support\Facades\DB;

class UpdateCapacity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'capacity:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            DB::beginTransaction();
            $users = DB::table('images')->select(DB::raw('sum(file_size), created_by as id'))->whereNotNull('created_by')->groupBy('created_by')->get();
            for ($i = 0, $count = count($users); $i < $count; ++$i) {
                $users[$i] = (array)$users[$i];
            }
            if (!empty($users)) {
                foreach ($users as $user) {
                    $capacity = Account::find($user['id']);
                    if ($capacity) {
                        $capacity->capacity = $user['sum'];
                        $capacity->save();
                    }
                }
            }
            DB::commit();
            return Command::SUCCESS;
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            DB::rollback();
            return false;
        }
    }
}
