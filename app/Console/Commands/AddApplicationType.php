<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Elibs\eFunction;
use Illuminate\Support\Facades\DB;

class AddApplicationType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'application-type:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $applicationType = [
            'Quản trị',
            'Lưu trữ',
            'Văn phòng ảo',
            'To-do list',
            'Quản lí dự án',
            'Theo dõi mục tiêu',
            'Theo dõi tiến độ',
            'Bảng tính',
            'Email',
            'Chat',
            'Marketing',
            'Salekit',
            'Showroom ảo',
            'Chăm sóc khách hàng',
            'Docs & Wiki',
            'Đào tạo',
            'Cộng đồng',
            'Tin tức',
            'Sự kiện',
            'Họp trực tuyến',
        ];
        $arrInsertAppType = [];
        foreach ($applicationType as $type) {
            $arrInsertAppType[] = [
                'name' => $type,
                'slug' => eFunction::generateSlug($type, '-'),
                'created_at' => eFunction::getDateTimeNow(),
                'updated_at' => eFunction::getDateTimeNow(),
            ];
        }

        DB::table('applications_type')->insert($arrInsertAppType);
        
        return true;
    }
}
