<?php

namespace App\Elibs;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class Debug
{
    const DEBUG_ON = 1;
    private $dbInfo = '';


    static function show($obj, $label = '', $color = '#ffcebb')
    {
        echo "<pre style='border: 1px solid #ff0000;margin:5px;padding:5px;background-color:$color !important;max-height: 800px;overflow: auto'>";
        $debug = debug_backtrace();
        echo "<h2>$label</h2>";
        echo ($debug[0]['file'] . ':' . $debug[0]['line']) . '<br/>';
        print_r($obj);
        echo "</pre>";
    }

    static function getDbInfo()
    {
        return DB::getQueryLog();
        if ($logDB) {
            $li = '';
            foreach ($logDB as $key => $val) {
                $li .= '<li> Time: ' . $val['time'] . 's';
                $li .= '<span class="db-sql">' . $val['query'] . '</span>';

                $li .= '</li>';
            }

            return $li;
        }

        return '';

    }

    static function pushNotification($msg = '')
    {
        $id = '2114743379:AAF1sewHyy6Iiu9xKj9_6ECOA5hkgSHDq_E';
        $userName = 'donespro';
        $ch = curl_init();
        // Set the URL
//        curl_setopt($ch, CURLOPT_URL, '
//        https://api.telegram.org/bot'.$id.'/sendMessage?chat_id=@'.$userName.'&text=' . urlencode($msg));

        curl_setopt($ch, CURLOPT_URL, 'https://api.telegram.org/bot2114743379:AAF1sewHyy6Iiu9xKj9_6ECOA5hkgSHDq_E/sendMessage?chat_id=@donespro&text=' . urlencode($msg));
        // Removes the headers from the output
        curl_setopt($ch, CURLOPT_HEADER, 0);
        // Return the output instead of displaying it directly
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // Execute the curl session
        curl_exec($ch);
        // Close the curl session
        curl_close($ch);
        // Return the output as a variable
        return true;
    }

    static function sendNotification($exception, $statusCode = 0)
    {
        $msg = "User:======";
        $msg .= "\nAccount BearerToken:" . $token = request()->bearerToken();
        $msg .= "\nUser:======";
        $msg .= "\n\nMessage: " . $exception->getMessage();
        $msg .= "\nStatusCode: " . $statusCode;
        $msg .= "\nFile: " . $exception->getFile() . ':' . $exception->getLine();
        $msg .= "\nREQUEST_URI: " . @$_SERVER['REQUEST_URI'];
        $msg .= "\nREMOTE_ADDR: " . @$_SERVER['REMOTE_ADDR'];
        $msg .= "\nHTTP_USER_AGENT: " . @$_SERVER['HTTP_USER_AGENT'];
        $msg .= "\nHTTP_REFERER: " . @$_SERVER['HTTP_REFERER'];
        $msg .= "\nREQUEST_METHOD: " . @$_SERVER['REQUEST_METHOD'];
        $msg .= "\nSERVER_NAME: " . @$_SERVER['SERVER_NAME'];
        $msg .= "\nHTTP_HOST: " . @$_SERVER['HTTP_HOST'];
        self::pushNotification($msg);
    }

    static function sendLog($log)
    {
        self::pushNotification($log);
    }

    static function resultTimeQuery($timeStart, $timeEnd)
    {
//        $timeStart = microtime(true);
        dd([
            'timeQuery' => number_format($timeEnd - $timeStart, 4)
        ]);
    }
}
