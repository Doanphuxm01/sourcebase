<?php

namespace App\Elibs;

use App\Jobs\SendNotification;
use App\Jobs\SendNotificationWorkLife;
use App\Models\Postgres\Account;

//use App\Models\Postgres\Admin\Device;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DateInterval;
use DatePeriod;
use DateTime;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPAbstractCollection;

class eFunction
{

    const TIME_BLOCK_LOGIN = 300;
    const TIME_REMEMBER_LOGIN = 180;

    const TIME_EXP_TRIAL = 1296000; //15d

    const MIN_TIME_LOGIN = 1;
    const MAX_TIME_LOGIN = 5;

    public static function arrayInteger($value)
    {
        $response = collect($value)->filter(function ($q) {
            return is_numeric($q);
        })->values()->toArray();

        return $response;
    }

    public static function randomInt($length)
    {
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= mt_rand(1, 9);
        }
        return $code;
    }

    public static function isPassword($password)
    {
        $regex = '/^(?=.[a-z])(?=.[A-Z])(?=.[0-9])(?=.[!@#$%^&*]).{8,}$/m';
        return preg_match($regex, $password);
    }

    static function isEmail($email)
    {
        $regex = '/^[a-zA-Z0-9.]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/';
        return preg_match($regex, trim($email));
    }

    public static function generateRandomString($length = 14)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < ($length - 6); $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return date('ymd', strtotime('now')) . $randomString;
    }

    public static function generateRandomPassword($length = 14)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < ($length - 6); $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public static function getDateTimeNow()
    {
        $date = date('Y-m-d H:i:s', strtotime('now'));
        return $date;
    }

    public static function generateSlug($name, $characters)
    {
        $slug = Str::slug($name, $characters);
        return $slug;
    }

    static function setRedisWithExpiredTime($key, $value, $second)
    {
        $redis = Redis::connection();

        $redis->set($key, $value);
        $redis->expire($key, $second);

        return true;
    }

    public static function checkPlatformLogin($platform)
    {
        switch ($platform) {
            case 'web':
                return Account::TYPE_SIGN_IN_WEB;
                break;

            case 'web_app':
                return Account::TYPE_SIGN_IN_WEB_APP;
                break;

            case 'ios':
                return Account::TYPE_SIGN_IN_IOS;
                break;

            case 'android':
                return Account::TYPE_SIGN_IN_ANDROID;
                break;

            default:
                return false;
                break;
        }
    }


    static function resetRedisWithExpiredTime($key)
    {
        $redis = Redis::connection();

        $redis->persist($key);

        return true;
    }

    public static function getRedisExpiredTime($key)
    {
        $redis = Redis::connection();
        $second = $redis->ttl($key);

        return $second;
    }

    public static function getRedis($key)
    {
        $redis = Redis::connection();
        $value = $redis->get($key);

        return $value;
    }

    public static function delRedis($key)
    {
        $redis = Redis::connection();
        $value = $redis->del($key);

        return $value;
    }

    public static function getXFrameOptions($url)
    {
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
                'connect_timeout' => 0.10
            ]);
            $request = $client->get($url);
            $headers = $request->getHeaders();

            return isset($headers['X-Frame-Options']) ? 1 : 0;
        } catch (\Exception $ex) {
            return 1;
        }
    }

    public static function getHost($url)
    {
        $url = parse_url($url);
        $domain = '';

        if (isset($url['scheme']) && isset($url['host'])) {
            $domain = $url['scheme'] . '://' . $url['host'];
        }

        return $domain;
    }

    public static function getPathDomain($url)
    {
        $url = parse_url($url);

        return !empty($url['path']) ? asset($url['path']) : $url['path'];
    }

    public static function pushNotifictions($data, $accountRecive)
    {
        SendNotification::dispatch($accountRecive, $data);
        //        $sendFcm = \UserNotificationHelper::sendWithAccountId($accountRecive,$data);
        //        $data = json_encode([
        //            'is_loop' => is_array($accountRecive) ? 1 : 0,
        //            'user_recive' => $accountRecive,
        //            'payload' => eCrypt::encryptAES(json_encode($data))
        //        ]);
        //
        //        $payload = $data;
        //
        //        $ch = curl_init(config('systems.SOCKET_URL') . '/send-notification');
        //
        //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        //        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        //        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        //
        //        // Set HTTP Header for POST request
        //        curl_setopt(
        //            $ch,
        //            CURLOPT_HTTPHEADER,
        //            array(
        //                'Content-Type: application/json',
        //                'Content-Length: ' . strlen($payload)
        //            )
        //        );
        //
        //        // Submit the POST request
        //        $result = curl_exec($ch);
        //
        //        curl_close($ch);
        //
        //        return $result;
    }

    public static function pushNotifictionsWorkLife($data, $accountRecive)
    {
        SendNotificationWorkLife::dispatch($accountRecive, $data);
    }

    public static function getThrottleLoginExpiredTime($id, $string = '_account_blocked')
    {
        return self::getRedisExpiredTime($id . $string);
    }

    public static function getThrottleLogin($id, $string = '_account_blocked')
    {
        return self::getRedis($id . $string);
    }

    public static function delThrottleLogin($id, $string = '_account_blocked')
    {
        return self::delRedis($id . $string);
    }

    public static function setThrottleLogin($id, $string = '_account_blocked')
    {
        $key = $id . $string;
        $valueNumberUserLoginFailed = self::getRedis($key);
        $newValue = $valueNumberUserLoginFailed ? $valueNumberUserLoginFailed + 1 : 1;
        self::setRedisWithExpiredTime($key, $newValue, self::TIME_BLOCK_LOGIN);
        return (int)$newValue;
    }

    public static function setExpirationDateOrganization($id, $string = 'organization_expiration_', $expDate)
    {
        $key = $string . $id;
        // $valueNumberUserLoginFailed = self::getRedis($key); // get data
        self::setRedisWithExpiredTime($key, $expDate, self::TIME_EXP_TRIAL);
        return true;
    }

    public static function setExpirationDateOrganizationAddDay($id, $day, $expDate)
    {
        $seconds = $day * 24 * 60 * 60;
        $key = 'organization_expiration_' . $id;
        self::setRedisWithExpiredTime($key, $expDate, $seconds);
        return true;
    }

    public static function setExpirationDateOrganizationCommand($id, $string = 'organization_expiration_', $expDate, $time)
    {
        $key = $string . $id;
        // $valueNumberUserLoginFailed = self::getRedis($key); // get data
        self::setRedisWithExpiredTime($key, $expDate, $time);
        return true;
    }

    public static function getOrganizationExpiredDate($id, $string = 'organization_expiration_')
    {
        return self::getRedis($string . $id);
    }

    public static function sendMessageQueueFormSendEmail($params, $queue_name)
    {
        $connection = new AMQPStreamConnection(env('RABBITMQ_HOST', 'localhost'), env('RABBITMQ_PORT', 5672), env('RABBITMQ_USER', 'guest'), env('RABBITMQ_PASSWORD', 'guest'), '/', false, 'AMQPLAIN', null, 'en_US', 10);
        $channel = $connection->channel();
        $channel->queue_declare($queue_name, false, true, false, false, false);
        $channel->basic_publish(new AMQPMessage(json_encode($params), array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)), '', $queue_name);

        $channel->close();
        $connection->close();

        return true;
    }

    public static function getDomain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';
        if (isset($pieces['port'])) {
            return $domain;
        }
        // [^\.]+\.[^\.]+$
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,10})$/i', $domain, $regs)) {
            if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $regs['domain'], $min)) {
                return $min['domain'];
            }
            return $regs['domain'];
        }
        return null;
    }

    public static function getImageSource($image)
    {
        return !empty($image['source']) && file_exists(public_path($image['source'])) ? asset($image['source']) : '';
    }

    public static function getImageSourceThumb($image)
    {
        return !empty($image['source_thumb']) && file_exists(public_path($image['source_thumb'])) ? asset($image['source_thumb']) : '';
    }

    public static function getLocationWithIp($ip)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            // CURLOPT_URL => "http://ip-api.com/json/" . $ip,
            CURLOPT_URL => "https://geolocation-db.com/json/" . $ip,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response);
        if ($response && $response->city !== 'Not found') {
            // if (!empty($response) && $response->status !== 'fail') {
            // $result = $response->city . ', ' . $response->country;
            $result = $response->city . ', ' . $response->country_name;
        } else {
            $result = null;
        }

        return $result;
    }

    public static function randomNumber($length = 8)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public static function zaloCustomGroupUrl($str)
    {
        // $str = 'https://zalo.me/g/tefdnj084';
        $url = parse_url($str);
        if ($url['host'] == 'zalo.me' || $url['host'] == 'www.zalo.me') {
            if (preg_match('/\/g\/([a-z 0-9]+)$/', $url['path'])) {
                if (preg_match('/\/([a-z 0-9]+)$/', $url['path'], $match)) {
                    return $url['scheme'] . '://chat.zalo.me' . '/?g=' . $match[1];
                }
            }
        }
    }
}
