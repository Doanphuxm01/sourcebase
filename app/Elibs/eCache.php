<?php

namespace App\Elibs;

use Illuminate\Support\Facades\Log;

class eCache extends \Cache
{
    static $debug = '';

    static function add($key, $value, $minute=846000)
    {

        return parent::add($key, $value, $minute);
    }

    static function get($key, $default = NULL)
    {
        return parent::get($key, $default);
    }

    static function del($key)
    {
        return parent::forget($key);
    }

    static function setRequests($key, $value, $minute=846000){
        $str_key = 'requests:'.$key;
        return parent::add($str_key, $value, $minute);
    }

    static function getRequests($key, $default = NULL){
        $str_key = 'requests:'.$key;
        return parent::get($str_key, $default);
    }
}
