<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface DecentralizationPermissionRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function createMulti($params);

    public function countAllDecentralizationPermissionsByFilter($filter);
}
