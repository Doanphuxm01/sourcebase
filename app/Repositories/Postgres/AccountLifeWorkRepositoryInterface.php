<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface AccountLifeWorkRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function filterByAccountWork($id, $type, $request);

    public function filterByAccountLife($id, $type, $request);

    public function getAccountLifeInWhere($lsAccount, $id, $type, $request, $infoAccount);

//    public function filterByAccountRepository($type, $request);
}
