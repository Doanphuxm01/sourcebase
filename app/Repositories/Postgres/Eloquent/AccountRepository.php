<?php namespace App\Repositories\Postgres\Eloquent;

use App\Jobs\SendMailAccountActive;
use App\Repositories\Eloquent\AuthenticationRepository;
use \App\Repositories\Postgres\AccountRepositoryInterface;
use \App\Models\Postgres\Account;
use \App\Models\Postgres\AccountAccessTokens;
use App\Services\Postgres\AccountServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AccountRepository extends AuthenticationRepository implements AccountRepositoryInterface
{

//    protected $accountService;
//
//    public function __construct(
//
//        AccountServiceInterface $accountService
//    )
//    {
//        $this->accountService = $accountService;
//
//    }

    public function getBlankModel()
    {
        return new Account();
    }

    public function perAccessToken()
    {
        return new AccountAccessTokens();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function getOneArrayAccountForLoginByFilter($filter)
    {
        $query = $this->withDecentralization();

        $this->filter($filter, $query);
        $dataX = $query->with(['profileImage' => function ($query) {
            $query->select('images.id', 'images.name', 'images.md5_file', 'images.source', 'images.source_thumb');
        }])->first();

        $data = [];

        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    private function withDecentralization()
    {
        $query = $this->getBlankModel()
            ->with(['decentralization' => function ($query) {
                $query->select('decentralizations.id', 'decentralizations.name')
                    ->with(['decentralizationPermissions' => function ($query) {
                        $query->select('permissions.id', 'permissions.name', 'permissions.key', 'decentralization_permissions.read', 'decentralization_permissions.create', 'decentralization_permissions.update', 'decentralization_permissions.delete', 'decentralization_permissions.permission_id', 'decentralization_permissions.decentralization_id');
                    }]);
            }]);
        return $query;
    }

    public function getOneArrayAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $dataX = $query->first();

        $data = [];

        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getOneArrayAccountWithManagerByFilter($filter)
    {
        $query = $this->withManager();

        $this->filter($filter, $query);
        $dataX = $query->first();

        $data = [];

        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getOneObjectAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->first();

        return $data;
    }

    public function getOneArrayAccountWithModelByFilter($filter)
    {
        $query = $this->withImageAndPermissionGroup();

        $this->filter($filter, $query);
        $dataX = $query->first();

        $data = [];

        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function updateAllAccountByFilter($filter, $params)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->update($params);

        return $data;
    }

    public function getListAccountByFilter($limit, $filter)
    {
        $query = $this->withImageAndManager();
        $this->filter($filter, $query);

        $data = $query->orderBy('accounts.created_at', 'desc')->paginate($limit)->toArray();

        return $data;
    }

    public function totalAccount()
    {
        $query = $this->getBlankModel();

        $data = $query->count();

        return $data;
    }

    public function countAllAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->count();

        return $data;
    }

    public function deleteAllAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getAllAccountForSelectByFilter($filter)
    {

        $query = $this->withPositionAndImage();
        $this->filter($filter, $query);
        $data = $query->get()->toArray();

        return $data;
    }

    public function getAllAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->get()->toArray();

        return $data;
    }

    public function resetTokenAllAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->update([
            'api_web_access_token' => null,
            'api_ios_access_token' => null,
            'api_android_access_token' => null,
            'api_web_app_access_token' => null
        ]);

        $accountIds = $query->get()->pluck('id')->toArray();
        if (!empty($accountIds)) {
            $this->perAccessToken()->whereIn('account_id', $accountIds)->delete();
        }
        return $data;
    }

    private function withPositionAndImage()
    {

        // lấy all account bên work
        $query = $this->getBlankModel()->select([
            "accounts.id",
            "accounts.username",
            "accounts.name",
            "accounts.phone_number",
            "accounts.email",
            "accounts.organization_id",
            "accounts.is_active",
            "accounts.position_id",
            "accounts.profile_image_id",
            "accounts.created_by",
            "accounts.main_space",
            "accounts.type_account",
            "accounts.is_check_active",
        ])
            ->with(['profileImage' => function ($query) {
                $query->select('images.id', 'images.name', 'images.md5_file', 'images.source', 'images.source_thumb');
            }])
            // ->with(['position' => function ($query) {
            //     $query->select('positions.id', 'positions.name');
            // }])
            ->with(['createdBy' => function ($query) {
                $query->select('accounts.id', 'accounts.name', 'accounts.username');
            }]);
        // ->with(['managers']);
        return $query;
    }

    private function withManager()
    {
        $query = $this->getBlankModel()->select([
            "accounts.id",
            "accounts.username",
            "accounts.name",
            "accounts.birthday",
            "accounts.api_web_access_token",
            "accounts.api_web_app_access_token",
            "accounts.api_ios_access_token",
            "accounts.api_android_access_token",
            "accounts.email",
            "accounts.phone_number",
            "accounts.position_id",
            "accounts.is_root",
            "accounts.is_active",
            "accounts.language",
            "accounts.organization_id",
            "accounts.profile_image_id",
            "accounts.created_by",
            "accounts.space_id",
            "accounts.main_space",
        ])
            ->with(['profileImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            ->with(['backgroundImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            ->with(['position' => function ($query) {
                $query->select('positions.id', 'positions.name');
            }])
            ->with(['decentralization' => function ($query) {
                $query->select('decentralizations.id', 'decentralizations.name');
            }])
            ->with(['organization' => function ($query) {
                $query->select('organizations.id', 'organizations.name');
            }])
            ->with(['managers' => function ($query) {
                $query->select('accounts.id', 'accounts.name', 'accounts.username');
            }]);

        return $query;
    }

    private function withImageAndManager()
    {
        $query = $this->getBlankModel()->select([
            "accounts.id",
            "accounts.username",
            "accounts.name",
            "accounts.birthday",
            "accounts.email",
            "accounts.phone_number",
            "accounts.position_id",
            "accounts.is_root",
            "accounts.is_active",
            "accounts.profile_image_id",
            "accounts.decentralization_id",
            "accounts.created_by",
            "accounts.main_space",
            "accounts.type_account",
            "accounts.is_admin",
        ])
            ->with(['profileImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            ->with(['backgroundImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            // ->with(['position' => function ($query) {
            //     $query->select('positions.id', 'positions.name');
            // }])
            ->with(['decentralization' => function ($query) {
                $query->select('decentralizations.id', 'decentralizations.name');
            }])
            ->with(['createdBy' => function ($query) {
                $query->select('accounts.id', 'accounts.name', 'accounts.username');
            }]);
        // ->with(['managers' => function ($query) {
        //     $query->select('accounts.id', 'accounts.name', 'accounts.username');
        // }]);

        return $query;
    }

    private function withImageAndPermissionGroup()
    {
        $query = $this->getBlankModel()->select([
            "accounts.id",
            "accounts.username",
            "accounts.name",
            "accounts.birthday",
            "accounts.email",
            "accounts.phone_number",
            "accounts.position_id",
            "accounts.is_root",
            "accounts.profile_image_id",
            "accounts.created_by",
            'accounts.background_image_id',
            'accounts.organization_id',
            'accounts.decentralization_id',
            'accounts.space_id',
            'accounts.created_by',
            'accounts.main_space',
            'accounts.is_admin',
            'accounts.link_address',
        ])
            ->with(['profileImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            ->with(['backgroundImage' => function ($query) {
                $query->select('images.id', 'images.source');
            }])
            ->with(['createdBy' => function ($query) {
                $query->select('accounts.id', 'accounts.name', 'accounts.username');
            }])
            ->with(['position' => function ($query) {
                $query->select('positions.id', 'positions.name');
            }])
            ->with(['decentralization' => function ($query) {
                $query->select('decentralizations.id', 'decentralizations.name');
            }])
            ->with(['organization' => function ($query) {
                $query->select('organizations.id', 'organizations.name', 'organizations.package_id')
                    ->with(['package' => function ($query) {
                        $query->select('packages.id', 'packages.name');
                    }]);
            }])
            ->with(['managers' => function ($query) {
                $query->select('accounts.id', 'accounts.name', 'accounts.username');
            }]);

        return $query;
    }

    private function filter($filter, &$query)
    {
//        $query = $query->where('accounts.is_check_active', 1);
        if (isset($filter['api_web_access_token'])) {
            $query = $query->where('accounts.api_web_access_token', $filter['api_web_access_token']);
        }
        if (isset($filter['is_check_active'])) {
            $query = $query->where('accounts.is_check_active', 1);
        }

        if (isset($filter['not']['is_root'])) {
            $query = $query->where('accounts.is_root', Account::IS_NOT_ROOT);
        }

        if (isset($filter['type_account'])) {
            $query = $query->where('accounts.type_account', $filter['type_account']);
        }

        if (isset($filter['api_ios_access_token'])) {
            $query = $query->where('accounts.api_ios_access_token', $filter['api_ios_access_token']);
        }

        if (isset($filter['api_android_access_token'])) {
            $query = $query->where('accounts.api_android_access_token', $filter['api_android_access_token']);
        }

        if (isset($filter['api_web_app_access_token'])) {
            $query = $query->where('accounts.api_web_app_access_token', $filter['api_web_app_access_token']);
        }

        if (isset($filter['google_id'])) {
            $query = $query->where('accounts.google_id', $filter['google_id']);
        }

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('accounts.id', $filter['id']);
            } else {
                $query = $query->where('accounts.id', $filter['id']);
            }
        }

        if (isset($filter['decentralization_id'])) {
            if (is_array($filter['decentralization_id'])) {
                $query = $query->whereIn('accounts.decentralization_id', $filter['decentralization_id']);
            } else {
                $query = $query->where('accounts.decentralization_id', $filter['decentralization_id']);
            }
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('accounts.is_active', $filter['is_active']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('accounts.organization_id', $filter['organization_id']);
        }

        if (isset($filter['email'])) {
            $query = $query->where('accounts.email', $filter['email']);
        }

        if (isset($filter['username'])) {
            $key_word = $filter['username'];
            $query = $query->where(function ($query) use ($key_word) {
                $query->orWhere('accounts.email', $key_word);
                $query->orWhere('accounts.username', $key_word);
            });
        }


        if (isset($filter['deleted_at'])) {
            $query = $query->where('accounts.deleted_at', null);
        }

        if (isset($filter['password'])) {
            $query = $query->where('accounts.password', $filter['password']);
        }

        if (isset($filter['key_word'])) {
            $query = $query->where(function ($query) use ($filter) {
                $query->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower($filter['key_word']) . '%')
                    ->orWhere('accounts.email', 'like', '%' . $filter['key_word'] . '%');
                // ->orWhere('accounts.username', 'like', '%' . $filter['key_word'] . '%');
            });
        }
    }

    public function countSpaceAndRoom($queryString, $accountInfo)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 10;
        $userName = $queryString["userName"] ?? null;

        $query = $this->getBlankModel()
            ->select('id', 'name', 'username', 'profile_image_id')
            ->with(['profileImage' => function ($q) {
                $q = $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
            }])
            ->withCount('spaceAccount')->withCount('roomAccount')
            ->where('organization_id', $accountInfo['organization_id']);
        if ($userName) {
            $query = $query->where('username', 'like', '%' . $userName . '%');
        }
        $query = $query->paginate($limit);
        $query->map(function ($val) {
            if (!empty($val->profileImage)) {
                $val->profileImage['source_thumb'] = !empty($val->profileImage['source_thumb']) ? asset($val->profileImage['source_thumb']) : '';
                $val->profileImage['source'] = !empty($val->profileImage['source']) && file_exists(public_path() . '/' . $val->profileImage['source']) ? asset($val->profileImage['source']) : '';
            }
            return $val;
        });
        return $query;
    }

    public function listLoginHistory($accountInfo)
    {
        $now = now();
        $loginHistory = $this->perAccessToken()->where('account_id', $accountInfo['id'])->select([
            'id',
            'last_used_at',
            'location',
            'user_agent',
            'device_name',
        ])->whereNotNull('last_used_at')->orderBy('last_used_at', 'desc')->get();
        $loginHistory = $loginHistory->map(function ($data) use ($now) {
            $data->is_active = strtotime($now) - strtotime($data->last_used_at) > 60 ? 0 : 1;
            return $data;
        });
        return $loginHistory;
    }

    public function logoutDevice($accountInfo, $id)
    {
        $this->perAccessToken()->where('id', $id)->delete();
        return true;
    }

    public function logoutDeviceAll($accountInfo)
    {
        $this->perAccessToken()->where('account_id', $accountInfo['id'])->delete();
        return true;
    }

    public function getAllAccountCustomer($search)
    {
        $limit = 10;
        $account = $this->getBlankModel();
        $account = $account->where([
            'type_account' => Account::TYPE_ACCOUNT_CLIENT,
            'is_check_active' => Account::IS_ACTIVE
        ]);
//        if ($search) {
//            $account = $account
//                ->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::lower($search) . '%')
////                ->where('accounts.name', 'like', '%' . $search . '%')
//                ->orWhere('accounts.email', 'LIKE', '%' . $search . '%');
//        }
        $account = $account->with('profileImage')
            ->select(
                'accounts.id',
                'accounts.name',
                'accounts.email',
                'accounts.phone_number',
                'accounts.link_address',
                'accounts.rank',
                'accounts.profile_image_id',
                'accounts.type_account'
            )
            ->paginate($limit)
            ->toArray();
        if (!empty($account)) {
            $account['data'] = array_map(function ($val) {
                if (!encrypt($val['profile_image'])) {
                    $val['profile_image']['source_full'] = !empty($val['profile_image']['source_thumb']) ? $val['profile_image']['source_thumb'] : '';
                }
                return $val;
            }, $account['data']);
        }
        return $account;
    }

    public function approveJoinAllInAccount($data)
    {
        // duyet tat ca
        $accountSendMail = [];
        if ($data['approve_all'] == 1) {
            $account = $this->getBlankModel()->where('is_check_active', Account::IS_DISABLE)->pluck('id')->toArray();
            if ($data['is_accept'] == 1) {
                $accountSendMail['active'] = $account;
                $this->getBlankModel()->where('is_check_active', Account::IS_DISABLE)->update(['is_check_active' => Account::IS_ACTIVE]);
                $accountActive = $this->getListAccountForId($account);
                $accountActive['is_array'] = true;
                SendMailAccountActive::dispatch($accountActive, Account::ACCOUNT_APPROVAL);
                return true;
            } else {
                $accountActive = $this->getListAccountForId($account);
                $accountActive['is_array'] = true;
                SendMailAccountActive::dispatch($accountActive, Account::ACCOUNT_DENNY);
                $this->getBlankModel()->where('is_check_active', Account::IS_DISABLE)->delete();
                return true;
            }
        }
        if ($data['approve_all'] == 0) {
            if ($data['is_accept'] == 1) {
                $accountSendMail['active'][] = $data['id'];
                $this->getBlankModel()->where('id', $data['id'])->update(['is_check_active' => Account::IS_ACTIVE]);
                $accountActive = $this->getListAccountForId($data['id']);
                SendMailAccountActive::dispatch($accountActive, Account::ACCOUNT_APPROVAL);
                return true;
            } else {
                $accountActive = $this->getListAccountForId($data['id']);
                SendMailAccountActive::dispatch($accountActive, Account::ACCOUNT_DENNY);
                $this->getBlankModel()->where('id', $data['id'])->delete();
                return true;
            }
        }
        return false;
    }

    public function getListAccountForId($array)
    {
        if(is_array($array)) {
            $account = $this->getBlankModel()->whereIn('id', $array)->select('email', 'name', 'username', 'password')->get()->toArray();
        } else {
            $account = $this->getBlankModel()->where('id', $array)->select('email', 'name', 'username', 'password')->first();
            if(!empty($account)) {
                $account = $account->toArray();
            }
        }

        return $account;
    }

    public function listAccountWaitApprove($data)
    {
        $id = $data['id'] ?? null;
        $order_by = $data['order_by'] ?? 'desc';
        $key_word = $data['key_word'] ?? null;
        $limit = $data['limit'] ?? 10;

        $account = $this->getBlankModel();
        $account = $account->where([
            'type_account' => Account::TYPE_ACCOUNT_CLIENT,
            'is_check_active' => Account::IS_DISABLE
        ]);
        if ($key_word) {
            $account = $account->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::ascii(Str::lower($key_word)) . '%');
        }
        $account = $account->with('profileImage')
            ->select(
                'accounts.id',
                'accounts.name',
                'accounts.email',
                'accounts.profile_image_id',
                'accounts.is_check_active',
                'accounts.created_at'
            )
            ->orderBy('accounts.created_at', $order_by)->paginate($limit)->toArray();
        $account['data'] = array_map(function ($val) {
            if (!encrypt($val['profile_image'])) {
//                $val['profile_image']['source_full'] = !empty($val['profile_image']['source_thumb']) ? $val['profile_image']['source_thumb'] : '';
                $val['profile_image']['source_full'] = !empty($val['profile_image']['source_thumb']) && file_exists(public_path() . '/' . $val['profile_image']['source_thumb']) ? asset($val['profile_image']['source_thumb']) : '';
            }
            return $val;
        }, $account['data']);
        return $account;
    }

    public function getDetailCustomer($id)
    {
        $id = (int)$id;
        $account = $this->getBlankModel()->where('id', $id)->select(
            'accounts.id',
            'accounts.name',
            'accounts.email',
            'accounts.profile_image_id',
            'accounts.rank',
            'accounts.phone_number',
            'accounts.link_address'

        )->first();
        if (!empty($account)) {
            return $account->toArray();
        }
        return false;

    }

    public function updateCustomer($data, $id)
    {
        return $this->getBlankModel()->where('id', $id)->update(['rank' => $data['rank']]);
    }

    public function setAdminInCustomer($data)
    {
        if ($data['is_admin'] == Account::IS_ADMIN_CUSTOMER) {
            return $this->getBlankModel()->where('id', $data['id'])->update(['is_admin' => Account::IS_ADMIN_CUSTOMER]);
        }
        if ($data['is_admin'] == Account::IS_NOT_ADMIN_CUSTOMER) {
            return $this->getBlankModel()->where('id', $data['id'])->update(['is_admin' => Account::IS_NOT_ADMIN_CUSTOMER]);
        }
        return false;
    }
}
