<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationAdvancedSettingRepositoryInterface;
use \App\Models\Postgres\ApplicationAdvancedSetting;

class ApplicationAdvancedSettingRepository extends SingleKeyModelRepository implements ApplicationAdvancedSettingRepositoryInterface
{

    public function getBlankModel()
    {
        return new ApplicationAdvancedSetting();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function createMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insertUsers = $this->getBlankModel()->insert($params);
            if($insertUsers){
                return true;
            }
        }
        return false;
    }

    public function getAllApplicationAdvancedSettingByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function deleteAllApplicationAdvancedSettingByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('application_advanced_settings.id', $filter['id']);
            } else {
                $query = $query->where('application_advanced_settings.id', $filter['id']);
            }
        }

        if (isset($filter['application_id'])) {
            if (is_array($filter['application_id'])) {
                $query = $query->whereIn('application_advanced_settings.application_id', $filter['application_id']);
            } else {
                $query = $query->where('application_advanced_settings.application_id', $filter['application_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('application_advanced_settings.organization_id', $filter['organization_id']);
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('application_advanced_settings.created_by', $filter['created_by']);
        }

        // Lọc click theo ngày
        if (isset($filter['sunday'])) {
            $query = $query->where('application_advanced_settings.sunday', $filter['sunday']);
        }

        if (isset($filter['monday'])) {
            $query = $query->where('application_advanced_settings.monday', $filter['monday']);
        }

        if (isset($filter['tuesday'])) {
            $query = $query->where('application_advanced_settings.tuesday', $filter['tuesday']);
        }

        if (isset($filter['wednesday'])) {
            $query = $query->where('application_advanced_settings.wednesday', $filter['wednesday']);
        }

        if (isset($filter['thursday'])) {
            $query = $query->where('application_advanced_settings.thursday', $filter['thursday']);
        }

        if (isset($filter['friday'])) {
            $query = $query->where('application_advanced_settings.friday', $filter['friday']);
        }

        if (isset($filter['saturday'])) {
            $query = $query->where('application_advanced_settings.saturday', $filter['saturday']);
        }
    }

}
