<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PositionRepositoryInterface;
use \App\Models\Postgres\Position;

class PositionRepository extends SingleKeyModelRepository implements PositionRepositoryInterface
{

    public function getBlankModel()
    {
        return new Position();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function insertMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insert = $this->getBlankModel()->insert($params);
            if($insert){
                return true;
            }
        }
        return false;
    }

    public function getAllPositionByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('positions.id', $filter['id']);
            } else {
                $query = $query->where('positions.id', $filter['id']);
            }
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('positions.is_active', $filter['is_active']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('positions.deleted_at', null);
        }

    }
}
