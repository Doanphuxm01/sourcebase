<?php

namespace App\Repositories\Postgres\Eloquent;

use App\Elibs\Debug;
use App\Models\Postgres\RoomAccountsWorkLife;
use App\Models\Postgres\Space;
use App\Models\Postgres\SpaceAccountLifeWork;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\RoomRepositoryInterface;
use \App\Models\Postgres\Room;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use Illuminate\Support\Str;
use Exception;
use App\Elibs\eResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\RoomLocations;
use App\Models\Postgres\SpaceAccount;
use App\Models\Postgres\RoomAccount;
use App\Models\Postgres\Notification;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;
use App\Models\Postgres\Application;
use App\Models\Postgres\QuickBar;

class RoomRepository extends NotificationRepository implements RoomRepositoryInterface
{
    protected $spaceRepository;

    public function __construct(SpaceRepositoryInterface $spaceRepository)
    {
        $this->spaceRepository = $spaceRepository;
    }

    public function getBlankModel()
    {
        return new Room();
    }

    public function getBlankModelPosition()
    {
        return new RoomLocations();
    }

    public function getBlankModelAccount()
    {
        return new SpaceAccount();
    }

    public function getBlankModelAccountWorkLife()
    {
        return new SpaceAccountLifeWork();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function accountLeaveRoom($id, $accountInfo)
    {
        $room = Room::where('id', $id)->first();
        if (!empty($room)) {
            // $space = Space::where('id', $room->space_id)->first();
            // if (!empty($space)) {
            // if ($space->type == Space::TYPE_SPACE_INTERNAL) {
            RoomAccount::where('room_id', $id)->where('account_id', $accountInfo['id'])->delete();
            // } else {
            //     RoomAccountsWorkLife::where('room_id', $id)->where('account_id', $accountInfo['id'])->delete();
            // }
            return true;
            // }
        }
        return false;
        // eFunction::hdelRedis('life_room_account_' . $id, $accountInfo['id']);
    }

    public function getOneArrayRoomByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getOneObjectRoomByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    public function listAllRoom($requestData, $accountInfo)
    {
        try {
            $spaceId = $requestData['spaceId'];
            $room = $this->getBlankModel()->select('id', 'name', 'slug', 'type', 'is_pin', 'position')->where('space_id', $spaceId)->where('organization_id', $accountInfo['organization_id'])->get();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $room);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function totalRoom()
    {
        $data = $this->getBlankModel()->count();
        return $data;
    }

    public function listRoomInSpacePaginate($requestData, $accountInfo)
    {
        try {
            // $typeSpace = Space::where('id', $requestData['spaceId'])->first();
            $spaceId = $requestData['spaceId'];
            // if ($typeSpace->type != 2) {
            $room = RoomAccount::where('room_accounts.space_id', $spaceId)
                ->where('room_accounts.organization_id', $accountInfo['organization_id'])
                ->where('room_accounts.account_id', $accountInfo['id'])
                ->where('rooms.deleted_at', NULL)
                ->join('rooms', 'room_accounts.room_id', '=', 'rooms.id')
                ->leftJoin('room_locations', function ($join) {
                    $join->on('rooms.id', '=', 'room_locations.room_id')->on('room_accounts.account_id', '=', 'room_locations.account_id');
                })
                ->select(
                    'rooms.id',
                    'rooms.name',
                    'rooms.slug',
                    'rooms.type',
                    'rooms.is_pin',
                    'room_locations.position',
                    'rooms.created_by',
                    'room_accounts.account_id as account_id'
                )
                ->orderBy('rooms.is_pin', 'desc')
                ->orderBy('room_locations.position', 'asc')
                // ->orderBy('rooms.created_at', 'desc')
                ->get();
            $room = $room->map(function ($val) use ($accountInfo) {
                if ($val->created_by == $accountInfo['id']) {
                    $val->is_admin_room = RoomAccount::IS_ADMIN;
                } else {
                    $isAdmin = RoomAccount::where('account_id', $val->account_id)->where('room_id', $val->id)->first();
                    $val->is_admin_room = $isAdmin->is_admin;
                }
                return $val;
            });
            // } else {
            //     $room = RoomAccountsWorkLife::where('room_accounts_work_life.space_id', $spaceId)
            //         ->where('room_accounts_work_life.organization_id', $accountInfo['organization_id'])
            //         ->where('room_accounts_work_life.account_id', $accountInfo['id'])
            //         ->where('rooms.deleted_at', NULL)
            //         ->join('rooms', 'room_accounts_work_life.room_id', '=', 'rooms.id')
            //         ->leftJoin('room_locations', function ($join) {
            //             $join->on('rooms.id', '=', 'room_locations.room_id')->on('room_accounts_work_life.account_id', '=', 'room_locations.account_id');
            //         })
            //         ->select(
            //             'rooms.id',
            //             'rooms.name',
            //             'rooms.slug',
            //             'rooms.type',
            //             'rooms.is_pin',
            //             'room_locations.position',
            //             'rooms.created_by',
            //             'room_accounts_work_life.account_id as account_id'
            //         )
            //         ->orderBy('rooms.is_pin', 'desc')
            //         ->orderBy('room_locations.position', 'asc')
            //         // ->orderBy('rooms.created_at', 'desc')
            //         ->get();
            //     $room = $room->map(function ($val) use ($accountInfo) {
            //         if ($val->created_by == $accountInfo['id']) {
            //             $val->is_admin_room = RoomAccount::IS_ADMIN;
            //         } else {
            //             $isAdmin = RoomAccountsWorkLife::where('account_id', $val->account_id)->where('room_id', $val->id)->first();
            //             $val->is_admin_room = $isAdmin->is_admin;
            //         }
            //         return $val;
            //     });
            // }

            // old
            // $room = $this->getBlankModel()
            //     ->join('room_accounts', 'rooms.id', '=', 'room_accounts.room_id')
            //     ->where('room_accounts.account_id', $accountInfo['id'])
            //     ->with(['createdBy' => function ($q) {
            //         $q->select('id', 'name', 'email', 'is_root');
            //     }])
            //     ->leftJoin('room_locations', 'rooms.id', '=', 'room_locations.room_id')
            //     ->select('rooms.id', 'rooms.name', 'rooms.slug', 'rooms.type', 'rooms.is_pin', 'room_locations.position', 'rooms.created_by')
            //     ->where('rooms.space_id', $spaceId)
            //     ->where('rooms.organization_id', $accountInfo['organization_id'])
            //     ->orderBy('rooms.is_pin', 'desc')->orderBy('room_locations.position', 'asc')
            //     ->orderBy('rooms.created_at', 'desc')->get();
            return $room;
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function createRoom($requestData, $accountInfo)
    {
        try {
            $requestData['accountIds'] = isset($requestData['accountIds']) ? $requestData['accountIds'] : [];
            $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $requestData['accountIds'] = $accountDonesPro;
            DB::beginTransaction();
            $room = $this->getBlankModel();
            $room->name = $requestData['roomName'];
            $room->slug = Str::slug($requestData['roomName']);
            $room->type = $requestData['type'];
            $room->is_pin = $requestData['isPin'];
            $room->space_id = $requestData['spaceId'];
            $room->position = $requestData['position'] ?? null;
            $room->organization_id = $accountInfo['organization_id'];
            $room->created_by = $accountInfo['id'];
            $room->save();
            $room->account()->syncWithOutDetaching($accountInfo['id']);
            $room->account()->updateExistingPivot(
                $accountInfo['id'],
                [
                    'space_id' => $requestData['spaceId'],
                    'organization_id' => $accountInfo['organization_id'],
                    'is_admin' => RoomAccount::IS_ADMIN,
                ]
            );
            if ($requestData['type'] == Room::TYPE_ROOM_PUBLIC) {
                $account = SpaceAccount::join('accounts', 'accounts.id', '=', 'space_accounts.account_id')
                    ->where('space_accounts.space_id', $requestData['spaceId'])
                    ->where('accounts.type_account', Account::TYPE_ACCOUNT_STAFF)
                    ->get()->pluck('account_id')->unique()->values()->toArray();
                $id = $account;
                // foreach ($account as $value) {
                //     $id[] = $value->account_id;
                // }
                if (!empty($id)) {
                    $room->account()->syncWithOutDetaching($id);
                    $room->account()->updateExistingPivot(
                        $id,
                        [
                            'space_id' => $requestData['spaceId'],
                            'organization_id' => $accountInfo['organization_id'],
                        ]
                    );
                    $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $id);
                }
            } else {
                if (!empty($requestData['accountIds'])) {
                    $room->account()->syncWithOutDetaching($requestData['accountIds']);
                    $room->account()->updateExistingPivot(
                        $requestData['accountIds'],
                        [
                            'space_id' => $requestData['spaceId'],
                            'organization_id' => $accountInfo['organization_id'],
                        ]
                    );
                    $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $requestData['accountIds']);
                }
            }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), ['room_id' => $room->id]);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function createRoomWorkLife($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $room = $this->getBlankModel();
            $room->name = $requestData['roomName'];
            $room->slug = Str::slug($requestData['roomName']);
            $room->type = $requestData['type'];
            $room->is_pin = $requestData['isPin'];
            $room->space_id = $requestData['spaceId'];
            $room->position = $requestData['position'] ?? null;
            $room->organization_id = $accountInfo['organization_id'];
            $room->created_by = $accountInfo['id'];
            $room->save();
            if (isset($requestData['accountIds'])) {
                $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
                $accountDones = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES);
            }
            // thêm account đang đăng nhập
            $saveObj[] = [
                'account_id' => $accountInfo['id'],
                'room_id' => $room->id,
                'space_id' => $requestData['spaceId'],
                'organization_id' => $accountInfo['organization_id'] ?? '',
                'is_admin' => RoomAccount::IS_ADMIN,
            ];
            // thêm account đang đăng nhập vào RoomLocations
            $saveObjToRoom[] = [
                'account_id' => $accountInfo['id'],
                'room_id' => $room->id,
                'space_id' => $requestData['spaceId'],
                'organization_id' => $accountInfo['organization_id'] ?? '',
                'position' => 0,
            ];
            RoomAccountsWorkLife::insert($saveObj);
            RoomLocations::insert($saveObjToRoom);
            //thêm các account được add vào
            if ($requestData['type'] == Room::TYPE_ROOM_PUBLIC) {
                $lsAccount = [];
                $account = SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])->whereNotNull('account_life_id')->pluck('account_life_id')->toArray();
                foreach ($account as $key) {
                    $lsAccount[] = [
                        "id" => $key,
                        "type" => Account::DONES,
                    ];
                }
                $accountDones = SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])->where('account_id', '<>', $accountInfo['id'])->whereNotNull('account_id')->pluck('account_id')->toArray();
                foreach ($accountDones as $key) {
                    $lsAccount[] = [
                        "id" => $key,
                        "type" => Account::DONES_PRO,
                    ];
                }
                $id = $account;
                if (!empty($lsAccount)) {
                    $this->addRoomAccountsWorkLifeAndRoomAccountLocationWorkLife($lsAccount, $accountInfo, $room->id, $requestData['spaceId']);
                    //                    $this->addAccountLifeToRoomAccountLocationsWorkLife($lsAccount, $accountInfo, $room->id,  $requestData['spaceId']);
                    if (isset($accountDonesPro)) {
                        $requestData['accountIds'] = $accountDonesPro;
                        $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $id);
                    }
                    if ($accountDones) {
                        $requestData['accountIds'] = $accountDones;
                        $this->pushNotiAccountRoomWorkLife($accountInfo, $requestData, $room, $id);
                    }
                }
            }
            if ($requestData['type'] == Room::TYPE_ROOM_PRIVATE) {
                if (!empty($requestData['accountIds'])) {
                    $this->addRoomAccountsWorkLifeAndRoomAccountLocationWorkLife($requestData['accountIds'], $accountInfo, $room->id, $requestData);
                    if (isset($accountDonesPro)) {
                        $requestData['accountIds'] = $accountDonesPro;
                        $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $requestData['accountIds']);
                    }
                    if (isset($accountDones)) {
                        $requestData['accountIds'] = $accountDones;
                        $this->pushNotiAccountRoomWorkLife($accountInfo, $requestData, $room, $requestData['accountIds']);
                    }
                }
            }

            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'), ['room_id' => $room->id]);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    function addRoomAccountsWorkLifeAndRoomAccountLocationWorkLife($lsAccounts, $accountInfo, $idRoom, $requestData)
    {
        $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro(isset($requestData['accountIds']) ? $requestData['accountIds'] : $lsAccounts, Account::DONES_PRO);
        $accountDones = $this->spaceRepository->filterAccountForDonesOrDonesPro(isset($requestData['accountIds']) ? $requestData['accountIds'] : $lsAccounts, Account::DONES);
        $saveObj = [];
        $saveObjRoomAccountLocation = [];
        if ($accountDonesPro) {
            foreach ($accountDonesPro as $value) {
                if (!is_null($value)) {
                    $saveObj[] = [
                        'account_id' => (int)$value,
                        'account_life_id' => null,
                        'room_id' => $idRoom,
                        'space_id' => isset($requestData['spaceId']) ? $requestData['spaceId'] : $requestData,
                        'organization_id' => $accountInfo['organization_id'] ?? '',
                    ];
                    $saveObjRoomAccountLocation[] = [
                        'account_id' => (int)$value,
                        'account_life_id' => null,
                        'room_id' => $idRoom,
                        'position' => 0,
                        'space_id' => isset($requestData['spaceId']) ? $requestData['spaceId'] : $requestData,
                        'organization_id' => $accountInfo['organization_id'] ?? '',
                    ];
                }
            }
        }
        if ($accountDones) {
            foreach ($accountDones as $value) {
                if (!is_null($value)) {
                    $saveObj[] = [
                        'account_life_id' => (int)$value,
                        'account_id' => null,
                        'room_id' => $idRoom,
                        'space_id' => isset($requestData['spaceId']) ? $requestData['spaceId'] : $requestData,
                        'organization_id' => $accountInfo['organization_id'] ?? '',
                    ];
                    $saveObjRoomAccountLocation[] = [
                        'account_life_id' => (int)$value,
                        'account_id' => null,
                        'room_id' => $idRoom,
                        'position' => 0,
                        'space_id' => isset($requestData['spaceId']) ? $requestData['spaceId'] : $requestData,
                        'organization_id' => $accountInfo['organization_id'] ?? '',
                    ];
                }
            }
        }
        RoomAccountsWorkLife::insert($saveObj);
        RoomLocations::insert($saveObjRoomAccountLocation);
    }

    function addAccountLifeToRoomAccountLocationsWorkLife($lsAccounts, $accountInfo, $idRoom, $requestData)
    {
        $saveObj = [];
        foreach ($lsAccounts as $value) {
            if (!is_null($value)) {
                $saveObj[] = [
                    'account_life_id' => (int)$value,
                    'room_id' => $idRoom,
                    'position' => 0,
                    'space_id' => (int)$requestData['spaceId'],
                    'organization_id' => $accountInfo['organization_id'] ?? '',
                ];
            }
        }
        RoomLocations::insert($saveObj);
    }

    public function updateRoom($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $room = $this->getBlankModel()->find($requestData['roomId']);
            if ($accountInfo['id'] == $room->created_by) {
                $oldRoomName = $room->name;
                $room->name = $requestData['roomName'];
                $room->slug = Str::slug($requestData['roomName']);
                $room->type = $requestData['type'];
                $room->is_pin = $requestData['isPin'];
                $room->space_id = $requestData['spaceId'];
                $room->position = $requestData['position'] ?? null;
                $room->organization_id = $accountInfo['organization_id'];
                $room->save();
            } else {
                return eResponse::response(STATUS_API_FALSE, __('notification.system.not-have-access'));
            }

            // Lấy danh sách thành viên đã có trong room
            $accounts = RoomAccount::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
            $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

            if ($requestData['type'] == Room::TYPE_ROOM_PUBLIC) {
                $account = SpaceAccount::join('accounts', 'accounts.id', '=', 'space_accounts.account_id')
                    ->where('space_accounts.space_id', $requestData['spaceId'])
                    ->where('accounts.type_account', Account::TYPE_ACCOUNT_STAFF)
                    ->get()->get()->pluck('account_id')->unique()->values()->toArray();
                $accountExist = RoomAccount::where('room_id', $requestData['roomId'])->get()->pluck('account_id')->unique()->values()->toArray();
                $id = $account;
                // foreach ($account as $value) {
                //     $id[] = $value->account_id;
                // }
                if (!empty($id)) {
                    $room->account()->syncWithoutDetaching($id);
                    $room->account()->updateExistingPivot(
                        $id,
                        [
                            'space_id' => $requestData['spaceId'],
                            'organization_id' => $accountInfo['organization_id'],
                        ]
                    );
                    // Gửi thông báo
                    $id = array_diff($id, $accountExist);
                    $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $id);
                }
            } else {
                if (!empty($requestData['accountIds'])) {
                    $room->account()->syncWithoutDetaching($requestData['accountIds']);
                    $room->account()->updateExistingPivot(
                        $requestData['accountIds'],
                        [
                            'space_id' => $requestData['spaceId'],
                            'organization_id' => $accountInfo['organization_id'],
                        ]
                    );
                    // Gửi thông báo realtime gọi đến socket
                    $this->pushNotiAccountRoom($accountInfo, $requestData, $room, $requestData['accountIds']);
                }
            }

            DB::commit();

            $this->pushNotiUpdateRoom($accountInfo, $room, $oldRoomName, $listIds);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function sort($requestData)
    {
        try {
            DB::beginTransaction();
            $accountInfo = $requestData['accountInfo'];
            $this->getBlankModelPosition()->where('space_id', $requestData['spaceId'])->where('account_id', $accountInfo['id'])->where('organization_id', $accountInfo['organization_id'])->delete();
            $data = [];
            foreach ($requestData['sortRoom'] as $value) {
                $data[] = [
                    'space_id' => $requestData['spaceId'],
                    'account_id' => $accountInfo['id'],
                    'room_id' => $value['room_id'],
                    'position' => $value['position'],
                    'organization_id' => $accountInfo['organization_id'],
                ];
            }
            $this->getBlankModelPosition()->insert($data);

            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function deleteRoom($id, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $room = $this->getBlankModel()->find($id);

            $this->pushNotiDeleteRoom($accountInfo, $room);
            if ($accountInfo['is_root'] == Account::IS_ROOT || $room->created_by == $accountInfo['id']) {
                RoomAccount::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id'])->delete();
                // lay id app
                $app = Application::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id']);
                $idsApp = $app->get()->pluck('id')->unique()->values()->toArray();

                if (!empty($idsApp)) {
                    QuickBar::whereIn('application_id', $idsApp)->delete();
                }
                $app->delete();
                $room->delete();
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteRoomWorkLife($id, $accountInfo, $lsAccountLife)
    {
        try {
            DB::beginTransaction();
            $room = $this->getBlankModel()->find($id);
            $this->pushNotiDeleteRoomWorkLife($accountInfo, $room);
            if ($accountInfo['is_root'] == Account::IS_ROOT || $room->created_by == $accountInfo['id']) {
                RoomAccountsWorkLife::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id'])->delete();
                RoomLocations::where('room_id', $room->id)
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->whereIn('account_life_id', $lsAccountLife)
                    ->delete();
                RoomLocations::where('room_id', $room->id)
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->where('account_id', $accountInfo['id'])
                    ->delete();
                // lay id app
                $app = Application::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id']);
                $idsApp = $app->get()->pluck('id')->unique()->values()->toArray();

                if (!empty($idsApp)) {
                    QuickBar::whereIn('application_id', $idsApp)->delete();
                }
                $app->delete();
                $room->delete();
                DB::commit();
                return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
            }
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        } catch (Exception $ex) {
            DB::rollBack();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function getRoomTypeInSpace($id)
    {
        $room = $this->getBlankModel()->where('id', $id)->first();
        if (empty($room)) {
            return false;
        } else {
            $type = $room['space_id'];
            $typeSpace = Space::where('id', $type)->first();
            if (empty($typeSpace)) {
                return false;
            } else {
                if ($typeSpace->type == Space::TYPE_SPACE_CUSTOMER) {
                    return [
                        'type' => Space::TYPE_SPACE_CUSTOMER,
                        'space_id' => $type
                    ];
                } else {
                    return [
                        'type' => Space::TYPE_SPACE_INTERNAL,
                        'space_id' => $type
                    ];
                }
            }
        }
    }

    public function listAccountInRoom($idRoom, $search)
    {
        try {
            DB::beginTransaction();
            $query = $this->getBlankModel()->with(['account' => function ($q) use ($search) {
                $q = $q->select('accounts.id AS account_id', 'name', 'email', 'phone_number', 'profile_image_id', 'is_active', 'is_root');
                if (!empty($search)) {
                    $q = $q->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                }
                $q = $q->with('managers')->with(['profileImage' => function ($q) {
                    $q = $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
                }])->where('accounts.is_active', Account::IS_ACTIVE);
            }])->find($idRoom);
            if (!empty($query->account)) {
                $query->account->map(function ($val) {
                    if ($val['account_id']) {
                        $val['id'] = $val['account_id'];
                    }
                    unset($val['pivot']);
                    if (!empty($val->profileImage)) {
                        $val->profileImage['source_thumb'] = !empty($val->profileImage['source_thumb']) ? asset($val->profileImage['source_thumb']) : '';
                        $val->profileImage['source'] = !empty($val->profileImage['source']) && file_exists(public_path() . '/' . $val->profileImage['source']) ? asset($val->profileImage['source']) : '';
                    }
                    return $val;
                });
            }
            DB::commit();
            $result = $query->toArray();
            return $result['account'];
            // return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function updateAccountRoomPublic($room, $spaceId, $type, $accountInfo, $accountIds)
    {
        if ($type == Room::TYPE_ROOM_PUBLIC) {
            $query = $this->getBlankModelAccount()->join('accounts', 'accounts.id', '=', 'space_accounts.account_id')
                ->where('space_accounts.space_id', $spaceId)
                ->where('accounts.type_account', Account::TYPE_ACCOUNT_STAFF);
            $accounts = $query->get()->toArray();
            $id = collect($accounts)->pluck('account_id')->filter()->unique()->values()->toArray();
            $accountExist = RoomAccount::where('room_id', $room->id)->get()->pluck('account_id')->unique()->values()->toArray();
            if (!empty($id)) {
                $room->account()->syncWithoutDetaching($id);
                $room->account()->updateExistingPivot(
                    $id,
                    [
                        'space_id' => $spaceId,
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
                // Gửi thông báo realtime gọi đến socket
                $id = array_diff($id, $accountExist);
                $this->pushNotiAccountRoom($accountInfo, [], $room, $id);
            }
        } else {
            if (!empty($accountIds)) {
                $room->account()->syncWithoutDetaching($accountIds);
                $room->account()->updateExistingPivot(
                    $accountIds,
                    [
                        'space_id' => $spaceId,
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
                // Gửi thông báo realtime gọi đến socket
                $this->pushNotiAccountRoom($accountInfo, [], $room, $accountIds);
            }
        }
    }

    public function updateAccountRoomPublicWorkLife($room, $spaceId, $type, $accountInfo, $accountIds)
    {
        if ($type == Room::TYPE_ROOM_PUBLIC) {
            $query = $this->getBlankModelAccountWorkLife()->where('space_id', $spaceId);
            $accounts = $query->get()->toArray();
            $idAccountPro = collect($accounts)->pluck('account_id')->filter()->unique()->values()->toArray();
            $idAccountLife = collect($accounts)->pluck('account_life_id')->filter()->unique()->values()->toArray();
            $accountExistPro = RoomAccountsWorkLife::where('room_id', $room->id)->get()->pluck('account_id')->unique()->values()->toArray();
            $accountExistLife = RoomAccountsWorkLife::where('room_id', $room->id)->get()->pluck('account_life_id')->unique()->values()->toArray();
            if (!empty($idAccountPro)) {
                $room->accountLifeInAccountId()->syncWithoutDetaching($idAccountPro);
                $room->accountLifeInAccountId()->updateExistingPivot(
                    $idAccountPro,
                    [
                        'space_id' => $spaceId,
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
                // Gửi thông báo realtime gọi đến socket
                $id = array_diff($idAccountPro, $accountExistPro);
                $this->pushNotiAccountRoom($accountInfo, [], $room, $id);
            }
            if (!empty($idAccountLife)) {
                $room->accountLife()->syncWithoutDetaching($idAccountLife);
                $room->accountLife()->updateExistingPivot(
                    $idAccountLife,
                    [
                        'space_id' => $spaceId,
                    ]
                );
                // Gửi thông báo realtime gọi đến socket
                $id = array_diff($idAccountLife, $accountExistLife);
                $this->pushNotiAccountRoomWorkLife($accountInfo, [], $room, $id);
            }
        } else {
            if (!empty($accountIds)) {
                $room->account()->syncWithoutDetaching($accountIds);
                $room->account()->updateExistingPivot(
                    $accountIds,
                    [
                        'space_id' => $spaceId,
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
                // Gửi thông báo realtime gọi đến socket
                $this->pushNotiAccountRoom($accountInfo, [], $room, $accountIds);
            }
        }
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('rooms.id', $filter['id']);
            } else {
                $query = $query->where('rooms.id', $filter['id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('rooms.space_id', $filter['space_id']);
            } else {
                $query = $query->where('rooms.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('rooms.deleted_at', null);
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('rooms.is_active', $filter['is_active']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('rooms.organization_id', $filter['organization_id']);
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('applications.created_by', $filter['created_by']);
        }
    }

    public function addAccount($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $room = $this->getBlankModel()->find($requestData['roomId']);
            $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            // $accountDones = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES);
            if (!empty($room)) {
                // $space = Space::find($room->space_id);
                // if ($space->type == Space::TYPE_SPACE_INTERNAL) {
                $requestData['accountIds'] = $accountDonesPro;
                $room->account()->syncWithOutDetaching($requestData['accountIds']);
                $room->account()->updateExistingPivot(
                    $requestData['accountIds'],
                    [
                        'space_id' => $requestData['spaceId'],
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
                $this->pushNotiAccountRoom($accountInfo, [], $room, $requestData['accountIds']);
                // } else {
                //     if ($accountDonesPro) {
                //         $requestData['accountIds'] = $accountDonesPro;
                //         $room->accountLifeInAccountId()->syncWithOutDetaching($requestData['accountIds']);
                //         $room->accountLifeInAccountId()->updateExistingPivot(
                //             $requestData['accountIds'],
                //             [
                //                 'space_id' => $requestData['spaceId'],
                //                 'organization_id' => $accountInfo['organization_id'],
                //             ]
                //         );
                //         $this->pushNotiAccountRoom($accountInfo, [], $room, $requestData['accountIds']);
                //     }
                //     if ($accountDones) {
                //         $requestData['accountIds'] = $accountDones;
                //         $room->accountLife()->syncWithOutDetaching($requestData['accountIds']);
                //         $room->accountLife()->updateExistingPivot(
                //             $requestData['accountIds'],
                //             [
                //                 'space_id' => $requestData['spaceId'],
                //                 'organization_id' => $accountInfo['organization_id'],
                //             ]
                //         );
                //         $this->pushNotiAccountRoomWorkLife($accountInfo, [], $room, $requestData['accountIds']);
                //     }
                //     // Bắn socket
                // }
            }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function deleteAccountInRoom($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $requestData['accountIds'] = $accountDonesPro;
            RoomAccount::whereIn('account_id', $requestData['accountIds'])
                ->where('room_id', $requestData['roomId'])
                ->where('organization_id', $accountInfo['organization_id'])
                ->delete();
            DB::commit();
            $this->pushNotiDeleteAccountInRoom($requestData, $accountInfo);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteAccountInRoomWorkLife($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $accountDonesPro = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $accountDones = $this->spaceRepository->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES);
            if ($accountDones) {
                $requestData['accountIds'] = $accountDones;
                RoomAccountsWorkLife::whereIn('account_life_id', $requestData['accountIds'])
                    ->where('room_id', $requestData['roomId'])
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->delete();
                $this->pushNotiDeleteAccountInRoomWorkLife($requestData, $accountInfo);
            }
            if ($accountDonesPro) {
                $requestData['accountIds'] = $accountDonesPro;
                RoomAccountsWorkLife::whereIn('account_id', $requestData['accountIds'])
                    ->where('room_id', $requestData['roomId'])
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->delete();
                $this->pushNotiDeleteAccountInRoom($requestData, $accountInfo);
            }
            DB::commit();

            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function paginateAccountInRoom($requestData, $accountInfo)
    {
        try {
            $limit = isset($requestData["limit"]) && ctype_digit($requestData["limit"]) ? (int)$requestData["limit"] : 10;
            $keyWord = $requestData["keyWord"] ?? null;
            // $space = Space::find($requestData['spaceId']);
            // if ($space->type == Space::TYPE_SPACE_INTERNAL) {
            $query = RoomAccount::where('room_accounts.room_id', $requestData['roomId'])
                ->where('room_accounts.space_id', $requestData['spaceId'])
                ->where('room_accounts.organization_id', $accountInfo['organization_id'])
                ->join('accounts', 'room_accounts.account_id', '=', 'accounts.id')
                ->leftJoin('images', 'accounts.profile_image_id', '=', 'images.id')
                ->leftJoin('decentralizations', 'accounts.decentralization_id', '=', 'decentralizations.id');

            if ($keyWord) {
                $query = $query->where(function ($q) use ($keyWord) {
                    $q->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')->orWhere('accounts.email', 'like', '%' . $keyWord . '%');
                });
            }

            $query = $query->select(
                'accounts.id',
                'accounts.name',
                'accounts.username',
                'accounts.is_root',
                'accounts.is_active',
                'images.source',
                'decentralizations.name as decentralization_name',
                'room_accounts.is_admin as is_admin',
            )
                ->paginate($limit)->toArray();
            $query['data'] = array_map(function ($val) {
                $val['type'] = Account::DONES_PRO;
                $val['source'] = !empty($val['source']) && file_exists(public_path() . '/' . $val['source']) ? asset($val['source']) : '';
                $val['avatar'] = $val['source'];
                return $val;
            }, $query['data']);
            // } else {
            //     $accountRoom = RoomAccountsWorkLife::where('room_accounts_work_life.room_id', $requestData['roomId'])
            //         ->where('room_accounts_work_life.space_id', $requestData['spaceId'])
            //         ->where('room_accounts_work_life.organization_id', $accountInfo['organization_id'])
            //         ->join('accounts', 'room_accounts_work_life.account_id', '=', 'accounts.id')
            //         ->leftJoin('images', 'accounts.profile_image_id', '=', 'images.id');
            //     // ->leftJoin('decentralizations', 'accounts.decentralization_id', '=', 'decentralizations.id');

            //     if ($keyWord) {
            //         $accountRoom = $accountRoom->where(function ($q) use ($keyWord) {
            //             $q->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')->orWhere('accounts.email', 'like', '%' . $keyWord . '%');
            //         });
            //     }

            //     $accountRoom = $accountRoom->select(
            //         'accounts.id',
            //         'accounts.name',
            //         'accounts.username',
            //         'accounts.is_root',
            //         'accounts.is_active',
            //         'images.source',
            //         // 'decentralizations.name as decentralization_name',
            //         'room_accounts_work_life.is_admin as is_admin',
            //     )->get()->toArray();

            //     $accountRoom = array_map(function ($val) {
            //         $val['type'] = Account::DONES_PRO;
            //         $val['source'] = !empty($val['source']) && file_exists(public_path() . '/' . $val['source']) ? asset($val['source']) : '';
            //         $val['avatar'] = $val['source'];
            //         return $val;
            //     }, $accountRoom);

            //     $accountWorkLife = RoomAccountsWorkLife::where('room_accounts_work_life.room_id', $requestData['roomId'])
            //         ->where('room_accounts_work_life.space_id', $requestData['spaceId'])
            //         ->where('room_accounts_work_life.organization_id', $accountInfo['organization_id'])
            //         ->join('life_accounts', 'room_accounts_work_life.account_life_id', '=', 'life_accounts.id')
            //         ->leftJoin('life_images', 'life_accounts.profile_image_id', '=', 'life_images.id');
            //     // ->leftJoin('decentralizations', 'accounts.decentralization_id', '=', 'decentralizations.id');

            //     if ($keyWord) {
            //         $accountWorkLife = $accountWorkLife->where(function ($q) use ($keyWord) {
            //             $q->orWhere(DB::raw('vn_unaccent(life_accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')->orWhere('life_accounts.email', 'like', '%' . $keyWord . '%');
            //         });
            //     }

            //     $accountWorkLife = $accountWorkLife->select(
            //         'life_accounts.id',
            //         'life_accounts.name',
            //         'life_accounts.username',
            //         // 'life_accounts.is_root',
            //         'life_accounts.is_active',
            //         'life_images.source',
            //         // 'decentralizations.name as decentralization_name',
            //         'room_accounts_work_life.is_admin as is_admin',
            //     )
            //         ->paginate($limit)->toArray();

            //     $accountWorkLife['data'] = array_map(function ($val) {
            //         $val['type'] = Account::DONES;
            //         $val['source'] = !empty($val['source']) && file_exists(public_path() . '/' . $val['source']) ? asset($val['source']) : '';
            //         $val['avatar'] = $val['source'];
            //         return $val;
            //     }, $accountWorkLife['data']);

            //     $query['current_page'] = $accountWorkLife['current_page'];
            //     $query['data'] = [];
            //     if ($accountWorkLife['current_page'] == 1) {
            //         foreach ($accountRoom as $value) {
            //             array_push($query['data'], $value);
            //         }
            //         foreach ($accountWorkLife['data'] as $value) {
            //             array_push($query['data'], $value);
            //         }
            //     } else {
            //         array_push($query['data'], $accountWorkLife['data']);
            //     }
            //     $query['total'] = $accountWorkLife['total'] + count($accountRoom);
            // }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
        } catch (\Exception $ex) {
            \Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function updateAdminInRoom($request, $accountInfo)
    {
        $adminRoom = RoomAccount::where('account_id', $request['account_id'])->where('room_id', $request['room_id'])->update(['is_admin' => $request['is_admin']]);
        if (!empty($adminRoom)) {
            $this->pushNotiAdminRoom($request, $accountInfo);
            return true;
        }

        return false;
    }

    public function updateAdminInRoomWorkLife($request, $accountInfo)
    {
        $adminRoom = RoomAccountsWorkLife::where('account_id', $request['account_id'])->where('room_id', $request['room_id'])->update(['is_admin' => $request['is_admin']]);
        if (!empty($adminRoom)) {
            $this->pushNotiAdminRoom($request, $accountInfo);
            return true;
        }

        return false;
    }

    public function checkAdminRoom($accountId, $filter)
    {
        $isAdmin = RoomAccount::where('room_id', $filter['id'])
            ->where('account_id', (int)$accountId)
            ->where('is_admin', RoomAccount::IS_ADMIN)->first();
        if (!empty($isAdmin)) {
            return $isAdmin;
        }
        return false;
    }

    public function checkAdminRoomWorkLife($accountId, $filter)
    {
        $isAdmin = RoomAccountsWorkLife::where('room_id', $filter['id'])
            ->where('account_id', (int)$accountId)
            ->where('is_admin', RoomAccount::IS_ADMIN)->first();
        if (!empty($isAdmin)) {
            return $isAdmin;
        }
        return false;
    }
}
