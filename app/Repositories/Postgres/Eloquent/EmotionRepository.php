<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\EmotionRepositoryInterface;
use \App\Models\Postgres\Emotion;

class EmotionRepository extends SingleKeyModelRepository implements EmotionRepositoryInterface
{

    public function getBlankModel()
    {
        return new Emotion();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function insertMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insert = $this->getBlankModel()->insert($params);
            if($insert){
                return true;
            }
        }
        return false;
    }

    public function getAllEmotionByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->get()->toArray();

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('emotions.id', $filter['id']);
            } else {
                $query = $query->where('emotions.id', $filter['id']);
            }
        }

    }
}
