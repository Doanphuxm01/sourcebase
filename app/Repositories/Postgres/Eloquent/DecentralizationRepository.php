<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\DecentralizationRepositoryInterface;
use \App\Models\Postgres\Decentralization;
use App\Elibs\eFunction;

class DecentralizationRepository extends SingleKeyModelRepository implements DecentralizationRepositoryInterface
{

    public function getBlankModel()
    {
        return new Decentralization();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function getListDecentralizationByFilter($limit, $filter)
    {
        $query = $this->withCreatedBy();
        $this->filter($filter, $query);

        $data = $query->orderBy('decentralizations.created_at', 'desc')->paginate($limit)->toArray();

        return $data;
    }

    public function getOneArrayDecentralizationWithPermissionByFilter($filter)
    {
        $query = $this->withPermission();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getOneObjectDecentralizationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    public function deleteAllDecentralizationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getAllDecentralizationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    private function withCreatedBy()
    {
        $query = $this->getBlankModel()->select([
            "decentralizations.id",
            "decentralizations.name",
            "decentralizations.is_active",
            "decentralizations.organization_id",
            "decentralizations.created_by",
            "decentralizations.created_at",
            "decentralizations.updated_at",
        ])
            ->with(['createdBy' => function ($query) {
                $query->select([
                    "accounts.id",
                    "accounts.name",
                    "accounts.username",
                ]);
            }]);

        return $query;
    }

    private function withPermission()
    {
        $query = $this->getBlankModel()->select([
            "decentralizations.id",
            "decentralizations.name",
            "decentralizations.is_active",
            "decentralizations.organization_id",
            "decentralizations.created_by",
            "decentralizations.created_at",
            "decentralizations.updated_at",
        ])
            ->with(['createdBy' => function ($query) {
                $query->select([
                    "accounts.id",
                    "accounts.name",
                    "accounts.username",
                ]);
            }])
            ->with(['decentralizationPermissions' => function ($query) {
                $query->select([
                    "permissions.id",
                    "permissions.name",
                    "permissions.key",
                    "decentralization_permissions.id as decentralization_permission_id",
                    "decentralization_permissions.create",
                    "decentralization_permissions.read",
                    "decentralization_permissions.update",
                    "decentralization_permissions.delete",
                    "decentralization_permissions.permission_id",
                    "decentralization_permissions.decentralization_id",

                ]);
            }]);

        return $query;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('decentralizations.id', $filter['id']);
            } else {
                $query = $query->where('decentralizations.id', $filter['id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('decentralizations.deleted_at', null);
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('decentralizations.is_active', $filter['is_active']);
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('decentralizations.created_by', $filter['created_by']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('decentralizations.organization_id', $filter['organization_id']);
        }

        if (isset($filter['key_word'])) {
            $query = $query->where('decentralizations.slug', 'like', '%' . eFunction::generateSlug($filter['key_word'], '-') . '%');
        }

        if (isset($filter['start_date']) && isset($filter['end_date'])) {
            $query = $query->whereDate('decentralizations.created_at', '>=', $filter['start_date'])->whereDate('decentralizations.created_at', '<=', $filter['end_date']);
        }
    }
}
