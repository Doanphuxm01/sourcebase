<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\MajorRepositoryInterface;
use \App\Models\Postgres\Major;
use App\Elibs\eFunction;

class MajorRepository extends SingleKeyModelRepository implements MajorRepositoryInterface
{

    public function getBlankModel()
    {
        return new Major();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function insertMulti($params)
    {
        if (!empty($params) && is_array($params)) {
            $insert = $this->getBlankModel()->insert($params);
            if ($insert) {
                return true;
            }
        }
        return false;
    }

    public function getAllMajorByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['key_word'])) {
            $query = $query->where('majors.slug', 'like', '%' . eFunction::generateSlug($filter['key_word'], '-') . '%');
        }

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('majors.id', $filter['id']);
            } else {
                $query = $query->where('majors.id', $filter['id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('majors.deleted_at', null);
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('majors.is_active', $filter['is_active']);
        }
    }
}
