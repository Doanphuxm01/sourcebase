<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\CommentRepositoryInterface;
use \App\Models\Postgres\Comment;

class CommentRepository extends SingleKeyModelRepository implements CommentRepositoryInterface
{

    public function getBlankModel()
    {
        return new Comment();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getAllCommentWithAllByFilter($filter)
    {
        $query = $this->withAll();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function deleteAllCommentByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    private function withAll()
    {
        $query = $this->getBlankModel()->select([
            'id',
            'content',
            'image_id',
            'post_id',
            'parent_id',
            'space_id',
            'created_by',
        ])
            ->with(['createdBy' => function($query){
                $query->select('accounts.id', 'accounts.name', 'accounts.profile_image_id')
                    ->with(['profileImage' => function($query){
                        $query->select('images.id', 'images.source');
                    }]);
            }]);

        return $query;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('comments.id', $filter['id']);
            } else {
                $query = $query->where('comments.id', $filter['id']);
            }
        }

        if (isset($filter['post_id'])) {
            if (is_array($filter['post_id'])) {
                $query = $query->whereIn('comments.post_id', $filter['post_id']);
            } else {
                $query = $query->where('comments.post_id', $filter['post_id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('comments.space_id', $filter['space_id']);
            } else {
                $query = $query->where('comments.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('comments.organization_id', $filter['organization_id']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('comments.deleted_at', null);
        }

    }
}
