<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\RoomAccountRepositoryInterface;
use \App\Models\Postgres\RoomAccount;

class RoomAccountRepository extends SingleKeyModelRepository implements RoomAccountRepositoryInterface
{

    public function getBlankModel()
    {
        return new RoomAccount();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getOneArrayRoomAccountByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('room_accounts.id', $filter['id']);
            } else {
                $query = $query->where('room_accounts.id', $filter['id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('room_accounts.space_id', $filter['space_id']);
            } else {
                $query = $query->where('room_accounts.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['room_id'])) {
            if (is_array($filter['room_id'])) {
                $query = $query->whereIn('room_accounts.room_id', $filter['room_id']);
            } else {
                $query = $query->where('room_accounts.room_id', $filter['room_id']);
            }
        }

        if (isset($filter['account_id'])) {
            if (is_array($filter['account_id'])) {
                $query = $query->whereIn('room_accounts.account_id', $filter['account_id']);
            } else {
                $query = $query->where('room_accounts.account_id', $filter['account_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('room_accounts.organization_id', $filter['organization_id']);
        }
    }

}
