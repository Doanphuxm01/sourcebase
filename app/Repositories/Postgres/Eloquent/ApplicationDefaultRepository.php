<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationDefaultRepositoryInterface;
use \App\Models\Postgres\ApplicationDefault;

class ApplicationDefaultRepository extends SingleKeyModelRepository implements ApplicationDefaultRepositoryInterface
{

    public function getBlankModel()
    {
        return new ApplicationDefault();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

}
