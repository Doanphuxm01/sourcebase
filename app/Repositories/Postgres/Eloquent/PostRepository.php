<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PostRepositoryInterface;
use \App\Models\Postgres\Post;

class PostRepository extends SingleKeyModelRepository implements PostRepositoryInterface
{

    public function getBlankModel()
    {
        return new Post();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function deleteAllPostByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getOneObjectPostByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $dataX = $query->first();

        $data = [];

        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getAllPostWithAllByFilter($limit, $filter)
    {
        $query = $this->withAll();

        $this->filter($filter, $query);
        $data = $query->paginate($limit)->toArray();

        return $data;
    }

    private function withAll()
    {
        $query = $this->getBlankModel()->select([
            'id',
            'content',
            'space_id',
            'created_by',
        ])
            ->with(['createdBy' => function($query){
                $query->select('accounts.id', 'accounts.name', 'accounts.profile_image_id')
                    ->with(['profileImage' => function($query){
                        $query->select('images.id', 'images.source');
                    }]);
            }])
            ->with(['postImage' => function($query){
                $query->select('post_images.id as post_image_id', 'post_images.post_id', 'post_images.image_id', 'images.id', 'images.source');
            }])
            ->withCount(['postComment'])
            ->withCount(['postEmotion']);

        return $query;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('posts.id', $filter['id']);
            } else {
                $query = $query->where('posts.id', $filter['id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('posts.space_id', $filter['space_id']);
            } else {
                $query = $query->where('posts.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('posts.organization_id', $filter['organization_id']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('posts.deleted_at', null);
        }
    }

}
