<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ManagementAccountRepositoryInterface;
use \App\Models\Postgres\ManagementAccount;

class ManagementAccountRepository extends SingleKeyModelRepository implements ManagementAccountRepositoryInterface
{

    public function getBlankModel()
    {
        return new ManagementAccount();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function insertMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insert = $this->getBlankModel()->insert($params);
            if($insert){
                return true;
            }
        }
        return false;
    }

    public function deleteAllManagementAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getAllManagementAccountByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('management_accounts.id', $filter['id']);
            } else {
                $query = $query->where('management_accounts.id', $filter['id']);
            }
        }

        if (isset($filter['account_id'])) {
            if (is_array($filter['account_id'])) {
                $query = $query->whereIn('management_accounts.account_id', $filter['account_id']);
            } else {
                $query = $query->where('management_accounts.account_id', $filter['account_id']);
            }
        }

        if (isset($filter['manager_id'])) {
            if (is_array($filter['manager_id'])) {
                $query = $query->whereIn('management_accounts.manager_id', $filter['manager_id']);
            } else {
                $query = $query->where('management_accounts.manager_id', $filter['manager_id']);
            }
        }

        if (isset($filter['created_by'])) {
            if (is_array($filter['created_by'])) {
                $query = $query->whereIn('management_accounts.created_by', $filter['created_by']);
            } else {
                $query = $query->where('management_accounts.created_by', $filter['created_by']);
            }
        }


        if (isset($filter['organization_id'])) {
            $query = $query->where('management_accounts.organization_id', $filter['organization_id']);
        }

    }
}
