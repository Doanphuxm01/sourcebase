<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PackageRepositoryInterface;
use \App\Models\Postgres\Package;

class PackageRepository extends SingleKeyModelRepository implements PackageRepositoryInterface
{

    public function getBlankModel()
    {
        return new Package();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getOneArrayPackageByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $dataX = $query->first();

        $data = [];

        if (!empty($dataX)){
            $data = $dataX->toArray();
        }

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('packages.id', $filter['id']);
            } else {
                $query = $query->where('packages.id', $filter['id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('packages.deleted_at', null);
        }

        if (isset($filter['is_trial'])) {
            $query = $query->where('packages.is_trial', $filter['is_trial']);
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('packages.is_active', $filter['is_active']);
        }
    }

}
