<?php namespace App\Repositories\Postgres\Eloquent;

use App\Models\Postgres\Account;
use App\Models\Postgres\StatisticAccount;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationRepositoryInterface;
use \App\Models\Postgres\Application;
use App\Models\Postgres\ApplicationAdvancedSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\QuickBar;

class ApplicationRepository extends SingleKeyModelRepository implements ApplicationRepositoryInterface
{
    public function getBlankModel()
    {
        return new Application();
    }

    public function getBlankAppAvanModel()
    {
        return new ApplicationAdvancedSetting();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function getAllApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function getAllApplicationWithImageByFilter($filter)
    {
        $query = $this->withImage();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function statisticApplication($filter)
    {
        $limit = isset($filter['limit']) ? $filter['limit'] : 30;
        $query = $this->withImage()->select('applications.*', DB::raw('COALESCE(report_count_user_by_app.total_accounts, 0) as total_accounts'), DB::raw('COALESCE(report_sum_clicks_by_app.total_clicks, 0) as total_clicks'))->with([
            'space' => function ($query) {
                $query->withTrashed();
            },
            'room' => function ($query) {
                $query->withTrashed();
            }
        ])
            ->join('rooms', 'rooms.id', '=', 'applications.room_id')
            ->join('spaces', 'spaces.id', '=', 'applications.space_id')
            ->where('applications.deleted_at', null)
            ->where('rooms.deleted_at', null)
            ->where('spaces.deleted_at', null)
            ->leftJoin(DB::raw('(select application_id,cast(count(DISTINCT account_id) as INT) as total_accounts from public.statistic_accounts inner join public.accounts on public.statistic_accounts.account_id = public.accounts.id where public.accounts.deleted_at is null group by statistic_accounts.application_id) as report_count_user_by_app'), 'report_count_user_by_app.application_id', '=', 'applications.id')
            ->leftJoin(DB::raw('(select application_id,cast(SUM(clicks) as int) as total_clicks from public.statistic_applications inner join public.spaces on public.statistic_applications.space_id = public.spaces.id inner join public.rooms on public.statistic_applications.room_id = public.rooms.id where public.rooms.deleted_at is null and public.spaces.deleted_at is null group by statistic_applications.application_id) as report_sum_clicks_by_app'), 'report_sum_clicks_by_app.application_id', '=', 'applications.id');

        $totalUser = Account::where('organization_id', $filter['organization_id'])->count();
        //    return $query->toSql();
        $this->filter($filter, $query);
        $data = $query->orderBy('total_clicks', 'desc')->paginate($limit);
        $data = collect(['totalUser' => $totalUser])->merge($data);
        return $data;
    }

    /*
     * $filters = [organization_id,date_start,date-end, application_id]
     * */
    public function statisticApplicationByDay($filters)
    {
        return DB::select('With report_app as( select date_at,cast(SUM(clicks) as INT) as clicks from public.statistic_applications inner join public.applications on public.applications.id = public.statistic_applications.application_id inner join public.spaces on public.spaces.id = public.statistic_applications.space_id inner join public.rooms on public.rooms.id = public.statistic_applications.room_id where public.statistic_applications.date_at>=? and public.statistic_applications.date_at <= ? and public.statistic_applications.organization_id = ? and public.statistic_applications.application_id = ? and public.applications.deleted_at is null and public.spaces.deleted_at is null and public.rooms.deleted_at is null group by public.statistic_applications.date_at) , report_account as( select date_at,COUNT(DISTINCT account_id) as accounts from public.statistic_accounts inner join public.accounts on public.accounts.id = public.statistic_accounts.account_id where public.statistic_accounts.date_at>=? and date_at <= ? and public.statistic_accounts.organization_id= ? and public.statistic_accounts.application_id = ? and public.accounts.deleted_at is null group by public.statistic_accounts.date_at ) select report_app.date_at,COALESCE(report_app.clicks,0) as clicks,COALESCE(report_account.accounts,0) as accounts from report_app left join report_account on report_app.date_at = report_account.date_at order by report_app.date_at', [
            $filters['date_start'],
            $filters['date_end'],
            $filters['organization_id'],
            $filters['application_id'],
            $filters['date_start'],
            $filters['date_end'],
            $filters['organization_id'],
            $filters['application_id']
        ]);
    }
    /*
     * $filters = [date_at, application_id, name, page, limit]
     * */
    public function statisticApplicationByDayDetail($filters)
    {
        $data = StatisticAccount::select('statistic_accounts.*', 'accounts.name', 'accounts.email', 'accounts.username', 'accounts.phone_number')
            ->join('applications', 'applications.id', '=', 'statistic_accounts.application_id')
            ->join('accounts', 'accounts.id', '=', 'statistic_accounts.account_id')
            ->join('rooms', 'rooms.id', '=', 'applications.room_id')
            ->join('spaces', 'spaces.id', '=', 'applications.space_id')
            ->where('application_id', $filters['application_id'])
            ->where('date_at', $filters['date_at'])
            ->when(isset($filters['name']), function ($query) use ($filters) {
                $query->where('accounts.name', 'like', '%' . $filters['name'] . '%');
            })
            ->where('applications.deleted_at', null)
            ->where('rooms.deleted_at', null)
            ->where('spaces.deleted_at', null)
            ->where('accounts.deleted_at', null)
            ->paginate($filters['limit']);
        return $data;
    }
    /*
     * $filters = [date_start, date_end, room_id, space_id , page, limit, organization_id]
     * */
    public function statisticAccountByDay($filters)
    {
        return StatisticAccount::select('statistic_accounts.*', 'applications.room_id', 'applications.space_id')->with(['room', 'space', 'application'])
            ->join('applications', 'applications.id', '=', 'statistic_accounts.application_id')
            ->join('accounts', 'accounts.id', '=', 'statistic_accounts.account_id')
            ->join('rooms', 'rooms.id', '=', 'applications.room_id')
            ->join('spaces', 'spaces.id', '=', 'applications.space_id')
            ->where('statistic_accounts.organization_id', $filters['organization_id'])
            ->when(isset($filters['space_id']), function ($query) use ($filters) {
                $query->where('applications.space_id', $filters['space_id']);
            })->when(isset($filters['room_id']), function ($query) use ($filters) {
                $query->where('applications.room_id', $filters['room_id']);
            })
            ->where('statistic_accounts.date_at', '>=', $filters['date_start'])
            ->where('statistic_accounts.date_at', '<=', $filters['date_end'])
            ->where('rooms.deleted_at', null)
            ->where('spaces.deleted_at', null)
            ->where('accounts.deleted_at', null)
            ->paginate($filters['limit']);
    }

    public function deleteApplicationFromApplicationGroupByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->update(['application_group_id' => null]);

        return $data;
    }

    public function getOneArrayApplicationWithAdvancedByFilter($filter, $accountInfo)
    {
        if (!empty($filter['app_library'])) {
            unset($filter['room_id']);
            unset($filter['space_id']);
            unset($filter['organization_id']);
        }
        $query = $this->withAdvanced();
        $this->filter($filter, $query);

        $dataX = $query->with(['quickBar' => function ($q) use ($accountInfo) {
            $q->select('id', 'application_id', 'enable_all')->where('quick_bars.account_id', $accountInfo['id'])->take(1)->get();
        }])->first();

        // $dataX = $query->join('quick_bars', function($join){
        //     $join->on('applications.id', '=', 'quick_bars.application_id')
        //     ->on('applications.space_id', '=', 'quick_bars.space_id')
        //     ->on('applications.organization_id', '=', 'quick_bars.organization_id');
        // })->where('quick_bars.account_id', $accountInfo['id'])->first();

        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }
        $data['enable_all'] = !empty($data['quick_bar']) ? $data['quick_bar'][0]['enable_all'] : '';

        unset($data['quick_bar']);
        return $data;
    }

    public function getOneArrayApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function getOneObjectApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    public function totalApplication()
    {
        $data = $this->getBlankModel()->count();
        return $data;
    }

    public function deleteApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->delete();

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                QuickBar::whereIn('application_id', $filter['id'])->delete();
            } else {
                QuickBar::where('application_id', $filter['id'])->delete();
            }
        }

        return $data;
    }

    private function withImage()
    {
        $query = $this->getBlankModel()->select([
            "applications.id",
            'applications.name',
            'applications.type as type_application',
            'applications.link_android',
            'applications.link_ios',
            'applications.link_url',
            'applications.date_start',
            'applications.date_end',
            'applications.application_image_id',
            'applications.created_by',
            'applications.account_id',
            'applications.room_id',
            'applications.space_id',
            'applications.is_accept',
            'applications.organization_id',
            'applications.application_group_id',
            'applications.application_store_id',
        ])
            ->with(['applicationStore' => function ($query) {
                $query->with('createdBy', function ($s) {
                    $s->select(
                        'id',
                        'name',
                        'email',
                        'phone_number',
                        'language',
                        'is_root',
                        'birthday',
                        'gender',
                        'is_active',
                        'profile_image_id',
                        'background_image_id',
                        'organization_id',
                        'decentralization_id',
                        'is_first_login',
                        'main_space',
                        'capacity'
                    );
                });
                $query->with('applicationFile');
            }])
            ->with(['applicationImage' => function ($query) {
                $query->select([
                    "images.id",
                    "images.source",
                ]);
            }]);

        return $query;
    }

    private function withAdvanced()
    {
        $query = $this->getBlankModel()->select([
            "applications.id",
            'applications.name',
            'applications.type',
            'applications.link_android',
            'applications.link_ios',
            'applications.link_url',
            'applications.date_start',
            'applications.date_end',
            'applications.application_image_id',
            'applications.created_by',
            'applications.account_id',
            'applications.room_id',
            'applications.space_id',
            'applications.is_accept',
            'applications.organization_id',
            'applications.application_group_id',
            'applications.x_frame',
            'applications.application_store_id',
        ])
            ->with(['applicationStore' => function ($query) {
                $query->with('createdBy');
                $query->with('applicationFile');
            }])
            ->with(['applicationImage' => function ($query) {
                $query->select([
                    "images.id",
                    "images.source",
                ]);
            }])
            ->with(['account' => function ($query) {
                $query->select([
                    "accounts.id",
                    "accounts.name",
                    "accounts.email",
                ]);
            }])
            ->with(['createdBy' => function ($query) {
                $query->select([
                    "accounts.id",
                    "accounts.name",
                    "accounts.email",
                ]);
            }])
            ->with(['applicationGroup' => function ($query) {
                $query->select([
                    "application_groups.id",
                    "application_groups.name",
                ]);
            }])
            ->with(['advancedSetting' => function ($query) {
                $query->select([
                    "application_advanced_settings.id",
                    'application_advanced_settings.application_id',
                    'application_advanced_settings.time_start',
                    'application_advanced_settings.time_end',
                    'application_advanced_settings.monday',
                    'application_advanced_settings.tuesday',
                    'application_advanced_settings.wednesday',
                    'application_advanced_settings.thursday',
                    'application_advanced_settings.friday',
                    'application_advanced_settings.saturday',
                    'application_advanced_settings.sunday',
                    'application_advanced_settings.clicks',
                ]);
            }]);

        return $query;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['name'])) {
            $query = $query->where('applications.name', 'LIKE', '%' . $filter['name'] . '%');
        }


        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('applications.id', $filter['id']);
            } else {
                $query = $query->where('applications.id', $filter['id']);
            }
        }

        if (isset($filter['application_not_in'])) {
            if (is_array($filter['application_not_in'])) {
                $query = $query->whereNotIn('applications.id', $filter['application_not_in']);
            } else {
                $query = $query->where('applications.id', '!=', $filter['application_not_in']);
            }
        }

        if (isset($filter['application_group_id'])) {
            if (is_array($filter['application_group_id'])) {
                $query = $query->whereIn('applications.application_group_id', $filter['application_group_id']);
            } else {
                $query = $query->where('applications.application_group_id', $filter['application_group_id']);
            }
        }

        if (isset($filter['application_group_id_null'])) {
            $query = $query->where('applications.application_group_id', null);
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('applications.space_id', $filter['space_id']);
            } else {
                $query = $query->where('applications.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['room_id'])) {
            if (is_array($filter['room_id'])) {
                $query = $query->whereIn('applications.room_id', $filter['room_id']);
            } else {
                $query = $query->where('applications.room_id', $filter['room_id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('applications.deleted_at', null);
        }

        if (isset($filter['is_accept'])) {
            $query = $query->where('applications.is_accept', $filter['is_accept']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('applications.organization_id', $filter['organization_id']);
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('applications.created_by', $filter['created_by']);
        }

        // Lọc filter ứng dụng trong khoảng time
        $now = now();
        $date = $now->format('Y-m-d');

        $query = $query->where(function ($q) use ($date) {
            $q->whereDate('applications.date_start', '<=', $date)->whereDate('applications.date_end', '>=', $date)
                ->orWhereNull('applications.date_end');
        });
    }

    public function filterAppAvancedSetting($date, $timeCurrent, $timeAddHour, $dayNumberInWeek)
    {
        // Lọc những ứng dụng cần thông báo
        $application = $this->getBlankAppAvanModel()
            ->leftJoin('applications', 'application_advanced_settings.application_id', '=', 'applications.id')
            ->leftJoin('rooms', 'applications.room_id', '=', 'rooms.id')
            ->leftJoin('spaces', 'applications.space_id', '=', 'spaces.id')
            ->leftJoin('images', 'applications.application_image_id', '=', 'images.id')
            ->where(function ($query) use ($date) {
                $query->whereDate('applications.date_start', '<=', $date)->whereDate('applications.date_end', '>=', $date)
                    ->orWhereNull('applications.date_end');
            })
            ->where('application_advanced_settings.time_start', '>', $timeCurrent)
            ->where('application_advanced_settings.time_start', '<=', $timeAddHour)
            ->whereNull('applications.deleted_at')
            ->where('applications.is_accept', Application::ACCEPT);

        switch ($dayNumberInWeek) {
            case 1:
                $application = $application->where('application_advanced_settings.monday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 2:
                $application = $application->where('application_advanced_settings.tuesday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 3:
                $application = $application->where('application_advanced_settings.wednesday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 4:
                $application = $application->where('application_advanced_settings.thursday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 5:
                $application = $application->where('application_advanced_settings.friday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 6:
                $application = $application->where('application_advanced_settings.saturday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 7:
                $application = $application->where('application_advanced_settings.sunday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            default:
                return false;
                break;
        }

        $application = $application->select(
            'applications.id as application_id',
            'applications.name as application_name',
            'images.source as application_images',
            'application_advanced_settings.time_start as time_start',
            'application_advanced_settings.time_end as time_end',
            'applications.room_id as room_id',
            'rooms.name as room_name',
            'applications.space_id as space_id',
            'spaces.name as space_name',
            'applications.organization_id as organization_id',
            'applications.created_by as created_by'
        )->get()->toArray();

        return $application;
    }

    public function filterAppAvancedSettingSendMail($date, $dayNumberInWeek)
    {
        // Lọc những ứng dụng cần thông báo
        $application = $this->getBlankAppAvanModel()
            ->join('applications', 'application_advanced_settings.application_id', '=', 'applications.id')
            ->join('rooms', 'applications.room_id', '=', 'rooms.id')
            ->join('spaces', 'applications.space_id', '=', 'spaces.id')
            ->join('room_accounts', 'applications.room_id', '=', 'room_accounts.room_id')
            ->join('accounts', 'accounts.id', '=', 'room_accounts.account_id')
            ->where(function ($query) use ($date) {
                $query->whereDate('applications.date_start', '<=', $date)->whereDate('applications.date_end', '>=', $date)
                    ->orWhereNull('applications.date_end');
            })
            // ->where('application_advanced_settings.time_start', '>', $timeCurrent)
            // ->where('application_advanced_settings.time_start', '<=', $timeAddHour)
            ->whereNull('applications.deleted_at')
            /**
                @debug send mail notification
             */
            ->whereNull('spaces.deleted_at')
            ->whereNull('rooms.deleted_at')

            ->where('applications.is_accept', Application::ACCEPT);

        switch ($dayNumberInWeek) {
            case 1:
                $application = $application->where('application_advanced_settings.monday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 2:
                $application = $application->where('application_advanced_settings.tuesday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 3:
                $application = $application->where('application_advanced_settings.wednesday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 4:
                $application = $application->where('application_advanced_settings.thursday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 5:
                $application = $application->where('application_advanced_settings.friday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 6:
                $application = $application->where('application_advanced_settings.saturday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            case 7:
                $application = $application->where('application_advanced_settings.sunday', ApplicationAdvancedSetting::VALUE_WEEKDAYS);
                break;
            default:
                return false;
                break;
        }

        $application = $application->select(
            'applications.id as application_id',
            'applications.name as application_name',
            'application_advanced_settings.time_start as time_start',
            'application_advanced_settings.time_end as time_end',
            'application_advanced_settings.clicks as clicks',
            'applications.room_id as room_id',
            'rooms.name as room_name',
            'applications.space_id as space_id',
            'spaces.name as space_name',
            'applications.organization_id as organization_id',
            'applications.created_by as created_by',
            'accounts.id as account_id',
            'accounts.name as account_name',
            'accounts.username as account_username',
            'accounts.email as account_email',
        )->get();
        $appArr = $application->toArray();
        $accountId = $application->pluck('account_id')->unique()->values()->toArray();
        $appArr = array_map(function ($val) {
            $val['time_start'] = date('H:i', strtotime($val['time_start']));
            $val['time_end'] = date('H:i', strtotime($val['time_end']));
            return $val;
        }, $appArr);
        $data = [];
        if (!empty($appArr)) {
            foreach ($accountId as $id) {
                foreach ($appArr as $value) {
                    if ($id == $value['account_id']) {
                        $data[$value['account_id']]['account_id'] = $value['account_id'];
                        $data[$value['account_id']]['account_name'] = $value['account_name'];
                        $data[$value['account_id']]['account_email'] = $value['account_email'];
                        $data[$value['account_id']]['account_username'] = $value['account_username'];
                        $data[$value['account_id']]['applications'][] = $value;
                    }
                }
            }
        }
        return $data;
    }

    public function getAppDefault()
    {
        $query = $this->getBlankModel()
            ->with(['applicationImage' => function ($query) {
                $query->select([
                    "images.id",
                    "images.source",
                ]);
            }])
            ->with(['applicationType' => function ($query) {
                $query->select([
                    "applications_type.id",
                    "applications_type.name",
                ]);
            }])
            ->where('is_system', Application::IS_SYSTEM);
        $data = $query->select([
            "applications.id",
            'applications.name',
            'applications.type',
            'applications.link_android',
            'applications.link_ios',
            'applications.link_url',
            'applications.date_start',
            'applications.date_end',
            'applications.application_image_id',
            'applications.created_by',
            'applications.account_id',
            'applications.room_id',
            'applications.space_id',
            'applications.is_accept',
            'applications.organization_id',
            'applications.application_group_id',
            'applications.x_frame',
            'applications.is_system',
            'applications.applications_type_id',
        ])->get()->toArray();

        if (!empty($data)) {
            $data = array_map(function ($value) {
                $value['icon'] = !empty($value['application_image']) && !empty($value['application_image']['source']) && file_exists(public_path() . '/' . $value['application_image']['source']) ? asset($value['application_image']['source']) : '';
                return $value;
            }, $data);
        }

        return $data;
    }

}
