<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\StatisticApplicationRepositoryInterface;
use \App\Models\Postgres\StatisticApplication;
use Illuminate\Support\Facades\DB;

class StatisticApplicationRepository extends SingleKeyModelRepository implements StatisticApplicationRepositoryInterface
{

    public function getBlankModel()
    {
        return new StatisticApplication();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getAllStatisticApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->get()->toArray();

        return $data;
    }

    public function getOneObjectStatisticApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    public function sumClicksApp($filter){
        $query = $this->getBlankModel()->select('application_id', DB::raw('SUM(clicks) as clicks'));
        $this->filter($filter, $query);

        $data = $query->groupBy('application_id')->get();

        return $data;
    }

    public function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('statistic_applications.id', $filter['id']);
            } else {
                $query = $query->where('statistic_applications.id', $filter['id']);
            }
        }

        if (isset($filter['application_advanced_setting_id'])) {
            if (is_array($filter['application_advanced_setting_id'])) {
                $query = $query->whereIn('statistic_applications.application_advanced_setting_id', $filter['application_advanced_setting_id']);
            } else {
                $query = $query->where('statistic_applications.application_advanced_setting_id', $filter['application_advanced_setting_id']);
            }
        }

        if (isset($filter['application_id'])) {
            if (is_array($filter['application_id'])) {
                $query = $query->whereIn('statistic_applications.application_id', $filter['application_id']);
            } else {
                $query = $query->where('statistic_applications.application_id', $filter['application_id']);
            }
        }

        if (isset($filter['room_id'])) {
            if (is_array($filter['room_id'])) {
                $query = $query->whereIn('statistic_applications.room_id', $filter['room_id']);
            } else {
                $query = $query->where('statistic_applications.room_id', $filter['room_id']);
            }
        }


        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('statistic_applications.space_id', $filter['space_id']);
            } else {
                $query = $query->where('statistic_applications.space_id', $filter['space_id']);
            }
        }


        if (isset($filter['date_at'])) {
            $query = $query->where('statistic_applications.date_at', $filter['date_at']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('statistic_applications.organization_id', $filter['organization_id']);
        }

        if (isset($filter['date_start'])) {
            $query = $query->whereDate('statistic_applications.date_at','>=', $filter['date_start']);
        }

        if (isset($filter['date_end'])) {
            $query = $query->whereDate('statistic_applications.date_at','<=', $filter['date_end']);
        }
    }
}
