<?php

namespace App\Repositories\Postgres\Eloquent;

use App\Elibs\Debug;
use App\Models\Base;
use App\Models\Postgres\Space;
use App\Models\Postgres\SpaceAccountLifeWork;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\QuickBarRepositoryInterface;
use \App\Models\Postgres\QuickBar;
use App\Elibs\eResponse;
use App\Models\Postgres\Application;
use App\Repositories\Postgres\SpaceRepositoryInterface;
use App\Services\Postgres\ApplicationServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\SpaceAccount;
use App\Models\Postgres\ApplicationStore;

class QuickBarRepository extends SingleKeyModelRepository implements QuickBarRepositoryInterface
{
    protected $spaceRepository;

    public function __construct(SpaceRepositoryInterface $spaceRepository)
    {
        $this->spaceRepository = $spaceRepository;
    }

    public function getBlankModel()
    {
        return new QuickBar();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function getOneQuickBar($appId, $spaceId, $accountInfo)
    {
        $quickbar = $this->getBlankModel()->where('application_id', $appId)->where('space_id', $spaceId)->first();
        if (!empty($quickbar)) {
            return $quickbar;
        }
        return false;
    }

    public function quickbarDetail($quickbarId, $accountInfo)
    {
        $quickbar = $this->getBlankModel()->with(['application' => function ($q) {
            $q->with('applicationImage');
            $q->with('applicationFile');
        }])->find($quickbarId);
        if (!empty($quickbar)) {
            $quickbar->toArray();
            $quickbar['application_type'] = Application::TYPE_LINK;
            $quickbar['application_store'] = $quickbar['application'];
            if (!empty($quickbar->application_store->applicationFile)) {
                $quickbar['application_type'] = Application::TYPE_FILE;
            }
            unset($quickbar['application']);
            return $quickbar;
        }
        return false;
    }

    public function listQuickBarSpaceId($query, $accountInfo)
    {
        /*Quickbar space*/
        try {
            $spaceId = $query['spaceId'];
            $quickBar = $this->getBlankModel()->select('id', 'application_id', 'position', 'space_id', 'organization_id', 'enable_all', 'created_by')
                ->where('space_id', $spaceId)
                ->where('account_id', $accountInfo['id'])
                ->with('createdBy', function ($q) {
                    $q->select('id', 'name', 'email');
                })
                ->where('organization_id', $accountInfo['organization_id'])
                ->with(['application' => function ($q) {
                    // $q->select('id', 'name', 'type', 'slug', 'link_android', 'link_ios', 'link_url', 'application_image_id', 'room_id', 'account_id', 'created_by')
                    $q->select('id', 'name', 'type', 'slug', 'link_android', 'link_ios', 'link_url', 'application_image_id', 'application_file_id', 'created_by', 'is_system')
                        ->with(['applicationImage' => function ($q) {
                            $q->select('id', 'source');
                        }]);
                    $q->with('applicationFile');
                }])->orderBy('position', 'asc')->get();
            $quickBar->map(function ($data) {
                if ($data->application) {
                    if (!empty($data->application['applicationImage'])) {
                        $data->application['icon'] = !empty($data->application['applicationImage']['source']) && file_exists(public_path() . '/' . $data->application['applicationImage']['source']) ? asset($data->application['applicationImage']['source']) : '';
                    } else {
                        $data->application['icon'] = '';
                    }
                    $data['is_quick_bar'] = $data->application['type'] == 1 ? 1 : 0;
                    $data->application['type'] = Application::TYPE_VIEW_SINGLE;
                    $data->application_store = $data->application;
                    unset($data->application);
                }
            });
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $quickBar);
        } catch (\Exception $ex) {
            Debug::sendNotification($ex);
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }
    public function listQuickBarSpaceIdRe($query, $accountInfo)
    {
        try {
            $spaceId = $query['spaceId'];
            $quickBar = $this->getBlankModel()->select('id', 'account_id', 'application_id', 'position', 'space_id', 'organization_id', 'enable_all')
                ->where('space_id', $spaceId)
                ->where('account_id', $accountInfo['id'])
                ->where('organization_id', $accountInfo['organization_id'])
                ->with(['application' => function ($q) {
                    // $q->select('id', 'name', 'type', 'slug', 'link_android', 'link_ios', 'link_url', 'application_image_id', 'room_id', 'account_id', 'created_by')
                    $q->select('id', 'name', 'type', 'slug', 'link_android', 'link_ios', 'link_url', 'application_image_id', 'application_file_id', 'created_by', 'is_system')
                        ->with(['applicationImage' => function ($q) {
                            $q->select('id', 'source');
                        }]);
                    $q->with('applicationFile');
                }])->orderBy('position', 'asc')->get();
            $quickBar->map(function ($data) {
                if ($data->application) {
                    if (!empty($data->application['applicationImage'])) {
                        $data->application['icon'] = !empty($data->application['applicationImage']['source']) && file_exists(public_path() . '/' . $data->application['applicationImage']['source']) ? asset($data->application['applicationImage']['source']) : '';
                    } else {
                        $data->application['icon'] = '';
                    }
                    $data['is_quick_bar'] = $data->application['type'] == 1 ? 1 : 0;
                    $data->application['type'] = Application::TYPE_VIEW_SINGLE;
                    $data->application_store = $data->application;
                    unset($data->application);
                }
            });
            return $quickBar;
        } catch (\Exception $ex) {
            Debug::sendNotification($ex);

            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function insertMulti($params, $accountInfo)
    {
        //        $id = $accountInfo['id'];
        if (!empty($params) && is_array($params)) {
            //            $lsObj = array_map(function ($el) use ($id) {
            //                Log::info([
            //                    'key' => $el
            //                ]);
            //                if (is_array($el)) {
            //                    $el['created_by'] = $id;
            //                }
            //                return $el;
            //            }, $params);
            //            Log::info([
            //                'array_info' => $params
            //            ]);
            $insert = $this->getBlankModel()->insert($params);
            if ($insert) {
                return true;
            }
        }
        return false;
    }

    public function checkOwnQuickbar($idApp, $accountInfo, $space_id)
    {
        $application = ApplicationStore::find($idApp);
        $isAdmin = SpaceAccount::where('space_id', $space_id)->where('account_id', $accountInfo['id'])->first();
        if (!empty($application) && !empty($isAdmin)) {
            if (($accountInfo['id'] == $application->created_by) || ($isAdmin->is_admin == SpaceAccount::IS_ADMIN)) {
                return true;
            }
        }

        return false;
    }

    public function checkOwnQuickbarWorkLife($idApp, $accountInfo, $space_id)
    {

        $application = ApplicationStore::find($idApp);
        $isAdmin = SpaceAccountLifeWork::where('space_id', $space_id)->where('account_id', $accountInfo['id'])->first();
        if (!empty($application) && !empty($isAdmin)) {
            if (($accountInfo['id'] == $application->created_by) || ($isAdmin->is_admin == SpaceAccount::IS_ADMIN)) {
                return true;
            }
        }

        return false;
    }

    public function deleteQuickBar($idQuickBar, $accountInfo)
    {
        try {
            DB::beginTransaction();

            $quickBar = QuickBar::where('id', $idQuickBar)->first();

            if (!$quickBar) {
                return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
            }

            if ($quickBar->enable_all == QuickBar::ENABLE_SELECT || $quickBar->enable_all == QuickBar::ENABLE_ALL) {
                $delete = $this->getBlankModel()->where('space_id', $quickBar->space_id)->where('application_id', $quickBar->application_id);
                $delete->delete();
            }
            // $application = Application::find($quickBar->application_id);
            // /**
            //  * check space stype cua Quibar
            //  */
            // $checkSpaceType = $this->spaceRepository->checkSpaceType($accountInfo['id'], $application->space_id);
            // if ($checkSpaceType == 2) {
            //     $isAdmin = SpaceAccountLifeWork::where('space_id', $application->space_id)->where('account_id', $accountInfo['id'])->first();
            //     if (!empty($application) && !empty($isAdmin)) {
            //         if (($accountInfo['id'] != $application->created_by) && (($isAdmin->is_admin != SpaceAccount::IS_ADMIN)) && ($application->type == Application::TYPE_APP_BAR)) {
            //             return eResponse::response(STATUS_API_FALSE, __('notification.api-not-permission-quickbar'));
            //         }
            //     }
            //     if ($application->type == Application::TYPE_APP_BAR) {
            //         $application->delete();
            //     }
            //     if ($accountInfo['is_root'] == 1 && $quickBar->enable_all == 1) {
            //         $delete = QuickBar::where('space_id', $quickBar->space_id)
            //             ->where('application_id', $quickBar->application_id)
            //             ->where('organization_id', $quickBar->organization_id);
            //         $delete->delete();
            //     }
            // } else {
            //     $isAdmin = SpaceAccount::where('space_id', $application->space_id)->where('account_id', $accountInfo['id'])->first();
            //     if (!empty($application) && !empty($isAdmin)) {
            //         if (($accountInfo['id'] != $application->created_by) && (($isAdmin->is_admin != SpaceAccount::IS_ADMIN)) && ($application->type == Application::TYPE_APP_BAR)) {
            //             return eResponse::response(STATUS_API_FALSE, __('notification.api-not-permission-quickbar'));
            //         }
            //     }
            //     if ($application->type == Application::TYPE_APP_BAR) {
            //         $application->delete();
            //     }

            //     if ($accountInfo['is_root'] == 1 && $quickBar->enable_all == 1) {
            //         $delete = QuickBar::where('space_id', $quickBar->space_id)->where('application_id', $quickBar->application_id)->where('organization_id', $quickBar->organization_id);
            //         $delete->delete();
            //     }
            // }
            $quickBar->delete();
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (\Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteQuickBarUsingEnable($spaceId, $accountId, $applicationId)
    {
        try {
            DB::beginTransaction();
            // $typeSpace = $this->spaceRepository->checkSpaceType(null, $spaceId);
            // if (!empty($accountId['space_type'])) {
            //     QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->where('account_id', '<>', $accountId)->delete();
            // } else {
            // if ($typeSpace == Space::TYPE_SPACE_CUSTOMER) {
            //     $listAccountInSpace = $this->spaceRepository->listAccountIdInSpaceLifeWork($spaceId, $accountId);
            //     if (!empty($listAccountInSpace[Base::DOENS])) {
            //         QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->whereIn('account_life_id', $listAccountInSpace[Base::DOENS])->delete();
            //     }
            //     if (!empty($listAccountInSpace[Base::DOENS_PRO])) {
            // QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->whereIn('account_id', $listAccountInSpace[Base::DOENS_PRO])->delete();
            //     }
            // } else {
            // bên DONES PRO
            $quickbar = QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->first();
            $id = $quickbar ? $quickbar->created_by : $accountId;
            QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->where('account_id', '<>', $id)->delete();
            // }
            // }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (\Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteQuickBarUsingEnableAll($spaceId, $applicationId, $accountId)
    {
        try {
            QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->where('account_id', '<>', $accountId)->delete();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (\Exception $ex) {
            DB::rollback();
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function updateEnable($spaceId, $accountId, $applicationId, $enable)
    {
        try {
            // if (!empty($accountId['space_type'])) {
            //     $listAccountInSpace = $this->spaceRepository->listAccountIdInSpaceLifeWork($spaceId, $accountId);
            //     if (!empty($listAccountInSpace[Base::DOENS])) {
            //         QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->whereIn('account_life_id', $listAccountInSpace[Base::DOENS])->update(['enable_all' => $enable]);
            //     }
            //     if (!empty($listAccountInSpace[Base::DOENS_PRO])) {
            //         QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->whereIn('account_id', $listAccountInSpace[Base::DOENS_PRO])->update(['enable_all' => $enable]);
            //     }
            // }
            QuickBar::where('application_id', $applicationId)->where('space_id', $spaceId)->where('account_id', $accountId)->update(['enable_all' => $enable]);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (\Exception $ex) {
            Debug::sendNotification($ex);
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function sort($requestData)
    {
        try {
            DB::beginTransaction();
            \Batch::update($this->getBlankModel(), $requestData['sortQuickBar'], 'id');
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function getAllApplicationInQuickByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->get()->toArray();

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('quick_bars.id', $filter['id']);
            } else {
                $query = $query->where('quick_bars.id', $filter['id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('quick_bars.space_id', $filter['space_id']);
            } else {
                $query = $query->where('quick_bars.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['application_id'])) {
            if (is_array($filter['application_id'])) {
                $query = $query->whereIn('quick_bars.application_id', $filter['application_id']);
            } else {
                $query = $query->where('quick_bars.application_id', $filter['application_id']);
            }
        }

        if (isset($filter['account_id'])) {
            $query = $query->where('quick_bars.account_id', $filter['account_id']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('quick_bars.organization_id', $filter['organization_id']);
        }
    }

    public function getIdAppInQuickbar($applicationIds, $spaceId, $accountInfo)
    {
        $data = $this->getBlankModel()
            ->whereIn('application_id', $applicationIds)
            ->where('space_id', $spaceId)
            ->where('account_id', $accountInfo['id'])
            ->get()->pluck('application_id')->unique()->values()->toArray();

        return $data;
    }

    public function deleteQuickbarApp($app_ids)
    {
        $this->getBlankModel()->whereIn('application_id', $app_ids)->delete();
    }

    public function listAccountUsingQuickbarOtherAccount($space_id, $app_id, $accountInfo)
    {
        $data = $this->getBlankModel()
            ->whereIn('application_id', $app_id)
            ->where('space_id', $space_id)
            ->where('account_id', '!=', $accountInfo['id'])
            ->get()->pluck('account_id')->unique()->values()->toArray();

        return $data;
    }
}
