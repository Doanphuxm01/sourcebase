<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PostEmotionRepositoryInterface;
use \App\Models\Postgres\PostEmotion;

class PostEmotionRepository extends SingleKeyModelRepository implements PostEmotionRepositoryInterface
{

    public function getBlankModel()
    {
        return new PostEmotion();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function deleteAllPostEmotionByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getAllEmotionByFilter($filter)
    {
        $query = $this->withAccount();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    private function withAccount()
    {
        $query = $this->getBlankModel()
            ->with(['createdBy' => function($query){
                $query->select('accounts.id', 'accounts.name', 'accounts.profile_image_id')
                    ->with(['profileImage' => function($query){
                        $query->select('images.id', 'images.source');
                    }]);
            }]);
        return $query;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('post_emotions.id', $filter['id']);
            } else {
                $query = $query->where('post_emotions.id', $filter['id']);
            }
        }

        if (isset($filter['post_id'])) {
            if (is_array($filter['post_id'])) {
                $query = $query->whereIn('post_emotions.post_id', $filter['post_id']);
            } else {
                $query = $query->where('post_emotions.post_id', $filter['post_id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('post_emotions.space_id', $filter['space_id']);
            } else {
                $query = $query->where('post_emotions.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('post_emotions.organization_id', $filter['organization_id']);
        }

    }

}
