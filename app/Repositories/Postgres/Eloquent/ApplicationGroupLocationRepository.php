<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationGroupLocationRepositoryInterface;
use \App\Models\Postgres\ApplicationGroupLocation;

class ApplicationGroupLocationRepository extends SingleKeyModelRepository implements ApplicationGroupLocationRepositoryInterface
{

    public function getBlankModel()
    {
        return new ApplicationGroupLocation();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getAllApplicationGroupLocationByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->orderBy('position', 'asc')->get()->toArray();

        return $data;
    }

    public function insertMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insert = $this->getBlankModel()->insert($params);
            if($insert){
                return true;
            }
        }
        return false;
    }

    public function deleteAllApplicationGroupLocationByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->delete();

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('application_group_locations.id', $filter['id']);
            } else {
                $query = $query->where('application_group_locations.id', $filter['id']);
            }
        }

        if (isset($filter['application_id'])) {
            if (is_array($filter['application_id'])) {
                $query = $query->whereIn('application_group_locations.application_id', $filter['application_id']);
            } else {
                $query = $query->where('application_group_locations.application_id', $filter['application_id']);
            }
        }

        if (isset($filter['application_group_id'])) {
            if (is_array($filter['application_group_id'])) {
                $query = $query->whereIn('application_group_locations.application_group_id', $filter['application_group_id']);
            } else {
                $query = $query->where('application_group_locations.application_group_id', $filter['application_group_id']);
            }
        }

        if (isset($filter['account_id'])) {
            if (is_array($filter['account_id'])) {
                $query = $query->whereIn('application_group_locations.account_id', $filter['account_id']);
            } else {
                $query = $query->where('application_group_locations.account_id', $filter['account_id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('application_group_locations.space_id', $filter['space_id']);
            } else {
                $query = $query->where('application_group_locations.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['room_id'])) {
            if (is_array($filter['room_id'])) {
                $query = $query->whereIn('application_group_locations.room_id', $filter['room_id']);
            } else {
                $query = $query->where('application_group_locations.room_id', $filter['room_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('application_group_locations.organization_id', $filter['organization_id']);
        }

    }
}
