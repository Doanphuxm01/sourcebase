<?php namespace App\Repositories\Postgres\Eloquent;

use App\Elibs\Debug;
use App\Models\Base;
use App\Models\Postgres\AccountLifeWork;
use App\Models\Postgres\LifeNotifications;
use App\Models\Postgres\LifeSpace;
use App\Models\Postgres\RoomAccountsWorkLife;
use App\Models\Postgres\SpaceAccountLifeWork;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\NotificationRepositoryInterface;
use \App\Models\Postgres\Notification;
use Illuminate\Support\Facades\DB;
use App\Elibs\eResponse;
use App\Elibs\eFunction;
use App\Models\Postgres\Space;
use App\Models\Postgres\SpaceAccount;
use App\Models\Postgres\RoomAccount;
use App\Models\Postgres\Room;
use App\Models\Postgres\Application;
use Throwable;
use Illuminate\Validation\ValidationException;
use App\Models\Postgres\Account;
use App\Models\Postgres\ApplicationStore;
use App\Models\Postgres\Image;

class NotificationRepository extends SingleKeyModelRepository implements NotificationRepositoryInterface
{

    public function getBlankModelNoti()
    {
        return new Notification();
    }

    public function getBlankModelWorkLife()
    {
        return new LifeNotifications();
    }

    public function getBlankSpace()
    {
        return new Space();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function showNotification($accountInfo, $requestData)
    {
        $limit = $requestData['limit'] ?? 10;
        $notification = $this->getBlankModelNoti()
            ->select(
                'notifications.id',
                'notifications.message',
                'notifications.is_read',
                'notifications.created_at as date_time',
            )
            ->where('account_id', $accountInfo['id'])
            ->where('organization_id', $accountInfo['organization_id'])
            ->where('notifications.event', '<>', LifeNotifications::REQUEST_JOIN_SPACE)
            // ->orderBy('is_read', 'asc')
            ->orderBy('created_at', 'desc');

        if (!empty($requestData['read_all']) && $requestData['read_all'] == Notification::READ_ALL) {
            $notification = $notification->get()->toArray();
            $notification = array_map(function ($val) {
                $val['message']['avatar'] = eFunction::getPathDomain($val['message']['avatar']);
                return $val;
            }, $notification);
        } else {
            $notification = $notification->paginate($limit)->toArray();
            $notification['data'] = array_map(function ($val) {
                $val['message']['avatar'] = eFunction::getPathDomain($val['message']['avatar']);
                return $val;
            }, $notification['data']);
        }
        return $notification;
    }

    public function updateStatusNotification($requestData)
    {
        try {
            DB::beginTransaction();
            $this->getBlankModelNoti()->whereIn('id', $requestData['notificationIds'])->update(['is_read' => Notification::IS_READ]);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (\Exception $ex) {
            DB::rollBack();
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function notification($accountInfo, $accountIds, $messageNotification, $type)
    {
        $typeSpace = Space::find($messageNotification['space_id']);
        $data = [];
        if (empty($typeSpace)) {
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        } else {
            if (is_array($accountIds)) {
                foreach ($accountIds as $id) {
                    $data[] = [
                        'is_read' => Notification::NOT_READ,
                        'created_by' => $accountInfo['id'],
                        'organization_id' => $accountInfo['organization_id'],
                        'account_id' => $id,
                        'message' => json_encode($messageNotification),
                        'type' => $type,
                        'created_at' => now(),
                        'updated_at' => now(),
                        'event' => !empty($messageNotification['event']) ? $messageNotification['event'] : NULL,
                        'space_id' => !empty($messageNotification['space_id']) ? $messageNotification['space_id'] : NULL,
                        'room_id' => !empty($messageNotification['room_id']) ? $messageNotification['room_id'] : NULL,
                    ];
                }
            } else {
                $data = [
                    'is_read' => Notification::NOT_READ,
                    'created_by' => $accountInfo['id'],
                    'organization_id' => $accountInfo['organization_id'],
                    'account_id' => $accountIds,
                    'message' => json_encode($messageNotification),
                    'created_at' => now(),
                    'updated_at' => now(),
                    'event' => !empty($messageNotification['event']) ? $messageNotification['event'] : NULL,
                    'space_id' => !empty($messageNotification['space_id']) ? $messageNotification['space_id'] : NULL,
                    'room_id' => !empty($messageNotification['room_id']) ? $messageNotification['room_id'] : NULL,
                ];
            }
            //            if($typeSpace['type'] == Space::TYPE_SPACE_CUSTOMER) {
            //                if (is_array($accountIds)) {
            //                    foreach ($accountIds as $id) {
            //                        $data[] = [
            //                            'is_read' => Notification::NOT_READ,
            //                            'created_by' => $accountInfo['id'],
            //                            'organization_id' => $accountInfo['organization_id'],
            //                            'account_life_id' => $id,
            //                            'message' => json_encode($messageNotification),
            //                            'type' => $type,
            //                            'created_at' => now(),
            //                            'updated_at' => now()
            //                        ];
            //                    }
            //                } else {
            //                    $data = [
            //                        'is_read' => Notification::NOT_READ,
            //                        'created_by' => $accountInfo['id'],
            //                        'organization_id' => $accountInfo['organization_id'],
            //                        'account_life_id' => $accountIds,
            //                        'message' => json_encode($messageNotification),
            //                        'created_at' => now(),
            //                        'updated_at' => now()
            //                    ];
            //                }
            //            } else {
            //                if (is_array($accountIds)) {
            //                    foreach ($accountIds as $id) {
            //                        $data[] = [
            //                            'is_read' => Notification::NOT_READ,
            //                            'created_by' => $accountInfo['id'],
            //                            'organization_id' => $accountInfo['organization_id'],
            //                            'account_id' => $id,
            //                            'message' => json_encode($messageNotification),
            //                            'type' => $type,
            //                            'created_at' => now(),
            //                            'updated_at' => now()
            //                        ];
            //                    }
            //                } else {
            //                    $data = [
            //                        'is_read' => Notification::NOT_READ,
            //                        'created_by' => $accountInfo['id'],
            //                        'organization_id' => $accountInfo['organization_id'],
            //                        'account_id' => $accountIds,
            //                        'message' => json_encode($messageNotification),
            //                        'created_at' => now(),
            //                        'updated_at' => now()
            //                    ];
            //                }
            //            }
        }
        if (!empty($data) && is_array($data)) {
            $this->getBlankModelNoti()->insert($data);
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
    }

    public function notificationWorkLife($accountInfo, $accountIds, $messageNotification, $type)
    {
        try {
            $data = [];
            if (is_array($accountIds)) {
                foreach ($accountIds as $id) {
                    $data[] = [
                        'is_read' => Notification::NOT_READ,
                        'created_by' => $accountInfo['id'],
                        'account_id' => $id,
                        'message' => json_encode($messageNotification),
                        'type' => $type,
                        'created_at' => now(),
                        'updated_at' => now(),
                        'event' => !empty($messageNotification['event']) ? $messageNotification['event'] : NULL,
                        'space_id' => !empty($messageNotification['space_id']) ? $messageNotification['space_id'] : NULL,
                        'room_id' => !empty($messageNotification['room_id']) ? $messageNotification['room_id'] : NULL,
                        'is_dones_life' => 0,
                    ];
                }
            } else {
                $data = [
                    'is_read' => Notification::NOT_READ,
                    'created_by' => $accountInfo['id'],
                    'account_id' => $accountIds,
                    'type' => $type,
                    'message' => json_encode($messageNotification),
                    'created_at' => now(),
                    'updated_at' => now(),
                    'event' => !empty($messageNotification['event']) ? $messageNotification['event'] : NULL,
                    'space_id' => !empty($messageNotification['space_id']) ? $messageNotification['space_id'] : NULL,
                    'room_id' => !empty($messageNotification['room_id']) ? $messageNotification['room_id'] : NULL,
                    'is_dones_life' => 0,
                ];
            }
            if (!empty($data) && is_array($data)) {
                $this->getBlankModelWorkLife()->insert($data);
            }
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
        } catch (\Exception $ex) {
            Debug::sendNotification($ex);
            \Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    // Thêm thành viên vào space
    public function pushNotiAccountSpace($accountInfo, $requestData, $space, $listIds)
    {
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::ACCOUNT_SPACE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$space->id,
            'space_name' => $space->name
        ];

        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);

        if (!empty($requestData['accountIds'])) {
            eFunction::pushNotifictions($sendNotification, $requestData['accountIds']);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $requestData['accountIds'], $sendNotification, Notification::TYPE_SPACE);
        }
    }

    public function pushNotiAccountInvite($accountInfo, $requestData, $space, $listIds)
    {
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::INVITE_TO_SYSTEM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => !empty($accountInfo['avatar']) ? $accountInfo['avatar'] : '',
            'space_id' => !empty($space) ? $space->id : '',
            'space_name' => !empty($space) ? $space->name : ''
        ];

        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);
        if (!empty($requestData['accountIds'])) {
            eFunction::pushNotifictions($sendNotification, $requestData['accountIds']);
            // Gọi đến function của class parent
//            $this->notification($accountInfo, $requestData['accountIds'], $sendNotification, Notification::TYPE_SPACE);
        }
    }


    // Xóa account trong space
    public function pushNotiDeleteAccountInSpace($accountInfo, $requestData, $space)
    {
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::DEL_ACCOUNT_SPACE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$space->id,
            'space_name' => $space->name
        ];

        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);

        if (!empty($requestData['accountIds'])) {
            foreach ($requestData['accountIds'] as $account_id) {
                $spaceNext = SpaceAccount::where('space_id', '<>', $space->id)
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->where('account_id', $account_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $sendNotification['space_next'] = !empty($spaceNext) ? $spaceNext->space_id : '';

                eFunction::pushNotifictions($sendNotification, $account_id);
                // Gọi đến function của class parent
                $this->notification($accountInfo, $account_id, $sendNotification, Notification::TYPE_SPACE);
            }
        }
    }

    // Sửa Space
    public function pushNotiUpdateSpace($accountInfo, $requestData, $oldNameSpace, $idsOld)
    {
        // $accounts = SpaceAccount::where('space_id', $requestData['spaceId'])
        //     ->where('organization_id', $accountInfo['organization_id'])->get()->toArray();

        // $ids = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        if (!empty($idsOld)) {
            // Gửi thông báo realtime gọi đến socket
            $sendNotification = [
                'event' => Notification::UPDATE_SPACE,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => (int)$requestData['spaceId'],
                'space_name_new' => $requestData['spaceName'],
                'space_name_old' => $oldNameSpace,
            ];

            $key = array_search($accountInfo['id'], $idsOld);

            if (false !== $key) {
                unset($idsOld[$key]);
            }

            sort($idsOld);

            if (!empty($idsOld)) {
                eFunction::pushNotifictions($sendNotification, $idsOld);
                // Gọi đến function của class parent
                $this->notification($accountInfo, $idsOld, $sendNotification, Notification::TYPE_SPACE);
            }
        }
    }

    // Xóa Space
    public function pushNotiDeleteSpace($accountInfo, $spaceIds)
    {
        if (!empty($spaceIds)) {
            foreach ($spaceIds as $value) {
                $space = Space::find($value);
                // Gửi thông báo realtime gọi đến socket
                $sendNotification = [
                    'event' => Notification::DELETE_SPACE,
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'space_id' => !empty($space) ? (int)$space->id : '',
                    'space_name' => !empty($space) ? $space->name : '',
                ];

                $accounts = SpaceAccount::where('space_id', $value)
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
                $ids = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

                if (!empty($ids)) {
                    foreach ($ids as $account_id) {
                        $spaceNext = SpaceAccount::where('space_id', '<>', $value)
                            ->where('organization_id', $accountInfo['organization_id'])
                            ->where('account_id', $account_id)
                            ->orderBy('id', 'desc')
                            ->first();

                        $sendNotification['space_next'] = !empty($spaceNext) ? $spaceNext->space_id : '';

                        eFunction::pushNotifictions($sendNotification, $account_id);
                        // Gọi đến function của class parent
                        $this->notification($accountInfo, $account_id, $sendNotification, Notification::TYPE_SPACE);
                    }
                }
            }
        }
    }

    // Thêm thành viên vào room
    public function pushNotiAccountRoom($accountInfo, $requestData, $room, $listIds)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::ACCOUNT_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => !empty($requestData['spaceId']) ? (int)$requestData['spaceId'] : (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => !empty($requestData['roomName']) ? $requestData['roomName'] : $room->name,
        ];
        if (!empty($listIds)) {

            $key = array_search($accountInfo['id'], $listIds);

            if (false !== $key) {
                unset($listIds[$key]);
            }

            sort($listIds);

            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_ROOM);
        }
    }

    // Xóa room
    public function pushNotiDeleteRoom($accountInfo, $room)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::DELETE_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => $room->name,
        ];

        $accounts = RoomAccount::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();
        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);
            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_ROOM);
        }
    }

    //Cập nhật room
    public function pushNotiUpdateRoom($accountInfo, $room, $oldRoomName)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::UPDATE_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name_new' => $room->name,
            'room_name_old' => $oldRoomName,
        ];

        $accounts = RoomAccount::where('room_id', $room->id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_ROOM);
        }
    }

    // Xóa account khỏi room
    public function pushNotiDeleteAccountInRoom($requestData, $accountInfo)
    {
        $room = Room::find($requestData['roomId']);

        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::DEL_ACCOUNT_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => $room->name,
        ];

        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);

        if (!empty($requestData['accountIds'])) {
            eFunction::pushNotifictions($sendNotification, $requestData['accountIds']);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $requestData['accountIds'], $sendNotification, Notification::TYPE_ROOM);
        }
    }

    // Tạo mới ứng dụng
    public function pushNotiCreateNewApp($accountInfo, $application, $ownRoom, $is_accept)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);

        $infoApp = ApplicationStore::where('id', $application->application_store_id)->with('createdBy')->first();
        $infoAppInRoom = Application::where('id', $application->id)->with('createdBy')->first();
        if ($infoAppInRoom->createdBy && !empty($infoAppInRoom->createdBy['profile_image_id'])) {
            $image = Image::where('id', $infoAppInRoom->createdBy['profile_image_id'])->whereNull('deleted_at')->first();
            if (!empty($image)) {
                $image->toArray();
                $avt = !empty($image['source']) && file_exists(public_path() . '/' . $image['source']) ? asset($image['source']) : '';
            }
        }
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'created_by_id' => @(int)$infoAppInRoom->createdBy['id'],
            'created_by_name' => @$infoAppInRoom->createdBy['name'],
            'avatar' => $avt ?? null,
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => $room->name,
            'application_id' => (int)$application->id,
            'application_name' => $infoApp->name,
        ];

        // Nếu là chủ room gửi đến tất cả thành viên
        if ((!empty($ownRoom) && $ownRoom === (int)$accountInfo['id']) || $is_accept == 1) {
            // if ($application->is_accept == Application::ACCEPT) {
            $accounts = RoomAccount::where('room_id', $application->room_id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
            $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

            $key = array_search($accountInfo['id'], $listIds);

            if (false !== $key) {
                unset($listIds[$key]);
            }

            if (!empty($application->account_id)) {
                $sendNotification['event'] = Notification::ADD_LINK_APP;

                eFunction::pushNotifictions($sendNotification, $application->account_id);

                // Gọi đến function của class parent
                $this->notification($accountInfo, $application->account_id, $sendNotification, Notification::TYPE_APP);

                $keyVal = array_search($application->account_id, $listIds);

                if (false !== $keyVal) {
                    unset($listIds[$keyVal]);
                }
            }

            sort($listIds);

            $sendNotification['event'] = Notification::CREATE_APP;

            if (!empty($listIds)) {
                eFunction::pushNotifictions($sendNotification, $listIds);

                // Gọi đến function của class parent
                $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_APP);
            }
            // }
        } else {

            if (!empty($application->account_id)) {
                $sendNotification['event'] = Notification::ADD_LINK_APP;

                eFunction::pushNotifictions($sendNotification, $application->account_id);

                // Gọi đến function của class parent
                $this->notification($accountInfo, $application->account_id, $sendNotification, Notification::TYPE_APP);
            }

            $sendNotification['event'] = Notification::APP_WAIT_APPOVE;
            $adminRoom = RoomAccount::where('space_id', $application->space_id)
                ->where('room_id', $application->room_id)
                ->where('is_admin', RoomAccount::IS_ADMIN)
                ->get()->pluck('account_id')->unique()->values()->toArray();
            array_push($adminRoom, $ownRoom);
            $listAccountIds = array_unique($adminRoom);
            sort($listAccountIds);
            eFunction::pushNotifictions($sendNotification, $listAccountIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listAccountIds, $sendNotification, Notification::TYPE_APP);
        }
    }

    // Thông báo duyệt app
    public function pushNotiApproveApp($accountInfo, $application)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);
        $accountCreateApp = Account::where('id', $application->created_by)->first();
        $infoApp = ApplicationStore::where('id', $application->application_store_id)->first();
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            // 'created_by_id' => (int)$application->created_by,
            'created_by_id' => (int)$accountInfo['id'],
            // 'created_by_name' => $accountCreateApp->name,
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => $room->name,
            'application_id' => (int)$application->id,
            'application_name' => $infoApp->name,
        ];

        // Thông báo đến người yêu cầu
        $sendNotification['event'] = Notification::APP_APPROVE;
        eFunction::pushNotifictions($sendNotification, $application->created_by);
        $this->notification($accountInfo, $application->created_by, $sendNotification, Notification::TYPE_APP);

        // Thông báo đến mọi người
        $accounts = RoomAccount::where('room_id', $application->room_id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        $keyVal = array_search($application->created_by, $listIds);

        if (false !== $keyVal) {
            unset($listIds[$keyVal]);
        }

        sort($listIds);

        $sendNotification['event'] = Notification::CREATE_APP;
        $sendNotification['created_by_id'] = (int)$application->created_by;
        $sendNotification['created_by_name'] = $accountCreateApp->name;

        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_APP);
        }
        // }
    }

    // Thông báo từ chối app
    public function pushNotiRejectApp($accountInfo, $application)
    {
        $room = Room::find($application['room_id']);
        $space = Space::find($application['space_id']);
        $infoApp = ApplicationStore::where('id', $application['application_store_id'])->first();
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::APP_REJECT,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$application['space_id'],
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application['room_id'],
            'room_name' => $room->name,
            'application_id' => (int)$application['id'],
            'application_name' => $infoApp->name,
        ];

        eFunction::pushNotifictions($sendNotification, $application['created_by']['id']);

        // Gọi đến function của class parent
        $this->notification($accountInfo, $application['created_by']['id'], $sendNotification, Notification::TYPE_APP);
    }

    // Xóa App
    public function pushNotiDeleteApp($accountInfo, $ids)
    {
        foreach ($ids as $id) {
            $application = Application::find($id);
            if (!empty($application) && $application->is_accept == Application::ACCEPT) {
                $room = Room::find($application->room_id);
                $space = Space::find($application->space_id);
                $infoApp = ApplicationStore::where('id', $application->application_store_id)->first();
                // Gửi thông báo realtime gọi đến socket
                $sendNotification = [
                    'event' => Notification::DELETE_APP,
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'space_id' => (int)$application->space_id,
                    'space_name' => !empty($space) ? $space->name : '',
                    'room_id' => (int)$application->room_id,
                    'room_name' => !empty($room) ? $room->name : '',
                    'application_id' => (int)$application->id,
                    'application_name' => $infoApp->name,
                    'application_store_id' => $application->application_store_id,
                ];

                $accounts = RoomAccount::where('room_id', $application->room_id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
                $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

                $key = array_search($accountInfo['id'], $listIds);

                if (false !== $key) {
                    unset($listIds[$key]);
                }

                sort($listIds);

                if (!empty($listIds)) {
                    eFunction::pushNotifictions($sendNotification, $listIds);

                    // Gọi đến function của class parent
                    $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_APP);
                }
                // }
            }
        }
    }

    // Sửa App
    public function pushNotiUpdateApp($accountInfo, $application, $appOld)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);
        $infoApp = ApplicationStore::where('id', $application->application_store_id)->first();
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::UPDATE_APP,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => !empty($room) ? $room->name : '',
            'application_id' => (int)$application->id,
            'application_store_id' => (int)$application->application_store_id,
            'application_name_new' => $infoApp->name,
            'application_name_old' => $appOld->name,
        ];

        $accounts = RoomAccount::where('room_id', $application->room_id)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        sort($listIds);

        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_APP);
        }
    }

    // Thông báo quick bar mới
    public function pushNotiQuickBarEnableAll($accountInfo, $applicationId, $ids, $spaceId, $dataOpenApp = null)
    {
        if (!empty($ids)) {
            $space = Space::find($spaceId);
            $application = ApplicationStore::find($applicationId);
            // Gửi thông báo realtime gọi đến socket
            $sendNotification = [
                'event' => Notification::QUICKBAR_ENABLE_ALL,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => (int)$spaceId,
                'space_name' => !empty($space) ? $space->name : '',
                'application_id' => !empty($application) ? (int)$application->id : '',
                'application_name' => !empty($application) ? $application->name : '',
                'application' => !empty($dataOpenApp) ? json_encode($dataOpenApp) : null
            ];
            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }

            sort($ids);

            eFunction::pushNotifictions($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $ids, $sendNotification, Notification::TYPE_QUICKBAR);
        }
    }

    // Thông báo xóa quickbar
    public function pushNotiDeleteQuickBar($accountInfo, $applicationId, $ids, $spaceId)
    {
        if (!empty($ids)) {
            $space = Space::find($spaceId);
            $application = Application::find($applicationId);
            // Gửi thông báo realtime gọi đến socket
            $sendNotification = [
                'event' => Notification::QUICKBAR_DELETE,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => (int)$spaceId,
                'space_name' => !empty($space) ? $space->name : '',
                'application_id' => !empty($application) ? (int)$application->id : '',
                'application_name' => !empty($application) ? $application->name : '',
            ];

            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }

            sort($ids);

            eFunction::pushNotifictions($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $ids, $sendNotification, Notification::TYPE_QUICKBAR);
        }
    }

    // Thông báo cập nhật quickbar
    public function pushNotiUpdateQuickbar($accountInfo, $applicationId, $ids, $spaceId, $appOld)
    {
        if (!empty($ids)) {
            $space = Space::find($spaceId);
            $application = Application::find($applicationId);
            // Gửi thông báo realtime gọi đến socket
            $sendNotification = [
                'event' => Notification::QUICKBAR_UPDATE,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => (int)$spaceId,
                'space_name' => !empty($space) ? $space->name : '',
                'application_id' => !empty($application) ? (int)$application->id : '',
                'application_name_new' => !empty($application) ? $application->name : '',
                'application_name_old' => !empty($appOld) ? $appOld->name : '',
            ];

            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }

            sort($ids);

            eFunction::pushNotifictions($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $ids, $sendNotification, Notification::TYPE_QUICKBAR);
        }
    }

    // Thông báo tạo app group
    public function pushNotiCreateGroupApp($accountInfo, $roomId, $spaceId, $appGroupName)
    {
        $room = Room::find($roomId);
        $space = Space::find($spaceId);

        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::CREATE_GROUP_APP,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => !empty($space) ? (int)$space->id : '',
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => !empty($room) ? (int)$room->id : '',
            'room_name' => !empty($room) ? $room->name : '',
            'application_group_name' => $appGroupName,
        ];

        $accounts = RoomAccount::where('room_id', $roomId)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        sort($listIds);

        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_GROUP_APP);
        }
    }

    // Thông báo sửa app group
    public function pushNotiUpdateGroupApp($accountInfo, $roomId, $spaceId, $appGroupName)
    {
        $room = Room::find($roomId);
        $space = Space::find($spaceId);

        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::UPDATE_GROUP_APP,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => !empty($space) ? (int)$space->id : '',
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => !empty($room) ? (int)$room->id : '',
            'room_name' => !empty($room) ? $room->name : '',
            'application_group_name' => $appGroupName,
        ];

        $accounts = RoomAccount::where('room_id', $roomId)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
        $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        sort($listIds);

        if (!empty($listIds)) {
            eFunction::pushNotifictions($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_GROUP_APP);
        }
    }

    // Thông báo xóa app group
    public function pushNotiDeleteGroupApp($accountInfo, $roomId, $spaceId, $appGroupName)
    {
        $room = Room::find($roomId);
        $space = Space::find($spaceId);
        if (!empty($appGroupName)) {
            foreach ($appGroupName as $name) {
                // Gửi thông báo realtime gọi đến socket
                $sendNotification = [
                    'event' => Notification::DELETE_GROUP_APP,
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'space_id' => !empty($space) ? (int)$space->id : '',
                    'space_name' => !empty($space) ? $space->name : '',
                    'room_id' => !empty($room) ? (int)$room->id : '',
                    'room_name' => !empty($room) ? $room->name : '',
                    'application_group_name' => $name,
                ];

                $accounts = RoomAccount::where('room_id', $roomId)->where('organization_id', $accountInfo['organization_id'])->where('account_id', '<>', $accountInfo['id'])->get()->toArray();
                $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

                $key = array_search($accountInfo['id'], $listIds);

                if (false !== $key) {
                    unset($listIds[$key]);
                }

                sort($listIds);

                if (!empty($listIds)) {
                    eFunction::pushNotifictions($sendNotification, $listIds);
                    // Gọi đến function của class parent
                    $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_GROUP_APP);
                }
            }
        }
    }

    // Thông báo nhắc nhở tương tác với app
    public function pushNotiInteractApp($applications)
    {
        if (!empty($applications)) {
            // Gửi thông báo realtime gọi đến socket
            foreach ($applications as $application) {
                $sendNotification = [
                    'event' => Notification::APP_INTERACT,
                    'space_id' => (int)$application['space_id'],
                    'space_name' => $application['space_name'],
                    'room_id' => (int)$application['room_id'],
                    'room_name' => $application['room_name'],
                    'application_id' => (int)$application['application_id'],
                    'application_name' => $application['application_name'],
                    'avatar' => !empty($application['application_images']) && file_exists(public_path() . '/' . $application['application_images']) ? config('systems.URL_ASSET') . $application['application_images'] : '',
                    'time_start' => $application['time_start'],
                    'time_end' => $application['time_end'],
                ];

                // Thông báo đến mọi người
                $accounts = RoomAccount::where('room_id', $application['room_id'])->where('organization_id', $application['organization_id'])->get()->toArray();
                $listIds = collect($accounts)->pluck('account_id')->unique()->values()->toArray();

                // $key = array_search($accountInfo['id'], $listIds);

                // if (false !== $key) {
                //     unset($listIds[$key]);
                // }

                sort($listIds);

                if (!empty($listIds)) {
                    eFunction::pushNotifictions($sendNotification, $listIds);
                    $accountInfo['id'] = (int)$application['created_by'];
                    $accountInfo['organization_id'] = $application['organization_id'];
                    // Gọi đến function của class parent
                    $this->notification($accountInfo, $listIds, $sendNotification, Notification::TYPE_SYSTEM);
                }
            }
        }
    }

    // Thông báo cập nhật admin space
    public function pushNotiAdminSpace($request, $accountInfo)
    {
        $space = Space::find($request['space_id']);
        if (!empty($space)) {
            $sendNotification = [
                'event' => Notification::ADMIN_SPACE,
                'is_admin' => $request['is_admin'] == SpaceAccount::IS_ADMIN ? (int)SpaceAccount::IS_ADMIN : 0,
                'admin_name' => $request['is_admin'] == RoomAccount::IS_ADMIN ? __('notification.admin.true') : __('notification.admin.false'),
                'for_me' => $request['is_admin'] == RoomAccount::IS_ADMIN ? __('notification.admin.me.true') : __('notification.admin.me.false'),
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => !empty($space) ? (int)$space->id : '',
                'space_name' => !empty($space) ? $space->name : '',
            ];

            eFunction::pushNotifictions($sendNotification, $request['account_id']);
            // Gọi đến function của class parent
            $this->notification($accountInfo, $request['account_id'], $sendNotification, Notification::TYPE_SPACE);
        }
    }

    // Thông báo cập nhật admin room
    public function pushNotiAdminRoom($request, $accountInfo)
    {
        $room = Room::find($request['room_id']);
        if (!empty($room)) {
            $space = Space::find($room->space_id);
            if (!empty($space)) {
                $sendNotification = [
                    'event' => Notification::ADMIN_ROOM,
                    'is_admin' => $request['is_admin'] == RoomAccount::IS_ADMIN ? (int)RoomAccount::IS_ADMIN : (int)RoomAccount::IS_MEMBER,
                    'admin_name' => $request['is_admin'] == RoomAccount::IS_ADMIN ? __('notification.admin.true') : __('notification.admin.false'),
                    'for_me' => $request['is_admin'] == RoomAccount::IS_ADMIN ? __('notification.admin.me.true') : __('notification.admin.me.false'),
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'space_id' => !empty($space) ? (int)$space->id : '',
                    'space_name' => !empty($space) ? $space->name : '',
                    'room_id' => !empty($room) ? (int)$room->id : '',
                    'room_name' => !empty($room) ? $room->name : '',
                ];

                eFunction::pushNotifictions($sendNotification, $request['account_id']);
                // Gọi đến function của class parent
                $this->notification($accountInfo, $request['account_id'], $sendNotification, Notification::TYPE_ROOM);
            }
        }
    }

    // Thông báo cập nhật chấm than app
    public function pushNotiWarningApplication($request, $accountInfo)
    {
        $sendNotification = [
            'event' => Notification::CLICK_APP,
            'space_id' => !empty($request['space_id']) ? (int)$request['space_id'] : '',
            'room_id' => !empty($request['room_id']) ? (int)$request['room_id'] : '',
            'account_id' => $accountInfo['id'],
            'application_id' => !empty($request['application_id']) ? (int)$request['application_id'] : '',
        ];
        $request['account_id'] = $accountInfo['id'];
        eFunction::pushNotifictions($sendNotification, $request['account_id']);
    }
    /**
     * Notication Work Life
     * @param $accountInfo
     * @param $data
     */

    /* @todo
     * Thêm thành viên vào space & thêm space
     */
    public function pushNotiAccountSpaceWorkLife($accountInfo, $data, $space = null)
    {
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::ACCOUNT_SPACE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => !empty($space) ? (int)$space->id : '',
            'space_name' => !empty($space) ? $space->name : ''
        ];
        $key = array_search($accountInfo['id'], $data['accountIds']);

        if (false !== $key) {
            unset($data['accountIds'][$key]);
        }

        sort($data['accountIds']);

        if (!empty($data['accountIds'])) {
            // Xóa thông báo cũ

            if (isset($data['spaceId'])) {
                $this->getBlankModelWorkLife()
                    ->whereIn('account_id', $data['accountIds'])
                    ->where('space_id', $data['spaceId'])
                    ->where('event', LifeNotifications::INVITE_SPACE)
                    ->delete();
            }
            eFunction::pushNotifictionsWorkLife($sendNotification, $data['accountIds']);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $data['accountIds'], $sendNotification, Notification::TYPE_SPACE);
        }
    }

    /* @todo
     * Sửa Space
     */
    public function pushNotiUpdateSpaceWorkLife($accountInfo, $requestData, $oldNameSpace, $idsOld)
    {
        // $accounts = SpaceAccount::where('space_id', $requestData['spaceId'])
        //     ->get()->toArray();

        // $ids = collect($accounts)->pluck('account_id')->unique()->values()->toArray();
        if (!empty($idsOld)) {
            // Gửi thông báo realtime gọi đến socket
            $sendNotification = [
                'event' => LifeNotifications::UPDATE_SPACE,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'is_dones_life' => 0,
                'space_id' => (int)$requestData['spaceId'],
                'space_name_new' => $requestData['spaceName'],
                'space_name_old' => '',
            ];

            $key = array_search($accountInfo['id'], $idsOld);

            if (false !== $key) {
                unset($idsOld[$key]);
            }

            sort($idsOld);
            if (!empty($idsOld)) {
                $idsOld = array_filter($idsOld, 'ucfirst');
                eFunction::pushNotifictionsWorkLife($sendNotification, $idsOld);
                // Gọi đến function của class parent
                $this->notificationWorkLife($accountInfo, $idsOld, $sendNotification, LifeNotifications::TYPE_SPACE);
            }
        }
    }

    /* @todo
     * Xóa account trong space
     */
    public function pushNotiDeleteAccountInSpaceWorkLife($accountInfo, $requestData, $space)
    {
        //        dd($accountInfo, $requestData, $space);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::DELETE_ACCOUNT_SPACE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$space->id,
            'space_name' => $space->name
        ];
        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);

        if (!empty($requestData['accountIds'])) {
            foreach ($requestData['accountIds'] as $account_id) {
                $spaceNext = SpaceAccountLifeWork::where('space_id', '<>', $space->id)
                    ->where('account_id', $account_id)
                    ->orderBy('id', 'desc')
                    ->first();

                $sendNotification['space_next'] = !empty($spaceNext) ? $spaceNext->space_id : '';

                eFunction::pushNotifictionsWorkLife($sendNotification, $account_id);
                // Gọi đến function của class parent
                $this->notificationWorkLife($accountInfo, $account_id, $sendNotification, LifeNotifications::TYPE_SPACE);
            }
        }
    }

    /* @todo
     * Xóa  space
     */
    public function pushNotiDeleteSpaceWorkLife($accountInfo, $spaceIds)
    {
        if (!empty($spaceIds)) {
            foreach ($spaceIds as $value) {
                $space = Space::find($value);
                // Gửi thông báo realtime gọi đến socket
                $sendNotification = [
                    'event' => LifeNotifications::DELETE_SPACE,
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'is_dones_life' => 0,
                    'space_id' => !empty($space) ? (int)$space->id : '',
                    'space_name' => !empty($space) ? $space->name : '',
                ];
                $accounts = SpaceAccountLifeWork::where('space_id', $value)
                    ->where('account_life_id', '<>', $accountInfo['id'])->get()->toArray();
                $ids = collect($accounts)->pluck('account_life_id')->unique()->values()->toArray();

                if (!empty($ids)) {
                    foreach ($ids as $account_id) {
                        $spaceNext = SpaceAccountLifeWork::where('space_id', '<>', $value)
                            ->where('account_id', $account_id)
                            ->orderBy('id', 'desc')
                            ->first();

                        $sendNotification['space_next'] = !empty($spaceNext) ? $spaceNext->space_id : '';

                        eFunction::pushNotifictionsWorkLife($sendNotification, $account_id);
                        // Gọi đến function của class parent
                        $this->notificationWorkLife($accountInfo, $account_id, $sendNotification, LifeNotifications::TYPE_SPACE);
                    }
                }
            }
        }
    }

    /* @todo
     * Thêm thành viên vào room & thêm room
     */
    public function pushNotiAccountRoomWorkLife($accountInfo, $requestData, $room, $listIds)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::ADD_ACCOUNT_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'space_id' => !empty($requestData['spaceId']) ? (int)$requestData['spaceId'] : (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => !empty($requestData['roomName']) ? $requestData['roomName'] : $room->name,
        ];
        if (!empty($listIds)) {

            $key = array_search($accountInfo['id'], $listIds);

            if (false !== $key) {
                unset($listIds[$key]);
            }
            $listIds = array_filter($listIds, 'ucfirst');
            $listIds = array_unique($listIds);
            sort($listIds);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_ROOM);
        }
    }

    /* @todo
     * Xóa room
     */
    public function pushNotiDeleteRoomWorkLife($accountInfo, $room)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::DELETE_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => $room->name,
        ];

        $listIds = RoomAccountsWorkLife::where('room_id', $room->id)->whereNotNull('account_life_id')->pluck('account_life_id')->unique()->values()->toArray();
        if (!empty($listIds)) {
            $listIds = array_filter($listIds, 'ucfirst');
            sort($listIds);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);
            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_ROOM);
        }
    }

    /* @todo
     * Sửa room
     */
    public function pushNotiUpdateRoomWorkLife($accountInfo, $room, $oldRoomName)
    {
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::UPDATE_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name_new' => $room->name,
            'room_name_old' => $oldRoomName,
        ];

        $listIdsDones = RoomAccountsWorkLife::where('room_id', $room->id)->whereNotNull('account_life_id')->pluck('account_life_id')->unique()->values()->toArray();
        $listIdsDonesPro = RoomAccountsWorkLife::where('room_id', $room->id)->where('account_id', '<>', $accountInfo['id'])->whereNotNull('account_id')->pluck('account_id')->unique()->values()->toArray();
        if ($listIdsDones) {
            $listIdsDones = array_filter($listIdsDones, 'ucfirst');
            sort($listIdsDones);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listIdsDones);
            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listIdsDones, $sendNotification, LifeNotifications::TYPE_ROOM);
        }

        if ($listIdsDonesPro) {
            $sendNotification = [
                'event' => Notification::UPDATE_ROOM,
                'created_by_id' => (int)$accountInfo['id'],
                'created_by_name' => $accountInfo['name'],
                'avatar' => $accountInfo['avatar'],
                'space_id' => (int)$room->space_id,
                'space_name' => !empty($space) ? $space->name : '',
                'room_id' => (int)$room->id,
                'room_name_new' => $room->name,
                'room_name_old' => $oldRoomName,
            ];
            $listIdsDonesPro = array_filter($listIdsDonesPro, 'ucfirst');
            sort($listIdsDonesPro);
            eFunction::pushNotifictions($sendNotification, $listIdsDonesPro);
            // Gọi đến function của class parent
            $this->notification($accountInfo, $listIdsDonesPro, $sendNotification, Notification::TYPE_ROOM);
        }
    }

    /* @todo
     * Xóa account khỏi room
     */
    public function pushNotiDeleteAccountInRoomWorkLife($requestData, $accountInfo)
    {
        $room = Room::find($requestData['roomId']);
        $space = Space::find($room->space_id);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::DELETE_ACCOUNT_ROOM,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$room->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$room->id,
            'room_name' => $room->name,
        ];

        $key = array_search($accountInfo['id'], $requestData['accountIds']);

        if (false !== $key) {
            unset($requestData['accountIds'][$key]);
        }

        sort($requestData['accountIds']);

        if (!empty($requestData['accountIds'])) {
            eFunction::pushNotifictionsWorkLife($sendNotification, $requestData['accountIds']);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $requestData['accountIds'], $sendNotification, LifeNotifications::TYPE_ROOM);
        }
    }

    /* @todo
     * Thông báo quickbar bar mới
     */
    public function pushNotiQuickBarEnableAllWorkLife($accountInfo, $applicationId, $ids, $spaceId)
    {
        $space = Space::find($spaceId);
        $application = Application::find($applicationId);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => Notification::QUICKBAR_ENABLE_ALL,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$spaceId,
            'space_name' => !empty($space) ? $space->name : '',
            'application_id' => !empty($application) ? (int)$application->id : '',
            'application_name' => !empty($application) ? $application->name : '',
        ];
        $idsDonePro = $ids[Base::DOENS_PRO];
        $idsDone = $ids[Base::DOENS];
        if (!empty($idsDonePro)) {
            $ids = $idsDonePro;
            $key = array_search($accountInfo['id'], $ids);
            if (false !== $key) {
                unset($ids[$key]);
            }
            sort($ids);
            $ids = array_filter($ids, 'ucfirst');
            sort($ids);
            eFunction::pushNotifictions($sendNotification, $ids);
            // Gọi đến function của class parent
            $this->notification($accountInfo, $ids, $sendNotification, Notification::TYPE_QUICKBAR);
        }
        if (!empty($idsDone)) {
            $sendNotification['event'] = LifeNotifications::QUICKBAR_ENABLE_ALL;
            $key = array_search($accountInfo['id'], $idsDone);
            if (false !== $key) {
                unset($ids[$key]);
            }
            sort($idsDone);
            $ids = array_filter($idsDone, 'ucfirst');
            sort($idsDone);
            eFunction::pushNotifictionsWorkLife($sendNotification, $idsDone);
            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $idsDone, $sendNotification, LifeNotifications::TYPE_QUICKBAR);
        }
    }

    /* @todo
     * Thông báo xóa quickbar
     */
    public function pushNotiDeleteQuickBarWorkLife($accountInfo, $applicationId, $ids, $spaceId)
    {
        $space = Space::find($spaceId);
        $application = Application::find($applicationId);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::QUICKBAR_DELETE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$spaceId,
            'space_name' => !empty($space) ? $space->name : '',
            'application_id' => !empty($application) ? (int)$application->id : '',
            'application_name' => !empty($application) ? $application->name : '',
        ];

        $idsDonePro = $ids[Base::DOENS_PRO];
        $idsDone = $ids[Base::DOENS];
        if (!empty($idsDone)) {
            $ids = $idsDone;
            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }
            $ids = array_filter($ids, 'ucfirst');
            sort($ids);
            eFunction::pushNotifictionsWorkLife($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $ids, $sendNotification, LifeNotifications::TYPE_QUICKBAR);
        }
        if (!empty($idsDone)) {
            $ids = $idsDonePro;
            $sendNotification['event'] = Notification::QUICKBAR_DELETE;

            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }

            sort($ids);

            eFunction::pushNotifictions($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notification($accountInfo, $ids, $sendNotification, Notification::TYPE_QUICKBAR);
        }
    }

    /* @todo
     * Thông báo cập nhật quickbar
     */
    public function pushNotiUpdateQuickbarWorkLife($accountInfo, $applicationId, $ids, $spaceId, $appOld)
    {
        $space = Space::find($spaceId);
        $application = Application::find($applicationId);
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::QUICKBAR_UPDATE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$spaceId,
            'space_name' => !empty($space) ? $space->name : '',
            'application_id' => !empty($application) ? (int)$application->id : '',
            'application_name_new' => !empty($application) ? $application->name : '',
            'application_name_old' => !empty($appOld) ? $appOld->name : '',
        ];
        $idsDonePro = $ids[Base::DOENS_PRO];
        $idsDone = $ids[Base::DOENS];
        if (!empty($idsDone)) {
            $ids = $idsDone;
            $key = array_search($accountInfo['id'], $ids);

            if (false !== $key) {
                unset($ids[$key]);
            }

            $ids = array_filter($ids, 'ucfirst');
            sort($ids);
            eFunction::pushNotifictionsWorkLife($sendNotification, $ids);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $ids, $sendNotification, LifeNotifications::TYPE_QUICKBAR);
        }
        if (!empty($idsDonePro)) {
            $sendNotification['event'] = Notification::QUICKBAR_UPDATE;
            $key = array_search($accountInfo['id'], $idsDonePro);
            if (false !== $key) {
                unset($ids[$key]);
            }
            sort($idsDonePro);
            $idsDonePro = array_filter($idsDonePro, 'ucfirst');
            sort($idsDonePro);
            eFunction::pushNotifictions($sendNotification, $idsDonePro);
            // Gọi đến function của class parent
            $this->notification($accountInfo, $idsDonePro, $sendNotification, Notification::TYPE_QUICKBAR);
        }
    }


    /* @todo
     * Thông Tạo mới ứng dụng
     */
    public function pushNotiCreateNewAppWorkLife($accountInfo, $application, $ownRoom)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);

        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => $room->name,
            'application_id' => (int)$application->id,
            'application_name' => $application->name,
        ];
        // Nếu là chủ room hoặc là admin gửi đến tất cả thành viên
        $isAdminRoom = RoomAccountsWorkLife::where([
            'account_id' => $accountInfo['id'],
            'is_admin' => 1,
            'room_id' => (int)$application->room_id,
            'space_id' => (int)$application->space_id,
        ])->first();
        //        if (!empty($ownRoom) && $ownRoom === (int)$accountInfo['id']) {
        if (!empty($isAdminRoom)) {
            $listIds = RoomAccountsWorkLife::where('room_id', $application->room_id)
                ->where('organization_id', $accountInfo['organization_id'])
                ->pluck('account_life_id')->unique()->values()->toArray();
            $listIdsPro = RoomAccountsWorkLife::where('room_id', $application->room_id)
                ->where('organization_id', $accountInfo['organization_id'])
                ->where('account_id', '<>', $accountInfo['id'])
                ->whereNotNull('account_id')
                ->pluck('account_id')->unique()->values()->toArray();
            $key = array_search($accountInfo['id'], $listIds);

            if (false !== $key) {
                unset($listIds[$key]);
            }

            if (!empty($application->account_id)) {
                $sendNotification['event'] = LifeNotifications::ADD_LINK_APP;

                eFunction::pushNotifictionsWorkLife($sendNotification, $application->account_id);

                // Gọi đến function của class parent
                $this->notificationWorkLife($accountInfo, $application->account_id, $sendNotification, LifeNotifications::TYPE_APP);

                $keyVal = array_search($application->account_id, $listIds);

                if (false !== $keyVal) {
                    unset($listIds[$keyVal]);
                }
            }
            $listIds = array_filter($listIds, 'ucfirst');
            sort($listIds);

            $sendNotification['event'] = LifeNotifications::CREATE_APP;
            if (!empty($listIds)) {
                eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);
                // Gọi đến function của class parent
                $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_APP);
            }
            //             gửi notifications đến account Dones Pro
            if ($listIdsPro) {
                $sendNotificationPro = [
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'space_id' => (int)$application->space_id,
                    'space_name' => !empty($space) ? $space->name : '',
                    'room_id' => (int)$application->room_id,
                    'room_name' => $room->name,
                    'application_id' => (int)$application->id,
                    'application_name' => $application->name,
                ];

                sort($listIdsPro);
                $sendNotificationPro['event'] = Notification::CREATE_APP;

                if (!empty($listIdsPro)) {
                    eFunction::pushNotifictions($sendNotificationPro, $listIdsPro);
                    // Gọi đến function của class parent
                    $this->notification($accountInfo, $listIdsPro, $sendNotificationPro, Notification::TYPE_APP);
                }
            }
        } else {
            if (!empty($application->account_id)) {
                $sendNotification['event'] = LifeNotifications::ADD_LINK_APP;
                eFunction::pushNotifictionsWorkLife($sendNotification, $application->account_id);

                // Gọi đến function của class parent
                $this->notificationWorkLife($accountInfo, $application->account_id, $sendNotification, LifeNotifications::TYPE_APP);
            }

            $sendNotification['event'] = LifeNotifications::APP_WAIT_APPOVE;
            $adminRoom = RoomAccountsWorkLife::where('space_id', $application->space_id)
                ->where('room_id', $application->room_id)
                ->where('is_admin', RoomAccount::IS_ADMIN)
                ->get()->pluck('account_life_id')->unique()->values()->toArray();
            array_push($adminRoom, $ownRoom);
            $listAccountIds = array_unique($adminRoom);
            $listAccountIds = array_filter($listAccountIds, 'ucfirst');
            sort($listAccountIds);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listAccountIds);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listAccountIds, $sendNotification, LifeNotifications::TYPE_APP);
        }
    }

    /* @todo
     * Xóa App
     */
    public function pushNotiDeleteAppWorkLife($accountInfo, $ids)
    {
        foreach ($ids as $id) {
            $application = Application::find($id);
            if (!empty($application) && $application->is_accept == Application::ACCEPT) {
                $room = Room::find($application->room_id);
                $space = Space::find($application->space_id);

                // Gửi thông báo realtime gọi đến socket
                $sendNotification = [
                    'event' => LifeNotifications::DELETE_APP,
                    'created_by_id' => (int)$accountInfo['id'],
                    'created_by_name' => $accountInfo['name'],
                    'avatar' => $accountInfo['avatar'],
                    'is_dones_life' => 0,
                    'space_id' => (int)$application->space_id,
                    'space_name' => !empty($space) ? $space->name : '',
                    'room_id' => (int)$application->room_id,
                    'room_name' => !empty($room) ? $room->name : '',
                    'application_id' => (int)$application->id,
                    'application_name' => $application->name,
                    'application_store_id' => $application->application_store_id,
                ];

                $listIds = RoomAccountsWorkLife::where('room_id', $application->room_id)
                    ->where('organization_id', $accountInfo['organization_id'])
                    ->whereNotNull('account_life_id')
                    ->pluck('account_life_id')->unique()->values()->toArray();
                $key = array_search($accountInfo['id'], $listIds);

                if (false !== $key) {
                    unset($listIds[$key]);
                }

                $listIds = array_filter($listIds, 'ucfirst');
                sort($listIds);
                if (!empty($listIds)) {
                    eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);

                    // Gọi đến function của class parent
                    $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_APP);
                }
                // }
            }
        }
    }

    /* @todo
     * Thông báo duyệt app
     */
    public function pushNotiApproveAppWorkLife($accountInfo, $application)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);

        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => $room->name,
            'application_id' => (int)$application->id,
            'application_name' => $application->name,
        ];

        // Thông báo đến người yêu cầu
        $sendNotification['event'] = LifeNotifications::APP_APPROVE;
        eFunction::pushNotifictionsWorkLife($sendNotification, $application->created_by);
        $this->notificationWorkLife($accountInfo, $application->created_by, $sendNotification, LifeNotifications::TYPE_APP);

        // Thông báo đến mọi người
        $listIds = RoomAccountsWorkLife::where('room_id', $application->room_id)->whereNotNull('account_life_id')->pluck('account_life_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        $keyVal = array_search($application->created_by, $listIds);

        if (false !== $keyVal) {
            unset($listIds[$keyVal]);
        }

        $sendNotification['event'] = LifeNotifications::CREATE_APP;

        if (!empty($listIds)) {
            $listIds = array_filter($listIds, 'ucfirst');
            sort($listIds);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_APP);
        }
        // }
    }

    /* @todo
     * Sửa App
     */
    public function pushNotiUpdateAppWorkLife($accountInfo, $application, $appOld)
    {
        $room = Room::find($application->room_id);
        $space = Space::find($application->space_id);

        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::UPDATE_APP,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => 0,
            'space_id' => (int)$application->space_id,
            'space_name' => !empty($space) ? $space->name : '',
            'room_id' => (int)$application->room_id,
            'room_name' => !empty($room) ? $room->name : '',
            'application_id' => (int)$application->id,
            'application_store_id' => (int)$application->application_store_id,
            'application_name_new' => $application->name,
            'application_name_old' => $appOld->name,
        ];

        $listIds = RoomAccountsWorkLife::where('room_id', $application->room_id)->whereNotNull('account_life_id')->pluck('account_life_id')->unique()->values()->toArray();

        $key = array_search($accountInfo['id'], $listIds);

        if (false !== $key) {
            unset($listIds[$key]);
        }

        if (!empty($listIds)) {
            $listIds = array_filter($listIds, 'ucfirst');
            sort($listIds);
            eFunction::pushNotifictionsWorkLife($sendNotification, $listIds);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $listIds, $sendNotification, LifeNotifications::TYPE_APP);
        }
    }

    // Thông báo cho người dùng bên LIFE sau khi đc admin duyệt vào space
    public function pushNotiApproveAccountJoinSpace($idSpace, $accountInfo, $idAccount, $is_accept)
    {
        $space = $this->getBlankSpace()->where('id', $idSpace)->first();
        // Gửi thông báo realtime gọi đến socket
        $sendNotification = [
            'event' => LifeNotifications::REPLY_JOIN_SPACE,
            'created_by_id' => (int)$accountInfo['id'],
            'created_by_name' => $accountInfo['name'],
            'avatar' => $accountInfo['avatar'],
            'is_dones_life' => Space::NOT_DONES_LIFE,
            'space_id' => !empty($space) ? (int)$space->id : (int)$space->id,
            'space_name' => !empty($space) ? $space->name : '',
            'is_accept' => (int)$is_accept,
        ];

        if (!empty($idAccount)) {
            eFunction::pushNotifictionsWorkLife($sendNotification, $idAccount);

            // Gọi đến function của class parent
            $this->notificationWorkLife($accountInfo, $idAccount, $sendNotification, Notification::TYPE_SPACE);
        }
    }

    // Xóa thông báo sau khi bắn event bên LIFE
    public function deleteNotificationWithEvent($space_id, $account_id, $type)
    {
        $q = $this->getBlankModelNoti();
        if (is_array($space_id)) {
            $q->whereIn('space_id', $space_id)->where('event', $type)->delete();
        } else {
            $q->where('space_id', $space_id)->where('created_by', $account_id)->where('event', $type)->delete();
        }
    }
}
