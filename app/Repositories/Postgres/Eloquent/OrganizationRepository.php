<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\OrganizationRepositoryInterface;
use \App\Models\Postgres\Organization;

class OrganizationRepository extends SingleKeyModelRepository implements OrganizationRepositoryInterface
{

    public function getBlankModel()
    {
        return new Organization();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getOneObjectOrganizationByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);
        $data = $query->first();

        return $data;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('organizations.id', $filter['id']);
            } else {
                $query = $query->where('organizations.id', $filter['id']);
            }
        }

        if (isset($filter['is_active'])) {
            $query = $query->where('organizations.is_active', $filter['is_active']);
        }

        if (isset($filter['major_id'])) {
            $query = $query->where('organizations.major_id', $filter['major_id']);
        }

        if (isset($filter['package_id'])) {
            $query = $query->where('organizations.package_id', $filter['package_id']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('organizations.deleted_at', null);
        }

    }

    public function getAllObjectOrganization()
    {
        $data = $this->getBlankModel()->get();

        return $data;
    }

    public function getIdOrganizationMandu($slug)
    {
        $organization = $this->getBlankModel()->where('slug', $slug)->first();
        if(!empty($organization)) {
            $organization = $organization->toArray();
            return $organization['id'];
        }
        return null;

    }
}
