<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\AccountAccessTokensRepositoryInterface;
use \App\Models\Postgres\AccountAccessTokens;
use \App\Models\Postgres\Account;
use \App\Models\Postgres\Image;

class AccountAccessTokensRepository extends SingleKeyModelRepository implements AccountAccessTokensRepositoryInterface
{

    public function getBlankModel()
    {
        return new AccountAccessTokens();
    }

    public function getModelAccount()
    {
        return new Account();
    }

    public function getModelImage()
    {
        return new Image();
    }

    public function clearAllTokenForUser($id)
    {
        $this->getBlankModel()->where('account_id', $id)->delete();
    }

    public function deleteToken($token)
    {
        $this->getBlankModel()->where('token', $token)->delete();
    }

    public function getIdForToken($token)
    {
        $account = $this->getBlankModel()->where('token', $token)->first();

        if (!empty($account)) {
            return $account->account_id;
        }

        return false;
    }

    public function createTokenAccount($account, $dataToken)
    {
        if (empty($account['id'])) {
            $account = $this->getModelAccount()->create($account);

            $code = md5(\Hash::make($account['id'] . $account['email'] . time() . mt_rand()));
            $data = [
                'account_id' => $account['id'],
                'token' => $code,
                'last_used_at' => now(),
                'ip_used' => $dataToken['ip'],
                'location' => $dataToken['location'],
                'device_name' => $dataToken['device_name'],
                'user_agent' => $dataToken['user_agent'],
            ];

            if (!empty($account['profile_image_id'])) {
                $profile_image = $this->getModelImage()->find($account['profile_image_id']);
                if (!empty($profile_image)) {
                    $account['avatar'] = $profile_image->source;
                }
            }

            if (!empty($account['background_image_id'])) {
                $profile_image = $this->getModelImage()->find($account['background_image_id']);
                if (!empty($profile_image)) {
                    $account['cover'] = $profile_image->source;
                }
            }

            $this->getBlankModel()->create($data);
            $account['token'] = $code;
        } else {
            $code = md5(\Hash::make($account['id'] . $account['email'] . time() . mt_rand()));
            $data = [
                'account_id' => $account['id'],
                'token' => $code,
                'last_used_at' => now(),
                'ip_used' => $dataToken['ip'],
                'location' => $dataToken['location'],
                'device_name' => $dataToken['device_name'],
                'user_agent' => $dataToken['user_agent'],
            ];

            if (!empty($account['profile_image_id'])) {
                $profile_image = $this->getModelImage()->find($account['profile_image_id']);
                if (!empty($profile_image)) {
                    $account['avatar'] = $profile_image->source;
                }
            }

            if (!empty($account['background_image_id'])) {
                $profile_image = $this->getModelImage()->find($account['background_image_id']);
                if (!empty($profile_image)) {
                    $account['cover'] = $profile_image->source;
                }
            }

            $this->getBlankModel()->create($data);
            $account['token'] = $code;
        }
        return $account;
    }
}
