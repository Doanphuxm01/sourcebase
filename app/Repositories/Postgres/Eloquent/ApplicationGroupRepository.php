<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationGroupRepositoryInterface;
use \App\Models\Postgres\ApplicationGroup;

class ApplicationGroupRepository extends SingleKeyModelRepository implements ApplicationGroupRepositoryInterface
{

    public function getBlankModel()
    {
        return new ApplicationGroup();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function getOneArrayApplicationGroupByFilter($filter)
    {
        $query = $this->withApplications();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function deleteAllApplicationGroupByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function getAllGroupApplicationByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function getOneObjectApplicationGroupByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    private function withApplications()
    {
        $query = $this->getBlankModel()->select([
            "application_groups.id",
            "application_groups.name",
            "application_groups.is_active",
            "application_groups.room_id",
            "application_groups.space_id",
        ])
            ->with(['applications' => function ($query) {
                $query->select([
                    "applications.id",
                    "applications.name",
                    "applications.application_image_id",
                    "applications.application_group_id",
                    "applications.link_android",
                    "applications.link_ios",
                    "applications.link_url",
                    "applications.is_accept",
                    "applications.application_store_id",
                ])->with(['applicationStore'  => function ($query) {
                    $query->with('createdBy');
                }]);
            }]);
        return $query;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('application_groups.id', $filter['id']);
            } else {
                $query = $query->where('application_groups.id', $filter['id']);
            }
        }

        if (isset($filter['room_id'])) {
            if (is_array($filter['room_id'])) {
                $query = $query->whereIn('application_groups.room_id', $filter['room_id']);
            } else {
                $query = $query->where('application_groups.room_id', $filter['room_id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('application_groups.space_id', $filter['space_id']);
            } else {
                $query = $query->where('application_groups.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('application_groups.created_by', $filter['created_by']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('application_groups.deleted_at', null);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('application_groups.organization_id', $filter['organization_id']);
        }
    }
}
