<?php

namespace App\Repositories\Postgres\Eloquent;

use App\Elibs\Debug;
use App\Models\Base;
use App\Models\Postgres\AccountLife;
use App\Models\Postgres\AccountLifeWork;
use App\Models\Postgres\RoomAccountsWorkLife;
use App\Models\Postgres\SpaceAccountLifeWork;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\SpaceRepositoryInterface;
use \App\Models\Postgres\Space;
use Illuminate\Support\Str;
use Exception;
use App\Elibs\eResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\Room;
use App\Models\Postgres\Application;
use App\Models\Postgres\Account;
use App\Models\Postgres\StatictisSpace;
use Carbon\Carbon;
use App\Models\Postgres\SpaceAccount;
use App\Models\Postgres\RoomAccount;
use App\Models\Postgres\AccountGroup;
use App\Models\Postgres\QuickBar;
use App\Elibs\eFunction;
use App\Models\Postgres\Notification;
use App\Models\Postgres\SpaceGroup;
use App\Models\Postgres\StatisticApplication;
use App\Models\Postgres\LifeNotifications;
use App\Models\Postgres\Major;

class SpaceRepository extends NotificationRepository implements SpaceRepositoryInterface
{

    public function getBlankModel()
    {
        return new Space();
    }

    public function getBlankModelRoom()
    {
        return new Room();
    }

    public function getBlankModelRoomAccount()
    {
        return new RoomAccount();
    }

    public function getBlankModelRoomAccountWorkLife()
    {
        return new RoomAccountsWorkLife();
    }

    public function getBlankModelAccount()
    {
        return new Account();
    }

    public function getBlankModelApp()
    {
        return new Application();
    }

    public function getBlankSpaceAccount()
    {
        return new SpaceAccount();
    }

    public function getBlankSpaceAccountLifeWork()
    {
        return new SpaceAccountLifeWork();
    }


    public function getBlankModelQuickBar()
    {
        return new QuickBar();
    }

    public function getBlankModelStatictis()
    {
        return new StatictisSpace();
    }

    public function getModelAccountLife()
    {
        return new AccountLife();
    }


    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function accountLeaveSpace($id, $accountInfo)
    {
        $space = $this->getBlankModel()->where('id', $id)->first();
        if ($accountInfo['id'] == $space->created_by) {
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-fail'), []);
        }
        $this->getBlankSpaceAccount()->where('space_id', $id)->where('account_id', $accountInfo['id'])->delete();
        $this->getBlankModelQuickBar()->where('space_id', $id)->where('account_id', $accountInfo['id'])->delete();
        $this->getBlankModelRoomAccount()->where('space_id', $id)->where('account_id', $accountInfo['id'])->delete();
        if ($id == $accountInfo['main_space']) {
            $spaceDones = $this->getBlankSpaceAccount()->select('space_id')->where('account_id', $accountInfo['id'])->first();
            if (!empty($spaceDones)) {
                $data = [
                    'main_space' => $spaceDones->space_id,
                ];
            }
            $this->getBlankModelAccount()->where('id', $accountInfo['id'])->update([
                'main_space' => null,
            ]);
            return $data;
        }
        $data = [
            'main_space' => $accountInfo['main_space'],
        ];
        return $data;
    }

    public function listSpaceWaitApprove($accountInfo)
    {
        $spaceIds = $this->getBlankSpaceAccountLifeWork()->where('account_id', $accountInfo['id'])->where('is_admin', SpaceAccountLifeWork::IS_ADMIN)->get()->pluck('space_id')->toArray();
        $checkSpace = $this->getBlankSpaceAccountLifeWork()->whereIn('space_id', $spaceIds)->where('is_accept', SpaceAccountLifeWork::NOT_ACCEPT)->get()->pluck('space_id')->toArray();
        $listSpace = $this->getBlankModel()->whereIn('id', $checkSpace)->select('id', 'name')->get();
        return $listSpace;
    }

    public function listAccountWaitApprove($accountInfo, $data)
    {
        $id = $data['id'] ?? null;
        $order_by = $data['order_by'] ?? 'desc';
        $key_word = $data['key_word'] ?? null;
        $limit = $data['limit'] ?? 10;

        $paginate = $this->getModelAccountLife()
            ->leftJoin('life_images', 'life_images.id', '=', 'life_accounts.profile_image_id')
            ->join('space_accounts_work_life', 'space_accounts_work_life.account_life_id', '=', 'life_accounts.id')
            ->whereNull('life_images.deleted_at')
            ->whereNull('life_accounts.deleted_at')
            ->where('space_accounts_work_life.space_id', $id)
            ->where('space_accounts_work_life.is_accept', SpaceAccountLifeWork::NOT_ACCEPT);

        if ($key_word) {
            $paginate = $paginate->where(DB::raw('vn_unaccent(life_accounts.name)'), 'LIKE', '%' . Str::ascii(Str::lower($key_word)) . '%');
        }

        $paginate = $paginate
            ->select(
                'life_accounts.id',
                'life_accounts.name',
                'life_accounts.username',
                'life_accounts.email',
                'life_accounts.phone_number',
                'life_accounts.language',
                'life_accounts.birthday',
                'life_accounts.gender',
                'life_accounts.profile_image_id',
                'life_accounts.background_image_id',
                'life_images.source as avatar',
                'life_images.source_thumb as avatar_thumb',
                'space_accounts_work_life.created_at',
            )
            ->orderBy('space_accounts_work_life.created_at', $order_by)->paginate($limit)->toArray();
        $paginate['data'] = array_map(function ($val) {
            $val['avatar'] = !empty($val['avatar']) && file_exists(public_path() . '/' . $val['avatar']) ? asset($val['avatar']) : '';
            $val['avatar_thumb'] = !empty($val['avatar_thumb']) && file_exists(public_path() . '/' . $val['avatar_thumb']) ? asset($val['avatar_thumb']) : '';
            return $val;
        }, $paginate['data']);

        return $paginate;
    }

    public function findById($id, $queryString)
    {
        $accountInfo = $queryString['accountInfo'];
        $limit = $queryString["limit"] ?? null;
        $keyWord = $queryString['keyWord'] ?? null;
        $query = $this->getBlankModel()->with(['createdBy' => function ($q) {
            $q->select('id', 'name');
        }])->with(['major' => function ($q) {
            $q->select('id', 'name');
        }])->with(['organization' => function ($q) {
            $q->select('id', 'name');
        }])->with(['spaceImage' => function ($q) {
            $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
        }])->select('id', 'name', 'slug', 'created_at', 'major_id', 'space_image_id', 'type', 'created_by', 'is_show')->withCount('account')->find($id);
        // ->with(['account' => function ($q) use ($limit, $keyWord) {
        //     $q = $q->with(['managers' => function ($q) {
        //         $q->select('accounts.id', 'accounts.name');
        //     }])->with(['profileImage' => function ($q) {
        //         $q = $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
        //     }]);
        //     if ($keyWord) {
        //         $q = $q->where(DB::raw('lower(slug)'), 'like', '%' . strtolower($keyWord) . '%')->orWhere(DB::raw('lower(email)'), 'like', '%' . strtolower($keyWord) . '%')->orWhere('phone_number', 'like', '%' . $keyWord . '%');
        //     }
        //     if ($limit) {
        //         $q = $q->paginate($limit, ['accounts.id', 'name', 'email', 'phone_number', 'profile_image_id', 'is_active']);
        //     } else {
        //         $q = $q->select('accounts.id', 'name', 'email', 'phone_number', 'profile_image_id', 'is_active')->get();
        //     }
        // }])

        if ($accountInfo['main_space'] == $id) {
            $query->main_space = 1;
        } else {
            $query->main_space = 0;
        }

        if (!empty($queryString['account_id'])) {
            if ($queryString['account_id'] == $query['created_by']) {
                $query['is_admin_space'] = SpaceAccount::IS_ADMIN;
            } else {
                // $checkTypeSpace = $this->checkSpaceType(null, $id);
                // if ($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
                //     $isAdminSpace = SpaceAccountLifeWork::where('account_id', $queryString['account_id'])->where('space_id', $id)->first();
                //     if (!empty($isAdminSpace)) {
                //         $query['is_admin_space'] = $isAdminSpace->is_admin;
                //     }
                // } else {
                $isAdminSpace = SpaceAccount::where('account_id', $queryString['account_id'])->where('space_id', $id)->first();
                if (!empty($isAdminSpace)) {
                    $query['is_admin_space'] = $isAdminSpace->is_admin;
                }
                // }
            }
        }

        $accountInSpace = $this->getBlankSpaceAccount()->where('space_accounts.space_id', $id)
            ->leftJoin('accounts', 'space_accounts.account_id', '=', 'accounts.id')
            ->leftJoin('images', 'accounts.profile_image_id', '=', 'images.id')
            ->with(['accounts' => function ($q) {
                $q->select('accounts.id')->with(['managers' => function ($q) {
                    $q->select('accounts.id', 'accounts.name');
                }]);
            }])
            // ->leftJoin(DB::raw('(SELECT management_accounts.*, images.source as manager_avatar, accounts.name as manager_name FROM management_accounts LEFT JOIN accounts ON management_accounts.manager_id = accounts.id LEFT JOIN images ON accounts.profile_image_id = images.id) management_accounts'), function ($join) {
            //     $join->on('management_accounts.account_id', '=', 'accounts.id');
            // })
            ->select(
                'space_accounts.account_id',
                'accounts.id',
                'accounts.name',
                'accounts.email',
                'accounts.phone_number',
                'space_accounts.is_admin',
                'accounts.profile_image_id',
                'accounts.is_active',
                'images.id as image_id',
                'images.source',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
                'accounts.main_space',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
                'accounts.main_space',
                'accounts.type_account',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
            )
            ->whereNull('accounts.deleted_at');

        if ($keyWord) {
            $accountInfo = $queryString['accountInfo'];
            $accountInSpace = $accountInSpace->where(function ($q) use ($keyWord) {
                $q->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere(DB::raw('vn_unaccent(accounts.email)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere('accounts.phone_number', 'like', '%' . $keyWord . '%');
            })
                ->where('accounts.organization_id', $accountInfo['organization_id']);
        }
        if ($limit) {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->paginate(10)->toArray();
            sort($accountInSpace['data']);
            $query['account'] = $accountInSpace['data'];
        } else {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->get()->toArray();
            $query['account'] = $accountInSpace;
        }

        if (!empty($query->spaceImage)) {
            $query->spaceImage['source_thumb'] = !empty($query->spaceImage['source_thumb']) ? asset($query->spaceImage['source_thumb']) : '';
            $query->spaceImage['source'] = !empty($query->spaceImage['source']) && file_exists(public_path() . '/' . $query->spaceImage['source']) ? asset($query->spaceImage['source']) : '';
        }

        if (!empty($query['account'])) {
            // $query->account->map(function ($val) {
            //     unset($val['pivot']);
            //     if (!empty($val->profileImage)) {
            //         $val->profileImage['source_thumb'] = !empty($val->profileImage['source_thumb']) ? asset($val->profileImage['source_thumb']) : '';
            //         $val->profileImage['source'] = !empty($val->profileImage['source']) ? asset($val->profileImage['source']) : '';
            //     }
            //     return $val;
            // });

            $query['account'] = array_map(function ($val) {
                $val['type'] = Account::DONES_PRO;
                if (!empty($val['source']) && file_exists(public_path() . '/' . $val['source'])) {
                    $val['source'] = asset($val['source']);
                }
                if (!empty($val['accounts']['managers'])) {
                    $val['managers'] = $val['accounts']['managers'];
                } else {
                    $val['managers'] = [];
                }
                unset($val['accounts']);
                return $val;
            }, $query['account']);
        }
        $idsAdminSpace = $this->getAdminSpace($id);
        $query[Space::IS_ADMIN_TEXT] = 0;
        if (!empty($idsAdminSpace)) {
            if (in_array($accountInfo['id'], (array)$idsAdminSpace)) {
                $query[Space::IS_ADMIN_TEXT] = 1;
            }
        }
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function findBySpaceIdAndQuickbar($id, $queryString)
    {
        $accountInfo = $queryString['accountInfo'];
        $limit = $queryString["limit"] ?? null;
        $keyWord = $queryString['keyWord'] ?? null;
        $query = $this->getBlankModel()->with(['createdBy' => function ($q) {
            $q->select('id', 'name');
        }])->with(['major' => function ($q) {
            $q->select('id', 'name');
        }])->with(['organization' => function ($q) {
            $q->select('id', 'name');
        }])->with(['spaceImage' => function ($q) {
            $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
        }])->select(
            'id',
            'name',
            'slug',
            'created_at',
            'major_id',
            'space_image_id',
            'type',
            'created_by',
            'is_show'
        )
            ->withCount('account')
            ->find($id);

        if ($accountInfo['main_space'] == $id) {
            $query->main_space = 1;
        } else {
            $query->main_space = 0;
        }

        if (!empty($queryString['account_id'])) {
            if ($queryString['account_id'] == $query['created_by']) {
                $query['is_admin_space'] = SpaceAccount::IS_ADMIN;
            } else {
                // $checkTypeSpace = $this->checkSpaceType(null, $id);
                // if ($checkTypeSpace == Space::TYPE_SPACE_CUSTOMER) {
                //     $isAdminSpace = SpaceAccountLifeWork::where('account_id', $queryString['account_id'])->where('space_id', $id)->first();
                //     if (!empty($isAdminSpace)) {
                //         $query['is_admin_space'] = $isAdminSpace->is_admin;
                //     }
                // } else {
                $isAdminSpace = SpaceAccount::where('account_id', $queryString['account_id'])->where('space_id', $id)->first();
                if (!empty($isAdminSpace)) {
                    $query['is_admin_space'] = $isAdminSpace->is_admin;
                }
                // }
            }
        }

        $accountInSpace = $this->getBlankSpaceAccount()->where('space_accounts.space_id', $id)
            ->leftJoin('accounts', 'space_accounts.account_id', '=', 'accounts.id')
            ->leftJoin('images', 'accounts.profile_image_id', '=', 'images.id')
            ->with(['accounts' => function ($q) {
                $q->select('accounts.id')->with(['managers' => function ($q) {
                    $q->select('accounts.id', 'accounts.name');
                }]);
            }])
            // ->leftJoin(DB::raw('(SELECT management_accounts.*, images.source as manager_avatar, accounts.name as manager_name FROM management_accounts LEFT JOIN accounts ON management_accounts.manager_id = accounts.id LEFT JOIN images ON accounts.profile_image_id = images.id) management_accounts'), function ($join) {
            //     $join->on('management_accounts.account_id', '=', 'accounts.id');
            // })
            ->select(
                'space_accounts.account_id',
                'accounts.id',
                'accounts.name',
                'accounts.email',
                'accounts.phone_number',
                'space_accounts.is_admin',
                'accounts.profile_image_id',
                'accounts.is_active',
                'images.id as image_id',
                'images.source',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
                'accounts.main_space',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
                'accounts.main_space',
                'accounts.type_account',
                // 'management_accounts.manager_id',
                // 'manager_name',
                // 'manager_avatar',
            )
            ->whereNull('accounts.deleted_at');

        if ($keyWord) {
            $accountInfo = $queryString['accountInfo'];
            $accountInSpace = $accountInSpace->where(function ($q) use ($keyWord) {
                $q->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere(DB::raw('vn_unaccent(accounts.email)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere('accounts.phone_number', 'like', '%' . $keyWord . '%');
            })
                ->where('accounts.organization_id', $accountInfo['organization_id']);
        }
        if ($limit) {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->paginate($limit)->toArray();
            sort($accountInSpace['data']);
            $query['account'] = $accountInSpace['data'];
        } else {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->get()->toArray();
            $query['account'] = $accountInSpace;
        }

        if (!empty($query->spaceImage)) {
            $query->spaceImage['source_thumb'] = !empty($query->spaceImage['source_thumb']) ? asset($query->spaceImage['source_thumb']) : '';
            $query->spaceImage['source'] = !empty($query->spaceImage['source']) && file_exists(public_path() . '/' . $query->spaceImage['source']) ? asset($query->spaceImage['source']) : '';
        }

        if (!empty($query['account'])) {
            // $query->account->map(function ($val) {
            //     unset($val['pivot']);
            //     if (!empty($val->profileImage)) {
            //         $val->profileImage['source_thumb'] = !empty($val->profileImage['source_thumb']) ? asset($val->profileImage['source_thumb']) : '';
            //         $val->profileImage['source'] = !empty($val->profileImage['source']) ? asset($val->profileImage['source']) : '';
            //     }
            //     return $val;
            // });

            $query['account'] = array_map(function ($val) {
                $val['type'] = Account::DONES_PRO;
                if (!empty($val['source']) && file_exists(public_path() . '/' . $val['source'])) {
                    $val['source'] = asset($val['source']);
                }
                if (!empty($val['accounts']['managers'])) {
                    $val['managers'] = $val['accounts']['managers'];
                } else {
                    $val['managers'] = [];
                }
                unset($val['accounts']);
                return $val;
            }, $query['account']);
        }

        return $query;
    }

    public function finByIdLifeWork($id, $queryString)
    {
        $accountInfo = $queryString['accountInfo'];
        $limit = $queryString["limit"] ?? null;
        $keyWord = $queryString['keyWord'] ?? null;
        $query = $this->getBlankModel()->with(['createdBy' => function ($q) {
            $q->select('id', 'name');
        }])->with(['major' => function ($q) {
            $q->select('id', 'name');
        }])->with(['organization' => function ($q) {
            $q->select('id', 'name');
        }])->with(['spaceImage' => function ($q) {
            $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
        }])->select('id', 'name', 'slug', 'created_at', 'major_id', 'space_image_id', 'type', 'created_by', 'is_show')->withCount('account')->find($id);

        if ($accountInfo['main_space'] == $id) {
            $query->main_space = 1;
        } else {
            $query->main_space = 0;
        }

        $accountInSpace = $this->getBlankSpaceAccountLifeWork()->where('' . SpaceAccountLifeWork::table_name . '.space_id', $id)
            ->leftJoin('accounts', '' . SpaceAccountLifeWork::table_name . '.account_id', '=', 'accounts.id')
            ->leftJoin('life_accounts', '' . SpaceAccountLifeWork::table_name . '.account_life_id', '=', 'life_accounts.id')
            ->leftJoin('images', 'accounts.profile_image_id', '=', 'images.id')
            ->with(['accounts' => function ($q) {
                $q->select('accounts.id')->with(['managers' => function ($q) {
                    $q->select('accounts.id', 'accounts.name');
                }]);
            }])
            ->select(
                '' . SpaceAccountLifeWork::table_name . '.account_id',
                '' . SpaceAccountLifeWork::table_name . '.account_life_id',
                DB::raw('CASE WHEN '
                    . SpaceAccountLifeWork::table_name . '.account_life_id
                     IS NULL THEN
                      accounts.name
                       ELSE
                       life_accounts.name
                        END'),
                DB::raw('CASE WHEN ' . SpaceAccountLifeWork::table_name . '.account_life_id IS NULL THEN accounts.email ELSE life_accounts.email END'),
                DB::raw('CASE WHEN ' . SpaceAccountLifeWork::table_name . '.account_life_id IS NULL THEN accounts.phone_number ELSE life_accounts.phone_number END'),
                DB::raw('CASE WHEN ' . SpaceAccountLifeWork::table_name . '.account_life_id IS NULL THEN accounts.is_active ELSE life_accounts.is_active END'),


                //                'accounts.id',
                //                'accounts.email',
                //                'accounts.phone_number',
                'space_accounts_work_life.is_admin',
                //                'accounts.profile_image_id',
                //                'accounts.is_active',
                'images.id as image_id',
                'images.source'
            );
        if ($keyWord) {
            $accountInfo = $queryString['accountInfo'];
            $accountInSpace = $accountInSpace->where(function ($q) use ($keyWord) {
                $q->orWhere(DB::raw('vn_unaccent(accounts.name)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere(DB::raw('vn_unaccent(accounts.email)'), 'like', '%' . Str::lower(Str::ascii($keyWord)) . '%')
                    ->orWhere('accounts.phone_number', 'like', '%' . $keyWord . '%');
            })
                ->where('accounts.organization_id', $accountInfo['organization_id']);
        }
        if ($limit) {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->paginate(10)->toArray();
            sort($accountInSpace['data']);
            $query['account'] = $accountInSpace['data'];
        } else {
            $accountInSpace = $accountInSpace->orderBy('accounts.id', 'asc')->get()->toArray();
            $query['account'] = $accountInSpace;
        }

        if (!empty($query->spaceImage)) {
            $query->spaceImage['source_thumb'] = !empty($query->spaceImage['source_thumb']) ? asset($query->spaceImage['source_thumb']) : '';
            $query->spaceImage['source'] = !empty($query->spaceImage['source']) && file_exists(public_path() . '/' . $query->spaceImage['source']) ? asset($query->spaceImage['source']) : '';
        }
        if (!empty($query['account'])) {
            $query['account_count'] = count($query['account']);
            $query['account'] = array_map(function ($val) {
                if (isset($val['account_id'])) {
                    $val['type'] = Account::DONES_PRO;
                }
                if (isset($val['account_life_id'])) {
                    $val['type'] = Account::DONES;
                }
                if (!empty($val['source']) && file_exists(public_path() . '/' . $val['source'])) {
                    $val['source'] = asset($val['source']);
                }
                if (!empty($val['accounts']['managers'])) {
                    $val['managers'] = $val['accounts']['managers'];
                } else {
                    $val['managers'] = [];
                }
                unset($val['accounts']);
                return $val;
            }, $query['account']);
            //            $query['account'] = array_map("unserialize", array_unique(array_map("serialize", $query['account'])));
        }
        $idsAdminSpace = $this->getAdminSpace($id);
        $query[Space::IS_ADMIN_TEXT] = 0;
        if (!empty($idsAdminSpace)) {
            if (in_array($accountInfo['id'], (array)$idsAdminSpace)) {
                $query[Space::IS_ADMIN_TEXT] = 1;
            }
        }
        //        $query['account'] = array_values(array_unique($query['account'], SORT_REGULAR));
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function listSpace($queryString, $accountInfo)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 10;
        $startDate = $queryString['startDate'] ?? null;
        $endDate = $queryString['endDate'] ?? null;
        $type = $queryString['type'] ?? null;
        $majorId = $queryString['majorId'] ?? null;
        $spaceName = $queryString['spaceName'] ?? null;

        $query = $this->getBlankModel()
            ->join('space_accounts', 'spaces.id', '=', 'space_accounts.space_id')
            ->where('space_accounts.account_id', $accountInfo['id'])
            ->withCount('account')
            ->withCount('room')
            ->with(['createdBy' => function ($q) {
                $q->select('id', 'name');
            }])->with(['major' => function ($q) {
                $q->select('id', 'name');
            }])->with(['organization' => function ($q) {
                $q->select('id', 'name');
            }])->with(['spaceImage' => function ($q) {
                $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
            }]);

        if ($startDate && $endDate) {
            $query = $query->whereDate('spaces.created_at', '>=', $startDate)->whereDate('spaces.created_at', '<=', $endDate);
        }

        if ($spaceName) {
            $query = $query->where('spaces.slug', 'like', '%' . eFunction::generateSlug($spaceName, '-') . '%');
        }

        if ($type) {
            $query = $query->where('spaces.type', $type);
        }

        if ($majorId) {
            $query = $query->where('spaces.major_id', $majorId);
        }

        $query = $query->where('spaces.organization_id', $accountInfo['organization_id'])->orderBy('spaces.created_at', 'desc')->paginate($limit)->toArray();
        $query['data'] = array_map(function ($val) use ($accountInfo) {
            if ($accountInfo['main_space'] == $val['id']) {
                $val['main_space'] = 1;
            } else {
                $val['main_space'] = 0;
            }
            $val['space_image']['source_thumb'] = !empty($val['space_image']['source_thumb']) ? asset($val['space_image']['source_thumb']) : '';
            $val['space_image']['source'] = !empty($val['space_image']['source']) && file_exists(public_path() . '/' . $val['space_image']['source']) ? asset($val['space_image']['source']) : '';
            return $val;
        }, $query['data']);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function getAllListSpace($queryString, $accountInfo)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 10;
        $startDate = $queryString['startDate'] ?? null;
        $endDate = $queryString['endDate'] ?? null;
        $type = $queryString['type'] ?? null;
        $majorId = $queryString['majorId'] ?? null;
        $spaceName = $queryString['spaceName'] ?? null;
        $organizationId = $queryString['organization_id'] ?? null;
        $query = $this->getBlankModel()
            ->select(
                'spaces.id',
                'spaces.name',
                'spaces.slug',
                'spaces.type',
                'spaces.major_id',
                'spaces.space_image_id',
                'spaces.organization_id',
                'spaces.created_by',
                'spaces.is_default',
                'spaces.is_public',
                'spaces.is_show',
                'space_accounts.is_admin',
                'spaces.created_at',
            )
            ->leftJoin('space_accounts', function ($query) use ($accountInfo) {
                $query->on('spaces.id', '=', 'space_accounts.space_id');
            })
            ->where('space_accounts.account_id', $accountInfo['id'])
            ->where('space_accounts.is_accept', Account::IS_ACTIVE)
            ->withCount(['account' => function ($q) {
                $q->where('space_accounts.is_accept', Account::IS_ACTIVE);
            }])
            ->withCount('room')
            ->with(['createdBy' => function ($q) {
                $q->select('id', 'name');
            }])->with(['major' => function ($q) {
                $q->select('id', 'name', 'slug');
            }])->with(['organization' => function ($q) {
                $q->select('id', 'name');
            }])->with(['spaceImage' => function ($q) {
                $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
            }]);

        if ($startDate && $endDate) {
            $query = $query->whereDate('spaces.created_at', '>=', $startDate)->whereDate('spaces.created_at', '<=', $endDate);
        }

        if ($spaceName) {
            $query = $query->where('spaces.slug', 'like', '%' . eFunction::generateSlug($spaceName, '-') . '%');
        }

        if ($type) {
            $query = $query->where('spaces.type', $type);
        }

        if ($majorId) {
            $query = $query->where('spaces.major_id', $majorId);
        }

        if (!empty($organizationId)) {
            $query = $query->where('spaces.organization_id', $organizationId);
        }

        $query = $query
            ->orderBy('spaces.created_at', 'desc')->distinct()->paginate($limit)->toArray();

        $query['data'] = array_map(function ($val) use ($accountInfo) {
            if ($accountInfo['main_space'] == $val['id']) {
                $val['main_space'] = 1;
            } else {
                $val['main_space'] = 0;
            }
            $val['space_image']['source_thumb'] = !empty($val['space_image']['source_thumb']) ? asset($val['space_image']['source_thumb']) : '';
            $val['space_image']['source'] = !empty($val['space_image']['source']) && file_exists(public_path() . '/' . $val['space_image']['source']) ? asset($val['space_image']['source']) : '';
            // if ($accountInfo['language'] == 'en') {
            //     $val['major']['name'] = array_values(Major::LANGUAGE_MAJOR[$val['major']['slug']]);
            //     $val['major']['name'] = $val['major']['name'][0];
            //     unset($val['major']['slug']);
            // }
            return $val;
        }, $query['data']);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function getAdminSpace($idSpace)
    {
        $queryIsAdmin = $this->getBlankSpaceAccount()->where([
            'space_id' => $idSpace,
            'is_admin' => Space::IS_ADMIN
        ])->whereNotNull('account_id')->pluck('account_id')->toArray();
        return $queryIsAdmin;
    }

    public function createSpaceAfterRegister($id, $data, $organization)
    {
        try {
            DB::beginTransaction();
            $space = $this->getBlankModel();
            $space->name = $data['organization_name'];
            $space->slug = Str::slug($data['organization_name']);
            $space->type = Space::TYPE_SPACE_INTERNAL;
            $space->major_id = $data['major_id'];
            $space->is_default = Space::DEFAULT_SPACE;
            $space->organization_id = $organization['id'];
            $space->created_by = $id;
            $space->save();
            $space->account()->sync(
                $id,
                [
                    'organization_id' => $organization['id'],
                    'is_admin' => SpaceAccount::IS_ADMIN,
                ]
            );
            $space->account()->updateExistingPivot(
                $id,
                [
                    'organization_id' => $organization['id'],
                    'is_admin' => SpaceAccount::IS_ADMIN,
                ]
            );
            DB::commit();
            return $space;
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return false;
        }
    }

    public function updateVisit($idSpace, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $now = now();
            $visited = $this->getBlankModelStatictis()::where('space_id', $idSpace)->whereDate('created_at', $now)->first();
            if ($visited) {
                $visited->visited += 1;
            } else {
                $visited = $this->getBlankModelStatictis();
                $visited->space_id = $idSpace;
                $visited->visited = 1;
                $visited->organization_id = $accountInfo['organization_id'];
            }
            $visited->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return false;
        }
    }

    public function statictis($query = null, $accountInfo)
    {
        $spaceId = $query['spaceId'] ?? null;
        $roomId = $query['roomId'] ?? null;
        $startDate = $query['startDate'] ?? null;
        $endDate = $query['endDate'] ?? null;

        $data = [];

        if ($accountInfo['is_root'] == Account::IS_ROOT) {
            $totalSpace = $this->getBlankModel()->where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $totalRoom = Room::where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $totalApplication = Application::where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $totalAccount = Account::where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $view = $this->getBlankModelStatictis()->with(['space' => function ($q) {
                $q->select('id', 'name', 'slug', 'type', 'major_id', 'organization_id', 'created_by');
            }])->where('statictis_spaces.organization_id', $accountInfo['organization_id'])
                ->join('spaces', 'spaces.id', '=', 'statictis_spaces.space_id')
                ->where('spaces.deleted_at', null);
            $applicationVisited = StatisticApplication::with(['application' => function ($q) {
                $q->select('id', 'name', 'slug', 'type', 'organization_id', 'created_by', 'link_android', 'link_ios', 'link_url');
            }])->where('statistic_applications.organization_id', $accountInfo['organization_id'])
                ->join('applications', 'applications.id', '=', 'statistic_applications.application_id')
                ->where('applications.deleted_at', null);
        } else {
            $totalSpace = $this->getBlankModel()->where('organization_id', $accountInfo['organization_id'])->where('created_by', $accountInfo['id'])->where('deleted_at', null);
            $totalRoom = Room::where('created_by', $accountInfo['id'])->where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $totalApplication = Application::where('created_by', $accountInfo['id'])->where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $totalAccount = Account::where('created_by', $accountInfo['id'])->where('organization_id', $accountInfo['organization_id'])->where('deleted_at', null);
            $view = $this->getBlankModelStatictis()->with(['space' => function ($q) use ($accountInfo) {
                $q->select('id', 'name', 'slug', 'type', 'major_id', 'organization_id', 'created_by');
            }])->where('statictis_spaces.organization_id', $accountInfo['organization_id'])
                ->join('spaces', 'spaces.id', '=', 'statictis_spaces.space_id')
                ->join('space_accounts', 'space_accounts.space_id', '=', 'spaces.id')
                ->where('spaces.deleted_at', null)
                ->where('space_accounts.account_id', $accountInfo['id']);
            $applicationVisited = StatisticApplication::with(['application' => function ($q) use ($accountInfo) {
                $q->select('id', 'name', 'slug', 'type', 'organization_id', 'created_by', 'link_android', 'link_ios', 'link_url');
            }])->where('statistic_applications.organization_id', $accountInfo['organization_id'])
                ->join('applications', 'applications.id', '=', 'statistic_applications.application_id')
                ->where('applications.deleted_at', null);
        }

        if ($spaceId) {
            $totalSpace = $totalSpace->where('id', $spaceId);
            $totalRoom = $totalRoom->where('space_id', $spaceId);
            $totalApplication = $totalApplication->where('space_id', $spaceId);
            $view = $view->where('statictis_spaces.space_id', $spaceId);
            $applicationVisited = $applicationVisited->where('statistic_applications.space_id', $spaceId);
        }

        if ($roomId) {
            $totalRoom = $totalRoom->where('id', $roomId);
            $totalApplication = $totalApplication->where('space_id', $spaceId)->where('room_id', $roomId);
            $applicationVisited = $applicationVisited->where('statistic_applications.room_id', $roomId);
        }

        if ($startDate && $endDate) {
            $totalSpace = $totalSpace->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
            $totalRoom = $totalRoom->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
            $totalApplication = $totalApplication->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
            $view = $view->whereDate('statictis_spaces.created_at', '>=', $startDate)->whereDate('statictis_spaces.created_at', '<=', $endDate);
            $applicationVisited = $applicationVisited->whereDate('statistic_applications.created_at', '>=', $startDate)->whereDate('statistic_applications.created_at', '<=', $endDate);
        }

        $data['totalSpace'] = $totalSpace->count();
        $data['totalRoom'] = $totalRoom->count();
        $data['totalApplication'] = $totalApplication->count();
        $data['totalAccount'] = $totalAccount->count();
        $data['space'] = $view->select('statictis_spaces.space_id', DB::raw('SUM(visited) as visit'))
            ->groupBy('statictis_spaces.space_id')->orderBy('visit', 'desc')
            ->get();
        $data['applicationVisited'] = $applicationVisited->select('application_id', DB::raw('SUM(clicks) as visit'))
            ->groupBy('statistic_applications.application_id')->orderBy('visit', 'desc')
            ->get();

        return $data;
    }

    public function getOneSpaceOfAccount($accountId)
    {
        $spacePro = $this->getBlankSpaceAccount()
            ->select(
                'space_accounts.space_id'
            )
            ->leftJoin('spaces', 'space_accounts.space_id', '=', 'spaces.id')
            ->where('space_accounts.account_id', $accountId)
            ->where('spaces.deleted_at', null)->orderBy('spaces.created_at', 'asc')->first();
        //        $spaceWorkLife = $this->getBlankSpaceAccountLifeWork()
        //            ->select(
        //                'space_accounts_work_life.space_id'
        //            )
        //            ->leftJoin('spaces', 'space_accounts_work_life.space_id', '=', 'spaces.id')
        //            ->where('space_accounts_work_life.account_id', $accountId)
        //            ->where('spaces.deleted_at', null)->orderBy('spaces.created_at', 'asc')->first();
        //        $space = !empty($spacePro) ? $spacePro : $spaceWorkLife;
        return $spacePro;
    }

    public function getOneArraySpaceByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $dataX = $query->first();
        $data = [];
        if (!empty($dataX)) {
            $data = $dataX->toArray();
        }

        return $data;
    }

    public function createSpace($requestData, $accountInfo, $collectionId)
    {
        $space = $this->getBlankModel();
        $space->name = $requestData['spaceName'];
        $space->slug = Str::slug($requestData['spaceName']);
        $space->type = $requestData['type'];
        $space->is_show = !empty($requestData['is_show']) ? $requestData['is_show'] : 1;
        $space->major_id = $requestData['majorId'];
        $space->space_image_id = $collectionId;
        $space->organization_id = $accountInfo['organization_id'];
        $space->created_by = $accountInfo['id'];
        $space->save();
        $id = $space->id;
        if (!empty($requestData['main_space'])) {
            if ($requestData['main_space'] == Space::DEFAULT_SPACE) {
                $this->getBlankModelAccount()->where('id', $accountInfo['id'])->update(['main_space' => $id]);
            }
        }
        $accountIdInSpace = [];
        // if ((int)$requestData['type'] === Space::TYPE_SPACE_CUSTOMER) {
        //     $space->accountLife()->syncWithOutDetaching($accountInfo['id']);
        //     $space->accountLife()->updateExistingPivot(
        //         $accountInfo['id'],
        //         [
        //             'organization_id' => $accountInfo['organization_id'],
        //             'is_admin' => SpaceAccount::IS_ADMIN,
        //         ]
        //     );
        // } else {
        $space->account()->syncWithOutDetaching($accountInfo['id']);
        $space->account()->updateExistingPivot(
            $accountInfo['id'],
            [
                'organization_id' => $accountInfo['organization_id'],
                'is_admin' => SpaceAccount::IS_ADMIN,
            ]
        );
        // }
        if (!empty($requestData['accountIds'])) {
            $this->addAccountToSpace($requestData, $accountInfo, $space, $accountIdInSpace);
        }
        return $id;
    }

    private function listAccountInGroup($groupIds)
    {
        $account = AccountGroup::whereIn('group_id', $groupIds)->get('account_id');
        $id = [];
        foreach ($account as $value) {
            $id[] = $value->account_id;
        }
        $listAccountId = array_unique($id);
        return $listAccountId;
    }

    private function listAccountInGroupEmployee($groupIds)
    {
        $account = AccountGroup::join('accounts', 'accounts.id', '=', 'account_groups.account_id')
            ->where('accounts.type_account', Account::TYPE_ACCOUNT_STAFF)
            ->whereIn('account_groups.group_id', $groupIds)
            ->get()->pluck('account_id')->unique()->values()->toArray();

        return $account;
    }

    public function listAccountInSpaceOtherAccount($spaceId, $accountId)
    {
        $account = SpaceAccount::where('space_id', $spaceId)->where('account_id', '<>', $accountId)->get('account_id');
        //        $accountHaveInAdmin = SpaceAccount::where('space_id', $spaceId)->where([
        //            'account_id' => $accountId,
        //            'is_admin' => SpaceAccount::IS_ADMIN
        //        ])->with('accounts')->get()->first();
        //        if (!empty($accountHaveInAdmin)) {
        //            $accountHaveInAdmin = $accountHaveInAdmin->toArray();
        //            if (!empty($accountHaveInAdmin['accounts']['created_by'])) {
        //                $account = SpaceAccount::where('space_id', $spaceId)->where('account_id', $accountId)->get('account_id');
        //            }
        //            \Log::info($account);
        //        }
        //        \Log::info(['check' => $accountHaveInAdmin]);
        return collect($account)->pluck('account_id')->unique()->values()->toArray();
    }

    public function listAccountInSpace($spaceId)
    {
        $account = SpaceAccount::where('space_id', $spaceId)->get('account_id');
        $id = [];
        foreach ($account as $value) {
            $id[] = $value->account_id;
        }
        return $id;
    }

    public function listAccountInSpaceLifeWork($spaceId)
    {
        $account = SpaceAccountLifeWork::where('space_id', $spaceId)->get('account_id');
        $id = [];
        foreach ($account as $value) {
            $id[] = $value->account_id;
        }
        return $id;
    }

    public function listAccountLifeIdInSpaceLifeWork($spaceId)
    {
        $account = SpaceAccountLifeWork::where('space_id', $spaceId)->pluck('account_life_id')->unique()->values();
        $id = [];
        foreach ($account as $value) {
            if (!is_null($value)) {
                $id[] = $value;
            }
        }
        return $id;
    }

    public function listOfMemberOfTheSameOrganization($infoAccount)
    {
        $account = Account::where([
            'organization_id' => $infoAccount['organization_id'],
            'is_active' => Account::IS_ACTIVE
        ])->pluck('id')->toArray();
        return $account;
    }

    public function addAccount($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $space = $this->getBlankModel()->find($requestData['spaceId']);
            $accountIdInSpace = $this->listAccountInSpace($requestData['spaceId']);
            $this->addAccountToSpace($requestData, $accountInfo, $space, $accountIdInSpace);
            DB::commit();
            return $space;
        } catch (Exception $ex) {
            Debug::sendNotification($ex);
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function filterAccountForDonesOrDonesPro($lsAccount, $select = Account::DONES_PRO)
    {
        /**
         * @param $select => loại acouunt thuộc bên nào
         * @param $lsAccount => data
         */
        $tpl = [];
        foreach ($lsAccount as $key => $item) {
            if ($item['type'] == $select) {
                $tpl[] = $item['id'];
            }
        }
        return $tpl;
    }

    public function addAccountToSpace($requestData, $accountInfo, $space = null, $accountIdInSpace = [])
    {
        // $space_default = $this->getBlankModel()->where('organization_id', $accountInfo['organization_id'])->where('is_default', Space::DEFAULT_SPACE)->first();
        if ($space) {
            $roomPub = $this->getBlankModelRoom()
                ->where('space_id', $space->id)
                ->where('organization_id', $accountInfo['organization_id'])
                ->where('type', Room::TYPE_ROOM_PUBLIC)->get('id')
                ->pluck('id')
                ->toArray();
            $roomPub = array_unique($roomPub);
            $quickbarPublic = $this->getBlankModelQuickBar()
                ->where('space_id', $space->id)
                ->where('organization_id', $accountInfo['organization_id'])
                ->where('enable_all', QuickBar::ENABLE_ALL)
                ->get()
                ->pluck('application_id')
                ->toArray();
            $appQuickBar = array_unique($quickbarPublic);
        }
        $accountOld = SpaceAccount::where('space_id', $space->id)
            ->get()->pluck('account_id')->unique()->values()->toArray();

        $requestData['accountIds'] = array_diff($requestData['accountIds'], $accountOld);

        // $account_employee = Account::whereIn('id', $requestData['accountIds'])->where('type_account', Account::TYPE_ACCOUNT_STAFF)->get()->pluck('id')->values()->toArray();
        // $requestData['accountIds'] = $account_employee;
        if (!empty($requestData['accountIds']) && empty($requestData['groupIds'])) {
            if (!empty($appQuickBar)) {
                foreach ($requestData['accountIds'] as $value) {
                    foreach ($appQuickBar as $el) {
                        $quickBar[] = [
                            'account_id' => $value,
                            'organization_id' => $accountInfo['organization_id'],
                            'application_id' => $el,
                            'space_id' => $space->id,
                            'enable_all' => QuickBar::ENABLE_ALL,
                        ];
                    }
                }
                $this->getBlankModelQuickBar()->insert($quickBar);
            }
            $listIdAccountSpaceNull = Account::whereIn('id', $requestData['accountIds'])->where('space_id', null)->get()->pluck('id')->toArray();
            Account::whereIn('id', $listIdAccountSpaceNull)->update(['space_id' => $space->id]);
            $space->account()->syncWithOutDetaching(array_unique(array_merge($requestData['accountIds'], $accountIdInSpace)));
            $space->account()->updateExistingPivot(
                array_unique(array_merge($requestData['accountIds'], $accountIdInSpace)),
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            if (!empty($roomPub)) {
                $accountList = array_unique(array_merge($requestData['accountIds'], $accountIdInSpace));
                $accountEmployee = Account::whereIn('id', $accountList)->where('type_account', Account::TYPE_ACCOUNT_STAFF)->get()->pluck('id')->values()->toArray();
                if (!empty($accountEmployee)) {
                    foreach ($accountEmployee as $value) {
                        $account = Account::find($value);
                        if (!empty($account)) {
                            $account->rooms()->syncWithOutDetaching($roomPub);
                            $account->rooms()->updateExistingPivot(
                                $roomPub,
                                [
                                    'organization_id' => $accountInfo['organization_id'],
                                    'space_id' => $space->id,
                                ]
                            );
                        }
                    }
                }
            }
        } else if (empty($requestData['accountIds']) && !empty($requestData['groupIds'])) {
            $listAccountId = $this->listAccountInGroup($requestData['groupIds']);
            Account::whereIn('id', $listAccountId)->update(['space_id' => $space->id]);
            $space->account()->syncWithOutDetaching(
                array_unique(array_merge($listAccountId, $accountIdInSpace)),
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $space->account()->updateExistingPivot(
                array_unique(array_merge($listAccountId, $accountIdInSpace)),
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            if (!empty($roomPub)) {
                $accountList = array_unique(array_merge($listAccountId, $accountIdInSpace));
                $accountEmployee = Account::whereIn('id', $accountList)->where('type_account', Account::TYPE_ACCOUNT_STAFF)->get()->pluck('id')->values()->toArray();
                foreach ($accountEmployee as $value) {
                    $account = Account::find($value);
                    $account->rooms()->syncWithOutDetaching($roomPub);
                    $account->rooms()->updateExistingPivot(
                        $roomPub,
                        [
                            'organization_id' => $accountInfo['organization_id'],
                            'space_id' => $space->id,
                        ]
                    );
                }
            }
            $space->group()->syncWithoutDetaching(
                $requestData['groupIds'],
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $space->group()->updateExistingPivot(
                $requestData['groupIds'],
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
        } else if (!empty($requestData['accountIds']) && !empty($requestData['groupIds'])) {
            $listId = $this->listAccountInGroup($requestData['groupIds']);
            $listAccountId = array_unique(array_merge($listId, $requestData['accountIds'], $accountIdInSpace));
            Account::whereIn('id', $listAccountId)->update(['space_id' => $space->id]);
            $space->account()->syncWithOutDetaching(
                $listAccountId,
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $space->account()->updateExistingPivot(
                $listAccountId,
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $space->group()->syncWithoutDetaching(
                $requestData['groupIds'],
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $space->group()->updateExistingPivot(
                $requestData['groupIds'],
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
        }
        $requestData['accountIds'] = array_diff($requestData['accountIds'], $accountOld);

        if (!empty($appQuickBar)) {
            foreach ($requestData['accountIds'] as $value) {
                foreach ($appQuickBar as $el) {
                    $quickBar[] = [
                        'account_id' => $value,
                        'organization_id' => $accountInfo['organization_id'],
                        'application_id' => $el,
                        'space_id' => $space->id,
                        'enable_all' => QuickBar::ENABLE_ALL,
                    ];
                }
            }
            $this->getBlankModelQuickBar()->insert($quickBar);
        }

        if (!empty($roomPub)) {
            $accountList = array_unique(array_merge($requestData['accountIds'], $accountIdInSpace));
            $accountEmployee = Account::whereIn('id', $accountList)->where('type_account', Account::TYPE_ACCOUNT_STAFF)->get()->pluck('id')->values()->toArray();
            if (!empty($accountEmployee)) {
                foreach ($accountEmployee as $value) {
                    $account = Account::find($value);
                    if (!empty($account)) {
                        $account->rooms()->syncWithOutDetaching($roomPub);
                        $account->rooms()->updateExistingPivot(
                            $roomPub,
                            [
                                'organization_id' => $accountInfo['organization_id'],
                                'space_id' => $space->id,
                            ]
                        );
                    }
                }
            }
        }

        $this->pushNotiAccountSpace($accountInfo, $requestData, $space, []);
    }


    public function updateSpace($requestData, $accountInfo, $collectionId)
    {
        try {
            DB::beginTransaction();
            $space = $this->getBlankModel()->find($requestData['spaceId']);
            $oldSpaceName = $space->name;
            $accountIdInSpace = $this->listAccountInSpace($requestData['spaceId']);
            $space->name = $requestData['spaceName'];
            $space->slug = Str::slug($requestData['spaceName']);
            $space->type = $requestData['type'];
            $space->major_id = $requestData['majorId'];
            $space->space_image_id = $collectionId;
            $space->save();
            if ($requestData['main_space'] == Space::DEFAULT_SPACE) {
                $this->getBlankModelAccount()->where('id', $accountInfo['id'])->update(['main_space' => $requestData['spaceId']]);
            }

            $accounts = SpaceAccount::
                // join('accounts', 'accounts.id', '=', 'space_accounts.account_id')
                //     ->where('accounts.type_account', Account::TYPE_ACCOUNT_STAFF)
                where('space_accounts.space_id', $requestData['spaceId'])
                ->where('organization_id', $accountInfo['organization_id'])->get()->toArray();

            $idsOld = collect($accounts)->pluck('account_id')->unique()->values()->toArray();
            if (!empty($requestData['accountIds'])) {
                // foreach ($requestData['accountIds'] as $key => $value) {
                //     foreach ($idsOld as $el) {
                //         if ($value['id'] == $el) {
                //             unset($key);
                //         }
                //     }
                // }
                $accountIds = array_diff($requestData['accountIds'], $idsOld);
                sort($accountIds);
                // $idsNew = array_diff($requestData['accountIds'], $idsOld);
                $requestData['accountIds'] = $accountIds;
                $this->addAccountToSpace($requestData, $accountInfo, $space, $accountIdInSpace);
            }
            DB::commit();
            $this->pushNotiUpdateSpace($accountInfo, $requestData, $oldSpaceName, $idsOld);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function updateSpaceLifeWork($requestData, $accountInfo, $collectionId)
    {
        $space = $this->getBlankModel()->find($requestData['spaceId']);
        $oldSpaceName = $space->name;
        $accountIdInSpace = $this->listAccountInSpaceLifeWork($requestData['spaceId']);
        $space->name = $requestData['spaceName'];
        $space->slug = Str::slug($requestData['spaceName']);
        $space->type = $requestData['type'];
        $space->is_show = $requestData['is_show'] ?? 1;
        $space->major_id = $requestData['majorId'];
        $space->space_image_id = $collectionId;
        $space->save();
        if (isset($requestData['main_space'])) {
            if ($requestData['main_space'] == Space::DEFAULT_SPACE && !empty($requestData['main_space'])) {
                $this->getBlankModelAccount()->where('id', $accountInfo['id'])->update(['main_space' => $requestData['spaceId']]);
            }
        }
        $accounts = SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])
            ->where('organization_id', $accountInfo['organization_id'])->get()->toArray();
        $idsOld = collect($accounts)->whereNotNull('account_life_id')->pluck('account_life_id')->unique()->values()->toArray();
        $accountPro = SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])
            //            ->where('account_id', '<>', $accountInfo['id'])
            ->where('organization_id', $accountInfo['organization_id'])->whereNotNull('account_id')->pluck('account_id')->unique()->values()->toArray();
        $userDones = $idsOld;
        $userDonesPro = $accountPro;

        if (!empty($requestData['accountIds'])) {
            $lsAcDones = [];
            $lsAcDonesPro = [];
            foreach ($requestData['accountIds'] as $key) {
                if ($key['type'] == Account::DONES) {
                    $lsAcDones[] = $key['id'];
                }
                if ($key['type'] == Account::DONES_PRO) {
                    $lsAcDonesPro[] = $key['id'];
                }
            }
            $tplDones = array_diff($lsAcDones, $userDones);
            sort($tplDones);
            $tplDonesPro = array_diff($lsAcDonesPro, $userDonesPro);
            sort($tplDonesPro);
            $requestDones = $this->mergeRequestType($tplDones, Account::DONES);
            $requestDonesPro = $this->mergeRequestType($tplDonesPro, Account::DONES_PRO);
            $requestData['accountIds'] = array_merge($requestDones, $requestDonesPro);
            //            $idsNew = array_diff($requestData['accountIds'], $idsOld);
            //            $requestData['accountIds'] = $idsNew;
            $this->addAccountToSpace($requestData, $accountInfo, $space, $accountIdInSpace);
        }
        if ($accountPro) {
            // $accountDonesPro = $this->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $this->pushNotiUpdateSpace($accountInfo, $requestData, $oldSpaceName, $accountPro);
        }
        $this->pushNotiUpdateSpaceWorkLife($accountInfo, $requestData, $oldSpaceName, $idsOld);
    }

    public function mergeRequestType($lsAccount, $type)
    {
        $tpl = [];
        foreach ($lsAccount as $el) {
            $tpl[] = [
                'id' => $el,
                'type' => $type
            ];
        }
        return $tpl;
    }

    public function deleteSpace($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $this->getBlankModelAccount()->whereIn('main_space', $requestData['spaceIds'])->update(['main_space' => null]);
            if ($accountInfo['is_root'] == Account::IS_ROOT) {
                Account::whereIn('space_id', $requestData['spaceIds'])->update(['space_id' => null]);
                // Gửi thông báo xóa
                //                $typeSpace = $this->checkSpaceType(null, $requestData['spaceIds']);
                //                if ($typeSpace == 2) {
                //                    $this->pushNotiDeleteSpaceWorkLife($accountInfo, $requestData['spaceIds']);
                //                } else {
                //                    $this->pushNotiDeleteSpace($accountInfo, $requestData['spaceIds']);
                //                }

                $this->pushNotiDeleteSpace($accountInfo, $requestData['spaceIds']);

                $this->getBlankModel()->whereIn('id', $requestData['spaceIds'])->where('organization_id', $accountInfo['organization_id'])->delete();
                //                foreach ($requestData['spaceIds'] as $key => $item) {
                //                    $checkSpaceType = $this->checkSpaceType(null, $item);
                //                    if ($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
                //                        $idSpaceWorkLife[] = $item;
                //                    } else {
                //                        $idSpaceWork[] = $item;
                //                    }
                //                }
                $this->getBlankSpaceAccount()->whereIn('space_id', $requestData['spaceIds'])->where('organization_id', $accountInfo['organization_id'])->delete();
                //                $idSpaceWork = [];
                //                $idSpaceWorkLife = [];
                //                if (!empty($idSpaceWork)) {
                //                    $this->getBlankSpaceAccount()->whereIn('space_id', $requestData['spaceIds'])->where('organization_id', $accountInfo['organization_id'])->delete();
                //                }
                //                if (!empty($idSpaceWorkLife)) {
                //                    $this->getBlankSpaceAccountLifeWork()->whereIn('space_id', $requestData['spaceIds'])->where('organization_id', $accountInfo['organization_id'])->delete();
                //                }
                $this->getBlankSpaceAccount()->whereIn('space_id', $requestData['spaceIds'])->where('organization_id', $accountInfo['organization_id'])->delete();
                $space = $this->getOneSpaceOfAccount($accountInfo['id']);

                $data = [
                    'main_space' => '',
                ];
                if (!empty($space)) {
                    $data = [
                        'main_space' => $space->space_id
                    ];
                }
            } else {
                $spaceId = $this->getBlankModel()->whereIn('id', $requestData['spaceIds'])->where('created_by', $accountInfo['id'])->get()->pluck('id')->unique()->values()->toArray();
                Account::whereIn('space_id', $spaceId)->update(['space_id' => null]);
                // Gửi thông báo xóa
                //                $typeSpace = $this->checkSpaceType(null, $requestData['spaceIds']);
                //                if ($typeSpace == 2) {
                //                    $this->pushNotiDeleteSpaceWorkLife($accountInfo, $requestData['spaceIds']);
                //                } else {
                //                    $this->pushNotiDeleteSpace($accountInfo, $requestData['spaceIds']);
                //                }
                $this->pushNotiDeleteSpace($accountInfo, $requestData['spaceIds']);

                $this->getBlankModel()->whereIn('id', $spaceId)->where('organization_id', $accountInfo['organization_id'])->delete();


                //                $checkSpaceType = $this->checkSpaceType(null, $spaceId);
                //                if ($checkSpaceType == Space::TYPE_SPACE_INTERNAL) {
                //                    $this->getBlankSpaceAccount()->whereIn('space_id', $spaceId)->where('organization_id', $accountInfo['organization_id'])->delete();
                //                }
                //                if ($checkSpaceType == Space::TYPE_SPACE_CUSTOMER) {
                //                    $this->getBlankSpaceAccountLifeWork()->whereIn('space_id', $spaceId)->delete();
                //                }
                $this->getBlankSpaceAccount()->whereIn('space_id', $spaceId)->where('organization_id', $accountInfo['organization_id'])->delete();

                $space = $this->getOneSpaceOfAccount($accountInfo['id']);
                $data = [
                    'main_space' => '',
                ];
                if (!empty($space)) {
                    $data = [
                        'main_space' => $space->space_id
                    ];
                }
            }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'), $data);
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteAccountInSpace($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $accountDonesPro = $this->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $requestData['accountIds'] = $accountDonesPro;
            Account::where('main_space', $requestData['spaceId'])->whereIn('id', $requestData['accountIds'])->update(['main_space' => null]);
            Account::where('space_id', $requestData['spaceId'])->whereIn('id', $requestData['accountIds'])->update(['space_id' => null]);
            $accountGroup = AccountGroup::whereIn('account_id', $requestData['accountIds'])->get()->toArray();
            $idGroup = collect($accountGroup)->pluck('group_id')->unique()->values()->toArray();
            if (!empty($idGroup)) {
                SpaceGroup::whereIn('group_id', $idGroup)->delete();
            }
            SpaceAccount::where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            RoomAccount::where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            $this->getBlankModelQuickBar()->where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            DB::commit();

            $space = $this->getBlankModel()->find($requestData['spaceId']);
            $this->pushNotiDeleteAccountInSpace($accountInfo, $requestData, $space);

            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            DB::rollback();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteAccountInSpaceLifeWork($requestData, $accountInfo)
    {
        /**
         * {
         * "spaceId": "704",
         * "accountIds": [
         *      {
         *          "id": 502,
         *          "type": 1
         *      }
         * ]
         * }
         */
        try {

            DB::beginTransaction();

            $accountDonesPro = $this->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES_PRO);
            $accountDones = $this->filterAccountForDonesOrDonesPro($requestData['accountIds'], Account::DONES);

            $requestData['accountIds'] = $accountDonesPro;
            Account::where('space_id', $requestData['spaceId'])->whereIn('id', $requestData['accountIds'])->update(['space_id' => null]);
            $accountGroup = AccountGroup::whereIn('account_id', $requestData['accountIds'])->get()->toArray();
            $idGroup = collect($accountGroup)->pluck('group_id')->unique()->values()->toArray();
            if (!empty($idGroup)) {
                SpaceGroup::whereIn('group_id', $idGroup)->delete();
            }
            if (!empty($accountDonesPro)) {
                SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
                RoomAccountsWorkLife::where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
                $this->getBlankModelQuickBar()->where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            }
            if (!empty($accountDones)) {
                SpaceAccountLifeWork::where('space_id', $requestData['spaceId'])->whereIn('account_life_id', $accountDones)->delete();
                RoomAccountsWorkLife::where('space_id', $requestData['spaceId'])->whereIn('account_life_id', $accountDones)->delete();
                $this->getBlankModelQuickBar()->where('space_id', $requestData['spaceId'])->whereIn('account_life_id', $accountDones)->delete();
            }
            //            RoomAccount::where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            //            $this->getBlankModelQuickBar()->where('space_id', $requestData['spaceId'])->whereIn('account_id', $requestData['accountIds'])->delete();
            DB::commit();

            $space = $this->getBlankModel()->find($requestData['spaceId']);
            if ($accountDonesPro) {
                $requestData['accountIds'] = $accountDonesPro;
                $this->pushNotiDeleteAccountInSpace($accountInfo, $requestData, $space);
            }
            if ($accountDones) {
                $requestData['accountIds'] = $accountDones;
                $this->pushNotiDeleteAccountInSpaceWorkLife($accountInfo, $requestData, $space);
            }


            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            DB::rollback();
            Debug::sendNotification($ex);
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function listGroupInSpace($id, $type)
    {
        try {
            $space = $this->getBlankModel()->select('spaces.id', 'spaces.name')->with(['group' => function ($q) use ($type) {
                $q->select('groups.id', 'groups.name', 'groups.type')
                    ->with(['account' => function ($q) {
                        $q->select('accounts.id', 'accounts.name');
                    }]);

                if (!empty($type)) {
                    $q = $q->where('type', $type);
                }
            }])->find($id);
            if (!empty($space['group'])) {
                return $space['group'];
            }
            return [];
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function listRoomOfSpace($spaceId)
    {
        try {
            $room = $this->getBlankModelRoom()->where('space_id', $spaceId)->select('id', 'name', 'slug', 'type', 'is_pin', 'position')->orderBy('is_pin', 'desc')->orderBy('position', 'asc')->paginate(1);
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $room);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function listAccount($id, $search, $type)
    {
        try {
            $query = $this->getBlankModel()->with(['account' => function ($q) use ($search, $type) {
                $q = $q->select(
                    'accounts.id AS account_id',
                    'accounts.name',
                    'accounts.email',
                    'accounts.phone_number',
                    'accounts.profile_image_id',
                    'accounts.type_account'
                );

                if (!empty($search)) {
                    $q = $q->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                }

                if ($type == 0 || $type == 1) { // 0 va 1
                    $q = $q->where('accounts.type_account', $type);
                }

                $q = $q->where('accounts.is_active', Account::IS_ACTIVE);
            }])->find($id);
            if (!empty($query->account)) {
                $query->account->map(function ($val) {
                    // $val['type'] = Account::DONES_PRO;
                    if ($val['account_id']) {
                        $val['id'] = $val['account_id'];
                    }
                    unset($val['pivot']);
                    if (!empty($val->profileImage)) {
                        $val->profileImage['source_thumb'] = !empty($val->profileImage['source_thumb']) ? asset($val->profileImage['source_thumb']) : '';
                        $val->profileImage['source'] = !empty($val->profileImage['source']) && file_exists(public_path() . '/' . $val->profileImage['source']) ? asset($val->profileImage['source']) : '';
                    }
                    return $val;
                });
            }
            $result = $query->toArray();
            return $result['account'];
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function listAccountWorkLife($id, $search)
    {
        try {
            $accountLife = $this->getBlankSpaceAccountLifeWork()
                ->where('space_id', $id)
                ->with('account', function ($life) use ($search) {
                    if (!empty($search)) {
                        $life->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                    }
                    $life->select('id', 'name', 'email', 'profile_image_id');
                })
                ->with('accountLife', function ($life) use ($search) {
                    $life->select('id', 'name', 'email', 'profile_image_id');
                    if (!empty($search)) {
                        $life->where(DB::raw('vn_unaccent(life_accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                    }
                })
                ->select('account_life_id', 'account_id')
                ->get()->toArray();
            if (!empty($accountLife)) {
                $accountLife = array_map(function ($check) {
                    if (!empty($check['account_life'])) {
                        $check['account_life']['type'] = Account::DONES;
                        $check['account_life']['profile_image']['source_thumb'] = !empty($check['account_life']['profile_image']['source_thumb']) ? config('systems.URL_IMAGE_LIFE') . $check['account_life']['profile_image']['source_thumb'] : '';
                        $check['account_life']['profile_image']['source'] = !empty($check['account_life']['profile_image']['source']) ? config('systems.URL_IMAGE_LIFE') . $check['account_life']['profile_image']['source'] : '';
                        return $check['account_life'];
                    }
                    if (!empty($check['account'])) {
                        $check['account']['type'] = Account::DONES_PRO;
                        $check['account']['profile_image']['source_thumb'] = !empty($check['account']['profile_image']['source_thumb']) ? config('systems.URL_IMAGE_LIFE') . $check['account']['profile_image']['source_thumb'] : '';
                        $check['account']['profile_image']['source'] = !empty($check['account']['profile_image']['source']) ? config('systems.URL_IMAGE_LIFE') . $check['account']['profile_image']['source'] : '';
                        return $check['account'];
                    }
                }, $accountLife);
            }
            $query = $this->getBlankModel()
                ->with(['account' => function ($q) use ($search) {
                    $q = $q->select('accounts.id AS account_id', 'name', 'email', 'phone_number', 'profile_image_id');
                    if (!empty($search)) {
                        $q = $q->where(DB::raw('vn_unaccent(accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                    }
                    $q = $q->with('managers')->with(['profileImage' => function ($q) {
                        $q = $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
                    }])
                        ->where('accounts.is_active', Account::IS_ACTIVE);
                }])->find($id);
            if (!empty($query)) {
                $lsObj = $query->toArray();
                $lsObj['account'] = [];
                $lsObj['account'] = $accountLife;
                $query = $lsObj;
            }
            if (!empty($query['account'])) {
                $query = array_map(function ($val) {
                    if (!empty($val['profileImage'])) {
                        $val['profileImage']['source_thumb'] = !empty($val['profileImage']['source_thumb']) ? asset($val['profileImage']['source_thumb']) : '';
                        $val['profileImage']['source'] = !empty($val['profileImage']['source']) && file_exists(public_path() . '/' . $val['profileImage']['source']) ? asset($val['profileImage']['source']) : '';
                    }
                    return $val;
                }, $query['account']);
            }
            $query = array_filter($query);
            sort($query);
            if (!empty($result)) {
                $result = $query->toArray();
            }
            return $query;
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function searchAccountInSpaceWorkLife($id, $search)
    {
        $accountLife = $this->getBlankSpaceAccountLifeWork()
            ->distinct()
            ->where('space_id', $id)
            ->with(['accountLife' => function ($life) use ($search) {
                $life = $life->select('id', 'name', 'email', 'phone_number', 'profile_image_id');
                if (!empty($search)) {
                    $life = $life->where(DB::raw('vn_unaccent(life_accounts.name)'), 'LIKE', '%' . Str::lower(Str::ascii($search)) . '%');
                }
                $life->with(['profileImage' => function ($q) {
                    $q = $q->select('id', 'name', 'source_thumb', 'source');
                }]);
            }])
            ->whereNotNull('account_life_id')
            ->select('account_life_id')
            ->get()->toArray();
        if (!empty($accountLife)) {
            $accountLife = array_map(function ($check) {
                $check['account_life']['profile_image']['source_thumb'] = !empty($check['account_life']['profile_image']['source_thumb']) ? config('systems.URL_IMAGE_LIFE') . $check['account_life']['profile_image']['source_thumb'] : '';
                $check['account_life']['profile_image']['source'] = !empty($check['account_life']['profile_image']['source']) ? config('systems.URL_IMAGE_LIFE') . $check['account_life']['profile_image']['source'] : '';

                $un = $check['account_life'];
                return $un;
            }, $accountLife);
        }
        return $accountLife;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('spaces.id', $filter['id']);
            } else {
                $query = $query->where('spaces.id', $filter['id']);
            }
        }

        if (isset($filter['is_default'])) {
            $query = $query->where('spaces.is_default', $filter['is_default']);
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('spaces.organization_id', $filter['organization_id']);
        }

        if (isset($filter['created_by'])) {
            $query = $query->where('spaces.created_by', $filter['created_by']);
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('spaces.deleted_at', null);
        }
    }

    public function addAcountSpaceDefault($idAccount, $accountInfo)
    {
        $data = [];
        $space_default = $this->getBlankModel()->where('organization_id', $accountInfo['organization_id'])->where('is_default', Space::DEFAULT_SPACE)->first();
        if (!empty($space_default)) {
            $space_default->account()->syncWithOutDetaching($idAccount);
            $space_default->account()->updateExistingPivot(
                $idAccount,
                [
                    'organization_id' => $accountInfo['organization_id'],
                ]
            );
            $roomPub = $this->getBlankModelRoom()->where('space_id', $space_default->id)->where('organization_id', $accountInfo['organization_id'])->where('type', Room::TYPE_ROOM_PUBLIC)->get('id');
            if (!empty($roomPub)) {
                $listRoomId = [];
                foreach ($roomPub as $value) {
                    $listRoomId[] = $value->id;
                }
                $data = [
                    'space_id' => $space_default->id,
                    'rooms' => $listRoomId,
                ];
            } else {
                $data = [
                    'space_id' => $space_default->id,
                    'rooms' => [],
                ];
            }
        }

        return $data;
    }

    public function searchRoomApplication($request, $accountInfo)
    {
        $data = [];
        if ($request['name']) {
            $roomIdHasPermission = $this->getBlankModelRoomAccount()->where('account_id', $accountInfo['id'])->where('space_id', $request['space_id'])->where('organization_id', $accountInfo['organization_id'])->get()->pluck('room_id')->toArray();

            $data['rooms'] = $this->getBlankModelRoom()->select('id', 'name')->whereIn('id', $roomIdHasPermission)->where('slug', 'like', '%' . eFunction::generateSlug($request['name'], '-') . '%')->get();

            $data['applications'] = $this->getBlankModelApp()
                ->with(['applicationStore' => function ($query) {
                    $query->with('createdBy');
                }])
                ->leftJoin('images', 'images.id', '=', 'applications.application_image_id')
                ->leftJoin('rooms', 'rooms.id', '=', 'applications.room_id')
                ->select(
                    'applications.id as application_id',
                    'applications.name as application_name',
                    'applications.room_id as room_id',
                    'applications.link_url',
                    'applications.link_ios',
                    'applications.link_android',
                    'rooms.name as room_name',
                    'rooms.created_by as room_created_by',
                    'images.source as icon',
                    'applications.created_by',
                    'applications.account_id',
                    'applications.is_accept',
                    'applications.application_store_id',
                    'applications.x_frame'
                )
                ->where('applications.slug', 'like', '%' . eFunction::generateSlug($request['name'], '-') . '%')
                ->whereIn('applications.room_id', $roomIdHasPermission)
                ->where('applications.space_id', $request['space_id'])
                ->where('applications.is_accept', Application::ACCEPT)
                ->whereNull('applications.deleted_at')
                ->whereNull('rooms.deleted_at')
                ->whereNull('images.deleted_at')
                ->get()->toArray();

            if (!empty($data['applications'])) {
                foreach ($data['applications'] as &$value) {
                    if (!empty($value['icon'])) {
                        $value['icon'] = asset($value['icon']);
                    }
                }
            }
        }

        return $data;
    }

    public function deleteAllAccount($accountIds, $accountInfo)
    {
        try {
            SpaceAccount::whereIn('account_id', $accountIds)->where('organization_id', $accountInfo['organization_id'])->delete();
            RoomAccount::whereIn('account_id', $accountIds)->where('organization_id', $accountInfo['organization_id'])->delete();
            $this->getBlankModelQuickBar()->whereIn('account_id', $accountIds)->where('organization_id', $accountInfo['organization_id'])->delete();
            return true;
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
    }

    public function updateAdminInSpace($request, $accountInfo)
    {
        $adminSpace = SpaceAccount::where('account_id', $request['account_id'])->where('space_id', $request['space_id'])->update(['is_admin' => $request['is_admin']]);
        if (!empty($adminSpace)) {
            $this->pushNotiAdminSpace($request, $accountInfo);
            return true;
        }

        return false;
    }

    public function updateAdminInSpaceLifeWork($request, $accountInfo)
    {
        $adminSpace = SpaceAccountLifeWork::where('account_id', $request['account_id'])->where('space_id', $request['space_id'])->update(['is_admin' => $request['is_admin']]);
        if (!empty($adminSpace)) {
            $this->pushNotiAdminSpace($request, $accountInfo);
            return true;
        }

        return false;
    }

    public function checkAccountInSpace($accountId, $spaceId)
    {
        $checkTypeSpaceId = Space::find($spaceId);
        if (!empty($checkTypeSpaceId)) {
            // if ($checkTypeSpaceId['type'] == 2) {
            //     $checkAccount = SpaceAccountLifeWork::where('account_id', $accountId)->where('space_id', $spaceId)->first();
            // } else {
            $checkAccount = SpaceAccount::where('account_id', $accountId)->where('space_id', $spaceId)->first();
            // }

            if (empty($checkAccount)) {
                return false;
            }

            return $spaceId;
        } else {
            $getOneSpacePro = SpaceAccount::where('account_id', $accountId)->first();
            if (!empty($getOneSpacePro)) {
                $spaceId = $getOneSpacePro->space_id;
            }
            // else {
            //     $getOneSpaceWork = SpaceAccountLifeWork::where('account_id', $accountId)->first();
            //     if (!empty($getOneSpaceWork)) {
            //         $spaceId = $getOneSpaceWork->space_id;
            //     }
            // }

            if (empty($spaceId)) {
                return false;
            }

            return $spaceId;
        }

        // if (empty($checkAccount)) {
        //     return false;
        // }

        // return $spaceId;

        //        $checkAccount = SpaceAccount::where('account_id', $accountId)->where('space_id', $spaceId)->first();
        //        if (empty($checkAccount)) {
        //            return false;
        //        }
        //        return true;
    }

    /**
     * @param $accountId
     * @param $spaceId
     * @return bool|int
     */
    public function checkSpaceType($accountId = null, $spaceId)
    {
        if ($accountId == null) {
            $spaceType = Space::where('id', $spaceId)->first();
            if (!empty($spaceType)) {
                $type = $spaceType->toArray();
                if ($type['type'] === Space::TYPE_SPACE_CUSTOMER) {
                    return Space::TYPE_SPACE_CUSTOMER;
                } else {
                    return Space::TYPE_SPACE_INTERNAL;
                }
            }
            return false;
        } else {
            //            if ($this->checkAccountInSpace($accountId, $spaceId) === true) {
            if (!empty($this->checkAccountInSpace($accountId, $spaceId))) {

                $checkTypeSpaceId = Space::find($spaceId);
                if (!empty($checkTypeSpaceId)) {
                    if ($checkTypeSpaceId['type'] == 2) {
                        $getSpaceId = SpaceAccount::where('account_id', $accountId)->where('space_id', $spaceId)->select('space_id')->first();
                    } else {
                        $getSpaceId = SpaceAccount::where('account_id', $accountId)
                            ->with('account', function ($q) {
                                $q->where('type_account', Account::TYPE_ACCOUNT_STAFF);
                            })
                            ->where('space_id', $spaceId)
                            ->select('space_id')
                            ->first();
                    }
                }

                //                $getSpaceId = SpaceAccount::where('account_id', $accountId)->where('space_id', $spaceId)->select('space_id')->first();
                if (!empty($spaceId)) {
                    $spaceType = Space::where('id', $getSpaceId->space_id)->first()->toArray();
                    if ($spaceType['type'] === Space::TYPE_SPACE_CUSTOMER) {
                        return Space::TYPE_SPACE_CUSTOMER;
                    }
                    if ($spaceType['type'] === Space::TYPE_SPACE_INTERNAL) {
                        return Space::TYPE_SPACE_INTERNAL;
                    }
                }
                return false;
            }
        }

        return false;
    }

    public function approveJoinInSpace($data, $accountInfo)
    {
        if ($data['is_accept'] == SpaceAccountLifeWork::IS_ACCEPT) {
            $this->getBlankSpaceAccountLifeWork()->where('space_id', $data['id'])->where('account_life_id', $data['account_life_id'])->update(['is_accept' => SpaceAccountLifeWork::IS_ACCEPT]);
            // eFunction::cacheAccountInSpace($data['id'], $data['account_id'], json_encode($data['account_id']));
            $space = $this->getBlankModel()->where('id', $data['id'])->first();
            $requestData['accountIds'] = [
                [
                    'id' => $data['account_life_id'],
                    'type' => SpaceAccountLifeWork::ACCOUNT_LIFE
                ]
            ];
            $this->addAccountToSpace($requestData, $accountInfo, $space, []);
        } else {
            $this->getBlankSpaceAccountLifeWork()->where('space_id', $data['id'])->where('account_life_id', $data['account_life_id'])->delete();
        }

        // Thông báo cho người dùng khi được duyệt vào space
        $this->pushNotiApproveAccountJoinSpace($data['id'], $accountInfo, $data['account_life_id'], $data['is_accept']);
        $this->deleteNotificationWithEvent($data['id'], $data['account_life_id'], LifeNotifications::REQUEST_JOIN_SPACE);
        return true;
    }

    public function approveJoinAllInSpace($data, $accountInfo)
    {
        $account_not_accept = $this->getBlankSpaceAccountLifeWork()->whereIn('space_id', $data['ids'][0])->where('is_accept', SpaceAccountLifeWork::NOT_ACCEPT);
        $lstAccount = $account_not_accept->get()->pluck('account_life_id')->toArray();
        if ($data['is_accept'] == SpaceAccountLifeWork::IS_ACCEPT) {
            $account_not_accept->update(['is_accept' => SpaceAccountLifeWork::IS_ACCEPT]);
            $space = $this->getBlankModel()->where('id', $data['ids'][0])->first();
            $requestData['accountIds'] = [];
            foreach ($lstAccount as $id) {
                $requestData['accountIds'][] = [
                    [
                        'id' => $id,
                        'type' => SpaceAccountLifeWork::ACCOUNT_LIFE
                    ]
                ];
            }
            $this->addAccountToSpace($requestData, $accountInfo, $space, []);
        } else {
            $account_not_accept->delete();
        }

        $this->pushNotiApproveAccountJoinSpace($data['ids'][0], $accountInfo, $lstAccount, $data['is_accept']);
        $this->deleteNotificationWithEvent($data['ids'], null, LifeNotifications::REQUEST_JOIN_SPACE);
        return true;
    }

    public function totalAccountSpaceCustom($spaceId, $accountInfo)
    {
        $getAccountAll = $this->getBlankSpaceAccountLifeWork()->where([
            'space_id' => $spaceId
        ])->distinct()->count();
        if (!empty($getAccountAll)) {
            return $getAccountAll;
        } else {
            return false;
        }
    }

    public function listAccountIdInSpaceLifeWork($spaceId, $accountId)
    {
        $accountDonesPro = $this->getBlankSpaceAccountLifeWork()
            ->where('account_id', '!=', $accountId['id'])
            ->where('space_id', $spaceId)
            ->whereNotNull('account_id')
            ->pluck('account_id')->toArray();

        return $accountDonesPro;
    }
}
