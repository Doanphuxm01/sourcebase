<?php namespace App\Repositories\Postgres\Eloquent;

use App\Models\Postgres\Account;
use App\Models\Postgres\Decentralization;
use App\Models\Postgres\DecentralizationPermission;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PermissionRepositoryInterface;
use \App\Models\Postgres\Permission;

class PermissionRepository extends SingleKeyModelRepository implements PermissionRepositoryInterface
{

    public function getBlankModel()
    {
        return new Permission();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function deleteAllPermissionsByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function createMulti($params)
    {
        if (!empty($params) && is_array($params)) {
            $insertUsers = $this->getBlankModel()->insert($params);
            if ($insertUsers) {
                return true;
            }
        }
        return false;
    }

    public function getAllPermissionByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function countAllPermissionsByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->count();

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('permissions.id', $filter['id']);
            } else {
                $query = $query->where('permissions.id', $filter['id']);
            }
        }

        if (isset($filter['deleted_at'])) {
            $query = $query->where('permissions.deleted_at', null);
        }

    }

    public function getAllAccountToPermission($key)
    {
        $query = $this->getBlankModel()->where('key', $key)->first();
        if (!empty($query)) {
            $query = $query->toArray();
            $decentralization = DecentralizationPermission::where([
                'permission_id'=> $query['id'],
                'update' => 1
            ])->first();
            if(!empty($decentralization)) {
                $decentralization = $decentralization->toArray();
                return Account::where('decentralization_id', $decentralization['decentralization_id'])->get()->pluck('id')->toArray();
            }
            return [];
        }
        return false;

    }
}
