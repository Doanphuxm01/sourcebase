<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\DecentralizationPermissionRepositoryInterface;
use \App\Models\Postgres\DecentralizationPermission;

class DecentralizationPermissionRepository extends SingleKeyModelRepository implements DecentralizationPermissionRepositoryInterface
{

    public function getBlankModel()
    {
        return new DecentralizationPermission();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function createMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insertUsers = $this->getBlankModel()->insert($params);
            if($insertUsers){
                return true;
            }
        }
        return false;
    }

    public function countAllDecentralizationPermissionsByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->count();

        return $data;
    }

    private function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('decentralization_permissions.id', $filter['id']);
            } else {
                $query = $query->where('decentralization_permissions.id', $filter['id']);
            }
        }
    }

}
