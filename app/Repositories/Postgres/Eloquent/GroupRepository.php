<?php

namespace App\Repositories\Postgres\Eloquent;

use App\Models\Postgres\Account;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\GroupRepositoryInterface;
use \App\Models\Postgres\Group;
use Illuminate\Support\Str;
use Exception;
use App\Elibs\eResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Postgres\Space;
use App\Elibs\eFunction;
use App\Models\Postgres\AccountGroup;
use App\Models\Postgres\SpaceAccount;

class GroupRepository extends SingleKeyModelRepository implements GroupRepositoryInterface
{

    public function getBlankModel()
    {
        return new Group();
    }

    public function getBlankModelSpace()
    {
        return new Space();
    }

    public function getBlankModelAccountGroup()
    {
        return new AccountGroup();
    }

    public function getBlankModelSpaceAccount()
    {
        return new SpaceAccount();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function findId($id)
    {
        $query = $this->getBlankModel()->with(['account' => function ($q) {
            $q->select('accounts.id', 'name', 'accounts.organization_id');
        }])->with(['createdBy' => function ($q) {
            $q->select('accounts.id', 'accounts.name');
        }])->with(['organization' => function ($q) {
            $q->select('organizations.id', 'organizations.name');
        }])->find($id);

        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'), $query);
    }

    public function listGroupWithPaginate($queryString, $accountInfo)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 20;
        $startDate = $queryString['startDate'] ?? null;
        $endDate = $queryString['endDate'] ?? null;
        $groupName = $queryString['keyword'] ?? null;
        $type = $queryString['type'] ?? null;

        $query = $this->getBlankModel()->with(['account' => function ($q) {
            $q->select('accounts.id', 'accounts.name', 'accounts.organization_id', 'accounts.email', 'accounts.phone_number');
        }])->with(['createdBy' => function ($q) {
            $q->select('accounts.id', 'accounts.name');
        }])->with(['organization' => function ($q) {
            $q->select('organizations.id', 'organizations.name');
        }]);

        if ($startDate && $endDate) {
            $query = $query->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
        }

        if ($type) {
            $query = $query->where('type', $type);
        }

        if ($groupName) {
            $query = $query->where('slug', 'like', '%' . eFunction::generateSlug($groupName, '-') . '%');
        }

        $query = $query->where('organization_id', $accountInfo['organization_id'])->paginate($limit)->toArray();
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function listGroup($accountInfo)
    {
        $query = $this->getBlankModel()
            ->with(['account' => function ($q) {
                $q->select('accounts.id', 'name', 'phone_number', 'email', 'accounts.organization_id', 'profile_image_id')
                    ->with(['profileImage' => function ($q) {
                        $q = $q->select('id', 'name', 'md5_file', 'source_thumb', 'source');
                    }])->with('position', 'managers');
            }])->with(['createdBy' => function ($q) {
                $q->select('accounts.id', 'accounts.name');
            }])->with(['organization' => function ($q) {
                $q->select('organizations.id', 'organizations.name');
            }]);
        $query = $query->where('organization_id', $accountInfo['organization_id'])->get()->toArray();
        $query = array_map(function ($val) {
            $val['account'] = array_map(function ($el) {
                unset($el['pivot']);
                // $el['type'] = Account::DONES_PRO;
                $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
                $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
                return $el;
            }, $val['account']);
            return $val;
        }, $query);
        return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-get-data-success'), $query);
    }

    public function createGroup($requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $group = $this->getBlankModel();
            $group->name = $requestData['groupName'];
            $group->slug = Str::slug($requestData['groupName']);
            $group->type = $requestData['type'] ?? Group::TYPE_EMPLOYEE;
            $group->is_active = $requestData['isActive'] ?? Group::IS_ACTIVE;
            $group->organization_id = $accountInfo['organization_id'];
            $group->created_by = $accountInfo['id'];
            $group->save();
            if (!empty($requestData['accountIds'])) {
                $group->account()->attach($requestData['accountIds'], [
                    'organization_id' => $accountInfo['organization_id']
                ]);
            }
            $space_default = $this->getBlankModelSpace()->where('is_default', Space::DEFAULT_SPACE)->where('organization_id', $accountInfo['organization_id'])->first();
            if (!empty($space_default)) {
                $space_default->group()->attach(
                    $group->id,
                    [
                        'organization_id' => $accountInfo['organization_id'],
                    ]
                );
            }
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-create-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-create-fail'));
        }
    }

    public function updateGroup($code, $requestData, $accountInfo)
    {
        try {
            DB::beginTransaction();
            $group = $this->getBlankModel()->find($code);
            $group->name = $requestData['groupName'];
            $group->slug = Str::slug($requestData['groupName']);
            $group->is_active = $requestData['isActive'] ?? Group::IS_ACTIVE;
            $group->organization_id = $accountInfo['organization_id'];
            $group->created_by = $accountInfo['id'];
            $group->save();
            $group->account()->sync($requestData['accountIds'], [
                'organization_id' => $accountInfo['organization_id']
            ]);
            DB::commit();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-update-success'));
        } catch (Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-update-fail'));
        }
    }

    public function deleteGroup($code)
    {
        try {
            $group = $this->getBlankModel()->find($code);
            if (!$group) {
                return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
            }
            $group->delete();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function deleteMany($requestData)
    {
        try {
            $this->getBlankModel()->whereIn('id', $requestData['groupIds'])->delete();
            return eResponse::response(STATUS_API_SUCCESS, __('notification.api-form-delete-success'));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-delete-fail'));
        }
    }

    public function listAccountGroupInSpace($ids, $space_id)
    {
        $accountInGroup = $this->getBlankModelAccountGroup()
            ->whereIn('group_id', $ids)
            ->get()->pluck('account_id')->unique()->values()->toArray();

        $accountIds = [];
        if (!empty($accountInGroup)) {
            $accountIds = $this->getBlankModelSpaceAccount()
                ->whereIn('account_id', $accountInGroup)
                ->where('space_id', $space_id)
                ->get()->pluck('account_id')->unique()->values()->toArray();
        }
        return $accountIds;
    }
}
