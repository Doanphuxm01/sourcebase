<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ApplicationStoreRepositoryInterface;
use \App\Models\Postgres\ApplicationStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Elibs\eFunction;

class ApplicationStoreRepository extends SingleKeyModelRepository implements ApplicationStoreRepositoryInterface
{

    public function getBlankModel()
    {
        return new ApplicationStore();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function buildFilters($filters, &$query)
    {
        $query = $query->when(isset($filters['id']), function ($query) use ($filters) {
            $query->where('id', $filters['id']);
        })->when(isset($filters['ids']), function ($query) use ($filters) {
            $query->whereIn('id', $filters['ids']);
        })->when(isset($filters['name']), function ($query) use ($filters) {
            // $query->where(DB::raw('vn_unaccent(application_stores.name)'), 'LIKE', '%' . Str::lower(Str::ascii($filters['name'])) . '%');
            $query->where('application_stores.slug', 'LIKE', '%' . eFunction::generateSlug($filters['name'], '-') . '%');
        });
        if (isset($filters['is_system'])) {
            if ($filters['is_system']) {
                $query = $query->where('is_system', true);
            } else {
                $query = $query->when(isset($filters['created_by']), function ($query) use ($filters) {
                    $query->where('created_by', $filters['created_by']);
                })->when(isset($filters['organization_id']), function ($query) use ($filters) {
                    $query->where('organization_id', $filters['organization_id']);
                });
            }
        } else {
            $query = $query->when(isset($filters['created_by']), function ($query) use ($filters) {
                $query->where(function ($query) use ($filters) {
                    $query->where('created_by', $filters['created_by'])->orWhere('is_system', true);
                });
            })->when(isset($filters['organization_id']), function ($query) use ($filters) {
                $query->where('organization_id', $filters['organization_id']);
            });
        }
    }

    public function countTotal($accountInfo)
    {
        $data = $this->getBlankModel()
            ->select('is_system', DB::raw('count(id) as total'))
            ->where('is_system', true)
            ->orWhere('created_by', $accountInfo['id'])
            ->groupBy('is_system')
            ->get();
        $result = [
            'total_system' => 0,
            'total_user_create' => 0,
            'total' => 0,
        ];
        foreach ($data as $value) {
            if ($value->is_system) {
                $result['total_system'] += $value->total;
            } else {
                $result['total_user_create'] += $value->total;
            }
            $result['total'] += $value->total;
        }
        return $result;
    }

    public function insertGetId($obj)
    {
        $this->getBlankModel()->insertGetId($obj);
    }

    public function findId($id)
    {
        $data = $this->getBlankModel()->find($id);
        if (!empty($data)) {
            return $data->toArray();
        }
        return false;
    }

    public function getApplicationStore($id, $accountInfo)
    {
        $app = $this->getBlankModel()->where('id', $id)->with('applicationFile')->first();
        if (!empty($app)) {
            $app = $app->toArray();
            return $app;
        }
        return null;
    }
}
