<?php namespace App\Repositories\Postgres\Eloquent;

use Exception;
use Illuminate\Support\Facades\Log;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\ImageRepositoryInterface;
use \App\Models\Postgres\Image;
use Illuminate\Support\Facades\File;
use App\Elibs\eResponse;
use App\Elibs\eFunction;
use App\Models\Postgres\Account;

// use Illuminate\Support\Facades\DB;


class ImageRepository extends SingleKeyModelRepository implements ImageRepositoryInterface
{

    public function getBlankModel()
    {
        return new Image();
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }

    public function insertMultiGetID($params)
    {
        $id = $this->getBlankModel()->insertGetId($params);
        return $id;
    }

    public function insertMulti($params)
    {
        if (!empty($params) && is_array($params)) {
            $insert = $this->getBlankModel()->insert($params);
            if ($insert) {
                return true;
            }
        }
        return false;
    }

    public function deleteIconDefault()
    {
        try {
            Image::where('collection_type', Image::COLLECTION_TYPE_LIBRARY)->delete();
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
    }

    public function listCollectionWithPaginate($queryString = null, $accountId)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 20;
        $startDate = $queryString['startDate'] ?? null;
        $endDate = $queryString['endDate'] ?? null;
        $type = $queryString['type'] ?? null;
        $createdBy = $queryString['createdBy'] ?? false; // true - false
        $collectionType = $queryString['collectionType'] ?? null;
        $collectionName = $queryString['collectionName'] ?? null;
        $listIconFlag = $queryString['listIconFlag'] ?? false;

        $query = $this->getBlankModel()
            ->where('type', '!=', Image::COLLECTION_TYPE_APP)
            ->withCount(['applications' => function ($q) {
                $q->whereNull('applications.deleted_at');
            }])
            ->withCount(['spaces' => function ($q) {
                $q->whereNull('spaces.deleted_at');
            }])
            ->withCount(['avatar' => function ($q) {
                $q->whereNull('accounts.deleted_at');
            }])
            ->withCount(['background' => function ($q) {
                $q->whereNull('accounts.deleted_at');
            }]);

        if ($startDate && $endDate) {
            $query = $query->whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate);
        }

        if ($type) {
            $query = $query->where('type', $type);
        }

        if ($collectionName) {
            $query = $query->where('name', 'like', '%' . eFunction::generateSlug($collectionName, '-') . '%');
        }

        // if ($createdBy) {
        $query = $query->where('created_by', $accountId['id']);
        // }

        if ($collectionType) {
            $query = $query->where('collection_type', $collectionType);
        }

        $query = $query->where('organization_id', $accountId['organization_id']);

        if ($listIconFlag) {
            $query = $query->orWhereNull('created_by')->orWhereNull('organization_id');
        }

        $query = $query->orderBy('created_at', 'desc')->paginate($limit);
        $query->map(function ($val) {
            if ($val->applications_count == 0 && $val->spaces_count == 0 && $val->avatar_count == 0 && $val->background_count == 0) {
                $val->is_using = Image::NOT_USING;
            } else {
                $val->is_using = Image::USING;
            }
            $val->source_thumb = !empty($val->source_thumb) && file_exists(public_path() . '/' . $val->source_thumb) ? asset($val->source_thumb) : '';
            $val->source = !empty($val->source) && file_exists(public_path() . '/' . $val->source) ? asset($val->source) : '';
        });
        return $query;
    }

    public function listCollectionAll($queryString = null)
    {
        $limit = isset($queryString["limit"]) && ctype_digit($queryString["limit"]) ? (int)$queryString["limit"] : 20;

        $query = $this->getBlankModel()
            ->select()
            ->where('collection_type', Image::COLLECTION_TYPE_LIBRARY)
            ->orderBy('created_at', 'desc')
            ->paginate($limit);

        $query->map(function ($val) {
            $val->source_thumb = asset($val->source_thumb);
            $val->source = asset($val->source);
            // if ($val->collection_type != 3) {
            //     $val->can_be_deleted = 1;
            // } else {
            //     $val->can_be_deleted = 0;
            // }
        });
        return $query;
    }

    public function updateCollectionType($code, $collectionType)
    {
        $collection = $this->getBlankModel()->find($code);
        $collection->collection_type = $collectionType;
        $collection->save();
    }

    public function deleteCollection($requestData)
    {
        try {
            $query = $this->getBlankModel()
                ->where('collection_type', '<>', Image::COLLECTION_TYPE_LIBRARY)
                ->withCount(['applications' => function ($q) {
                    $q->whereNull('applications.deleted_at');
                }])
                ->withCount(['spaces' => function ($q) {
                    $q->whereNull('spaces.deleted_at');
                }])
                ->withCount(['avatar' => function ($q) {
                    $q->whereNull('accounts.deleted_at');
                }])
                ->withCount(['background' => function ($q) {
                    $q->whereNull('accounts.deleted_at');
                }])
                ->whereIn('id', $requestData['colectionIds']);
            $deleteFile = $query->get()->toArray();
            if (empty($deleteFile)) {
                return false;
            }
            $idImage = [];
            // applications_count spaces_count avatar_count background_count
            foreach ($deleteFile as $value) {
                // if (!empty($value['applications_count']) && !empty($value['spaces_count']) && !empty($value['avatar_count']) && !empty($value['background_count'])) {
                if ($value['applications_count'] == 0 && $value['spaces_count'] == 0 && $value['avatar_count'] == 0 && $value['background_count'] == 0) {
                    // Xóa dữ liệu media sử dụng
                    $capacity = Account::where('id', $value['created_by_id'])->first();
                    $capacity->capacity -= $value['file_size'];
                    $capacity->save();

                    // Xòa file
                    File::delete(public_path() . '/' . $value['source_thumb']);
                    File::delete(public_path() . '/' . $value['source']);
                    $idImage[] = $value['id'];
                }
                // }
            }
            if (!empty($idImage)) {
                $query->whereIn('images.id', $idImage)->delete();
                return true;
            }
            return false;
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return false;
        }
    }

    public function listFileApplication($idAccount, $search)
    {
        $id = Image::where([
            'type' => Image::COLLECTION_TYPE_APP,
            'created_by' => $idAccount
        ])->where('name', 'like', '%' . trim($search) . '%')
      ->paginate(10)->toArray();
        return $id;
    }
}
