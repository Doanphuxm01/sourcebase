<?php namespace App\Repositories\Postgres\Eloquent;

use App\Elibs\Debug;
use App\Elibs\eResponse;
use App\Models\Postgres\SpaceAccount;
use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\AccountLifeWorkRepositoryInterface;
use \App\Models\Postgres\AccountLifeWork;
use \App\Models\Postgres\Account;
use \App\Models\Postgres\AccountLife;
use Illuminate\Support\Facades\DB;
use Elasticsearch\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class AccountLifeWorkRepositoryRepository extends SingleKeyModelRepository implements AccountLifeWorkRepositoryInterface
{

    function getBlankModel(): AccountLifeWork
    {
        return new AccountLifeWork();
    }

    function getBlankModelLife(): AccountLife
    {
        return new AccountLife();
    }

    function getBlankAccount(): Account
    {
        return new Account();
    }

    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function filterByAccountWork($id, $type, $request)
    {
        $where = [
            'is_active' => Account::IS_ACTIVE
        ];
        $listObj = Account::where($where);
        if (!empty($request)) {
            // nếu search theo từ khóa
            $listObj = $listObj->where('username', 'LIKE', '%' . trim($request) . '%');
        } else {
            // nếu sarch all
            $listObj = $listObj->with('profileImage', function ($q) {
                $q->select('id', 'name', 'source_thumb', 'source', 'organization_id');
            })
                ->with('decentralization', function ($q) {
                    $q->select('id', 'name', 'slug', 'organization_id');
                });
        }
        $listObj = $listObj->select('id', 'name', 'decentralization_id', 'profile_image_id')->get()->toArray();

        $query = array_map(function ($el) {
            if (is_array($el['profile_image'])) {
                $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
                $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
            }
            return $el;
        }, $listObj);
        $result = collect($query)->paginate(10)->toArray();
        sort($result['data']);
        return $result;
        // Nếu search theo từ khóa
        // search theo space lệ thuộc của account
        //        $data = SpaceAccount::where('space_id', $id)->whereHas('account', function ($q) use ($request){
        //            $q->where('username', 'LIKE', '%' . trim($request) . '%');
        //        })
        //            ->with('account.profileImage', function ($q) {
        //                $q->select('id', 'name', 'source_thumb', 'source', 'organization_id' );
        //            })
        //            ->with('account.decentralization', function ($q) {
        //                $q->select('id', 'name', 'slug', 'organization_id' );
        //            })
        //            ->with(['account'=>function($q){
        //                $q->select('id', 'name', 'decentralization_id', 'profile_image_id');
        //            }])
        //            ->get()->toArray();
    }


    public function filterByAccountLife($id, $type, $request)
    {
        try {
            $where = [
                'is_active' => Account::IS_ACTIVE
            ];
            $listObj = $this->getBlankModelLife()->where($where);
            if (!empty($request)) {
                // nếu search theo từ khóa
                $listObj = $listObj->where('username', 'LIKE', '%' . trim($request) . '%');
            } else {
                // nếu sarch all
                $listObj = $listObj->with('profileImage', function ($q) {
                    $q->select('id', 'name', 'source_thumb', 'source');
                });
            }
            $listObj = $listObj->select('id', 'name', 'username', 'profile_image_id')->get()->toArray();
            $query = array_map(function ($el) {
                if (!empty($el['profile_image'])) {
                    if (is_array($el['profile_image'])) {
                        $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
                        $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
                    }
                }
                return $el;
            }, $listObj);
            $result = collect($query)->paginate(10);
            return $result;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }

    public function getAccountLifeInWhere($lsAccount, $id, $type, $request, $infoAccount)
    {
        try {
            $where = [
                'is_active' => Account::IS_ACTIVE
            ];
            $listObj = $this->getBlankModelLife()->where($where)->whereNotIn('id', $lsAccount);

            if (!empty($request)) {
                // nếu search theo từ khóa
                // $listObj = $listObj->where(DB::raw('vn_unaccent(name)'), 'LIKE', '%' . Str::lower(Str::ascii($request)) . '%');
                $listObj = $listObj->where(function ($query) use ($request) {
                    $query->orWhere(DB::raw('vn_unaccent(name)'), 'like', '%' . Str::lower(Str::ascii($request) . '%'))
                        ->orWhere('email', 'like', '%' . Str::lower(Str::ascii($request) . '%'));
                });
            }

            // nếu sarch all
            $listObj = $listObj->with('profileImage', function ($q) {
                $q->select('id', 'name', 'source_thumb', 'source');
            });

            $listObj = $listObj->select('id', 'name', 'username', 'email', 'profile_image_id')->get()->toArray();
            $query = array_map(function ($el) {
                $el['type'] = Account::DONES;
                if (!empty($el['profile_image'])) {
                    if (is_array($el['profile_image'])) {
                        // $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
                        $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? config('systems.URL_IMAGE_LIFE') . ($el['profile_image']['source_thumb']) : '';
                        $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
                    }
                }
                return $el;
            }, $listObj);

//            /**
//             * lấy account có cùng tổ chức với account tạo ra
//             */
//
//            $lsAccountOrganization = Account::where([
//                'is_active' => Account::IS_ACTIVE,
//                'organization_id' => $infoAccount['organization_id'],
//            ])
//                ->where('id', '<>', $infoAccount['id'])
//                ->select('id', 'name', 'email', 'username', 'profile_image_id', 'organization_id')
//                ->with('profileImage', function ($q) {
//                    $q->select('id', 'name', 'source_thumb', 'source');
//                })
//                ->get()->toArray();
//            $tpl = [];
//            if (!empty($lsAccountOrganization)) {
//                $tpl = array_map(function ($el) {
//                    $el['type'] = Account::DONES_PRO;
//                    if ($el['profile_image']) {
//                        // $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
//                        $el['profile_image']['source_thumb'] = !empty($el['profile_image']['source_thumb']) ? asset($el['profile_image']['source_thumb']) : '';
//                        $el['profile_image']['source'] = !empty($el['profile_image']['source']) && file_exists(public_path() . '/' . $el['profile_image']['source']) ? asset($el['profile_image']['source']) : '';
//                    }
//                    return $el;
//                }, $lsAccountOrganization);
//            }
//            $tplArray = array_merge($query, $tpl);
            $result = collect($query)->paginate(10)->toArray();

            sort($result['data']);
            return $result;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            Debug::sendNotification($ex);
            return eResponse::response(STATUS_API_FALSE, __('notification.api-form-get-data-fail'));
        }
    }
}
