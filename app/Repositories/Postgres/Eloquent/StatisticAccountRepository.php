<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\StatisticAccountRepositoryInterface;
use \App\Models\Postgres\StatisticAccount;
use Illuminate\Support\Facades\DB;

class StatisticAccountRepository extends SingleKeyModelRepository implements StatisticAccountRepositoryInterface
{

    public function getBlankModel()
    {
        return new StatisticAccount();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function getOneObjectStatisticAccountByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->first();

        return $data;
    }

    public function getAllObjectStatisticAccountByFilter($filter)
    {
        $query = $this->getBlankModel();
        $this->filter($filter, $query);

        $data = $query->get()->toArray();

        return $data;
    }

    public function countUserClicksApp($filter){
        $query = $this->getBlankModel()->select('application_id', DB::raw('count(account_id) as number_user'));
        $this->filter($filter, $query);

        $data = $query->groupBy('application_id')->get();

        return $data;
    }

    public function filter($filter, &$query)
    {

        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('statistic_accounts.id', $filter['id']);
            } else {
                $query = $query->where('statistic_accounts.id', $filter['id']);
            }
        }

        if (isset($filter['application_id'])) {
            if (is_array($filter['application_id'])) {
                $query = $query->whereIn('statistic_accounts.application_id', $filter['application_id']);
            } else {
                $query = $query->where('statistic_accounts.application_id', $filter['application_id']);
            }
        }

        if (isset($filter['statistic_application_id'])) {
            if (is_array($filter['statistic_application_id'])) {
                $query = $query->whereIn('statistic_accounts.statistic_application_id', $filter['statistic_application_id']);
            } else {
                $query = $query->where('statistic_accounts.statistic_application_id', $filter['statistic_application_id']);
            }
        }


        if (isset($filter['account_id'])) {
            if (is_array($filter['account_id'])) {
                $query = $query->whereIn('statistic_accounts.account_id', $filter['account_id']);
            } else {
                $query = $query->where('statistic_accounts.account_id', $filter['account_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('statistic_accounts.organization_id', $filter['organization_id']);
        }

        if (isset($filter['date_at'])) {
            $query = $query->whereDate('statistic_accounts.date_at', $filter['date_at']);
        }

        if (isset($filter['date_start'])) {
            $query = $query->whereDate('statistic_accounts.date_at','>=', $filter['date_start']);
        }

        if (isset($filter['date_end'])) {
            $query = $query->whereDate('statistic_accounts.date_at','<=', $filter['date_end']);
        }
    }

}
