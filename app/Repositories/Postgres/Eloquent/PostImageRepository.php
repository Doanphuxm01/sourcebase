<?php namespace App\Repositories\Postgres\Eloquent;

use App\Repositories\Eloquent\SingleKeyModelRepository;
use \App\Repositories\Postgres\PostImageRepositoryInterface;
use \App\Models\Postgres\PostImage;

class PostImageRepository extends SingleKeyModelRepository implements PostImageRepositoryInterface
{

    public function getBlankModel()
    {
        return new PostImage();
    }

    public function rules()
    {
        return [
        ];
    }

    public function messages()
    {
        return [
        ];
    }

    public function deleteAllPostImageByFilter($filter)
    {
        $query = $this->getBlankModel();

        $this->filter($filter, $query);

        $data = $query->delete();

        return $data;
    }

    public function insertMulti($params)
    {
        if(!empty($params) && is_array($params)){
            $insert = $this->getBlankModel()->insert($params);
            if($insert){
                return true;
            }
        }
        return false;
    }

    private function filter($filter, &$query)
    {
        if (isset($filter['id'])) {
            if (is_array($filter['id'])) {
                $query = $query->whereIn('post_images.id', $filter['id']);
            } else {
                $query = $query->where('post_images.id', $filter['id']);
            }
        }

        if (isset($filter['post_id'])) {
            if (is_array($filter['post_id'])) {
                $query = $query->whereIn('post_images.post_id', $filter['post_id']);
            } else {
                $query = $query->where('post_images.post_id', $filter['post_id']);
            }
        }

        if (isset($filter['space_id'])) {
            if (is_array($filter['space_id'])) {
                $query = $query->whereIn('post_images.space_id', $filter['space_id']);
            } else {
                $query = $query->where('post_images.space_id', $filter['space_id']);
            }
        }

        if (isset($filter['organization_id'])) {
            $query = $query->where('post_images.organization_id', $filter['organization_id']);
        }

    }

}
