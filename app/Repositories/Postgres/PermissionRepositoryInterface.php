<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface PermissionRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function deleteAllPermissionsByFilter($filter);

    public function getAllPermissionByFilter($filter);

    public function countAllPermissionsByFilter($filter);

    public function createMulti($params);

    public function getAllAccountToPermission($key);
}
