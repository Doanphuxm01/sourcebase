<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface StatisticAccountRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getOneObjectStatisticAccountByFilter($filter);

    public function getAllObjectStatisticAccountByFilter($filter);

    public function countUserClicksApp($filter);

    public function filter($filter,&$query);
}
