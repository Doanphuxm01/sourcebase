<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface DecentralizationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getListDecentralizationByFilter($limit, $filter);

    public function getOneArrayDecentralizationWithPermissionByFilter($filter);

    public function getOneObjectDecentralizationByFilter($filter);

    public function deleteAllDecentralizationByFilter($filter);

    public function getAllDecentralizationByFilter($filter);
}
