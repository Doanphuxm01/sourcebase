<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface PostRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function deleteAllPostByFilter($filter);

    public function getOneObjectPostByFilter($filter);

    public function getAllPostWithAllByFilter($limit, $filter);
}
