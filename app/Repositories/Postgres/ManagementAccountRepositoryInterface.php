<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ManagementAccountRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function insertMulti($params);

    public function deleteAllManagementAccountByFilter($filter);

    public function getAllManagementAccountByFilter($filter);
}
