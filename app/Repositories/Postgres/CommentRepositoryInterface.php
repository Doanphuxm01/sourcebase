<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface CommentRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function deleteAllCommentByFilter($filter);

    public function getAllCommentWithAllByFilter($filter);
}
