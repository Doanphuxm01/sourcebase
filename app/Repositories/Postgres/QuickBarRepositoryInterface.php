<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface QuickBarRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function listQuickBarSpaceId($query, $accountInfo);

    public function deleteQuickBar($idQuickBar, $accountInfo);

    public function sort($requestData);

    public function insertMulti($params, $accountInfo);

    public function getAllApplicationInQuickByFilter($filter);

    public function deleteQuickBarUsingEnable($spaceId, $accountId, $applicationId);

    public function deleteQuickBarUsingEnableAll($spaceId, $applicationId, $accountId);

    public function updateEnable($spaceId, $accountId, $applicationId, $enable);

    public function getOneQuickBar($appId, $spaceId, $accountInfo);

    public function checkOwnQuickbar($idApp, $accountInfo, $space_id);

    public function checkOwnQuickbarWorkLife($idApp, $accountInfo, $space_id);

    public function getIdAppInQuickbar($applicationIds, $spaceId, $accountInfo);

    public function quickbarDetail($quickbarId, $accountInfo);

    public function listQuickBarSpaceIdRe($query, $accountInfo);

    public function deleteQuickbarApp($app_ids);

    public function listAccountUsingQuickbarOtherAccount($space_id, $app_id, $accountInfo);
}
