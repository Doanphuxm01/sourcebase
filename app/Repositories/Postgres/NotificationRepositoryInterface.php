<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface NotificationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function showNotification($accountInfo, $requestData);

    public function updateStatusNotification($requestData);

    public function notification($accountInfo, $accountIds, $messageNotification, $type);

    public function pushNotiAccountSpace($accountInfo, $requestData, $space, $listIds);

    public function pushNotiAccountSpaceWorkLife($accountInfo, $requestData, $space);

    public function pushNotiDeleteAccountInSpace($accountInfo, $requestData, $space);

    public function pushNotiDeleteSpace($accountInfo, $spaceIds);

    public function pushNotiDeleteSpaceWorkLife($accountInfo, $spaceIds);

    public function pushNotiAccountRoom($accountInfo, $requestData, $space, $listIds);

    public function pushNotiAccountRoomWorkLife($accountInfo, $requestData, $space, $listIds);

    public function pushNotiDeleteRoom($accountInfo, $room);

    public function pushNotiDeleteRoomWorkLife($accountInfo, $room);

    public function pushNotiCreateNewApp($accountInfo, $application, $isOwnRoom, $is_accept);

    public function pushNotiCreateNewAppWorkLife($accountInfo, $application, $isOwnRoom);

    public function pushNotiRejectApp($accountInfo, $application);

    public function pushNotiApproveApp($accountInfo, $application);

    public function pushNotiApproveAppWorkLife($accountInfo, $application);

    public function pushNotiDeleteApp($accountInfo, $ids);

    public function pushNotiDeleteAppWorkLife($accountInfo, $ids);

    public function pushNotiUpdateRoom($accountInfo, $room, $oldRoomName);

    public function pushNotiUpdateRoomWorkLife($accountInfo, $room, $oldRoomName);

    public function pushNotiUpdateApp($accountInfo, $application, $appOld);

    public function pushNotiUpdateAppWorkLife($accountInfo, $application, $appOld);

    public function pushNotiQuickBarEnableAll($accountInfo, $applicationId, $ids, $spaceId, $dataOpenApp);

    public function pushNotiQuickBarEnableAllWorkLife($accountInfo, $applicationId, $ids, $spaceId);

    public function pushNotiDeleteQuickBar($accountInfo, $applicationId, $ids, $spaceId);

    public function pushNotiDeleteQuickBarWorkLife($accountInfo, $applicationId, $ids, $spaceId);

    public function pushNotiUpdateQuickbar($accountInfo, $applicationId, $ids, $spaceId, $appOld);

    public function pushNotiUpdateQuickbarWorkLife($accountInfo, $applicationId, $ids, $spaceId, $appOld);

    public function pushNotiCreateGroupApp($accountInfo, $roomId, $spaceId, $appGroupName);

    public function pushNotiUpdateGroupApp($accountInfo, $roomId, $spaceId, $appGroupName);

    public function pushNotiInteractApp($applications);

    public function pushNotiDeleteAccountInRoomWorkLife($requestData, $accountInfo);

    public function pushNotiAccountInvite($accountInfo, $requestData, $space, $listIds);
}
