<?php namespace App\Repositories\Postgres;

use App\Repositories\AuthenticationRepositoryInterface;

interface AccountRepositoryInterface extends AuthenticationRepositoryInterface
{
    public function totalAccount();

    public function getOneObjectAccountByFilter($filter);

    public function getOneArrayAccountByFilter($filter);

    public function getOneArrayAccountForLoginByFilter($filter);

    public function getOneArrayAccountWithModelByFilter($filter);

    public function updateAllAccountByFilter($filter, $params);

    public function getListAccountByFilter($limit, $filter);

    public function countAllAccountByFilter($filter);

    public function getOneArrayAccountWithManagerByFilter($filter);

    public function getAllAccountForSelectByFilter($filter);

    public function getAllAccountByFilter($filter);

    public function deleteAllAccountByFilter($filter);

    public function resetTokenAllAccountByFilter($filter);

    public function countSpaceAndRoom($queryString, $accountInfo);

    public function logoutDeviceAll($accountInfo);

    public function getAllAccountCustomer($search);
}
