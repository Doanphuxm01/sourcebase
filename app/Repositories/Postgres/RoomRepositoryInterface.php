<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface RoomRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function listAllRoom($requestData, $accountInfo);

    public function listRoomInSpacePaginate($requestData, $accountInfo);

    public function createRoom($requestData, $accountInfo);

    public function createRoomWorkLife($requestData, $accountInfo);

    public function updateRoom($requestData, $accountInfo);

    public function deleteRoom($id, $accountInfo);

    public function deleteRoomWorkLife($id, $accountInfo, $lsAccountLife);

    public function sort($requestData);

    public function listAccountInRoom($idRoom, $search);

    public function totalRoom();

    public function addAccount($requestData, $accountInfo);

    public function getOneArrayRoomByFilter($filter);

    public function getOneObjectRoomByFilter($filter);

    public function deleteAccountInRoom($requestData, $accountInfo);

    public function paginateAccountInRoom($requestData, $accountInfo);

    public function updateAccountRoomPublic($room, $spaceId, $type, $accountInfo, $accountIds);

    public function updateAccountRoomPublicWorkLife($room, $spaceId, $type, $accountInfo, $accountIds);

    public function updateAdminInRoom($request, $accountInfo);

    public function checkAdminRoom($accountId, $filter);

    public function getRoomTypeInSpace($idRoom);

    public function accountLeaveRoom($id, $accountInfo);
}
