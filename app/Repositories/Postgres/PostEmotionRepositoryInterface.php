<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface PostEmotionRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function deleteAllPostEmotionByFilter($filter);

    public function getAllEmotionByFilter($filter);
}
