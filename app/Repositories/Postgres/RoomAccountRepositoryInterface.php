<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface RoomAccountRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getOneArrayRoomAccountByFilter($filter);
}
