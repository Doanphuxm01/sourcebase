<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface OrganizationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getOneObjectOrganizationByFilter($filter);

    public function getAllObjectOrganization();
}
