<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface SpaceRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function createSpaceAfterRegister($id, $requestData, $organization);

    public function updateVisit($idSpace, $accountInfo);

    public function statictis($query, $accountInfo);

    public function createSpace($requestData, $accountInfo, $collectionId);

    public function addAccount($requestData, $accountInfo);

    public function listAccountInSpace($spaceId);

    public function listAccountInSpaceLifeWork($spaceId);

    public function listAccountLifeIdInSpaceLifeWork($spaceId);

    public function listAccountIdInSpaceLifeWork($spaceId, $accountId);

    public function listOfMemberOfTheSameOrganization($infoAccount);

    public function updateSpace($requestData, $accountInfo, $collectionId);

    public function updateSpaceLifeWork($requestData, $accountInfo, $collectionId);

    public function listAccountInSpaceOtherAccount($spaceId, $accountId);

    public function deleteSpace($requestData, $accountInfo);

    public function listGroupInSpace($id, $type);

    public function listAccount($id, $search, $type);

    public function listSpace($requestData, $accountInfo);

    public function findById($id, $queryString);

    public function finByIdLifeWork($id, $queryString);

    public function getOneArraySpaceByFilter($filter);

    public function listRoomOfSpace($id);

    public function deleteAccountInSpace($requestData, $accountInfo);

    public function deleteAccountInSpaceLifeWork($requestData, $accountInfo);

    public function addAcountSpaceDefault($idAccount, $accountInfo);

    public function searchRoomApplication($name, $accountInfo);

    public function getOneSpaceOfAccount($accountId);

    public function deleteAllAccount($accountIds, $accountInfo);

    public function updateAdminInSpace($request, $accountInfo);

    public function updateAdminInSpaceLifeWork($request, $accountInfo);

    public function checkAccountInSpace($accountId, $spaceId);

    public function checkSpaceType($accountId, $spaceId);

    public function listAccountWorkLife($id, $search);

    public function searchAccountInSpaceWorkLife($id, $search);

    public function approveJoinInSpace($data, $accountInfo);

    public function listSpaceWaitApprove($accountInfo);

    public function listAccountWaitApprove($accountInfo, $data);

    public function findBySpaceIdAndQuickbar($id, $queryString);

    public function accountLeaveSpace($id, $accountInfo);

    public function totalAccountSpaceCustom($spaceId, $accountInfo);
}
