<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationDefaultRepositoryInterface extends SingleKeyModelRepositoryInterface
{
}
