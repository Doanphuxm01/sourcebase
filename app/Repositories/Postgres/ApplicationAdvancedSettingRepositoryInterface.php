<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationAdvancedSettingRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function createMulti($params);

    public function getAllApplicationAdvancedSettingByFilter($filter);

    public function deleteAllApplicationAdvancedSettingByFilter($filter);
}
