<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationGroupLocationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getAllApplicationGroupLocationByFilter($filter);

    public function deleteAllApplicationGroupLocationByFilter($filter);

    public function insertMulti($params);
}
