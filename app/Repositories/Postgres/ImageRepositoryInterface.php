<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ImageRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function listCollectionWithPaginate($queryString, $accountId);

    public function updateCollectionType($code, $collectionType);

    public function insertMultiGetID($params);

    public function insertMulti($params);

    public function deleteIconDefault();

    public function listCollectionAll($queryString = null);

    public function listFileApplication($idAccount, $search);
}
