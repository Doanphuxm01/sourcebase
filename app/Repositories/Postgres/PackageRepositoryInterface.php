<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface PackageRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getOneArrayPackageByFilter($filter);
}
