<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface AccountAccessTokensRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function createTokenAccount($account, $dataToken);

    public function clearAllTokenForUser($id);

    public function getIdForToken($token);

    public function deleteToken($token);
}
