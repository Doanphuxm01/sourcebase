<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface GroupRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function findId($id);

    public function listGroupWithPaginate($queryString, $accountInfo);

    public function listGroup($accountInfo);

    public function createGroup($requestData, $accountInfo);

    public function updateGroup($code, $requestData, $accountInfo);

    public function deleteGroup($code);

    public function deleteMany($requestData);

    public function listAccountGroupInSpace($ids, $space_id);
}
