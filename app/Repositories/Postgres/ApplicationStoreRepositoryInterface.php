<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationStoreRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function countTotal($accountInfo);

    public function insertGetId($obj);

    public function getApplicationStore($id, $accountInfo);
}
