<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationLocationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getAllApplicationLocationByFilter($filter);

    public function getOneArrayApplicationLocationByFilter($filter);

    public function deleteAllApplicationLocationByFilter($filter);

    public function insertMulti($params);
}
