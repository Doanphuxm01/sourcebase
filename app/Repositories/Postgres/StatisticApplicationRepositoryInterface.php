<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface StatisticApplicationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getAllStatisticApplicationByFilter($filter);

    public function getOneObjectStatisticApplicationByFilter($filter);

    public function sumClicksApp($filter);

    public function filter($filter,&$query);
}
