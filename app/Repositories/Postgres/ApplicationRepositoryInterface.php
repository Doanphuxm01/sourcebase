<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function totalApplication();

    public function getAllApplicationByFilter($filter);

    public function deleteApplicationFromApplicationGroupByFilter($filter);

    public function getOneArrayApplicationWithAdvancedByFilter($filter, $accountInfo);

    public function getOneArrayApplicationByFilter($filter);

    public function getOneObjectApplicationByFilter($filter);

    public function deleteApplicationByFilter($filter);

    public function getAllApplicationWithImageByFilter($filter);

    public function filterAppAvancedSetting($date, $timeCurrent, $timeAddHour, $dayNumberInWeek);

    public function filterAppAvancedSettingSendMail($date, $dayNumberInWeek);

    public function getAppDefault();


    public function statisticApplication($filter);

    public function statisticApplicationByDay($filters);

    public function statisticAccountByDay($filters);
}
