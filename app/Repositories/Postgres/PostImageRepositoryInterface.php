<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface PostImageRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function deleteAllPostImageByFilter($filter);

    public function insertMulti($params);
}
