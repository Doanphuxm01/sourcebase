<?php namespace App\Repositories\Postgres;

use App\Repositories\SingleKeyModelRepositoryInterface;

interface ApplicationGroupRepositoryInterface extends SingleKeyModelRepositoryInterface
{
    public function getOneArrayApplicationGroupByFilter($filter);

    public function getOneObjectApplicationGroupByFilter($filter);

    public function deleteAllApplicationGroupByFilter($filter);

    public function getAllGroupApplicationByFilter($filter);
}
