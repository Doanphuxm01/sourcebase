<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class UserNotificationHelperWorkLife extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Helpers\UserNotificationHelperWorkLifeInterface';
    }
}
