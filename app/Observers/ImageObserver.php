<?php namespace App\Observers;

use Illuminate\Support\Facades\Redis;
use App\Observers\BaseObserver;
use App\Models\Postgres\Account;

class ImageObserver extends BaseObserver
{
    protected $cachePrefix = 'ImageModel';

    public function created($model)
    {
        // \Log::debug('created');
        if ($model->created_by) {
            $capacity = Account::where('id', $model->created_by)->first();
            $capacity->capacity += $model->file_size;
            $capacity->save();
        }
    }

    // public function updating($model)
    // {
    //     \Log::debug('updating');
    // }

    // public function updated($model)
    // {
    //     \Log::debug('updated');
    // }

    public function deleting($model)
    {
        // \Log::debug('deleting');
        if ($model->created_by) {
            $capacity = Account::where('id', $model->created_by)->first();
            $capacity->capacity -= $model->file_size;
            $capacity->save();
        }
    }

    public function deleted($model)
    {
        // \Log::debug('deleted');
    }
}
