<?php namespace App\Observers\Postgres;

use Illuminate\Support\Facades\Redis;
use App\Observers\BaseObserver;
use Illuminate\Support\Str;
use App\Models\Postgres\QuickBar;

class QuickBarObserver extends BaseObserver
{
    protected $cachePrefix = 'QuickBarModel';

    public function created($model)
    {
        if (\CacheHelper::cacheRedisEnabled()) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hsetnx($cacheKey, $model->id, $model);
        }
    }

    public function updated($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hset($cacheKey, $model->id, $model);
        }
    }

    public function deleted($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hdel($cacheKey, $model->id);
        }
    }
}
