<?php namespace App\Observers\Postgres;

use Illuminate\Support\Facades\Redis;
use App\Observers\BaseObserver;

class AccountAccessTokensObserver extends BaseObserver
{
    protected $cachePrefix = 'AccountAccessTokensModel';

    public function retrieved($model)
    {
        $token = request()->bearerToken();
        $model->where('token', $token)->update([
            'last_used_at' => now()
        ]);
    }

    // public function created($model)
    // {
    //     if (\CacheHelper::cacheRedisEnabled()) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hsetnx($cacheKey, $model->id, $model);
    //     }
    // }

    // public function updated($model)
    // {
    //     if (\CacheHelper::cacheRedisEnabled()) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hset($cacheKey, $model->id, $model);
    //     }
    // }

    // public function deleted($model)
    // {
    //     if (\CacheHelper::cacheRedisEnabled()) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hdel($cacheKey, $model->id);
    //     }
    // }
}
