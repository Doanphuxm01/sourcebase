<?php namespace App\Observers;

use App\Models\Postgres\OrganizationDomain;
use Illuminate\Support\Facades\Redis;
use App\Observers\BaseObserver;

class ApplicationObserver extends BaseObserver
{
    protected $cachePrefix = 'ApplicationModel';

    public function created($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hsetnx($cacheKey, $model->id, $model);
        }
        $data = [
            'organization_id' => $model->organization_id,
            'domain' => $model->domain
        ];
        OrganizationDomain::updateOrCreate($data,$data);
    }

    public function updated($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hset($cacheKey, $model->id, $model);
        }

        $data = [
            'organization_id' => $model->organization_id,
            'domain' => $model->domain
        ];
        OrganizationDomain::updateOrCreate($data,$data);
    }

    public function deleted($model)
    {
        if( \CacheHelper::cacheRedisEnabled() ) {
            $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
            Redis::hdel($cacheKey, $model->id);
        }
    }
}
