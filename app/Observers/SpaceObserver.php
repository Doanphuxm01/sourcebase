<?php namespace App\Observers;

use Illuminate\Support\Facades\Redis;
use App\Observers\BaseObserver;
use Illuminate\Support\Str;

class SpaceObserver extends BaseObserver
{
    // public function retrieved($model)
    // {
    //     if (!$model->uuid) {
    //         $model->uuid = (string)Str::uuid();
    //         $model->save();
    //     }
    // }

    public function creating($model)
    {
        $model->uuid = (string)Str::uuid();
    }

    // protected $cachePrefix = 'SpaceModel';

    // public function created($model)
    // {
    //     if( \CacheHelper::cacheRedisEnabled() ) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hsetnx($cacheKey, $model->id, $model);
    //     }
    // }

    // public function updated($model)
    // {
    //     if( \CacheHelper::cacheRedisEnabled() ) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hset($cacheKey, $model->id, $model);
    //     }
    // }

    // public function deleted($model)
    // {
    //     if( \CacheHelper::cacheRedisEnabled() ) {
    //         $cacheKey = \CacheHelper::keyForModel($this->cachePrefix);
    //         Redis::hdel($cacheKey, $model->id);
    //     }
    // }
}
