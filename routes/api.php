<?php


use App\Http\Controllers\Api\Postgres\ApiApplicationController;
use App\Http\Controllers\Api\Postgres\ApiAuthController;
use App\Http\Controllers\Api\Postgres\ApiCommentController;
use App\Http\Controllers\Api\Postgres\ApiDefaultController;
use App\Http\Controllers\Api\Postgres\ApiGroupApplicationController;
use App\Http\Controllers\Api\Postgres\ApiInfoAccountController;
use App\Http\Controllers\Api\Postgres\ApiManageAccountController;
use App\Http\Controllers\Api\Postgres\ApiPermissionController;
use App\Http\Controllers\Api\Postgres\ApiPostController;
use App\Http\Controllers\Api\Postgres\ApiUploadFileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Postgres\ApiImageController;
use App\Http\Controllers\Api\Postgres\ApiGroupController;
use App\Http\Controllers\Api\Postgres\ApiSpaceController;
use App\Http\Controllers\Api\Postgres\ApiMajorController;
use App\Http\Controllers\Api\Postgres\ApiRoomController;
use App\Http\Controllers\Api\Postgres\ApiQuickBarController;
use App\Http\Controllers\Api\Postgres\ApiStatisticController;
use \App\Http\Controllers\Api\Postgres\AccountLifeWorkController;
use Illuminate\Support\Facades\DB;
use App\Elibs\eCrypt;
use App\Elibs\eFunction;
use App\Http\Controllers\Api\Postgres\ApiPushNotification;
use App\Http\Controllers\Api\Postgres\CommonSettingController;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Postgres\Account;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('test', function (Request $request) {
//     $userIp = $request->ip();
//     // $locationData = \Location::get('171.251.239.148');
//     // return $locationData;
//     $curl = curl_init();

//     curl_setopt_array($curl, [
//         CURLOPT_URL => "http://ip-api.com/json/" . $userIp,
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_FOLLOWLOCATION => true,
//         CURLOPT_ENCODING => "",
//         CURLOPT_MAXREDIRS => 10,
//         CURLOPT_TIMEOUT => 30,
//         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//         CURLOPT_CUSTOMREQUEST => "GET",
//     ]);

//     $response = curl_exec($curl);

//     curl_close($curl);
//     return $response;
// });




Route::post('test-organizations', function (Request $request) {
    if (isset($_SERVER['HTTP_HOST']) && in_array($_SERVER['HTTP_HOST'], ['api-test.dones.ai'])) {
        $data = $request->all();
        if (!empty($data['day'] && !empty($data['email']))) {
            $account = Account::where('email', $data['email'])->whereNull('deleted_at')->first();
            if ($account) {
                $command = 'extend:expiration ' . $account->organization_id . ' ' . $data['day'];
                \Artisan::call($command);
                return res('', 200, 'Gia hạn thành công rồi cô nương [✖‿✖]');
            }
            return res('', 200, 'Cô nương nên uống bổ não vì nhớ sai email!');
        }
    }
    return res('', 200, 'Nơi đây cảnh đẹp người đẹp mời cô nương tá túc hàn huyên!');
});

Route::group(['prefix' => 'v1', 'namespace' => 'Api', 'middleware' => ['api.decrypt']], function () {
    Route::group(['middleware' => ['api.allow_domain']], function () {

        Route::group(['middleware' => ['api.language']], function () {
            Route::post('sign-in', [ApiAuthController::class, 'signIn']);
            Route::post('sign-up', [ApiAuthController::class, 'index']);
            Route::post('send-email', [ApiAuthController::class, 'mailForResetPasswordAccount']);
        });
        Route::get('get-info-new-version', [ApiDefaultController::class, 'getInfoNewVersion']);


        Route::post('update', [ApiAuthController::class, 'update']);

        Route::post('sign-out', [ApiAuthController::class, 'signOut']);
        Route::post('active-account', [ApiAuthController::class, 'activeAccount']);
        Route::post('send-code-active-account', [ApiAuthController::class, 'sendCodeActiveAccount']);

        Route::post('sign-in-with-google', [ApiAuthController::class, 'signInWithGoogle']);

        //        Route::post('check-otp-reset-password', [ApiAuthController::class, 'checkOtpResetPasswordAccount']);
        Route::get('get-reset-password', [ApiAuthController::class, 'tokenForResetPassword']);
        Route::post('reset-password', [ApiAuthController::class, 'resetPassword']);

        Route::get('space-no-per/list', [ApiSpaceController::class, 'indexNoPermission']);
        Route::get('space-no-per/detail/{id}', [ApiSpaceController::class, 'findByIdNoPermission']);
        Route::get('space-no-per/list/{id}/account', [ApiSpaceController::class, 'listAccountNoPermission']);

        Route::get('space/search/room-application', [ApiSpaceController::class, 'searchRoomApplication']);

        Route::get('space-no-per/list/{id}/group', [ApiSpaceController::class, 'listGroupInSpaceNoPermission']);
        Route::post('account/update-main-space', [ApiManageAccountController::class, 'updateMainSpace']);
        Route::group(['middleware' => ['api.account_auth']], function () {

            // Api login history
            Route::get('login-history', [ApiAuthController::class, 'loginHistory']);
            Route::get('logout-device/{id}', [ApiAuthController::class, 'logoutDeivce']);
            Route::get('logout-device-all', [ApiAuthController::class, 'logoutDeivceAll']);

            //Api Application store
            Route::group(['prefix' => 'application-store'], function () {
                Route::get('count-total', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'countTotal']);
                Route::get('', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'list']);
                Route::post('', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'create']);
                Route::get('{id}', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'view']);
                Route::post('{id}', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'update'])->name('application.store.update');
                Route::delete('', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'deleteMultiple']);
                Route::delete('{id}', [\App\Http\Controllers\Api\Postgres\ApiApplicationStoreController::class, 'delete']);
            });

            //Api settings
            Route::group(['prefix' => 'setting'], function () {
                Route::get('module', [CommonSettingController::class, 'moduleIndex']);
            });
            //Api data default
            Route::get('get-all-position',  [ApiDefaultController::class, 'getPosition']);
            Route::get('get-all-scale',  [ApiDefaultController::class, 'getScale']);
            Route::get('get-all-major',  [ApiDefaultController::class, 'getMajor']);
            Route::get('get-all-emotion',  [ApiDefaultController::class, 'getEmotion']);
            Route::get('get-all-permission',  [ApiDefaultController::class, 'getPermission']);
            Route::get('get-all-language',  [ApiDefaultController::class, 'getLanguage']);

            //Api collection & File
            Route::get('collection-type',  [ApiImageController::class, 'typeCollection']);
            Route::get('collection',  [ApiImageController::class, 'index']); // list default
            Route::get('icon',  [ApiImageController::class, 'listIconLibrary']);
            Route::post('collection',  [ApiImageController::class, 'collectionUpload']);
            Route::delete('collection',  [ApiImageController::class, 'deleteCollection']);
            Route::group(['prefix' => 'file'], function () {
                Route::get('list',  [ApiImageController::class, 'listFile']);
            });
            //Api Group
            Route::get('group/list',  [ApiDefaultController::class, 'list']);
            Route::get('group',  [ApiGroupController::class, 'index']);
            Route::get('group/{id}',  [ApiGroupController::class, 'show']);
            Route::post('group',  [ApiGroupController::class, 'store']);
            Route::post('group/{id}',  [ApiGroupController::class, 'update']);
            Route::delete('group/{id}',  [ApiGroupController::class, 'destroy']);
            // Route::delete('group-delete',  [ApiGroupController::class, 'deleteMany']);
            Route::match(['post', 'delete'], 'group-delete', [ApiGroupController::class, 'deleteMany']);

            //Api info account
            Route::group(['prefix' => 'info-account'], function () {
                Route::get('detail', [ApiInfoAccountController::class, 'index']);
                Route::post('update', [ApiInfoAccountController::class, 'update']);
                Route::post('update-device-token', [ApiInfoAccountController::class, 'updateDeviceToken']);
                Route::post('change-password', [ApiInfoAccountController::class, 'changePassword']);
                Route::post('change-language', [ApiInfoAccountController::class, 'changeLanguage']);
            });

            //Api decentralization
            Route::group(['prefix' => 'decentralization'], function () {
                Route::get('list', [ApiPermissionController::class, 'index']);
                Route::post('create', [ApiPermissionController::class, 'create']);
                Route::get('detail', [ApiPermissionController::class, 'detail']);
                Route::post('update', [ApiPermissionController::class, 'update']);
                Route::delete('delete', [ApiPermissionController::class, 'delete']);
                Route::get('get-all', [ApiPermissionController::class, 'getAllDecentralizations']);
            });

            //Api account
            Route::group(['prefix' => 'account'], function () {
                Route::get('list', [ApiManageAccountController::class, 'index']);
                Route::post('create', [ApiManageAccountController::class, 'create']);
                Route::get('detail', [ApiManageAccountController::class, 'detail']);
                Route::post('update', [ApiManageAccountController::class, 'update']);
                Route::delete('delete', [ApiManageAccountController::class, 'delete']);
                Route::post('change-status', [ApiManageAccountController::class, 'changeStatus']);
                Route::get('get-all', [ApiDefaultController::class, 'getAllAccount']);
                // Route::post('update-main-space', [ApiManageAccountController::class, 'updateMainSpace']);

                //customer
                Route::get('list-customer', [ApiManageAccountController::class, 'listCustomer']);
                Route::get('list-account-approve', [ApiManageAccountController::class, 'listAccountWaitApprove']);
                Route::post('approve-all', [ApiManageAccountController::class, 'approveJoinAll']);
                Route::get('detail-customer/{id}', [ApiManageAccountController::class, 'detailCustomer']);
                Route::post('detail-customer/{id}/update', [ApiManageAccountController::class, 'updateCustomer']);
                Route::post('admin-customer', [ApiManageAccountController::class, 'adminCustomer']);
            });

            //Api Group Application
            Route::group(['prefix' => 'group-application'], function () {
                Route::get('list', [ApiGroupApplicationController::class, 'index']);
                Route::post('create', [ApiGroupApplicationController::class, 'create']);
                Route::get('detail', [ApiGroupApplicationController::class, 'detail']);
                Route::post('update', [ApiGroupApplicationController::class, 'update']);
                Route::delete('delete', [ApiGroupApplicationController::class, 'delete']);
            });

            //Api Application
            Route::group(['prefix' => 'application'], function () {
                Route::get('list-default', [ApiApplicationController::class, 'listAppDefault']);
                Route::get('list', [ApiApplicationController::class, 'index']);
                Route::post('create', [ApiApplicationController::class, 'create']);
                Route::get('detail', [ApiApplicationController::class, 'detail']);
                Route::post('update', [ApiApplicationController::class, 'update']);
                Route::delete('delete', [ApiApplicationController::class, 'delete']);
                Route::get('click-app', [ApiApplicationController::class, 'clickApplication']);
                Route::get('list-application-in-room', [ApiApplicationController::class, 'listApplicationInRoom']);
                Route::get('list-application-for-quick-bar', [ApiApplicationController::class, 'getAllApplicationForQuickBar']);
            });

            // Api post
            Route::group(['prefix' => 'post'], function () {
                Route::get('list', [ApiPostController::class, 'index']);
                Route::post('create', [ApiPostController::class, 'create']);
                Route::post('update', [ApiPostController::class, 'update']);
                Route::post('react', [ApiPostController::class, 'react']);
                Route::get('list-react', [ApiPostController::class, 'listReact']);
                Route::delete('delete', [ApiPostController::class, 'delete']);
            });

            // Api comment
            Route::group(['prefix' => 'comment'], function () {
                Route::get('list', [ApiCommentController::class, 'index']);
                Route::post('create', [ApiCommentController::class, 'create']);
            });

            // Api Space
            Route::group(['prefix' => 'space'], function () {
                Route::get('list', [ApiSpaceController::class, 'index']);
                Route::post('admin/update', [ApiSpaceController::class, 'isAdminUpdate']);
                Route::get('list/{id}/room', [ApiSpaceController::class, 'listRoom']);
                // Route::get('list/wait-approve', [ApiSpaceController::class, 'listSpaceWaitApprove']);
                // Route::get('account/wait-approve', [ApiSpaceController::class, 'listAccountWaitApprove']);
                Route::get('list/{id}/account', [ApiSpaceController::class, 'listAccount']);
                Route::get('detail/{id}', [ApiSpaceController::class, 'findById']);
                Route::post('create', [ApiSpaceController::class, 'create']);
                Route::post('account/add', [ApiSpaceController::class, 'addAccount']);
                Route::post('update', [ApiSpaceController::class, 'update']);
                // Route::post('delete', [ApiSpaceController::class, 'delete']);
                Route::match(['post', 'delete'], 'delete', [ApiSpaceController::class, 'delete'])->middleware('api.delete_space');

                // Route::post('delete-account', [ApiSpaceController::class, 'deleteAccount']);
                Route::match(['post', 'delete'], 'delete-account', [ApiSpaceController::class, 'deleteAccount']);
                Route::get('list/{id}/group', [ApiSpaceController::class, 'listGroupInSpace']);

                // Duyệt vào space khách hàng
                // Route::post('approve-join', [ApiSpaceController::class, 'approveJoin']);
                // Duyệt all vào space
                // Route::post('approve-all', [ApiSpaceController::class, 'approveJoinAll']);
                // Hiển thị space với cộng đồng
                // Route::post('is-show/{id}', [ApiSpaceController::class, 'updateShowSpace']);
                // Account rời space
                // Route::delete('account/leave/{id}', [ApiSpaceController::class, 'accountLeaveSpace']);
            });

            // Api Room
            Route::group(['prefix' => 'room'], function () {
                Route::get('in-space', [ApiRoomController::class, 'index']);
                Route::get('list', [ApiRoomController::class, 'listAllRoom']);
                Route::get('list/{idRoom}/account', [ApiRoomController::class, 'listAccountInRoom']);
                Route::post('create', [ApiRoomController::class, 'store']);
                Route::post('update', [ApiRoomController::class, 'update']);
                Route::post('room-sort', [ApiRoomController::class, 'sort']);
                Route::delete('delete/{id}', [ApiRoomController::class, 'destroy']);
                Route::post('update-room-and-application', [ApiRoomController::class, 'updateRoomAndAllApplication']);
                Route::post('account/add', [ApiRoomController::class, 'addAccount']);
                Route::delete('account/delete', [ApiRoomController::class, 'deleteAccount']);
                Route::get('account/list', [ApiRoomController::class, 'listAccount']);
                Route::delete('account/leave/{id}', [ApiRoomController::class, 'accountLeave']);
                Route::post('admin/update', [ApiRoomController::class, 'updateAdmin']);
            });

            //Api Major
            Route::group(['prefix' => 'major'], function () {
                Route::get('list', [ApiMajorController::class, 'index']);
            });

            //Api Quickbar
            Route::group(['prefix' => 'quickbar'], function () {
                Route::get('detail/{id}', [ApiQuickBarController::class, 'detail']);
                Route::post('update', [ApiQuickBarController::class, 'update']);
                Route::post('sort', [ApiQuickBarController::class, 'sort']);
                Route::post('create', [ApiQuickBarController::class, 'create']);
                Route::get('in-space', [ApiQuickBarController::class, 'index']);
                Route::delete('delete', [ApiQuickBarController::class, 'delete']);
            });

            //Api Statictis
            Route::group(['prefix' => 'statistic'], function () {
                Route::get('total', [ApiStatisticController::class, 'listCount']);
                Route::get('visit', [ApiStatisticController::class, 'visit']);
                Route::get('account', [ApiStatisticController::class, 'countSpaceRoomForAccount']);
                Route::get('statistic-app',  [ApiStatisticController::class, 'statisticApp']);
                Route::get('statistic-app-by-day',  [ApiStatisticController::class, 'statisticAppByDay']);
                Route::get('statistic-app-by-day-detail',  [ApiStatisticController::class, 'statisticAppByDayDetail']);
                Route::get('statistic-account-by-day',  [ApiStatisticController::class, 'statisticAccountByDay']);
            });

            //Api Notifications
            Route::group(['prefix' => 'notification'], function () {
                Route::get('list', [ApiPushNotification::class, 'index']);
                Route::post('update', [ApiPushNotification::class, 'updateStatusRead']);
                Route::post('store', [ApiPushNotification::class, 'store']);
                Route::post('send-to-account', [ApiPushNotification::class, 'sendToAccount']);
            });

            //Api Account Work Life
            // Route::group(['prefix' => 'account-work-life'], function () {
            //     // Route::post('space/{id}', [AccountLifeWorkController::class, 'searchAccount']);
            //     Route::post('space/{id}', [AccountLifeWorkController::class, 'searchAccount']);
            // });
        });
    });
});
